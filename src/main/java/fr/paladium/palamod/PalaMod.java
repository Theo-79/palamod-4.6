package fr.paladium.palamod;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import fr.paladium.palamod.common.CommonProxy;
import fr.paladium.palamod.common.commands.CommandAnnounceBoss;
import fr.paladium.palamod.common.commands.CommandFactions;
import fr.paladium.palamod.common.commands.CommandFurnace;
import fr.paladium.palamod.common.commands.CommandFurnaceInfo;
import fr.paladium.palamod.common.commands.CommandSendMessage;
import fr.paladium.palamod.common.commands.CommandStopBoss;

@Mod(
   modid = "palamod",
   name = "Palamod",
   version = "4.6"
)
public class PalaMod {
   public static final String MODID = "palamod";
   public static final String MODNAME = "Palamod";
   public static final String MODVERSION = "4.6";
   public static final int modLoaded = 1;
   public static boolean charger = false;
   public static byte[] music;
   @Instance("palamod")
   public static PalaMod instance;
   @SidedProxy(
      clientSide = "fr.paladium.palamod.client.ClientProxy",
      serverSide = "fr.paladium.palamod.common.CommonProxy"
   )
   public static CommonProxy proxy;

   @EventHandler
   public void preInit(FMLPreInitializationEvent event) {
      proxy.preInit(event);
   }

   @EventHandler
   public void init(FMLInitializationEvent event) throws Exception {
      proxy.init(event);
   }

   @EventHandler
   public void postInit(FMLPostInitializationEvent event) {
      proxy.postInit(event);
   }

   @EventHandler
   public void serverLoad(FMLServerStartingEvent event) {
      event.registerServerCommand(new CommandSendMessage());
      event.registerServerCommand(new CommandAnnounceBoss());
      event.registerServerCommand(new CommandStopBoss());
      event.registerServerCommand(new CommandFurnace());
      event.registerServerCommand(new CommandFurnaceInfo());
      event.registerServerCommand(new CommandFactions());
   }
}
