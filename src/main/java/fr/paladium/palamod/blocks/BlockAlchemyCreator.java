package fr.paladium.palamod.blocks;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.tiles.TileEntityAlchemyCreator;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockAlchemyCreator extends BlockContainer {
   protected BlockAlchemyCreator() {
      super(Material.rock);
      this.setResistance(8.0F);
      this.setHarvestLevel("pickaxe", 2);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setBlockName("alchemycreator");
      this.setBlockTextureName("palamod:GuardianBlock");
      this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.8F, 1.0F);
      this.setHardness(12.0F);
   }

   public TileEntity createNewTileEntity(World world, int metadata) {
      return new TileEntityAlchemyCreator();
   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
      if (!world.isRemote) {
         player.openGui(PalaMod.instance, 1, world, x, y, z);
      }

      return true;
   }

   public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack) {
      super.onBlockPlacedBy(world, x, y, z, entity, stack);
      byte b0 = 3;
      byte b01 = this.rotateBlock(b0, entity);
      world.setBlockMetadataWithNotify(x, y, z, b0, 2);
   }

   public byte rotateBlock(byte b0, EntityLivingBase entity) {
      if (entity.rotationYaw >= 135.0F && entity.rotationYaw <= 181.0F || entity.rotationYaw <= -135.0F && entity.rotationYaw >= -181.0F) {
         b0 = 3;
      } else if (entity.rotationYaw > -135.0F && entity.rotationYaw < -45.0F) {
         b0 = 4;
      } else if (entity.rotationYaw >= -45.0F && entity.rotationYaw <= 45.0F) {
         b0 = 2;
      } else if (entity.rotationYaw > 45.0F && entity.rotationYaw < 135.0F) {
         b0 = 5;
      } else if (entity.rotationYaw >= 181.0F) {
         entity.rotationYaw -= 360.0F;
         b0 = this.rotateBlock(b0, entity);
      } else if (entity.rotationYaw <= -181.0F) {
         entity.rotationYaw += 360.0F;
         b0 = this.rotateBlock(b0, entity);
      }

      return b0;
   }

   public void breakBlock(World world, int x, int y, int z, Block block, int metadata) {
      TileEntity tileentity = world.getTileEntity(x, y, z);
      if (tileentity instanceof IInventory) {
         IInventory inv = (IInventory)tileentity;

         for(int i1 = 0; i1 < inv.getSizeInventory(); ++i1) {
            ItemStack itemstack = inv.getStackInSlot(i1);
            if (itemstack != null) {
               float f = world.rand.nextFloat() * 0.8F + 0.1F;
               float f1 = world.rand.nextFloat() * 0.8F + 0.1F;

               EntityItem entityitem;
               for(float f2 = world.rand.nextFloat() * 0.8F + 0.1F; itemstack.stackSize > 0; world.spawnEntityInWorld(entityitem)) {
                  int j1 = world.rand.nextInt(21) + 10;
                  if (j1 > itemstack.stackSize) {
                     j1 = itemstack.stackSize;
                  }

                  itemstack.stackSize -= j1;
                  entityitem = new EntityItem(world, (double)((float)x + f), (double)((float)y + f1), (double)((float)z + f2), new ItemStack(itemstack.getItem(), j1, itemstack.getItemDamage()));
                  float f3 = 0.05F;
                  entityitem.motionX = (double)((float)world.rand.nextGaussian() * f3);
                  entityitem.motionY = (double)((float)world.rand.nextGaussian() * f3 + 0.2F);
                  entityitem.motionZ = (double)((float)world.rand.nextGaussian() * f3);
                  if (itemstack.hasTagCompound()) {
                     entityitem.getEntityItem().setTagCompound((NBTTagCompound)itemstack.getTagCompound().copy());
                  }
               }
            }
         }

         world.func_147453_f(x, y, z, block);
      }

      super.breakBlock(world, x, y, z, block, metadata);
   }

   public int getRenderType() {
      return -1;
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }
}
