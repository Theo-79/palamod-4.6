package fr.paladium.palamod.blocks;

import fr.paladium.palamod.tiles.TileEntityCamera;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockCamera extends Block implements ITileEntityProvider {
   public BlockCamera() {
      super(Material.rock);
      this.setBlockTextureName("palamod:TitaneBlock");
   }

   public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase living, ItemStack stack) {
      int direction = ((MathHelper.floor_double((double)(living.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3) + 0) % 4;
      world.setBlockMetadataWithNotify(x, y, z, direction, 2);
   }

   public int getRenderType() {
      return -1;
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   public TileEntity createNewTileEntity(World world, int metadata) {
      return new TileEntityCamera();
   }

   public boolean hasTileEntity() {
      return true;
   }

   public void setBlockBoundsBasedOnState(IBlockAccess access, int x, int y, int z) {
      switch(access.getBlockMetadata(x, y, z)) {
      case 0:
         this.setBlockBounds(0.3F, 0.33F, 0.33F, 0.66F, 0.66F, 1.0F);
         break;
      case 1:
         this.setBlockBounds(0.0F, 0.33F, 0.33F, 0.66F, 0.66F, 0.7F);
         break;
      case 2:
         this.setBlockBounds(0.33F, 0.33F, 0.0F, 0.66F, 0.66F, 0.7F);
         break;
      case 3:
         this.setBlockBounds(0.3F, 0.33F, 0.33F, 1.0F, 0.66F, 0.7F);
      }

   }
}
