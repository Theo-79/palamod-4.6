package fr.paladium.palamod.blocks;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockCave extends Block {
   protected BlockCave() {
      super(Material.glass);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setStepSound(soundTypeStone);
      this.setHarvestLevel("pickaxe", 1);
      this.setBlockName("caveblock");
      this.setBlockTextureName("palamod:CaveBlock");
      this.setResistance(4.0F);
      this.setHardness(4.0F);
   }

   public int getRenderType() {
      return 0;
   }
}
