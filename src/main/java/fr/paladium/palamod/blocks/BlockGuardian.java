package fr.paladium.palamod.blocks;

import fr.paladium.palamod.blocks.ores.BlockPaladium;
import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.items.ItemGuardianStone;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.World;

public class BlockGuardian extends Block {
   protected BlockGuardian() {
      super(Material.iron);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setStepSound(soundTypeStone);
      this.setHarvestLevel("pickaxe", 2);
      this.setBlockName("guardianblock");
      this.setBlockTextureName("palamod:GuardianBlock");
      this.setResistance(40.0F);
      this.setHardness(10.0F);
   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
      if (player.getHeldItem() != null && player.getHeldItem().getItem() instanceof ItemGuardianStone) {
         ItemStack stack = player.getHeldItem();
         if (!world.isRemote) {
            Block block = world.getBlock(x, y, z);
            if (block instanceof BlockGuardian) {
               EntityGuardianGolem golem;
               if (world.getBlock(x + 1, y, z) instanceof BlockPaladium && world.getBlock(x - 1, y, z) instanceof BlockPaladium && world.getBlock(x, y - 1, z) instanceof BlockPaladium) {
                  world.setBlock(x, y, z, Blocks.air);
                  world.setBlock(x + 1, y, z, Blocks.air);
                  world.setBlock(x - 1, y, z, Blocks.air);
                  world.setBlock(x, y - 1, z, Blocks.air);
                  golem = new EntityGuardianGolem(world);
                  golem.setLocationAndAngles((double)x + 0.5D, (double)y, (double)z + 0.5D, 0.0F, 0.0F);
                  golem.setOwner(player);
                  if (stack.hasTagCompound()) {
                     this.spawnCustomGolem(world, golem, stack, player);
                  } else {
                     world.spawnEntityInWorld(golem);
                  }

                  stack.stackSize = 0;
                  return true;
               }

               if (world.getBlock(x, y, z + 1) instanceof BlockPaladium && world.getBlock(x, y, z - 1) instanceof BlockPaladium && world.getBlock(x, y - 1, z) instanceof BlockPaladium) {
                  world.setBlock(x, y, z, Blocks.air);
                  world.setBlock(x, y, z + 1, Blocks.air);
                  world.setBlock(x, y, z - 1, Blocks.air);
                  world.setBlock(x, y - 1, z, Blocks.air);
                  golem = new EntityGuardianGolem(world);
                  golem.setOwner(player);
                  golem.setLocationAndAngles((double)x + 0.5D, (double)y, (double)z + 0.5D, 0.0F, 0.0F);
                  if (stack.hasTagCompound()) {
                     this.spawnCustomGolem(world, golem, stack, player);
                  } else {
                     world.spawnEntityInWorld(golem);
                  }

                  stack.stackSize = 0;
                  return true;
               }
            }
         }

         return false;
      } else {
         return false;
      }
   }

   private void spawnCustomGolem(World world, EntityGuardianGolem golem, ItemStack stack, EntityPlayer player) {
      NBTTagCompound tag = stack.getTagCompound();
      NBTTagList nbttaglist = tag.getTagList("Items", 10);
      ItemStack[] content = new ItemStack[golem.getSizeInventory()];

      int level;
      for(level = 0; level < nbttaglist.tagCount(); ++level) {
         NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(level);
         int j = nbttagcompound1.getByte("Slot") & 255;
         if (j >= 0 && j < content.length) {
            content[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
         }
      }

      level = tag.getInteger("Levels");
      int subLevel = tag.getInteger("SubLevels");
      float health = tag.getFloat("Health");
      String ownerUUID = tag.getString("player");
      golem.addInformations(content, level, subLevel, ownerUUID);
      world.spawnEntityInWorld(golem);
      golem.sync();
   }
}
