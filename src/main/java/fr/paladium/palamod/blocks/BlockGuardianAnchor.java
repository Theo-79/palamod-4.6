package fr.paladium.palamod.blocks;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.items.ItemGuardianWand;
import fr.paladium.palamod.tiles.TileEntityGuardianAnchor;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockGuardianAnchor extends BlockContainer {
   public BlockGuardianAnchor() {
      super(Material.rock);
      this.setResistance(8.0F);
      this.setHarvestLevel("pickaxe", 2);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setBlockName("guardianradius");
      this.setBlockTextureName("palamod:GuardianAnchor");
      this.setHardness(12.0F);
   }

   public TileEntity createNewTileEntity(World world, int metadata) {
      return new TileEntityGuardianAnchor();
   }

   public boolean hasTileEntity(int metadata) {
      return true;
   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
      if (player.getHeldItem() != null && player.getHeldItem().getItem() instanceof ItemGuardianWand) {
         ItemStack stack = player.getHeldItem();
         EntityGuardianGolem golem = ItemGuardianWand.getGolem(stack, world);
         if (golem != null && golem.checkWhitelist(player)) {
            golem.setAnchor(x, y, z);
         }

         return true;
      } else {
         if (!world.isRemote) {
            player.openGui(PalaMod.instance, 23, world, x, y, z);
         }

         return true;
      }
   }

   public void onBlockClicked(World world, int x, int y, int z, EntityPlayer p) {
      super.onBlockClicked(world, x, y, z, p);
   }
}
