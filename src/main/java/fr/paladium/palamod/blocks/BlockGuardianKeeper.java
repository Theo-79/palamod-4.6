package fr.paladium.palamod.blocks;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BlockGuardianKeeper extends Block {
   private IIcon top;
   private IIcon sides;

   protected BlockGuardianKeeper() {
      super(Material.rock);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setStepSound(soundTypeStone);
      this.setHarvestLevel("pickaxe", 1);
      this.setBlockName("guardiankeeper");
      this.setBlockTextureName("palamod:GuardianKeeper");
      this.setResistance(4.0F);
      this.setHardness(4.0F);
   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
      if (!world.getBlock(x, y + 1, z).isNormalCube()) {
         if (!world.isRemote) {
            player.openGui(PalaMod.instance, 16, world, x, y, z);
         }

         return true;
      } else {
         return false;
      }
   }

   public void registerBlockIcons(IIconRegister iiconRegister) {
      this.sides = iiconRegister.registerIcon("palamod:GuardianKeeper_Sides");
      this.top = iiconRegister.registerIcon("palamod:GuardianKeeper_Top");
   }

   public IIcon getIcon(int side, int metadata) {
      return side != 1 && side != 0 ? this.sides : this.top;
   }
}
