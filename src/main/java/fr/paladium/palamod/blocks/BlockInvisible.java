package fr.paladium.palamod.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class BlockInvisible extends Block {
   IIcon creativeIcon;

   protected BlockInvisible() {
      super(Material.rock);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setStepSound(soundTypeStone);
      this.setLightOpacity(0);
      this.setHarvestLevel("pickaxe", 1);
      this.setBlockName("invisibleblock");
      this.setBlockTextureName("palamod:InvisibleBlock");
      this.setBlockUnbreakable();
   }

   public boolean isOpaqueCube() {
      return false;
   }

   @SideOnly(Side.CLIENT)
   public int getRenderBlockPass() {
      return 0;
   }

   public boolean isSideSolid(IBlockAccess world, int x, int y, int z, ForgeDirection side) {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   public int getRenderType() {
      return super.getRenderType();
   }

   public boolean shouldSideBeRendered(IBlockAccess p_149646_1_, int p_149646_2_, int p_149646_3_, int p_149646_4_, int p_149646_5_) {
      return false;
   }

   public void registerBlockIcons(IIconRegister icon) {
      this.creativeIcon = icon.registerIcon("palamod:InvisibleBlock_Crea");
      super.registerBlockIcons(icon);
   }

   public void onUpdate(ItemStack ItemStack, World World, Entity Entity, int par4, boolean par5) {
      if (Entity instanceof EntityPlayer) {
         EntityPlayer e = (EntityPlayer)Entity;
         if (e.capabilities.isCreativeMode) {
            this.setBlockTextureName("palamod:InvisibleBlock_Crea");
         }
      }

   }

   @SideOnly(Side.CLIENT)
   public IIcon getIcon(int side, int metadata) {
      return metadata == 0 ? this.creativeIcon : super.getIcon(side, metadata);
   }
}
