package fr.paladium.palamod.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.tiles.TileEntityMagicalAnvil;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BlockMagicalAnvil extends BlockContainer {
   public IIcon[] icons = new IIcon[6];

   protected BlockMagicalAnvil() {
      super(Material.rock);
      this.setResistance(8.0F);
      this.setHarvestLevel("pickaxe", 2);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setBlockTextureName("minecraft:cobblestone");
      this.setBlockName("magicalanvil");
      this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.8F, 1.0F);
      this.setHardness(12.0F);
   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
      if (!world.isRemote) {
         player.openGui(PalaMod.instance, 20, world, x, y, z);
      }

      return true;
   }

   public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack) {
      super.onBlockPlacedBy(world, x, y, z, entity, stack);
      byte b0 = 3;
      byte b01 = this.rotateBlock(b0, entity);
      world.setBlockMetadataWithNotify(x, y, z, b0, 2);
   }

   public byte rotateBlock(byte b0, EntityLivingBase entity) {
      if (entity.rotationYaw >= 135.0F && entity.rotationYaw <= 181.0F || entity.rotationYaw <= -135.0F && entity.rotationYaw >= -181.0F) {
         b0 = 3;
      } else if (entity.rotationYaw > -135.0F && entity.rotationYaw < -45.0F) {
         b0 = 4;
      } else if (entity.rotationYaw >= -45.0F && entity.rotationYaw <= 45.0F) {
         b0 = 2;
      } else if (entity.rotationYaw > 45.0F && entity.rotationYaw < 135.0F) {
         b0 = 5;
      } else if (entity.rotationYaw >= 181.0F) {
         entity.rotationYaw -= 360.0F;
         b0 = this.rotateBlock(b0, entity);
      } else if (entity.rotationYaw <= -181.0F) {
         entity.rotationYaw += 360.0F;
         b0 = this.rotateBlock(b0, entity);
      }

      return b0;
   }

   public void breakBlock(World world, int x, int y, int z, Block block, int metadata) {
      TileEntity tileentity = world.getTileEntity(x, y, z);
      if (tileentity instanceof IInventory) {
         IInventory inv = (IInventory)tileentity;

         for(int i1 = 0; i1 < inv.getSizeInventory(); ++i1) {
            ItemStack itemstack = inv.getStackInSlot(i1);
            if (itemstack != null) {
               float f = world.rand.nextFloat() * 0.8F + 0.1F;
               float f1 = world.rand.nextFloat() * 0.8F + 0.1F;

               EntityItem entityitem;
               for(float f2 = world.rand.nextFloat() * 0.8F + 0.1F; itemstack.stackSize > 0; world.spawnEntityInWorld(entityitem)) {
                  int j1 = world.rand.nextInt(21) + 10;
                  if (j1 > itemstack.stackSize) {
                     j1 = itemstack.stackSize;
                  }

                  itemstack.stackSize -= j1;
                  entityitem = new EntityItem(world, (double)((float)x + f), (double)((float)y + f1), (double)((float)z + f2), new ItemStack(itemstack.getItem(), j1, itemstack.getItemDamage()));
                  float f3 = 0.05F;
                  entityitem.motionX = (double)((float)world.rand.nextGaussian() * f3);
                  entityitem.motionY = (double)((float)world.rand.nextGaussian() * f3 + 0.2F);
                  entityitem.motionZ = (double)((float)world.rand.nextGaussian() * f3);
                  if (itemstack.hasTagCompound()) {
                     entityitem.getEntityItem().setTagCompound((NBTTagCompound)itemstack.getTagCompound().copy());
                  }
               }
            }
         }

         world.func_147453_f(x, y, z, block);
      }

      super.breakBlock(world, x, y, z, block, metadata);
   }

   public int getRenderType() {
      return -1;
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
      return new TileEntityMagicalAnvil();
   }

   @SideOnly(Side.CLIENT)
   public void randomDisplayTick(World world, int x, int y, int z, Random random) {
      float f1 = (float)x + 0.5F;
      float f2 = (float)y + 1.1F;
      float f3 = (float)z + 0.5F;
      float f4 = random.nextFloat() * 0.6F - 0.3F;
      float f5 = random.nextFloat() * -0.6F - -0.3F;
      int i1 = random.nextInt(2) * 2 - 1;
      int j1 = random.nextInt(2) * 2 - 1;
      double d3 = 0.0D;
      double d4 = 0.0D;
      double d5 = 0.0D;
      d3 = ((double)random.nextFloat() - 0.5D) * 0.125D;
      d4 = ((double)random.nextFloat() - 0.5D) * 0.125D;
      d5 = ((double)random.nextFloat() - 0.5D) * 0.125D;
      d5 = (double)(random.nextFloat() * 1.0F * (float)j1);
      d3 = (double)(random.nextFloat() * 1.0F * (float)i1);
      world.spawnParticle("enchantmenttable", (double)(f1 + f4), (double)f2, (double)(f3 + f5), d3, d4, d5);
   }
}
