package fr.paladium.palamod.blocks;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;

public class BlockMysteryBoxBase extends Block {
   IIcon top;
   IIcon sides;
   IIcon bottom;
   String topString;
   String sidesString;
   String bottomString;

   public BlockMysteryBoxBase(String name, String texture, String texture2, String texture3) {
      super(Material.rock);
      this.setResistance(8.0F);
      this.setHarvestLevel("pickaxe", 2);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setBlockName(name);
      this.setBlockTextureName("palamod:" + texture);
      this.setHardness(12.0F);
      this.setResistance(8.0F);
      this.topString = texture;
      this.sidesString = texture2;
      this.bottomString = texture3;
   }

   public void registerBlockIcons(IIconRegister icons) {
      this.top = icons.registerIcon("palamod:" + this.topString);
      this.sides = icons.registerIcon("palamod:" + this.sidesString);
      this.bottom = icons.registerIcon("palamod:" + this.bottomString);
   }

   public IIcon getIcon(int side, int meta) {
      if (side == 1) {
         return this.top;
      } else {
         return side == 0 ? this.bottom : this.sides;
      }
   }
}
