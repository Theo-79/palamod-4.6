package fr.paladium.palamod.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.client.ClientProxy;
import fr.paladium.palamod.tiles.TileEntityObsidianUpgrader;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockObsidianUpgrader extends Block implements ITileEntityProvider {
   private IIcon[] icons = new IIcon[4];

   public BlockObsidianUpgrader() {
      super(Material.rock);
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   @SideOnly(Side.CLIENT)
   public int getRenderType() {
      return ClientProxy.renderObsidianUpgraderId;
   }

   @SideOnly(Side.CLIENT)
   public boolean shouldSideBeRendered(IBlockAccess blockAccess, int x, int y, int z, int side) {
      return true;
   }

   public TileEntity createNewTileEntity(World world, int metadata) {
      return new TileEntityObsidianUpgrader();
   }

   public boolean hasTileEntity(int metadata) {
      return true;
   }

   public void registerBlockIcons(IIconRegister register) {
      this.blockIcon = register.registerIcon("palamod:obsidian_upgrader");
      this.icons[0] = register.registerIcon("palamod:obsidian_upgrader_top_ul");
      this.icons[1] = register.registerIcon("palamod:obsidian_upgrader_top_ur");
      this.icons[2] = register.registerIcon("palamod:obsidian_upgrader_top_dl");
      this.icons[3] = register.registerIcon("palamod:obsidian_upgrader_top_dr");
   }

   public void addCollisionBoxesToList(World world, int x, int y, int z, AxisAlignedBB mark, List list, Entity entity) {
      if (world.getTileEntity(x, y, z) instanceof TileEntityObsidianUpgrader) {
         TileEntityObsidianUpgrader tile = (TileEntityObsidianUpgrader)world.getTileEntity(x, y, z);
         if (tile.hasMaster()) {
            TileEntityObsidianUpgrader master = (TileEntityObsidianUpgrader)world.getTileEntity(tile.getMasterX(), tile.getMasterY(), tile.getMasterZ());
            boolean flag = this.isSame(x + 1, y, z, tile);
            boolean flag1 = this.isSame(x, y, z + 1, tile);
            boolean flag2 = this.isSame(x - 1, y, z, tile);
            boolean flag3 = this.isSame(x, y, z - 1, tile);
            if (flag && flag1) {
               this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.1F, 1.0F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
               this.setBlockBounds(0.0F, 0.1F, 0.0F, 0.1F, 1.0F, 1.0F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
               this.setBlockBounds(0.0F, 0.1F, 0.0F, 1.0F, 1.0F, 0.1F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
               this.setBlockBounds(0.1F, 0.1F, 0.1F, 1.0F, (float)master.getObsidian() / 64.0F, 1.0F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
            } else if (flag2 && flag1) {
               this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.1F, 1.0F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
               this.setBlockBounds(0.9F, 0.1F, 0.0F, 1.0F, 1.0F, 1.0F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
               this.setBlockBounds(0.0F, 0.1F, 0.0F, 1.0F, 1.0F, 0.1F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
               this.setBlockBounds(0.0F, 0.1F, 0.1F, 0.9F, (float)master.getObsidian() / 64.0F, 1.0F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
            } else if (flag && flag3) {
               this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.1F, 1.0F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
               this.setBlockBounds(0.0F, 0.1F, 0.0F, 0.1F, 1.0F, 1.0F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
               this.setBlockBounds(0.0F, 0.1F, 0.9F, 1.0F, 1.0F, 1.0F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
               this.setBlockBounds(0.1F, 0.1F, 0.0F, 1.0F, (float)master.getObsidian() / 64.0F, 0.9F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
            } else if (flag2 && flag3) {
               this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.1F, 1.0F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
               this.setBlockBounds(0.9F, 0.1F, 0.0F, 1.0F, 1.0F, 1.0F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
               this.setBlockBounds(0.0F, 0.1F, 0.9F, 1.0F, 1.0F, 1.0F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
               this.setBlockBounds(0.0F, 0.1F, 0.0F, 0.9F, (float)master.getObsidian() / 64.0F, 0.9F);
               super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
            }
         } else {
            this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
            super.addCollisionBoxesToList(world, x, y, z, mark, list, entity);
         }
      }

      this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
   }

   public IIcon getIcon(IBlockAccess world, int x, int y, int z, int side) {
      TileEntity tile = world.getTileEntity(x, y, z);
      if (tile instanceof TileEntityObsidianUpgrader) {
         TileEntityObsidianUpgrader te = (TileEntityObsidianUpgrader)tile;
         if (te.hasMaster()) {
            TileEntityObsidianUpgrader master = (TileEntityObsidianUpgrader)world.getTileEntity(te.getMasterX(), te.getMasterY(), te.getMasterZ());
            if (side == 1 && master.hasObsidian()) {
               boolean flag = this.isSame(x + 1, y, z, te);
               boolean flag1 = this.isSame(x, y, z + 1, te);
               boolean flag2 = this.isSame(x - 1, y, z, te);
               boolean flag3 = this.isSame(x, y, z - 1, te);
               if (flag && flag1) {
                  return this.icons[0];
               }

               if (flag2 && flag1) {
                  return this.icons[1];
               }

               if (flag && flag3) {
                  return this.icons[2];
               }

               if (flag2 && flag3) {
                  return this.icons[3];
               }
            }
         }
      }

      return this.blockIcon;
   }

   private boolean isSame(int x, int y, int z, TileEntityObsidianUpgrader te) {
      World world = te.getWorldObj();
      TileEntity tile = world.getTileEntity(x, y, z);
      if (!(tile instanceof TileEntityObsidianUpgrader)) {
         return false;
      } else {
         TileEntityObsidianUpgrader te2 = (TileEntityObsidianUpgrader)tile;
         return te.getMasterX() == te2.getMasterX() && te.getMasterY() == te2.getMasterY() && te.getMasterZ() == te2.getMasterZ();
      }
   }

   public IIcon getIcon(int side, int metadata) {
      return this.blockIcon;
   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
      if (world.isRemote) {
         return true;
      } else {
         TileEntity tile = world.getTileEntity(x, y, z);
         if (tile instanceof TileEntityObsidianUpgrader) {
            TileEntityObsidianUpgrader te = (TileEntityObsidianUpgrader)tile;
            if (te.hasMaster()) {
               player.openGui(PalaMod.instance, 26, world, te.getMasterX(), te.getMasterY(), te.getMasterZ());
               return true;
            }
         }

         return false;
      }
   }

   public void breakBlock(World world, int x, int y, int z, Block block, int metadata) {
      TileEntity tileentity = world.getTileEntity(x, y, z);
      if (tileentity instanceof IInventory) {
         IInventory inv = (IInventory)tileentity;

         for(int i1 = 0; i1 < inv.getSizeInventory(); ++i1) {
            ItemStack itemstack = inv.getStackInSlot(i1);
            if (itemstack != null) {
               float f = world.rand.nextFloat() * 0.8F + 0.1F;
               float f1 = world.rand.nextFloat() * 0.8F + 0.1F;

               EntityItem entityitem;
               for(float f2 = world.rand.nextFloat() * 0.8F + 0.1F; itemstack.stackSize > 0; world.spawnEntityInWorld(entityitem)) {
                  int j1 = world.rand.nextInt(21) + 10;
                  if (j1 > itemstack.stackSize) {
                     j1 = itemstack.stackSize;
                  }

                  itemstack.stackSize -= j1;
                  entityitem = new EntityItem(world, (double)((float)x + f), (double)((float)y + f1), (double)((float)z + f2), new ItemStack(itemstack.getItem(), j1, itemstack.getItemDamage()));
                  float f3 = 0.05F;
                  entityitem.motionX = (double)((float)world.rand.nextGaussian() * f3);
                  entityitem.motionY = (double)((float)world.rand.nextGaussian() * f3 + 0.2F);
                  entityitem.motionZ = (double)((float)world.rand.nextGaussian() * f3);
                  if (itemstack.hasTagCompound()) {
                     entityitem.getEntityItem().setTagCompound((NBTTagCompound)itemstack.getTagCompound().copy());
                  }
               }
            }
         }

         world.func_147453_f(x, y, z, block);
      }

      super.breakBlock(world, x, y, z, block, metadata);
   }
}
