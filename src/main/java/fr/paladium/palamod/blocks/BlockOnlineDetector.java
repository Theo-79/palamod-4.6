package fr.paladium.palamod.blocks;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.tiles.TileEntityOnlineDetector;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BlockOnlineDetector extends BlockContainer {
   private IIcon[] icons;

   protected BlockOnlineDetector() {
      super(Material.rock);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setStepSound(soundTypeStone);
      this.setHarvestLevel("pickaxe", 1);
      this.setBlockName("onlinedetector");
      this.setResistance(4.0F);
      this.setHardness(4.0F);
   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
      if (!world.isRemote) {
         player.openGui(PalaMod.instance, 9, world, x, y, z);
      }

      return true;
   }

   public TileEntity createNewTileEntity(World world, int meta) {
      return new TileEntityOnlineDetector();
   }

   public void registerBlockIcons(IIconRegister icon) {
      this.icons = new IIcon[2];
      this.icons[0] = icon.registerIcon("palamod:OnlineDetector_off");
      this.icons[1] = icon.registerIcon("palamod:OnlineDetector_on");
   }

   public IIcon getIcon(int side, int meta) {
      return this.icons[meta];
   }
}
