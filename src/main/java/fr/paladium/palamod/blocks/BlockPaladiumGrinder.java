package fr.paladium.palamod.blocks;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.blocks.core.BlockGrinderCasing;
import fr.paladium.palamod.blocks.core.BlockGrinderFrame;
import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.tiles.TileEntityPaladiumGrinder;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BlockPaladiumGrinder extends BlockContainer {
   private IIcon front;

   protected BlockPaladiumGrinder() {
      super(Material.rock);
      this.setResistance(8.0F);
      this.setHarvestLevel("pickaxe", 2);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setBlockName("paladiumgrinder");
      this.setHardness(12.0F);
   }

   public boolean isMultiBlockStructure(World world, int x, int y, int z) {
      if (checkNorth(world, x, y, z)) {
         return true;
      } else if (checkEast(world, x, y, z)) {
         return true;
      } else if (checkSouth(world, x, y, z)) {
         return true;
      } else {
         return checkWest(world, x, y, z);
      }
   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
      if (this.isMultiBlockStructure(world, x, y, z)) {
         if (!world.isRemote) {
            player.openGui(PalaMod.instance, 12, world, x, y, z);
         }

         return true;
      } else {
         return false;
      }
   }

   public void breakBlock(World world, int x, int y, int z, Block block, int metadata) {
      TileEntity tileentity = world.getTileEntity(x, y, z);
      if (tileentity instanceof IInventory) {
         IInventory inv = (IInventory)tileentity;

         for(int i1 = 0; i1 < inv.getSizeInventory(); ++i1) {
            ItemStack itemstack = inv.getStackInSlot(i1);
            if (itemstack != null) {
               float f = world.rand.nextFloat() * 0.8F + 0.1F;
               float f1 = world.rand.nextFloat() * 0.8F + 0.1F;

               EntityItem entityitem;
               for(float f2 = world.rand.nextFloat() * 0.8F + 0.1F; itemstack.stackSize > 0; world.spawnEntityInWorld(entityitem)) {
                  int j1 = world.rand.nextInt(21) + 10;
                  if (j1 > itemstack.stackSize) {
                     j1 = itemstack.stackSize;
                  }

                  itemstack.stackSize -= j1;
                  entityitem = new EntityItem(world, (double)((float)x + f), (double)((float)y + f1), (double)((float)z + f2), new ItemStack(itemstack.getItem(), j1, itemstack.getItemDamage()));
                  float f3 = 0.05F;
                  entityitem.motionX = (double)((float)world.rand.nextGaussian() * f3);
                  entityitem.motionY = (double)((float)world.rand.nextGaussian() * f3 + 0.2F);
                  entityitem.motionZ = (double)((float)world.rand.nextGaussian() * f3);
                  if (itemstack.hasTagCompound()) {
                     entityitem.getEntityItem().setTagCompound((NBTTagCompound)itemstack.getTagCompound().copy());
                  }
               }
            }
         }

         world.func_147453_f(x, y, z, block);
      }

   }

   private static boolean checkNorth(World world, int x, int y, int z) {
      return world.getBlock(x + -1, y + -1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + -1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + -1, z + -2) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 0, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 0, z + -1) instanceof BlockGrinderCasing && world.getBlock(x + -1, y + 0, z + -2) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 1, z + -2) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + -1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + -1, z + -1) instanceof BlockGrinderCasing && world.getBlock(x + 0, y + -1, z + -2) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + 0, z + -1).equals(Blocks.lava) && world.getBlock(x + 0, y + 0, z + -2) instanceof BlockGrinderCasing && world.getBlock(x + 0, y + 1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + 1, z + -1) instanceof BlockGrinderCasing && world.getBlock(x + 0, y + 1, z + -2) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + -1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + -1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + -1, z + -2) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 0, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 0, z + -1) instanceof BlockGrinderCasing && world.getBlock(x + 1, y + 0, z + -2) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 1, z + -2) instanceof BlockGrinderFrame;
   }

   private static boolean checkEast(World world, int x, int y, int z) {
      return world.getBlock(x + 0, y + -1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + -1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + 2, y + -1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + 0, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 0, z + -1) instanceof BlockGrinderCasing && world.getBlock(x + 2, y + 0, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + 1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + 2, y + 1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + -1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + -1, z + 0) instanceof BlockGrinderCasing && world.getBlock(x + 2, y + -1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 0, z + 0).equals(Blocks.lava) && world.getBlock(x + 2, y + 0, z + 0) instanceof BlockGrinderCasing && world.getBlock(x + 0, y + 1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 1, z + 0) instanceof BlockGrinderCasing && world.getBlock(x + 2, y + 1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + -1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + -1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + 2, y + -1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + 0, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 0, z + 1) instanceof BlockGrinderCasing && world.getBlock(x + 2, y + 0, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + 1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + 2, y + 1, z + 1) instanceof BlockGrinderFrame;
   }

   private static boolean checkSouth(World world, int x, int y, int z) {
      return world.getBlock(x + 1, y + -1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + -1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + -1, z + 2) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 0, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 0, z + 1) instanceof BlockGrinderCasing && world.getBlock(x + 1, y + 0, z + 2) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + 1, y + 1, z + 2) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + -1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + -1, z + 1) instanceof BlockGrinderCasing && world.getBlock(x + 0, y + -1, z + 2) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + 0, z + 1).equals(Blocks.lava) && world.getBlock(x + 0, y + 0, z + 2) instanceof BlockGrinderCasing && world.getBlock(x + 0, y + 1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + 1, z + 1) instanceof BlockGrinderCasing && world.getBlock(x + 0, y + 1, z + 2) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + -1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + -1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + -1, z + 2) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 0, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 0, z + 1) instanceof BlockGrinderCasing && world.getBlock(x + -1, y + 0, z + 2) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 1, z + 2) instanceof BlockGrinderFrame;
   }

   private static boolean checkWest(World world, int x, int y, int z) {
      return world.getBlock(x + 0, y + -1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + -1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + -2, y + -1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + 0, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 0, z + 1) instanceof BlockGrinderCasing && world.getBlock(x + -2, y + 0, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + 1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + -2, y + 1, z + 1) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + -1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + -1, z + 0) instanceof BlockGrinderCasing && world.getBlock(x + -2, y + -1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 0, z + 0).equals(Blocks.lava) && world.getBlock(x + -2, y + 0, z + 0) instanceof BlockGrinderCasing && world.getBlock(x + 0, y + 1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 1, z + 0) instanceof BlockGrinderCasing && world.getBlock(x + -2, y + 1, z + 0) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + -1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + -1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + -2, y + -1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + 0, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 0, z + -1) instanceof BlockGrinderCasing && world.getBlock(x + -2, y + 0, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + 0, y + 1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + -1, y + 1, z + -1) instanceof BlockGrinderFrame && world.getBlock(x + -2, y + 1, z + -1) instanceof BlockGrinderFrame;
   }

   public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
      return new TileEntityPaladiumGrinder();
   }

   public boolean hasTileEntity() {
      return true;
   }

   public void registerBlockIcons(IIconRegister iiconRegister) {
      this.blockIcon = iiconRegister.registerIcon("palamod:PaladiumGrinder_Sides");
      this.front = iiconRegister.registerIcon("palamod:PaladiumGrinder_Front");
   }

   public IIcon getIcon(int side, int metadata) {
      if (metadata == 0 && side == 3) {
         return this.front;
      } else {
         return side != metadata ? this.blockIcon : this.front;
      }
   }

   public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack) {
      super.onBlockPlacedBy(world, x, y, z, entity, stack);
      byte b0 = 4;
      byte b01 = this.rotateBlock(b0, entity);
      world.setBlockMetadataWithNotify(x, y, z, b0, 2);
   }

   public byte rotateBlock(byte b0, EntityLivingBase entity) {
      if (entity.rotationYaw >= 135.0F && entity.rotationYaw <= 181.0F || entity.rotationYaw <= -135.0F && entity.rotationYaw >= -181.0F) {
         b0 = 3;
      } else if (entity.rotationYaw > -135.0F && entity.rotationYaw < -45.0F) {
         b0 = 4;
      } else if (entity.rotationYaw >= -45.0F && entity.rotationYaw <= 45.0F) {
         b0 = 2;
      } else if (entity.rotationYaw > 45.0F && entity.rotationYaw < 135.0F) {
         b0 = 5;
      } else if (entity.rotationYaw >= 181.0F) {
         entity.rotationYaw -= 360.0F;
         b0 = this.rotateBlock(b0, entity);
      } else if (entity.rotationYaw <= -181.0F) {
         entity.rotationYaw += 360.0F;
         b0 = this.rotateBlock(b0, entity);
      }

      return b0;
   }
}
