package fr.paladium.palamod.blocks;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockRich extends Block {
   public BlockRich() {
      super(Material.rock);
      this.setResistance(8.0F);
      this.setHarvestLevel("pickaxe", 1);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setBlockName("rich");
      this.setBlockTextureName("palamod:RichBlock");
      this.setHardness(12.0F);
   }
}
