package fr.paladium.palamod.blocks;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.entities.mobs.EntityCustomWither;
import fr.paladium.palamod.util.WitherData;
import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSkull;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.AchievementList;
import net.minecraft.tileentity.TileEntitySkull;
import net.minecraft.world.World;

public class BlockSkullUpgrade extends Block {
   public final WitherData.WitherUpgrade upgrade;

   public BlockSkullUpgrade(WitherData.WitherUpgrade upgrade) {
      super(Material.circuits);
      this.upgrade = upgrade;
      this.setBlockBounds(0.25F, 0.0F, 0.25F, 0.75F, 0.5F, 0.75F);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setBlockTextureName("palamod:skull_wither");
   }

   public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune) {
      ArrayList<ItemStack> ret = new ArrayList();
      int count = this.quantityDropped(metadata, fortune, world.rand);

      for(int i = 0; i < count; ++i) {
         Item item = this.getItemDropped(metadata, world.rand, fortune);
         if (item != null) {
            ret.add(new ItemStack(item, 1));
         }
      }

      return ret;
   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer p_149727_5_, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
      return checkForWitherSpawn(world, x, y, z);
   }

   public void onPostBlockPlaced(World world, int x, int y, int z, int p_149714_5_) {
      if (!checkForWitherSpawn(world, x, y, z) && !checkForWitherSpawn(world, x + 1, y, z) && !checkForWitherSpawn(world, x - 1, y, z) && !checkForWitherSpawn(world, x, y, z + 1)) {
         checkForWitherSpawn(world, x, y, z - 1);
      }

   }

   public static boolean isValidHeadBlock(World world, int x, int y, int z) {
      try {
         Block block = world.getBlock(x, y, z);
         if (block instanceof BlockSkullUpgrade) {
            return true;
         }

         if (block instanceof BlockSkull) {
            TileEntitySkull tileEntity = (TileEntitySkull)world.getTileEntity(x, y, z);
            return tileEntity.func_145904_a() == 1;
         }
      } catch (Exception var6) {
      }

      return false;
   }

   public static boolean checkForWitherSpawn(World world, int x, int y, int z) {
      if (world.getBlock(x, y - 1, z) == Blocks.soul_sand && world.getBlock(x, y - 2, z) == Blocks.soul_sand && isValidHeadBlock(world, x, y, z)) {
         boolean zaxis = false;
         if (isValidHeadBlock(world, x, y, z + 1) && isValidHeadBlock(world, x, y, z - 1) && world.getBlock(x, y - 1, z - 1) == Blocks.soul_sand && world.getBlock(x, y - 1, z + 1) == Blocks.soul_sand) {
            zaxis = true;
         } else if (!isValidHeadBlock(world, x + 1, y, z) || !isValidHeadBlock(world, x - 1, y, z) || world.getBlock(x - 1, y - 1, z) != Blocks.soul_sand || world.getBlock(x + 1, y - 1, z) != Blocks.soul_sand) {
            return false;
         }

         WitherData witherData = new WitherData();
         Block block = world.getBlock(x, y, z);
         if (block instanceof BlockSkullUpgrade && !((BlockSkullUpgrade)block).upgrade.proccesUpgrade(witherData)) {
            return false;
         } else {
            if (zaxis) {
               block = world.getBlock(x, y, z - 1);
               if (block instanceof BlockSkullUpgrade && !((BlockSkullUpgrade)block).upgrade.proccesUpgrade(witherData)) {
                  return false;
               }

               block = world.getBlock(x, y, z + 1);
               if (block instanceof BlockSkullUpgrade && !((BlockSkullUpgrade)block).upgrade.proccesUpgrade(witherData)) {
                  return false;
               }
            } else {
               block = world.getBlock(x - 1, y, z);
               if (block instanceof BlockSkullUpgrade && !((BlockSkullUpgrade)block).upgrade.proccesUpgrade(witherData)) {
                  return false;
               }

               block = world.getBlock(x + 1, y, z);
               if (block instanceof BlockSkullUpgrade && !((BlockSkullUpgrade)block).upgrade.proccesUpgrade(witherData)) {
                  return false;
               }
            }

            world.setBlockToAir(x, y, z);
            world.setBlockToAir(x, y - 1, z);
            world.setBlockToAir(x, y - 2, z);
            if (zaxis) {
               world.setBlockToAir(x, y, z - 1);
               world.setBlockToAir(x, y - 1, z - 1);
               world.setBlockToAir(x, y, z + 1);
               world.setBlockToAir(x, y - 1, z + 1);
            } else {
               world.setBlockToAir(x - 1, y, z);
               world.setBlockToAir(x - 1, y - 1, z);
               world.setBlockToAir(x + 1, y, z);
               world.setBlockToAir(x + 1, y - 1, z);
            }

            if (!world.isRemote) {
               EntityCustomWither customWither = new EntityCustomWither(world, witherData);
               if (zaxis) {
                  customWither.setLocationAndAngles((double)x + 1.5D, (double)y - 1.45D, (double)z + 0.5D, 90.0F, 0.0F);
                  customWither.renderYawOffset = 90.0F;
               } else {
                  customWither.setLocationAndAngles((double)x + 1.5D, (double)y - 1.45D, (double)z + 0.5D, 0.0F, 0.0F);

                  for(int i1 = 0; i1 < 120; ++i1) {
                     world.spawnParticle("snowballpoof", (double)(x + 1) + world.rand.nextDouble(), (double)(y - 2) + world.rand.nextDouble() * 3.9D, (double)z + world.rand.nextDouble(), 0.0D, 0.0D, 0.0D);
                  }
               }

               customWither.func_82206_m();
               world.spawnEntityInWorld(customWither);
               Iterator iterator = world.getEntitiesWithinAABB(EntityPlayer.class, customWither.boundingBox.expand(50.0D, 50.0D, 50.0D)).iterator();

               while(iterator.hasNext()) {
                  ((EntityPlayer)iterator.next()).triggerAchievement(AchievementList.field_150963_I);
               }
            }

            return true;
         }
      } else {
         return false;
      }
   }

   public boolean isOpaqueCube() {
      return false;
   }
}
