package fr.paladium.palamod.blocks;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class BlockSlowBlock extends Block {
   public BlockSlowBlock() {
      super(Material.rock);
      this.setResistance(8.0F);
      this.setHarvestLevel("pickaxe", 1);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setBlockName("slowblock");
      this.setBlockTextureName("palamod:SlowBlock");
      this.setHardness(12.0F);
   }

   public AxisAlignedBB getCollisionBoundingBoxFromPool(World p_149668_1_, int p_149668_2_, int p_149668_3_, int p_149668_4_) {
      float f = 0.125F;
      return AxisAlignedBB.getBoundingBox((double)p_149668_2_, (double)p_149668_3_, (double)p_149668_4_, (double)(p_149668_2_ + 1), (double)((float)(p_149668_3_ + 1) - f), (double)(p_149668_4_ + 1));
   }

   public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity e) {
      super.onEntityCollidedWithBlock(world, x, y, z, e);
      if (e instanceof EntityLivingBase) {
         EntityLivingBase entity = (EntityLivingBase)e;
         entity.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.005D);
      }
   }
}
