package fr.paladium.palamod.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.client.ClientProxy;
import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Facing;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.util.ForgeDirection;

public class BlockSpike extends Block {
   String texture1;
   float damage;
   IIcon icon;

   public BlockSpike(Material m, String name, String texture, String texture1, float damage, boolean isResistant) {
      super(m);
      this.setResistance(8.0F);
      this.setHarvestLevel("pickaxe", 2);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setBlockName(name);
      this.setHardness(12.0F);
      this.setBlockTextureName("palamod:" + texture);
      if (isResistant) {
         this.setResistance(100.0F);
      } else {
         this.setResistance(1.0F);
      }

      this.texture1 = texture1;
      this.damage = damage;
   }

   @SideOnly(Side.CLIENT)
   public int getRenderType() {
      return ClientProxy.renderBlockSpikeId;
   }

   public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity target) {
      if (!world.isRemote && world instanceof WorldServer) {
         target.attackEntityFrom(DamageSource.cactus, this.damage);
      }
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public void registerBlockIcons(IIconRegister icons) {
      super.registerBlockIcons(icons);
      this.icon = icons.registerIcon("palamod:" + this.texture1);
   }

   @SideOnly(Side.CLIENT)
   public IIcon getIcon(int par1, int par2) {
      int side = par2 % 6;
      return par1 == Facing.oppositeSide[side] ? this.blockIcon : this.icon;
   }

   public int onBlockPlaced(World par1World, int par2, int par3, int par4, int par5, float par6, float par7, float par8, int par9) {
      int meta = par5 % 6;
      int flag = 0;
      ForgeDirection side = ForgeDirection.getOrientation(meta);
      if (!par1World.isSideSolid(par2 - side.offsetX, par3 - side.offsetY, par4 - side.offsetZ, side.getOpposite())) {
         ForgeDirection[] var13 = ForgeDirection.VALID_DIRECTIONS;
         int var14 = var13.length;

         for(int var15 = 0; var15 < var14; ++var15) {
            ForgeDirection dir = var13[var15];
            if (side != dir) {
               if (par1World.isSideSolid(par2 - dir.offsetX, par3 - dir.offsetY, par4 - dir.offsetZ, dir.getOpposite())) {
                  return flag + dir.ordinal();
               }

               if (par1World.getBlock(par2 - dir.offsetX, par3 - dir.offsetY, par4 - dir.offsetZ) == this) {
                  par5 = par1World.getBlockMetadata(par2 - dir.offsetX, par3 - dir.offsetY, par4 - dir.offsetZ) % 6;
               }
            }
         }
      }

      return flag + par5;
   }

   public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4) {
      double h = 0.0625D;
      int side = par1World.getBlockMetadata(par2, par3, par4) % 6;
      switch(side) {
      case 0:
         return AxisAlignedBB.getBoundingBox((double)par2 + h, (double)par3 + h, (double)par4 + h, (double)par2 + 1.0D - h, (double)par3 + 1.0D, (double)par4 + 1.0D - h);
      case 1:
         return AxisAlignedBB.getBoundingBox((double)par2 + h, (double)par3, (double)par4 + h, (double)par2 + 1.0D - h, (double)par3 + 1.0D - h, (double)par4 + 1.0D - h);
      case 2:
         return AxisAlignedBB.getBoundingBox((double)par2 + h, (double)par3 + h, (double)par4 + h, (double)par2 + 1.0D - h, (double)par3 + 1.0D - h, (double)par4 + 1.0D);
      case 3:
         return AxisAlignedBB.getBoundingBox((double)par2 + h, (double)par3 + h, (double)par4, (double)par2 + 1.0D - h, (double)par3 + 1.0D - h, (double)par4 + 1.0D - h);
      case 4:
         return AxisAlignedBB.getBoundingBox((double)par2 + h, (double)par3 + h, (double)par4 + h, (double)par2 + 1.0D, (double)par3 + 1.0D - h, (double)par4 + 1.0D - h);
      case 5:
         return AxisAlignedBB.getBoundingBox((double)par2, (double)par3 + h, (double)par4 + h, (double)par2 + 1.0D - h, (double)par3 + 1.0D - h, (double)par4 + 1.0D - h);
      default:
         return AxisAlignedBB.getBoundingBox((double)par2 + h, (double)par3 + h, (double)par4 + h, (double)par2 + 1.0D - h, (double)par3 + 1.0D - h, (double)par4 + 1.0D - h);
      }
   }
}
