package fr.paladium.palamod.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.items.ItemToolTrap;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockGrass;
import net.minecraft.block.BlockLeaves;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.ColorizerGrass;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockTrap extends Block {
   public BlockTrap() {
      super(Material.rock);
      this.setResistance(8.0F);
      this.setHarvestLevel("pickaxe", 1);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setBlockName("blocktrap");
      this.setBlockTextureName("palamod:BlockTrap");
      this.setHardness(12.0F);
   }

   public boolean isNormalCube() {
      return false;
   }

   public boolean isNormalCube(IBlockAccess world, int x, int y, int z) {
      return false;
   }

   public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k) {
      return null;
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
      if (player.getHeldItem().getItem() instanceof ItemToolTrap) {
         int meta = world.getBlockMetadata(x, y, z);
         ++meta;
         if (meta > 3) {
            meta = 0;
         }

         world.setBlockToAir(x, y, z);
         world.setBlock(x, y, z, ModBlocks.blockTrap, meta, 0);
         return true;
      } else {
         return false;
      }
   }

   public IIcon getIcon(IBlockAccess access, int x, int y, int z, int side) {
      World world = Minecraft.getMinecraft().theWorld;
      int meta = world.getBlockMetadata(x, y, z);
      Block block = this.getBlock(world, meta, x, y, z);
      this.getBlockMetadata(world, meta, x, y, z);
      return !block.isAir(world, x, y, z) && block.getRenderType() == 0 ? block.getIcon(side, meta) : null;
   }

   public Block getBlock(World world, int meta, int x, int y, int z) {
      switch(meta) {
      case 0:
         return world.getBlock(x + 1, y, z);
      case 1:
         return world.getBlock(x, y, z + 1);
      case 2:
         return world.getBlock(x - 1, y, z);
      case 3:
         return world.getBlock(x, y, z - 1);
      default:
         return world.getBlock(x + 1, y, z);
      }
   }

   public int getBlockMetadata(World world, int meta, int x, int y, int z) {
      switch(meta) {
      case 0:
         return world.getBlockMetadata(x + 1, y, z);
      case 1:
         return world.getBlockMetadata(x, y, z + 1);
      case 2:
         return world.getBlockMetadata(x - 1, y, z);
      case 3:
         return world.getBlockMetadata(x, y, z - 1);
      default:
         return world.getBlockMetadata(x + 1, y, z);
      }
   }

   @SideOnly(Side.CLIENT)
   public int getBlockColor() {
      double d0 = 0.5D;
      double d1 = 1.0D;
      return ColorizerGrass.getGrassColor(d0, d1);
   }

   @SideOnly(Side.CLIENT)
   public int getRenderColor(int p_149741_1_) {
      return super.getBlockColor();
   }

   @SideOnly(Side.CLIENT)
   public int colorMultiplier(IBlockAccess p_149720_1_, int p_149720_2_, int p_149720_3_, int p_149720_4_) {
      World world = Minecraft.getMinecraft().theWorld;
      int meta = world.getBlockMetadata(p_149720_2_, p_149720_3_, p_149720_4_);
      Block block = this.getBlock(world, meta, p_149720_2_, p_149720_3_, p_149720_4_);
      if (!(block instanceof BlockGrass) && !(block instanceof BlockLeaves)) {
         return super.colorMultiplier(p_149720_1_, p_149720_2_, p_149720_3_, p_149720_4_);
      } else {
         int l = 0;
         int i1 = 0;
         int j1 = 0;

         for(int k1 = -1; k1 <= 1; ++k1) {
            for(int l1 = -1; l1 <= 1; ++l1) {
               int i2 = p_149720_1_.getBiomeGenForCoords(p_149720_2_ + l1, p_149720_4_ + k1).getBiomeGrassColor(p_149720_2_ + l1, p_149720_3_, p_149720_4_ + k1);
               l += (i2 & 16711680) >> 16;
               i1 += (i2 & '\uff00') >> 8;
               j1 += i2 & 255;
            }
         }

         return (l / 9 & 255) << 16 | (i1 / 9 & 255) << 8 | j1 / 9 & 255;
      }
   }

   @SideOnly(Side.CLIENT)
   public void randomDisplayTick(World p_149734_1_, int p_149734_2_, int p_149734_3_, int p_149734_4_, Random p_149734_5_) {
      float f = (float)p_149734_2_ + 0.5F;
      float f1 = (float)p_149734_3_ + 0.0F + p_149734_5_.nextFloat() * 6.0F / 16.0F + 1.0F;
      float f2 = (float)p_149734_4_;
      float f3 = 0.0F;
      float f4 = p_149734_5_.nextFloat() * 0.6F + 0.5F;
      if (p_149734_5_.nextInt(10) == 0) {
         p_149734_1_.spawnParticle("smoke", (double)(f - f3), (double)f1, (double)(f2 + f4), 0.0D, 0.0D, 0.0D);
      }

   }
}
