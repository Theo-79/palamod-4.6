package fr.paladium.palamod.blocks;

import fr.paladium.palamod.tiles.TileEntityTurret;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockTurret extends BlockContainer {
   protected BlockTurret() {
      super(Material.iron);
      this.setResistance(1.8E7F);
      this.setBlockTextureName("palamod:textures/models/Turret.png");
   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
      if (!world.isRemote) {
         TileEntity tile = world.getTileEntity(x, y, z);
         if (tile != null && tile instanceof TileEntityTurret) {
            TileEntityTurret turret = (TileEntityTurret)tile;
            turret.activate = !turret.activate;
            world.markBlockForUpdate(x, y, z);
            return true;
         }
      }

      return false;
   }

   public TileEntity createNewTileEntity(World world, int metadata) {
      return new TileEntityTurret();
   }

   public int getRenderType() {
      return -1;
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }
}
