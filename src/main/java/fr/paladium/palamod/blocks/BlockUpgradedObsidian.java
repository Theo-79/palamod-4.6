package fr.paladium.palamod.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.common.PassiveExplosion;
import fr.paladium.palamod.items.ItemObsidianUpgrade;
import fr.paladium.palamod.tiles.TileEntityUpgradedObsidian;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockUpgradedObsidian extends Block implements ITileEntityProvider {
   public BlockUpgradedObsidian() {
      super(Material.rock);
   }

   public TileEntity createNewTileEntity(World world, int metadata) {
      return new TileEntityUpgradedObsidian();
   }

   public boolean hasTileEntity(int metadata) {
      return true;
   }

   public void breakBlock(World world, int x, int y, int z, Block block, int metadata) {
      Boolean flag = false;
      Boolean flag1 = false;
      TileEntity tile = world.getTileEntity(x, y, z);
      if (tile instanceof TileEntityUpgradedObsidian) {
         TileEntityUpgradedObsidian te = (TileEntityUpgradedObsidian)tile;
         if (te.hasUpgrade(ItemObsidianUpgrade.Upgrade.TwoLife) && te.isAlife()) {
            flag = true;
         }

         if (te.hasUpgrade(ItemObsidianUpgrade.Upgrade.Fake)) {
            flag1 = true;
         }

         if (te.hasUpgrade(ItemObsidianUpgrade.Upgrade.Explode)) {
            PassiveExplosion.newExplosion((Entity)null, (double)x, (double)y, (double)z, 4.0F, true, false, world);
         }

         if (flag) {
            world.setBlock(x, y, z, block);
            world.setTileEntity(x, y, z, te);
         } else {
            super.breakBlock(world, x, y, z, block, metadata);
            world.removeTileEntity(x, y, z);
            if (flag1 && world.rand.nextInt(4) % 4 == 0) {
               world.setBlock(x, y, z, ModBlocks.sulfuricWater);
            }
         }
      } else {
         super.breakBlock(world, x, y, z, block, metadata);
      }

   }

   public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack) {
      TileEntity tile = world.getTileEntity(x, y, z);
      if (tile instanceof TileEntityUpgradedObsidian && stack.hasTagCompound()) {
         TileEntityUpgradedObsidian te = (TileEntityUpgradedObsidian)tile;
         NBTTagCompound compound = stack.getTagCompound();
         List<ItemObsidianUpgrade.Upgrade> upgrades = new ArrayList();
         if (compound.getBoolean("Explode")) {
            upgrades.add(ItemObsidianUpgrade.Upgrade.Explode);
         }

         if (compound.getBoolean("Fake")) {
            upgrades.add(ItemObsidianUpgrade.Upgrade.Fake);
         }

         if (compound.getBoolean("TwoLife")) {
            upgrades.add(ItemObsidianUpgrade.Upgrade.TwoLife);
         }

         if (compound.getBoolean("Camouflage")) {
            upgrades.add(ItemObsidianUpgrade.Upgrade.Camouflage);
         }

         te.setInformations(upgrades);
         te.setBaseLife();
      }

   }

   public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune) {
      ArrayList<ItemStack> drops = new ArrayList();
      drops.add(this.getUpgradedObsidian(world, x, y, z));
      return drops;
   }

   public ItemStack getUpgradedObsidian(World world, int x, int y, int z) {
      TileEntity tile = world.getTileEntity(x, y, z);
      if (tile instanceof TileEntityUpgradedObsidian) {
         TileEntityUpgradedObsidian te = (TileEntityUpgradedObsidian)tile;
         if (te.hasUpgrade(ItemObsidianUpgrade.Upgrade.Explode) || te.hasUpgrade(ItemObsidianUpgrade.Upgrade.Fake) || te.hasUpgrade(ItemObsidianUpgrade.Upgrade.TwoLife) || te.hasUpgrade(ItemObsidianUpgrade.Upgrade.Camouflage)) {
            ItemStack stack = new ItemStack(ModBlocks.upgraded_obsidian);
            if (!stack.hasTagCompound()) {
               stack.setTagCompound(new NBTTagCompound());
            }

            NBTTagCompound compound = stack.getTagCompound();
            compound.setBoolean("Explode", te.hasUpgrade(ItemObsidianUpgrade.Upgrade.Explode));
            compound.setBoolean("Fake", te.hasUpgrade(ItemObsidianUpgrade.Upgrade.Fake));
            compound.setBoolean("TwoLife", te.hasUpgrade(ItemObsidianUpgrade.Upgrade.TwoLife));
            compound.setBoolean("Camouflage", te.hasUpgrade(ItemObsidianUpgrade.Upgrade.Camouflage));
            stack.setTagCompound(compound);
            return stack;
         }
      }

      return new ItemStack(Blocks.obsidian);
   }

   @SideOnly(Side.CLIENT)
   public int colorMultiplier(IBlockAccess access, int x, int y, int z) {
      TileEntity tile = access.getTileEntity(x, y, z);
      if (tile instanceof TileEntityUpgradedObsidian) {
         TileEntityUpgradedObsidian te = (TileEntityUpgradedObsidian)tile;
         if (!te.hasUpgrade(ItemObsidianUpgrade.Upgrade.Camouflage)) {
            return -16711936;
         }
      }

      return 16777215;
   }
}
