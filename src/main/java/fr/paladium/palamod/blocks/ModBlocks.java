package fr.paladium.palamod.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import fr.paladium.palamod.blocks.bushes.BlockXPBush;
import fr.paladium.palamod.blocks.core.BlockGrinderCasing;
import fr.paladium.palamod.blocks.core.BlockGrinderFrame;
import fr.paladium.palamod.blocks.fluids.BlockAngelicWater;
import fr.paladium.palamod.blocks.fluids.BlockSlufuricWater;
import fr.paladium.palamod.blocks.ores.BlockAmethyst;
import fr.paladium.palamod.blocks.ores.BlockAmethystOre;
import fr.paladium.palamod.blocks.ores.BlockFindiumOre;
import fr.paladium.palamod.blocks.ores.BlockPaladium;
import fr.paladium.palamod.blocks.ores.BlockPaladiumOre;
import fr.paladium.palamod.blocks.ores.BlockTitane;
import fr.paladium.palamod.blocks.ores.BlockTitaneOre;
import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.items.ItemBlockMulticolorSlab;
import fr.paladium.palamod.items.ModItems;
import fr.paladium.palamod.util.WitherData;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

public class ModBlocks {
   public static BlockAmethystOre amethystOre;
   public static BlockTitaneOre titaneOre;
   public static BlockPaladiumOre paladiumOre;
   public static BlockFindiumOre findiumOre;
   public static BlockPaladium paladiumBlock;
   public static BlockTitane titaneBlock;
   public static BlockAmethyst amethystBlock;
   public static BlockPaladiumMachine paladiumMachine;
   public static BlockAlchemyCreator alchemyCreator;
   public static BlockPaladiumFurnace paladiumFurnace;
   public static BlockPaladiumOven paladiumOven;
   public static BlockPaladiumChest paladiumChest;
   public static BlockBowMachine bowMachine;
   public static BlockAlchemyStacker alchemyStacker;
   public static BlockGuardian guardianBlock;
   public static BlockOnlineDetector onlineDetector;
   public static BlockPaladiumGrinder paladiumGrinder;
   public static BlockGuardianKeeper guardianKeeper;
   public static BlockSlimePad slimePad;
   public static BlockSlowBlock slowBlock;
   public static BlockCustomWeb customWeb;
   public static BlockTrap blockTrap;
   public static BlockMysteryBoxBase uniqueBox;
   public static BlockMysteryBoxBase eliteBox;
   public static BlockMysteryBoxBase ultimeBox;
   public static BlockMysteryBoxBase legendaireBox;
   public static BlockXPBush experienceBush;
   public static BlockGrinderCasing grinderCasing;
   public static BlockGrinderFrame grinderFrame;
   public static BlockCave cave;
   public static BlockInvisible invisibleBlock;
   public static BlockSlufuricWater sulfuricWater;
   public static Fluid sulfuricWaterFluid;
   public static BlockAngelicWater angelicWater;
   public static Fluid angelicWaterFluid;
   public static BlockRich rich;
   public static BlockMagicalAnvil magicalAnvil;
   public static BlockArmorCompresor compressor;
   public static BlockSpike spikeIron;
   public static BlockSpike spikeWood;
   public static BlockSpike spikeDiamond;
   public static BlockSpike spikeGold;
   public static BlockSpike spikeAmethyst;
   public static BlockSpike spikeTitane;
   public static BlockSpike spikePaladium;
   public static Block multicolor_block;
   public static Block multicolor_stairs;
   public static Block multicolor_slab_1;
   public static Block multicolor_slab_2;
   public static Block obsidian_upgrader;
   public static Block upgraded_obsidian;
   public static BlockSkullUpgrade blockSkullUpgradeLife;
   public static BlockSkullUpgrade blockSkullUpgradeDamage;
   public static BlockSkullUpgrade blockSkullUpgradeNoFly;
   public static BlockSkullUpgrade blockSkullUpgradeNoSound;
   public static BlockSkullUpgrade blockSkullUpgradeNoFake;
   public static BlockSkullUpgrade blockSkullUpgradeAngry;
   public static BlockSkullUpgrade blockSkullUpgradeExplosionRes;
   public static BlockSkullUpgrade blockSkullUpgradeAnvilRes;
   public static BlockSkullUpgrade blockSkullUpgradeRemote;
   public static Block camera;
   public static Block turret;

   public static void init() {
      amethystOre = new BlockAmethystOre();
      titaneOre = new BlockTitaneOre();
      paladiumOre = new BlockPaladiumOre();
      findiumOre = new BlockFindiumOre();
      paladiumBlock = new BlockPaladium();
      titaneBlock = new BlockTitane();
      amethystBlock = new BlockAmethyst();
      paladiumMachine = new BlockPaladiumMachine();
      alchemyCreator = new BlockAlchemyCreator();
      paladiumFurnace = new BlockPaladiumFurnace();
      paladiumOven = new BlockPaladiumOven();
      paladiumChest = new BlockPaladiumChest();
      bowMachine = new BlockBowMachine();
      alchemyStacker = new BlockAlchemyStacker();
      guardianBlock = new BlockGuardian();
      onlineDetector = new BlockOnlineDetector();
      paladiumGrinder = new BlockPaladiumGrinder();
      guardianKeeper = new BlockGuardianKeeper();
      slimePad = new BlockSlimePad();
      slowBlock = new BlockSlowBlock();
      customWeb = new BlockCustomWeb();
      uniqueBox = new BlockMysteryBoxBase("uniquebox", "UniqueBox_Top", "UniqueBox_Sides", "UniqueBox_Bottom");
      eliteBox = new BlockMysteryBoxBase("elitebox", "EliteBox_Top", "EliteBox_Sides", "EliteBox_Bottom");
      ultimeBox = new BlockMysteryBoxBase("ultimebox", "UltimeBox_Top", "UltimeBox_Sides", "UltimeBox_Bottom");
      legendaireBox = new BlockMysteryBoxBase("legendarybox", "LegendaryBox_Top", "Legendary_Sides", "Legendary_Bottom");
      magicalAnvil = new BlockMagicalAnvil();
      compressor = new BlockArmorCompresor();
      experienceBush = new BlockXPBush(ModItems.experienceBerry);
      grinderFrame = new BlockGrinderFrame();
      grinderCasing = new BlockGrinderCasing();
      cave = new BlockCave();
      invisibleBlock = new BlockInvisible();
      rich = new BlockRich();
      sulfuricWaterFluid = (new Fluid("sulfuricWater")).setDensity(200).setViscosity(1);
      angelicWaterFluid = (new Fluid("angelicWater")).setDensity(200);
      FluidRegistry.registerFluid(angelicWaterFluid);
      FluidRegistry.registerFluid(sulfuricWaterFluid);
      sulfuricWater = new BlockSlufuricWater(sulfuricWaterFluid, Material.water);
      angelicWater = new BlockAngelicWater(angelicWaterFluid, Material.water);
      spikeIron = new BlockSpike(Material.iron, "spikeiron", "SpikeIron_1", "SpikeIron_2", 5.0F, false);
      spikeWood = new BlockSpike(Material.iron, "spikewood", "SpikeWood_2", "SpikeWood_1", 2.0F, false);
      spikeDiamond = new BlockSpike(Material.iron, "spikediamond", "SpikeDiamond_2", "SpikeDiamond_1", 10.0F, false);
      spikeGold = new BlockSpike(Material.iron, "spikegold", "SpikeGold_2", "SpikeGold_1", 7.0F, false);
      spikeAmethyst = new BlockSpike(Material.iron, "spikeamethyst", "SpikeAmethyst_1", "SpikeAmethyst_2", 10.0F, false);
      spikeTitane = new BlockSpike(Material.iron, "spiketitane", "SpikeTitane_1", "SpikeTitane_2", 12.0F, false);
      spikePaladium = new BlockSpike(Material.iron, "spikepaladium", "SpikePaladium_1", "SpikePaladium_2", 15.0F, true);
      multicolor_block = (new MulticolorBlock()).setBlockName("multicolor_block").setHardness(2.5F).setResistance(10.0F).setStepSound(Block.soundTypeWood).setLightLevel(1.0F).setCreativeTab(TabPaladium.INSTANCE);
      multicolor_stairs = (new MulticolorStairs(multicolor_block, 0)).setBlockName("multicolor_stairs").setHardness(2.5F).setResistance(10.0F).setStepSound(Block.soundTypeWood).setLightLevel(1.0F).setCreativeTab(TabPaladium.INSTANCE);
      multicolor_slab_1 = (new MulticolorSlab(false)).setBlockName("multicolor_slab_1").setHardness(2.5F).setResistance(10.0F).setStepSound(Block.soundTypeWood).setLightLevel(1.0F).setCreativeTab(TabPaladium.INSTANCE);
      multicolor_slab_2 = (new MulticolorSlab(true)).setBlockName("multicolor_slab_2").setHardness(2.5F).setResistance(10.0F).setStepSound(Block.soundTypeWood).setLightLevel(1.0F);
      obsidian_upgrader = (new BlockObsidianUpgrader()).setBlockName("obsidian_upgrader").setHardness(2.5F).setResistance(10.0F).setStepSound(Block.soundTypeWood).setCreativeTab(TabPaladium.INSTANCE);
      upgraded_obsidian = (new BlockUpgradedObsidian()).setHardness(50.0F).setResistance(2000.0F).setStepSound(Block.soundTypePiston).setBlockName("upgraded_obsidian").setBlockTextureName("obsidian");
      blockSkullUpgradeLife = new BlockSkullUpgrade(WitherData.WitherUpgrade.LIFE);
      blockSkullUpgradeDamage = new BlockSkullUpgrade(WitherData.WitherUpgrade.DAMAGE);
      blockSkullUpgradeNoFly = new BlockSkullUpgrade(WitherData.WitherUpgrade.NOFLY);
      blockSkullUpgradeNoSound = new BlockSkullUpgrade(WitherData.WitherUpgrade.NOSOUND);
      blockSkullUpgradeNoFake = new BlockSkullUpgrade(WitherData.WitherUpgrade.NOFAKE);
      blockSkullUpgradeAngry = new BlockSkullUpgrade(WitherData.WitherUpgrade.ANGRY);
      blockSkullUpgradeExplosionRes = new BlockSkullUpgrade(WitherData.WitherUpgrade.EXPLOSION);
      blockSkullUpgradeAnvilRes = new BlockSkullUpgrade(WitherData.WitherUpgrade.ANVIL);
      blockSkullUpgradeRemote = new BlockSkullUpgrade(WitherData.WitherUpgrade.REMOTE);
      blockSkullUpgradeLife.setBlockName("lifeskullupgrade");
      blockSkullUpgradeDamage.setBlockName("damageskullupgrade");
      blockSkullUpgradeNoFly.setBlockName("noflyskullupgrade");
      blockSkullUpgradeNoSound.setBlockName("nosoundskullupgrade");
      blockSkullUpgradeNoFake.setBlockName("nofakeskullupgrade");
      blockSkullUpgradeAngry.setBlockName("angryskullupgrade");
      blockSkullUpgradeExplosionRes.setBlockName("explosionskullupgrade");
      blockSkullUpgradeAnvilRes.setBlockName("anvilskullupgrade");
      blockSkullUpgradeRemote.setBlockName("remoteskullupgrade");
      camera = (new BlockCamera()).setBlockName("camera").setHardness(2.5F).setResistance(10.0F).setStepSound(Block.soundTypeWood).setCreativeTab(TabPaladium.INSTANCE);
      turret = (new BlockTurret()).setBlockName("turret").setCreativeTab(TabPaladium.INSTANCE);
      GameRegistry.registerBlock(amethystOre, "amethystOre");
      GameRegistry.registerBlock(titaneOre, "titaneOre");
      GameRegistry.registerBlock(paladiumOre, "paladiumOre");
      GameRegistry.registerBlock(findiumOre, "findiumOre");
      GameRegistry.registerBlock(paladiumBlock, "paladiumBlock");
      GameRegistry.registerBlock(titaneBlock, "titaneBlock");
      GameRegistry.registerBlock(amethystBlock, "amethystBlock");
      GameRegistry.registerBlock(paladiumMachine, "paladiumMachine");
      GameRegistry.registerBlock(alchemyCreator, "alchemyCreator");
      GameRegistry.registerBlock(paladiumFurnace, "paladiumFurnace");
      GameRegistry.registerBlock(paladiumOven, "paladiumOven");
      GameRegistry.registerBlock(paladiumChest, "paladiumChest");
      GameRegistry.registerBlock(bowMachine, "bowMachine");
      GameRegistry.registerBlock(alchemyStacker, "alchemyStacker");
      GameRegistry.registerBlock(guardianBlock, "guardianBlock");
      GameRegistry.registerBlock(onlineDetector, "onlineDetector");
      GameRegistry.registerBlock(paladiumGrinder, "paladiumGrinder");
      GameRegistry.registerBlock(guardianKeeper, "guardianKeeper");
      GameRegistry.registerBlock(slimePad, "slimePad");
      GameRegistry.registerBlock(slowBlock, "slowBlock");
      GameRegistry.registerBlock(customWeb, "customWeb");
      GameRegistry.registerBlock(uniqueBox, "uniqueBox");
      GameRegistry.registerBlock(eliteBox, "eliteBox");
      GameRegistry.registerBlock(ultimeBox, "ultimeBox");
      GameRegistry.registerBlock(legendaireBox, "legendaireBox");
      GameRegistry.registerBlock(magicalAnvil, "magicalAnvil");
      GameRegistry.registerBlock(compressor, "compressor");
      GameRegistry.registerBlock(rich, "rich");
      GameRegistry.registerBlock(experienceBush, "experienceBush");
      GameRegistry.registerBlock(grinderCasing, "grinderCasing");
      GameRegistry.registerBlock(grinderFrame, "grinderFrame");
      GameRegistry.registerBlock(cave, "cave");
      GameRegistry.registerBlock(invisibleBlock, "invisibleblock");
      GameRegistry.registerBlock(sulfuricWater, "sulfuricWater");
      GameRegistry.registerBlock(angelicWater, "angelicWater");
      GameRegistry.registerBlock(spikeIron, "spikeBlock");
      GameRegistry.registerBlock(spikeAmethyst, "spikeAmethyst");
      GameRegistry.registerBlock(spikeDiamond, "spikeDiamond");
      GameRegistry.registerBlock(spikeGold, "spikeGold");
      GameRegistry.registerBlock(spikePaladium, "spikePaladium");
      GameRegistry.registerBlock(spikeWood, "spikeWood");
      GameRegistry.registerBlock(spikeTitane, "spikeTitane");
      GameRegistry.registerBlock(multicolor_block, "multicolor_block");
      GameRegistry.registerBlock(multicolor_stairs, "multicolor_stairs");
      GameRegistry.registerBlock(multicolor_slab_1, ItemBlockMulticolorSlab.class, "multicolor_slab_1");
      GameRegistry.registerBlock(multicolor_slab_2, ItemBlockMulticolorSlab.class, "multicolor_slab_2");
      GameRegistry.registerBlock(obsidian_upgrader, "obsidian_upgrader");
      GameRegistry.registerBlock(upgraded_obsidian, "upgraded_obsidian");
      GameRegistry.registerBlock(blockSkullUpgradeLife, "blockskullupgradeLife");
      GameRegistry.registerBlock(blockSkullUpgradeDamage, "blockskullupgradeDamage");
      GameRegistry.registerBlock(blockSkullUpgradeNoFly, "blockskullupgradeNoFly");
      GameRegistry.registerBlock(blockSkullUpgradeNoSound, "blockskullupgradeNoSound");
      GameRegistry.registerBlock(blockSkullUpgradeNoFake, "blockskullupgradeNoFake");
      GameRegistry.registerBlock(blockSkullUpgradeAngry, "blockskullupgradeAngry");
      GameRegistry.registerBlock(blockSkullUpgradeExplosionRes, "blockskullupgradeExplosionRes");
      GameRegistry.registerBlock(blockSkullUpgradeAnvilRes, "blockskullupgradeAnvilRes");
      GameRegistry.registerBlock(blockSkullUpgradeRemote, "blockskullupgradeRemote");
      GameRegistry.registerBlock(camera, "camera");
      GameRegistry.registerBlock(turret, "turret");
   }
}
