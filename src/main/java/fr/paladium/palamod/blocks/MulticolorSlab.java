package fr.paladium.palamod.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.tiles.TileEntityMulticolorBlock;
import java.util.List;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class MulticolorSlab extends BlockSlab implements ITileEntityProvider {
   private IIcon[] icons = new IIcon[16];

   public MulticolorSlab(boolean isdouble) {
      super(isdouble, Material.wood);
      if (!this.field_150004_a) {
         this.setLightOpacity(0);
      }

   }

   private static boolean func_150003_a(Block block) {
      return block == ModBlocks.multicolor_slab_1;
   }

   @SideOnly(Side.CLIENT)
   private static boolean isBlockSingleSlab(int id) {
      return id == Block.getIdFromBlock(ModBlocks.multicolor_slab_1);
   }

   @SideOnly(Side.CLIENT)
   public Item getItem(World world, int x, int y, int z) {
      return func_150003_a(this) ? Item.getItemFromBlock(ModBlocks.multicolor_slab_1) : Item.getItemFromBlock(ModBlocks.multicolor_slab_2);
   }

   public Item getItemDropped(int metadata, Random rand, int fortune) {
      return Item.getItemFromBlock(ModBlocks.multicolor_slab_1);
   }

   protected ItemStack createStackedBlock(int metadata) {
      return new ItemStack(ModBlocks.multicolor_slab_1, 2, metadata & 7);
   }

   @SideOnly(Side.CLIENT)
   public void getSubBlocks(Item item, CreativeTabs creativeTabs, List list) {
      if (item != Item.getItemFromBlock(ModBlocks.multicolor_slab_2)) {
         list.add(new ItemStack(item, 1));
      }

   }

   public String func_150002_b(int metadata) {
      return super.getUnlocalizedName();
   }

   public void registerBlockIcons(IIconRegister register) {
      this.blockIcon = register.registerIcon("palamod:multicolor_block");

      for(int i = 0; i < this.icons.length; ++i) {
         this.icons[i] = register.registerIcon("palamod:multicolor_block_" + i);
      }

   }

   public IIcon getIcon(IBlockAccess world, int x, int y, int z, int side) {
      boolean a = false;
      boolean b = false;
      boolean c = false;
      boolean d = false;
      if (side != 0 && side != 1) {
         if (side == 2 || side == 3 || side == 4 || side == 5) {
            a = world.getBlock(x, y + 1, z) == ModBlocks.multicolor_block;
            c = world.getBlock(x, y - 1, z) == ModBlocks.multicolor_block;
         }
      } else {
         a = world.getBlock(x, y, z - 1) == ModBlocks.multicolor_block;
         c = world.getBlock(x, y, z + 1) == ModBlocks.multicolor_block;
      }

      if (side != 3 && side != 1 && side != 0) {
         if (side == 2) {
            b = world.getBlock(x - 1, y, z) == ModBlocks.multicolor_block;
            d = world.getBlock(x + 1, y, z) == ModBlocks.multicolor_block;
         } else if (side == 5) {
            b = world.getBlock(x, y, z - 1) == ModBlocks.multicolor_block;
            d = world.getBlock(x, y, z + 1) == ModBlocks.multicolor_block;
         } else if (side == 4) {
            b = world.getBlock(x, y, z + 1) == ModBlocks.multicolor_block;
            d = world.getBlock(x, y, z - 1) == ModBlocks.multicolor_block;
         }
      } else {
         b = world.getBlock(x + 1, y, z) == ModBlocks.multicolor_block;
         d = world.getBlock(x - 1, y, z) == ModBlocks.multicolor_block;
      }

      int result = 0;
      if (a) {
         result += 8;
      }

      if (b) {
         result += 4;
      }

      if (c) {
         result += 2;
      }

      if (d) {
         ++result;
      }

      return this.icons[result];
   }

   public IIcon getIcon(int side, int metadata) {
      return this.blockIcon;
   }

   public TileEntity createNewTileEntity(World world, int metadata) {
      return new TileEntityMulticolorBlock();
   }

   public boolean hasTileEntity(int metadata) {
      return true;
   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
      TileEntity tile = world.getTileEntity(x, y, z);
      if (tile instanceof TileEntityMulticolorBlock) {
         if (world.isRemote) {
            player.openGui(PalaMod.instance, 22, world, x, y, z);
         }

         return true;
      } else {
         return false;
      }
   }

   @SideOnly(Side.CLIENT)
   public int colorMultiplier(IBlockAccess access, int x, int y, int z) {
      TileEntity tile = access.getTileEntity(x, y, z);
      if (tile instanceof TileEntityMulticolorBlock) {
         TileEntityMulticolorBlock te = (TileEntityMulticolorBlock)tile;
         return te.getCurrentColor();
      } else {
         return 16777215;
      }
   }
}
