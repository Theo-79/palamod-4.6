package fr.paladium.palamod.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.tiles.TileEntityMulticolorBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockStairs;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class MulticolorStairs extends BlockStairs implements ITileEntityProvider {
   private IIcon[] icons = new IIcon[16];

   public MulticolorStairs(Block block, int metadata) {
      super(block, metadata);
   }

   public void registerBlockIcons(IIconRegister register) {
      this.blockIcon = register.registerIcon("palamod:multicolor_block");

      for(int i = 0; i < this.icons.length; ++i) {
         this.icons[i] = register.registerIcon("palamod:multicolor_block_" + i);
      }

   }

   public IIcon getIcon(IBlockAccess world, int x, int y, int z, int side) {
      boolean a = false;
      boolean b = false;
      boolean c = false;
      boolean d = false;
      if (side != 0 && side != 1) {
         if (side == 2 || side == 3 || side == 4 || side == 5) {
            a = world.getBlock(x, y + 1, z) == ModBlocks.multicolor_block;
            c = world.getBlock(x, y - 1, z) == ModBlocks.multicolor_block;
         }
      } else {
         a = world.getBlock(x, y, z - 1) == ModBlocks.multicolor_block;
         c = world.getBlock(x, y, z + 1) == ModBlocks.multicolor_block;
      }

      if (side != 3 && side != 1 && side != 0) {
         if (side == 2) {
            b = world.getBlock(x - 1, y, z) == ModBlocks.multicolor_block;
            d = world.getBlock(x + 1, y, z) == ModBlocks.multicolor_block;
         } else if (side == 5) {
            b = world.getBlock(x, y, z - 1) == ModBlocks.multicolor_block;
            d = world.getBlock(x, y, z + 1) == ModBlocks.multicolor_block;
         } else if (side == 4) {
            b = world.getBlock(x, y, z + 1) == ModBlocks.multicolor_block;
            d = world.getBlock(x, y, z - 1) == ModBlocks.multicolor_block;
         }
      } else {
         b = world.getBlock(x + 1, y, z) == ModBlocks.multicolor_block;
         d = world.getBlock(x - 1, y, z) == ModBlocks.multicolor_block;
      }

      int result = 0;
      if (a) {
         result += 8;
      }

      if (b) {
         result += 4;
      }

      if (c) {
         result += 2;
      }

      if (d) {
         ++result;
      }

      return this.icons[result];
   }

   public IIcon getIcon(int side, int metadata) {
      return this.blockIcon;
   }

   public TileEntity createNewTileEntity(World world, int metadata) {
      return new TileEntityMulticolorBlock();
   }

   public boolean hasTileEntity(int metadata) {
      return true;
   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
      TileEntity tile = world.getTileEntity(x, y, z);
      if (tile instanceof TileEntityMulticolorBlock) {
         if (world.isRemote) {
            player.openGui(PalaMod.instance, 22, world, x, y, z);
         }

         return true;
      } else {
         return false;
      }
   }

   @SideOnly(Side.CLIENT)
   public int colorMultiplier(IBlockAccess access, int x, int y, int z) {
      TileEntity tile = access.getTileEntity(x, y, z);
      if (tile instanceof TileEntityMulticolorBlock) {
         TileEntityMulticolorBlock te = (TileEntityMulticolorBlock)tile;
         return te.getCurrentColor();
      } else {
         return 16777215;
      }
   }
}
