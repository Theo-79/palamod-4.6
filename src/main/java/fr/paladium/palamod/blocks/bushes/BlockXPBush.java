package fr.paladium.palamod.blocks.bushes;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.blocks.ModBlocks;
import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.items.ModItems;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;

public class BlockXPBush extends Block implements IPlantable {
   public IIcon[] fastIcons;
   public IIcon[] fancyIcons;
   public String[] textureNames;
   public IIcon icon;
   public Item item;

   public BlockXPBush(Item item) {
      super(Material.leaves);
      this.setTickRandomly(true);
      this.setHardness(0.0F);
      this.setBlockName("xpbush");
      this.setStepSound(soundTypeGrass);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.disableStats();
      this.setTickRandomly(true);
      float minZ = 0.125F;
      float minX = 0.125F;
      float maxZ = 0.875F;
      float maxX = 0.875F;
      float maxY = 1.0F;
      this.setBlockBounds(minX, (float)this.minY, minZ, maxX, maxY, maxZ);
      this.item = item;
      this.textureNames = new String[2];
      this.textureNames[0] = "Bush1";
      this.textureNames[1] = "Bush2";
   }

   public int getLightOpacity() {
      return 0;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   public void onBlockClicked(World world, int x, int y, int z, EntityPlayer player) {
      if (!world.isRemote) {
         int meta = world.getBlockMetadata(x, y, z);
         if (meta > 1) {
            world.setBlock(x, y, z, this, 0, 3);
            world.spawnEntityInWorld(new EntityItem(world, (double)x, (double)y, (double)z, new ItemStack(this.item)));
         }
      }

   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {
      int meta = world.getBlockMetadata(x, y, z);
      if (meta >= 1) {
         if (world.isRemote) {
            return true;
         } else {
            world.setBlock(x, y, z, this, 0, 3);
            world.spawnEntityInWorld(new EntityItem(world, player.posX, player.posY, player.posZ, new ItemStack(ModItems.experienceBerry)));
            return true;
         }
      } else {
         return false;
      }
   }

   public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_) {
      return Item.getItemFromBlock(ModBlocks.experienceBush);
   }

   public void updateTick(World world, int x, int y, int z, Random random1) {
      if (!world.isRemote) {
         if (random1.nextInt(20) == 0) {
            int meta = world.getBlockMetadata(x, y, z);
            if (meta < 2) {
               world.setBlock(x, y, z, this, meta + 1, 3);
            }
         }

      }
   }

   public EnumPlantType getPlantType(IBlockAccess world, int x, int y, int z) {
      return EnumPlantType.Cave;
   }

   public Block getPlant(IBlockAccess world, int x, int y, int z) {
      return this;
   }

   public int getPlantMetadata(IBlockAccess world, int x, int y, int z) {
      return world.getBlockMetadata(x, y, z);
   }

   @SideOnly(Side.CLIENT)
   public void registerBlockIcons(IIconRegister icon) {
      this.fastIcons = new IIcon[2];
      this.fancyIcons = new IIcon[2];

      for(int i = 0; i < this.fastIcons.length; ++i) {
         if (this.textureNames[i] != "") {
            this.fancyIcons[i] = icon.registerIcon("palamod:" + this.textureNames[i] + "Fancy");
            this.fastIcons[i] = icon.registerIcon("palamod:" + this.textureNames[i] + "Fast");
            this.icon = icon.registerIcon("palamod:Bush2Fast");
         }
      }

   }

   @SideOnly(Side.CLIENT)
   public IIcon getIcon(int side, int metadata) {
      return metadata < 2 ? this.fancyIcons[metadata] : this.icon;
   }

   public boolean shouldSideBeRendered(IBlockAccess iblockaccess, int x, int y, int z, int meta) {
      return meta > 1 ? super.shouldSideBeRendered(iblockaccess, x, y, z, meta) : true;
   }

   public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
      if (entity instanceof EntityPlayer) {
         EntityPlayer player = (EntityPlayer)entity;
         player.attackEntityFrom(DamageSource.cactus, 0.5F);
      }

   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean canPlaceBlockAt(World world, int x, int y, int z) {
      return world.getBlock(x, y - 1, z).isOpaqueCube();
   }
}
