package fr.paladium.palamod.blocks.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockGrinderCasing extends Block {
   public BlockGrinderCasing() {
      super(Material.rock);
      this.setResistance(8.0F);
      this.setBlockTextureName("palamod:BlockPaladium");
      this.setHarvestLevel("pickaxe", 2);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setBlockName("grindercasing");
      this.setBlockTextureName("palamod:PaladiumCasing");
      this.setHardness(12.0F);
   }
}
