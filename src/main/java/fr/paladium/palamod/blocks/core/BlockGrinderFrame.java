package fr.paladium.palamod.blocks.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockGrinderFrame extends Block {
   public BlockGrinderFrame() {
      super(Material.rock);
      this.setResistance(8.0F);
      this.setHarvestLevel("pickaxe", 2);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setBlockName("grinderframe");
      this.setBlockTextureName("palamod:PaladiumFrame");
      this.setHardness(12.0F);
   }
}
