package fr.paladium.palamod.blocks.fluids;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;

public class BlockAngelicWater extends BlockFluidClassic {
   @SideOnly(Side.CLIENT)
   protected IIcon stillIcon;
   @SideOnly(Side.CLIENT)
   protected IIcon flowingIcon;
   int i = 0;

   public BlockAngelicWater(Fluid fluid, Material material) {
      super(fluid, material);
      this.setHardness(100.0F);
      this.setBlockName("angelicwater");
   }

   public IIcon getIcon(int side, int meta) {
      return side != 0 && side != 1 ? this.flowingIcon : this.stillIcon;
   }

   @SideOnly(Side.CLIENT)
   public void registerBlockIcons(IIconRegister register) {
      this.stillIcon = register.registerIcon("palamod:AngelicWaterStill");
      this.flowingIcon = register.registerIcon("palamod:AngelicWaterFlowing");
   }

   public boolean canDisplace(IBlockAccess world, int x, int y, int z) {
      return world.getBlock(x, y, z).getMaterial().isLiquid() && world.getBlock(x, y, z) instanceof BlockSlufuricWater ? true : super.canDisplace(world, x, y, z);
   }

   public boolean displaceIfPossible(World world, int x, int y, int z) {
      return world.getBlock(x, y, z).getMaterial().isLiquid() && world.getBlock(x, y, z) instanceof BlockSlufuricWater ? true : super.displaceIfPossible(world, x, y, z);
   }

   public void onEntityCollidedWithBlock(World wolrd, int x, int y, int z, Entity entity) {
      super.onEntityCollidedWithBlock(wolrd, x, y, z, entity);
      if (entity instanceof EntityPlayer) {
         EntityLivingBase livingBase = (EntityLivingBase)entity;
         ++this.i;
         if (livingBase.getHealth() < livingBase.getMaxHealth() && this.i >= 120) {
            this.i = 0;
            livingBase.heal(1.0F);
         }

      }
   }
}
