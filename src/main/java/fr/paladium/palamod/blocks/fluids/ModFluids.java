package fr.paladium.palamod.blocks.fluids;

import fr.paladium.palamod.blocks.ModBlocks;
import fr.paladium.palamod.common.EventHandler;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidContainerRegistry;

public class ModFluids {
   public static void init() {
      FluidContainerRegistry.registerFluidContainer(ModBlocks.sulfuricWaterFluid, new ItemStack(ModItems.bucketSulfuric), new ItemStack(Items.bucket));
      EventHandler.buckets.put(ModBlocks.sulfuricWater, ModItems.bucketSulfuric);
      FluidContainerRegistry.registerFluidContainer(ModBlocks.angelicWaterFluid, new ItemStack(ModItems.bucketAngelic), new ItemStack(Items.bucket));
      EventHandler.buckets.put(ModBlocks.angelicWater, ModItems.bucketAngelic);
   }
}
