package fr.paladium.palamod.blocks.ores;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockAmethyst extends Block {
   public BlockAmethyst() {
      super(Material.iron);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setStepSound(soundTypeStone);
      this.setHarvestLevel("pickaxe", 1);
      this.setBlockName("amethystblock");
      this.setBlockTextureName("palamod:AmethystBlock");
      this.setResistance(4.0F);
      this.setHardness(4.0F);
   }
}
