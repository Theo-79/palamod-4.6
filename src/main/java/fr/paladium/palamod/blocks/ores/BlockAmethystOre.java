package fr.paladium.palamod.blocks.ores;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockAmethystOre extends Block {
   public BlockAmethystOre() {
      super(Material.rock);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setStepSound(soundTypeStone);
      this.setHarvestLevel("pickaxe", 2);
      this.setBlockName("amethystore");
      this.setBlockTextureName("palamod:AmethystOre");
      this.setResistance(2.0F);
      this.setHardness(2.0F);
   }
}
