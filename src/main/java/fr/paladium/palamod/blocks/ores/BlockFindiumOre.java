package fr.paladium.palamod.blocks.ores;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.items.ModItems;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class BlockFindiumOre extends Block {
   public BlockFindiumOre() {
      super(Material.rock);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setStepSound(soundTypeStone);
      this.setHarvestLevel("pickaxe", 2);
      this.setBlockName("findiumore");
      this.setBlockTextureName("palamod:FindiumOre");
      this.setResistance(4.0F);
      this.setHardness(4.0F);
   }

   public Item getItemDropped(int amount, Random random, int fortune) {
      return ModItems.findium;
   }
}
