package fr.paladium.palamod.blocks.ores;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockPaladiumOre extends Block {
   public BlockPaladiumOre() {
      super(Material.rock);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setStepSound(soundTypeStone);
      this.setHarvestLevel("pickaxe", 2);
      this.setBlockName("paladiumore");
      this.setBlockTextureName("palamod:PaladiumOre");
      this.setResistance(4.0F);
      this.setHardness(4.0F);
   }
}
