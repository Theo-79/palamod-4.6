package fr.paladium.palamod.blocks.ores;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockTitane extends Block {
   public BlockTitane() {
      super(Material.iron);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setStepSound(soundTypeStone);
      this.setHarvestLevel("pickaxe", 1);
      this.setBlockName("titaneblock");
      this.setBlockTextureName("palamod:TitaneBlock");
      this.setResistance(4.0F);
      this.setHardness(4.0F);
   }
}
