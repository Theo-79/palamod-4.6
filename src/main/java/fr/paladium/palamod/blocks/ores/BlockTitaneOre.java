package fr.paladium.palamod.blocks.ores;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockTitaneOre extends Block {
   public BlockTitaneOre() {
      super(Material.rock);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setStepSound(soundTypeStone);
      this.setHarvestLevel("pickaxe", 2);
      this.setBlockName("titaneore");
      this.setBlockTextureName("palamod:TitaneOre");
      this.setResistance(3.0F);
      this.setHardness(3.0F);
   }
}
