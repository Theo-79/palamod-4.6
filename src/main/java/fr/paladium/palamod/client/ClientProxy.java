package fr.paladium.palamod.client;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import fr.paladium.palamod.blocks.ModBlocks;
import fr.paladium.palamod.client.commands.CommandHideBoss;
import fr.paladium.palamod.client.model.ModelGarag;
import fr.paladium.palamod.client.model.ModelTobalt;
import fr.paladium.palamod.client.overlay.OverlayBoss;
import fr.paladium.palamod.client.overlay.OverlayBow;
import fr.paladium.palamod.client.overlay.OverlayCamera;
import fr.paladium.palamod.client.overlay.OverlayGolem;
import fr.paladium.palamod.client.overlay.OverlayMessage;
import fr.paladium.palamod.client.overlay.OverlayMessageBoss;
import fr.paladium.palamod.client.overlay.OverlayUnclaimFinder;
import fr.paladium.palamod.client.render.block.RenderAlchemyCreator;
import fr.paladium.palamod.client.render.block.RenderBlockSpike;
import fr.paladium.palamod.client.render.block.RenderCompressor;
import fr.paladium.palamod.client.render.block.RenderMagicalAnvil;
import fr.paladium.palamod.client.render.block.RenderObsidianUpgrader;
import fr.paladium.palamod.client.render.block.RenderTurret;
import fr.paladium.palamod.client.render.entity.RenderCustomWither;
import fr.paladium.palamod.client.render.entity.RenderDynamite;
import fr.paladium.palamod.client.render.entity.RenderEntityPotion;
import fr.paladium.palamod.client.render.entity.RenderGarag;
import fr.paladium.palamod.client.render.entity.RenderGuardianGolem;
import fr.paladium.palamod.client.render.entity.RenderTobalt;
import fr.paladium.palamod.client.render.item.ItemBowRenderer;
import fr.paladium.palamod.client.render.item.ItemChestRender;
import fr.paladium.palamod.client.render.item.RenderItemAlchemyCreator;
import fr.paladium.palamod.client.render.item.RenderItemCamera;
import fr.paladium.palamod.client.render.item.RenderItemCompressor;
import fr.paladium.palamod.client.render.item.RenderItemMagicalAnvil;
import fr.paladium.palamod.client.render.item.RenderItemTurret;
import fr.paladium.palamod.client.render.tile.RenderCamera;
import fr.paladium.palamod.client.render.tile.TileEntityPaladiumChestRender;
import fr.paladium.palamod.common.CommonProxy;
import fr.paladium.palamod.entities.mobs.EntityCustomWither;
import fr.paladium.palamod.entities.mobs.EntityCustomWitherSkull;
import fr.paladium.palamod.entities.mobs.EntityGarag;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.entities.mobs.EntityTobalt;
import fr.paladium.palamod.entities.projectiles.EntityDynamite;
import fr.paladium.palamod.entities.projectiles.EntityDynamiteNinja;
import fr.paladium.palamod.entities.projectiles.EntityPotionGun;
import fr.paladium.palamod.entities.projectiles.EntitySplashPotion;
import fr.paladium.palamod.entities.projectiles.EntityTurretBullet;
import fr.paladium.palamod.items.ModItems;
import fr.paladium.palamod.tiles.TileEntityAlchemyCreator;
import fr.paladium.palamod.tiles.TileEntityArmorCompressor;
import fr.paladium.palamod.tiles.TileEntityCamera;
import fr.paladium.palamod.tiles.TileEntityMagicalAnvil;
import fr.paladium.palamod.tiles.TileEntityPaladiumChest;
import fr.paladium.palamod.tiles.TileEntityTurret;
import fr.paladium.palamod.util.ParticlesHandler;
import net.minecraft.client.renderer.entity.RenderSnowball;
import net.minecraft.client.renderer.tileentity.RenderWitherSkull;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.Entity;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;

public class ClientProxy extends CommonProxy {
   public static int renderBlockSpikeId;
   public static int renderObsidianUpgraderId;
   public static final KeyBinding BACKPACK_KEY = new KeyBinding("key.backpack.open", 37, "key.category.paladium");
   public static final KeyBinding NEXT_CAMERA_KEY = new KeyBinding("key.camera.next", 205, "key.category.paladium");

   public void registerItemRender() {
      MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.paladiumChest), new ItemChestRender());
      MinecraftForgeClient.registerItemRenderer(ModItems.paladiumBow, new ItemBowRenderer());
      MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.alchemyCreator), new RenderItemAlchemyCreator(new RenderAlchemyCreator(), new TileEntityAlchemyCreator()));
      MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.magicalAnvil), new RenderItemMagicalAnvil(new RenderMagicalAnvil(), new TileEntityMagicalAnvil()));
      MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.compressor), new RenderItemCompressor(new RenderCompressor(), new TileEntityArmorCompressor()));
      MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.camera), new RenderItemCamera(new RenderCamera(), new TileEntityCamera()));
      MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.turret), new RenderItemTurret());
   }

   public void registerBlockRender() {
      renderBlockSpikeId = RenderingRegistry.getNextAvailableRenderId();
      renderObsidianUpgraderId = RenderingRegistry.getNextAvailableRenderId();
      RenderingRegistry.registerBlockHandler(new RenderBlockSpike());
      RenderingRegistry.registerBlockHandler(new RenderObsidianUpgrader());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityPaladiumChest.class, new TileEntityPaladiumChestRender());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityAlchemyCreator.class, new RenderAlchemyCreator());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityMagicalAnvil.class, new RenderMagicalAnvil());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityArmorCompressor.class, new RenderCompressor());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCamera.class, new RenderCamera());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTurret.class, new RenderTurret());
   }

   public void registerEntityRender() {
      RenderingRegistry.registerEntityRenderingHandler(EntitySplashPotion.class, new RenderEntityPotion());
      RenderingRegistry.registerEntityRenderingHandler(EntityGuardianGolem.class, new RenderGuardianGolem());
      RenderingRegistry.registerEntityRenderingHandler(EntityPotionGun.class, new RenderSnowball(Items.potionitem, 16449));
      RenderingRegistry.registerEntityRenderingHandler(EntityGarag.class, new RenderGarag(new ModelGarag(), 0.8F));
      RenderingRegistry.registerEntityRenderingHandler(EntityTobalt.class, new RenderTobalt(new ModelTobalt(), 0.8F));
      RenderingRegistry.registerEntityRenderingHandler(EntityDynamite.class, new RenderDynamite(0));
      RenderingRegistry.registerEntityRenderingHandler(EntityDynamiteNinja.class, new RenderDynamite(1));
      RenderingRegistry.registerEntityRenderingHandler(EntityCustomWither.class, new RenderCustomWither());
      RenderingRegistry.registerEntityRenderingHandler(EntityCustomWitherSkull.class, new RenderWitherSkull());
      RenderingRegistry.registerEntityRenderingHandler(EntityTurretBullet.class, new RenderSnowball(Items.fire_charge));
   }

   public void registerOverlay() {
      MinecraftForge.EVENT_BUS.register(new OverlayBoss());
      MinecraftForge.EVENT_BUS.register(new OverlayUnclaimFinder());
      MinecraftForge.EVENT_BUS.register(new OverlayBow());
      MinecraftForge.EVENT_BUS.register(new OverlayMessage());
      MinecraftForge.EVENT_BUS.register(new OverlayMessageBoss());
      MinecraftForge.EVENT_BUS.register(new OverlayCamera());
      MinecraftForge.EVENT_BUS.register(new OverlayGolem());
   }

   public void generateCustomParticles(int type, Entity theEntity, int custom) {
      switch(type) {
      case 0:
         ParticlesHandler.generateCustomExplosion(theEntity, custom);
         return;
      default:
      }
   }

   public void registerClientCommand() {
      FMLCommonHandler.instance().bus().register(new TickHandlerClient());
      ClientCommandHandler.instance.registerCommand(new CommandHideBoss());
   }

   public void init(FMLInitializationEvent event) {
      super.init(event);
      ClientRegistry.registerKeyBinding(BACKPACK_KEY);
      ClientRegistry.registerKeyBinding(NEXT_CAMERA_KEY);
   }
}
