package fr.paladium.palamod.client;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import net.minecraft.entity.boss.IBossDisplayData;

@SideOnly(Side.CLIENT)
public class CustomBossStatus {
   public static float healthScale;
   public static int statusBarTime;
   public static String bossName;

   public static void setBossStatus(IBossDisplayData p_82824_0_, boolean p_82824_1_) {
      healthScale = p_82824_0_.getHealth() / p_82824_0_.getMaxHealth();
      statusBarTime = 10;
      if (p_82824_0_ instanceof EntityGuardianGolem) {
         bossName = ((EntityGuardianGolem)p_82824_0_).getGuardianName();
      } else {
         bossName = p_82824_0_.func_145748_c_().getFormattedText();
      }

      if (p_82824_0_.getHealth() == 0.0F || p_82824_0_.getHealth() == 0.0F) {
         healthScale = 1.0F;
         statusBarTime = 0;
         bossName = null;
      }

   }

   public static void update() {
      if (statusBarTime-- <= 0) {
         bossName = null;
      }

   }
}
