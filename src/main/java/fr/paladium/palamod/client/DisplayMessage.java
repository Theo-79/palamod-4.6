package fr.paladium.palamod.client;

public class DisplayMessage {
   public String title;
   public String subTitle;
   public int time;

   public DisplayMessage(String message, String subTitle, int time) {
      this.title = message;
      this.subTitle = subTitle;
      this.time = time;
   }
}
