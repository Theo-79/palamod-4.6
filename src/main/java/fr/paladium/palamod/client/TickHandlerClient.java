package fr.paladium.palamod.client;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import cpw.mods.fml.common.gameevent.TickEvent.RenderTickEvent;
import cpw.mods.fml.relauncher.ReflectionHelper;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.common.KiwiHandler;
import fr.paladium.palamod.items.armors.RepairableArmor;
import fr.paladium.palamod.util.LibObfuscation;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class TickHandlerClient {
   public static int cheatCheck = 0;
   public static float partialTicks = 0.0F;
   public static float ticksInGame = 0.0F;

   @SubscribeEvent
   public void onRenderTick(RenderTickEvent event) {
      if (event.phase == Phase.START) {
         ++cheatCheck;
         if (cheatCheck >= 600) {
            cheatCheck = 0;
            if (Minecraft.getMinecraft().theWorld != null && KiwiHandler.hasIllegalTexture()) {
               Minecraft.getMinecraft().shutdown();
            }
         }
      } else {
         this.render();
      }

      if (event.phase == Phase.END && (Minecraft.getMinecraft().currentScreen == null || !Minecraft.getMinecraft().currentScreen.doesGuiPauseGame())) {
         ++ticksInGame;
      }

   }

   public void render() {
      Minecraft mc = Minecraft.getMinecraft();
      GuiScreen gui = mc.currentScreen;
      if (gui != null && gui instanceof GuiContainer && mc.thePlayer != null && mc.thePlayer.inventory.getItemStack() == null) {
         GuiContainer container = (GuiContainer)gui;
         Slot slot = (Slot)ReflectionHelper.getPrivateValue(GuiContainer.class, container, LibObfuscation.THE_SLOT);
         if (slot != null && slot.getHasStack()) {
            ItemStack stack = slot.getStack();
            if (stack != null) {
               ScaledResolution res = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
               FontRenderer font = mc.fontRenderer;
               int mouseX = Mouse.getX() * res.getScaledWidth() / mc.displayWidth;
               int mouseY = res.getScaledHeight() - Mouse.getY() * res.getScaledHeight() / mc.displayHeight;

               Object tooltip;
               try {
                  tooltip = stack.getTooltip(mc.thePlayer, mc.gameSettings.advancedItemTooltips);
               } catch (Exception var18) {
                  tooltip = new ArrayList();
               }

               int width = 0;

               String s;
               for(Iterator var12 = ((List)tooltip).iterator(); var12.hasNext(); width = Math.max(width, font.getStringWidth(s) + 2)) {
                  s = (String)var12.next();
               }

               int tooltipHeight = (((List)tooltip).size() - 1) * 10 + 5;
               int height = 3;
               int offx = 11;
               int offy = 17;
               boolean offscreen = mouseX + width + 19 >= res.getScaledWidth();
               int fixY = res.getScaledHeight() - (mouseY + tooltipHeight);
               if (fixY < 0) {
                  offy -= fixY;
               }

               if (offscreen) {
                  int var21 = -13 - width;
               } else if (stack.getItem() instanceof RepairableArmor) {
                  drawBar(stack, (RepairableArmor)stack.getItem(), mouseX, mouseY, offx, offy, width, height);
               }
            }
         }
      }

   }

   private static void drawBar(ItemStack stack, RepairableArmor display, int mouseX, int mouseY, int offx, int offy, int width, int height) {
      float fraction = RepairableArmor.getFraction(stack);
      int manaBarWidth = (int)Math.ceil((double)((float)width * fraction));
      GL11.glDisable(2929);
      Gui.drawRect(mouseX + offx - 1, mouseY - offy - height - 1, mouseX + offx + width + 1, mouseY - offy, -16777216);
      Gui.drawRect(mouseX + offx, mouseY - offy - height, mouseX + offx + manaBarWidth, mouseY - offy, Color.HSBtoRGB(21.55F, 1.0F, 80.78F));
      Gui.drawRect(mouseX + offx + manaBarWidth, mouseY - offy - height, mouseX + offx + width, mouseY - offy, 268435455);
      Gui.drawRect(mouseX + offx + manaBarWidth, mouseY - offy - height, mouseX + offx + width, mouseY - offy, -11184811);
   }
}
