package fr.paladium.palamod.client.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;

public class CommandCamera extends CommandBase {
   public String getCommandName() {
      return "camera";
   }

   public String getCommandUsage(ICommandSender sender) {
      return "/camera [create/list/switch/remove/back]";
   }

   public boolean canCommandSenderUseCommand(ICommandSender sender) {
      return true;
   }

   public void processCommand(ICommandSender sender, String[] args) {
   }
}
