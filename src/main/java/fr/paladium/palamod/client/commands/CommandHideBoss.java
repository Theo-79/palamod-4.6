package fr.paladium.palamod.client.commands;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.List;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentTranslation;

@SideOnly(Side.CLIENT)
public class CommandHideBoss implements ICommand {
   public static boolean hide = false;

   public int compareTo(Object arg0) {
      return 0;
   }

   public String getCommandName() {
      return "hideboss";
   }

   public String getCommandUsage(ICommandSender sender) {
      return "/hideboss";
   }

   public List getCommandAliases() {
      return null;
   }

   public void processCommand(ICommandSender sender, String[] command) {
      if (hide) {
         hide = false;
         sender.addChatMessage(new ChatComponentTranslation("Annonce de boss affichï¿½e !", new Object[0]));
      } else {
         hide = true;
         sender.addChatMessage(new ChatComponentTranslation("Annonce de boss cachï¿½e !", new Object[0]));
      }

   }

   public boolean canCommandSenderUseCommand(ICommandSender sender) {
      return true;
   }

   public List addTabCompletionOptions(ICommandSender p_71516_1_, String[] p_71516_2_) {
      return null;
   }

   public boolean isUsernameIndex(String[] p_82358_1_, int p_82358_2_) {
      return false;
   }
}
