package fr.paladium.palamod.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class CustomGuiButton extends GuiButton {
   public ResourceLocation buttonTexture;

   public CustomGuiButton(int id, int width, int height, String displayString, String textureName) {
      super(id, width, height, displayString);
      this.buttonTexture = new ResourceLocation("palamod:textures/gui/" + textureName);
   }

   public void drawButton(Minecraft minecraft, int xCoord, int yCoord) {
      if (this.visible) {
         FontRenderer fontrenderer = minecraft.fontRenderer;
         minecraft.getTextureManager().bindTexture(this.buttonTexture);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         this.field_146123_n = xCoord >= this.xPosition && yCoord >= this.yPosition && xCoord < this.xPosition + this.width && yCoord < this.yPosition + this.height;
         int k = this.getHoverState(this.field_146123_n);
         GL11.glEnable(3042);
         OpenGlHelper.glBlendFunc(770, 771, 1, 0);
         GL11.glBlendFunc(770, 771);
         this.drawTexturedModalRect(this.xPosition, this.yPosition, 0, 46 + k * 20, this.width / 2, this.height);
         this.drawTexturedModalRect(this.xPosition + this.width / 2, this.yPosition, 200 - this.width / 2, 46 + k * 20, this.width / 2, this.height);
         this.mouseDragged(minecraft, xCoord, yCoord);
         int l = 14737632;
         if (this.packedFGColour != 0) {
            l = this.packedFGColour;
         } else if (!this.enabled) {
            l = 10526880;
         } else if (this.field_146123_n) {
            l = 16777120;
         }

         this.drawCenteredString(fontrenderer, this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, l);
      }

   }
}
