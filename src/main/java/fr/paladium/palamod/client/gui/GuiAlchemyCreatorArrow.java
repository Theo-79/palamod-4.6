package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.gui.ContainerAlchemyCreatorArrow;
import fr.paladium.palamod.network.packets.PacketOpenGui;
import fr.paladium.palamod.tiles.TileEntityAlchemyCreator;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiAlchemyCreatorArrow extends GuiContainer {
   public static ResourceLocation background = new ResourceLocation("palamod", "textures/gui/AlchemyCreator_Arrow.png");
   public static ResourceLocation widgets = new ResourceLocation("palamod", "textures/gui/AlchemyCreator_Widget.png");
   TileEntityAlchemyCreator tile;
   FontRenderer fr;

   public GuiAlchemyCreatorArrow(TileEntityAlchemyCreator tile, EntityPlayer player) {
      super(new ContainerAlchemyCreatorArrow(tile, player));
      this.tile = tile;
      this.ySize = 166;
      this.xSize = 200;
      this.fr = Minecraft.getMinecraft().fontRenderer;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      GL11.glColor4f(1.0F, 1.5F, 1.0F, 1.0F);
      this.mc.getTextureManager().bindTexture(background);
      this.drawTexturedModalRect(k, l, 0, 1, this.xSize, this.ySize);
      this.mc.getTextureManager().bindTexture(widgets);
      if (this.tile.isBurningArrow()) {
         this.mc.getTextureManager().bindTexture(widgets);
         double i = (double)this.tile.getCookProgressArrow() * 1.5D;
         this.drawTexturedModalRect(k + 96, l + 48, 0, 89, 10, (int)i);
         this.drawTexturedModalRect(k + 63, l + 48, 10, 89, 14, 27 - (int)i);
      } else {
         this.drawTexturedModalRect(k + 63, l + 48, 10, 89, 14, 29);
      }

      this.drawTab(k, l);
   }

   private void drawTab(int k, int l) {
      this.drawTexturedModalRect(k + 176, l + 5, 33, 0, 28, 28);
      this.drawTexturedModalRect(k + 173, l + 35, 0, 28, 30, 28);
   }

   protected void mouseClicked(int i, int j, int k) {
      super.mouseClicked(i, j, k);
      int u = (this.width - this.xSize) / 2;
      int v = (this.height - this.ySize) / 2;
      if (i > u + 175 && j > v + 5 && i < u + 205 && j < v + 33) {
         PacketOpenGui packet = new PacketOpenGui();
         packet.setInformations((byte)1);
         PalaMod var10000 = PalaMod.instance;
         PalaMod.proxy.packetPipeline.sendToServer(packet);
      }

   }
}
