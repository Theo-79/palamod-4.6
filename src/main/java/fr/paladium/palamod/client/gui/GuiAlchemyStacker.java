package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerAlchemyStacker;
import fr.paladium.palamod.tiles.TileEntityAlchemyStacker;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiAlchemyStacker extends GuiContainer {
   ResourceLocation background = new ResourceLocation("palamod", "textures/gui/AlchemyStacker.png");
   TileEntityAlchemyStacker tile;
   FontRenderer fr;

   public GuiAlchemyStacker(TileEntityAlchemyStacker tile, InventoryPlayer inventory) {
      super(new ContainerAlchemyStacker(tile, inventory));
      this.tile = tile;
      this.ySize = 166;
      this.xSize = 176;
      this.fr = Minecraft.getMinecraft().fontRenderer;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      GL11.glColor4f(1.0F, 1.5F, 1.0F, 1.0F);
      this.mc.getTextureManager().bindTexture(this.background);
      this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
      int time = this.tile.getCookProgressScaled(29);
      if (time > 0) {
         this.drawTexturedModalRect(k + 68, l + 38, 176, 0, time, 16);
      }

      this.fr.drawStringWithShadow("Alchemy Stacker (dÃ©sactivÃ©)", k + 5, l + 5, 16777215);
   }
}
