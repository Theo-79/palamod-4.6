package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerArmorCompressor;
import fr.paladium.palamod.tiles.TileEntityArmorCompressor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiArmorCompressor extends GuiContainer {
   public static ResourceLocation background = new ResourceLocation("palamod", "textures/gui/Compressor.png");
   TileEntityArmorCompressor tile;
   FontRenderer fr;

   public GuiArmorCompressor(TileEntityArmorCompressor tile, EntityPlayer player) {
      super(new ContainerArmorCompressor(tile, player));
      this.tile = tile;
      this.ySize = 231;
      this.xSize = 176;
      this.fr = Minecraft.getMinecraft().fontRenderer;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      GL11.glColor4f(1.0F, 1.5F, 1.0F, 1.0F);
      this.mc.getTextureManager().bindTexture(background);
      this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
      this.mc.getTextureManager().bindTexture(background);
      if (this.tile.isCompressing()) {
         this.mc.getTextureManager().bindTexture(background);
         double i = (double)(28 * this.tile.getCompressProgress() / 600);
         this.drawTexturedModalRect(this.guiLeft + 51, this.guiTop + 30, 180, 144, 28 - (int)i, 19);
         this.drawTexturedModalRect(this.guiLeft + 96, this.guiTop + 30, 211, 144, 28 - (int)i, 19);
      } else {
         this.drawTexturedModalRect(this.guiLeft + 51, this.guiTop + 30, 180, 144, 28, 19);
         this.drawTexturedModalRect(this.guiLeft + 96, this.guiTop + 30, 211, 144, 28, 19);
      }

   }
}
