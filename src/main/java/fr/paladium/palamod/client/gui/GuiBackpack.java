package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerBackpack;
import fr.paladium.palamod.common.inventory.InventoryBackpack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiBackpack extends GuiContainer {
   ResourceLocation background = new ResourceLocation("palamod", "textures/gui/Backpack.png");
   FontRenderer fr;

   public GuiBackpack(InventoryPlayer inventory, InventoryBackpack inventoryBackpack) {
      super(new ContainerBackpack(inventory, inventoryBackpack));
      this.xSize = 176;
      this.ySize = 221;
      this.fr = Minecraft.getMinecraft().fontRenderer;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      GL11.glColor4f(1.0F, 1.5F, 1.0F, 1.0F);
      this.mc.getTextureManager().bindTexture(this.background);
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
      this.fr.drawStringWithShadow("Backpack", k + 7, l + 5, 16777215);
   }
}
