package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerBowMachine;
import fr.paladium.palamod.tiles.TileEntityBowMachine;
import fr.paladium.palamod.util.BowHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiBowMachine extends GuiContainer {
   ResourceLocation background = new ResourceLocation("palamod", "textures/gui/BowMachine.png");
   TileEntityBowMachine tile;
   FontRenderer fr;

   public GuiBowMachine(TileEntityBowMachine tile, InventoryPlayer inventory) {
      super(new ContainerBowMachine(tile, inventory));
      this.tile = tile;
      this.ySize = 166;
      this.xSize = 176;
      this.fr = Minecraft.getMinecraft().fontRenderer;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      GL11.glColor4f(1.0F, 1.5F, 1.0F, 1.0F);
      this.mc.getTextureManager().bindTexture(this.background);
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
      if (this.tile.isBurning()) {
         int i1 = this.tile.getCookProgressScaled(48);
         this.drawTexturedModalRect(k + 101, l + 35, 176, 0, i1 + 1, 16);
      }

      this.fr.drawStringWithShadow("Bow Machine", k + 79, l + 7, -1);
      this.fr.drawStringWithShadow("§l§nModifiers", k + 10, l + 9, -1);
      ItemStack stack = this.tile.getStackInSlot(1);
      if (stack != null) {
         int[] modifiers = BowHelper.getModifiers(stack);
         if (modifiers != null) {
            if (modifiers.length == 1) {
               this.fr.drawStringWithShadow("§1 Modifier", k + 10, l + 21, -1);
            } else {
               this.fr.drawStringWithShadow("§c" + modifiers.length + " Modifiers", k + 10, l + 21, -1);
            }

            for(int i = 0; i < modifiers.length; ++i) {
               this.fr.drawStringWithShadow("- " + BowHelper.getModifierName(modifiers[i]), k + 10, l + 32 + i * 11, -1);
            }
         } else {
            this.fr.drawStringWithShadow("§0 Modifiers", k + 10, l + 21, -1);
         }
      } else {
         this.fr.drawStringWithShadow("§ No bow", k + 10, l + 21, -1);
      }

   }
}
