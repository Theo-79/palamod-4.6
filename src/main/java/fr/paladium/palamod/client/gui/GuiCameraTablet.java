package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.entities.mobs.EntityCamera;
import fr.paladium.palamod.network.packets.PacketCamera;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiCameraTablet extends GuiScreen {
   public static final ResourceLocation texture = new ResourceLocation("palamod", "textures/gui/camera.png");
   private EntityPlayer player;
   private ItemStack stack;
   private GuiButton delete1;
   private GuiButton delete2;
   private GuiButton delete3;
   private int xSize;
   private int ySize;

   public GuiCameraTablet(EntityPlayer myPlayer) {
      this.player = myPlayer;
      this.stack = this.player.getHeldItem();
      this.xSize = 150;
      this.ySize = 100;
      if (!this.stack.hasTagCompound()) {
         this.stack.setTagCompound(new NBTTagCompound());
         this.stack.getTagCompound().setBoolean("cam1", false);
         this.stack.getTagCompound().setBoolean("cam2", false);
         this.stack.getTagCompound().setBoolean("cam3", false);
      }

   }

   public void initGui() {
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      int posX = k + this.xSize - 10;
      int tmp = this.getButtonSize(this.getCoordonates(1));
      this.buttonList.add(this.delete1 = new GuiButton(0, posX - tmp, l + 10, tmp, 20, "Delete"));
      this.delete1.enabled = !this.getCoordonates(1).equals("Undefined");
      tmp = this.getButtonSize(this.getCoordonates(2));
      this.buttonList.add(this.delete2 = new GuiButton(1, posX - tmp, l + 40, tmp, 20, "Delete"));
      this.delete2.enabled = !this.getCoordonates(2).equals("Undefined");
      tmp = this.getButtonSize(this.getCoordonates(3));
      this.buttonList.add(this.delete3 = new GuiButton(2, posX - tmp, l + 70, tmp, 20, "Delete"));
      this.delete3.enabled = !this.getCoordonates(3).equals("Undefined");
   }

   private int getButtonSize(String text) {
      return 125 - this.fontRendererObj.getStringWidth(text);
   }

   private String getCoordonates(int id) {
      NBTTagCompound compound = this.stack.getTagCompound();
      if (id == 1 && compound.getBoolean("cam1")) {
         return compound.getInteger("cam1X") + " â€¢ " + compound.getInteger("cam1Y") + " â€¢ " + compound.getInteger("cam1Z");
      } else if (id == 2 && compound.getBoolean("cam2")) {
         return compound.getInteger("cam2X") + " â€¢ " + compound.getInteger("cam2Y") + " â€¢ " + compound.getInteger("cam2Z");
      } else {
         return id == 3 && compound.getBoolean("cam3") ? compound.getInteger("cam3X") + " â€¢ " + compound.getInteger("cam3Y") + " â€¢ " + compound.getInteger("cam3Z") : "Undefined";
      }
   }

   public void drawDefaultBackground() {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      this.mc.getTextureManager().bindTexture(texture);
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
   }

   public void drawScreen(int par1, int par2, float par3) {
      this.drawDefaultBackground();
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      int end = k + this.xSize - 5;
      this.fontRendererObj.drawString(this.getCoordonates(1), k + 10, l + 15, 65280);
      this.fontRendererObj.drawString(this.getCoordonates(2), k + 10, l + 45, 65535);
      this.fontRendererObj.drawString(this.getCoordonates(3), k + 10, l + 75, 16711935);
      super.drawScreen(par1, par2, par3);
   }

   protected void actionPerformed(GuiButton button) {
      if (button == this.delete1) {
         this.stack.getTagCompound().setBoolean("cam1", false);
         this.delete1.enabled = false;
      } else if (button == this.delete2) {
         this.stack.getTagCompound().setBoolean("cam2", false);
         this.delete2.enabled = false;
      } else if (button == this.delete3) {
         this.stack.getTagCompound().setBoolean("cam3", false);
         this.delete3.enabled = false;
      }

   }

   public void onGuiClosed() {
      EntityCamera.destroyCamera();
      PacketCamera packet = new PacketCamera();
      packet.addInformations(this.stack.getTagCompound());
      PalaMod var10000 = PalaMod.instance;
      PalaMod.proxy.packetPipeline.sendToServer(packet);
      super.onGuiClosed();
   }

   public boolean doesGuiPauseGame() {
      return false;
   }
}
