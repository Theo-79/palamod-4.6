package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerChestExplorer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiChestExplorer extends GuiContainer {
   TileEntity tile;
   ResourceLocation bg = new ResourceLocation("palamod:textures/gui/ChestExplorer.png");

   public GuiChestExplorer(TileEntity te) {
      super(new ContainerChestExplorer(te));
      this.xSize = 256;
      this.ySize = 177;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      this.mc.renderEngine.bindTexture(this.bg);
      this.drawDefaultBackground();
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      int x = (this.width - this.xSize) / 2;
      int y = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(x, y, 0, 0, 256, 256);
   }
}
