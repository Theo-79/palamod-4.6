package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerEmpty;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;

public class GuiFAnnounce extends GuiContainer {
   FontRenderer fr;

   public GuiFAnnounce() {
      super(new ContainerEmpty());
      this.ySize = 166;
      this.xSize = 200;
      this.fr = Minecraft.getMinecraft().fontRenderer;
   }

   public void initGui() {
      int k = (this.width - 50) / 2;
      int l = (this.height - 100) / 2;
      this.buttonList.add(new GuiButton(1, k, l + 50, 50, 20, "OK"));
      super.initGui();
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      int k1 = (this.width - this.fr.getStringWidth("Suite a une decision le Alpha va fermer le 28/09/2016")) / 2;
      int l1 = (this.height - this.ySize) / 2 + 10;
      int k2 = (this.width - this.fr.getStringWidth("Nous vous laissons jusqu'au 28/09/2016")) / 2;
      int k3 = (this.width - this.fr.getStringWidth("pour demenager pour le remboursement des AP")) / 2;
      int k4 = (this.width - this.fr.getStringWidth("Merci de voir avec un SM sur ts")) / 2;
      this.fr.drawStringWithShadow("Suite a une decision le Alpha va fermer le 28/09/2016", k1, l1, 16777215);
      this.fr.drawStringWithShadow("Nous vous laissons jusqu'au 28/09/2016", k2 + 5, l1 + 10, 16777215);
      this.fr.drawStringWithShadow("pour demenager pour le remboursement des AP", k3 + 5, l1 + 20, 16777215);
      this.fr.drawStringWithShadow("Merci de voir avec un SM sur ts", k4 + 5, l1 + 30, 16777215);
   }

   protected void actionPerformed(GuiButton button) {
      if (button.id == 1) {
         this.mc.thePlayer.closeScreen();
      }

   }
}
