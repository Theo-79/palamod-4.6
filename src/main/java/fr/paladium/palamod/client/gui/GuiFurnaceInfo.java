package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerEmpty;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;

public class GuiFurnaceInfo extends GuiContainer {
   FontRenderer fr;

   public GuiFurnaceInfo() {
      super(new ContainerEmpty());
      this.ySize = 166;
      this.xSize = 200;
      this.fr = Minecraft.getMinecraft().fontRenderer;
   }

   public void initGui() {
      int k = (this.width - 50) / 2;
      int l = (this.height - 100) / 2;
      this.buttonList.add(new GuiButton(1, k, l + 50, 50, 20, "OK"));
      super.initGui();
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      int k1 = (this.width - this.fr.getStringWidth("Les fours sont actuellement desactives !")) / 2;
      int l1 = (this.height - this.ySize) / 2 + 50;
      int k2 = (this.width - this.fr.getStringWidth("Si vous voulez cuire des choses, utilisez la commande /furnace")) / 2;
      int k3 = (this.width - this.fr.getStringWidth("qui cuit le contenu de votre main !")) / 2;
      this.fr.drawStringWithShadow("Les fours sont actuellement desactives !", k1, l1, 16777215);
      this.fr.drawStringWithShadow("Si vous voulez cuire des choses, utilisez la commande /furnace", k2 + 5, l1 + 10, 16777215);
      this.fr.drawStringWithShadow("qui cuit le contenu de votre main !", k3 + 5, l1 + 20, 16777215);
   }

   protected void actionPerformed(GuiButton button) {
      if (button.id == 1) {
         this.mc.thePlayer.closeScreen();
      }

   }
}
