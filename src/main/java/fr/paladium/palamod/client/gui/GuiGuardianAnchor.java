package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.gui.ContainerEmpty;
import fr.paladium.palamod.network.packets.PacketGuardianRadius;
import fr.paladium.palamod.tiles.TileEntityGuardianAnchor;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class GuiGuardianAnchor extends GuiContainer {
   GuiTextField radiusInput;
   TileEntityGuardianAnchor te;
   final ResourceLocation background = new ResourceLocation("palamod:textures/gui/GuardianRadius.png");

   public GuiGuardianAnchor(TileEntityGuardianAnchor te) {
      super(new ContainerEmpty());
      this.te = te;
      this.xSize = 161;
      this.ySize = 70;
   }

   public void initGui() {
      super.initGui();
      int x = (this.width - this.xSize) / 2;
      int y = (this.height - this.ySize) / 2;
      this.radiusInput = new GuiTextField(this.fontRendererObj, x + 5, y + 20, 150, 15);
      this.radiusInput.setFocused(false);
      this.radiusInput.setCanLoseFocus(true);
      this.radiusInput.setText("" + this.te.getRadius());
   }

   public void onGuiClosed() {
      Keyboard.enableRepeatEvents(false);
      this.radiusInput.setFocused(false);
   }

   protected void mouseClicked(int par1, int par2, int par3) {
      super.mouseClicked(par1, par2, par3);
      this.radiusInput.mouseClicked(par1, par2, par3);
   }

   protected void keyTyped(char par1, int pressedKey) {
      if (pressedKey == 1 || !this.radiusInput.isFocused() && pressedKey == this.mc.gameSettings.keyBindInventory.getKeyCode()) {
         int radius;
         try {
            radius = Integer.parseInt(this.radiusInput.getText());
         } catch (NumberFormatException var5) {
            return;
         }

         PacketGuardianRadius packet = new PacketGuardianRadius();
         packet.addInformations(radius, this.te);
         PalaMod var10000 = PalaMod.instance;
         PalaMod.proxy.packetPipeline.sendToServer(packet);
         this.mc.thePlayer.closeScreen();
      }

      this.radiusInput.textboxKeyTyped(par1, pressedKey);
   }

   protected void drawGuiContainerBackgroundLayer(float par1, int x, int y) {
      this.mc.renderEngine.bindTexture(this.background);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
      this.radiusInput.drawTextBox();
      this.fontRendererObj.drawStringWithShadow("Detecting: ", k + (this.xSize / 2 - this.fontRendererObj.getStringWidth("Detecting: ") / 2), l + 44, 16777215);
   }

   protected void drawGuiContainerForegroundLayer(int x, int y) {
      String title = "Guardian Anchor";
      this.fontRendererObj.drawString(title, (this.xSize - this.fontRendererObj.getStringWidth(title)) / 2, 5, 4210752);
   }

   public void updateScreen() {
      super.updateScreen();
      this.radiusInput.updateCursorCounter();
   }
}
