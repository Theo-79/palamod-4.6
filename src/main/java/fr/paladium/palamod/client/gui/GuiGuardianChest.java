package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.client.gui.button.ButtonGuardianChest;
import fr.paladium.palamod.common.gui.ContainerGuardianChest;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.network.packets.PacketOpenGui;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiGuardianChest extends GuiContainer {
   private EntityGuardianGolem golem;
   private final ResourceLocation background = new ResourceLocation("palamod:textures/gui/GuardianChest.png");
   private final ResourceLocation disabledSlots = new ResourceLocation("palamod:textures/gui/SlotsGui.png");
   private FontRenderer fr;
   public float xSizeFloat;
   public float ySizeFloat;

   public GuiGuardianChest(EntityGuardianGolem entity, InventoryPlayer inventory) {
      super(new ContainerGuardianChest(entity, inventory));
      this.fr = Minecraft.getMinecraft().fontRenderer;
      this.golem = entity;
      this.xSize = 238;
      this.ySize = 181;
   }

   public void initGui() {
      super.initGui();
      int x = (this.width - this.xSize) / 2;
      int y = (this.height - this.ySize) / 2;
      this.buttonList.add(new ButtonGuardianChest(GuiGuardianGolem.CHEST, x + 210, y + 157, "", this.golem));
   }

   public void drawScreen(int p_73863_1_, int p_73863_2_, float p_73863_3_) {
      super.drawScreen(p_73863_1_, p_73863_2_, p_73863_3_);
      this.xSizeFloat = (float)p_73863_1_;
      this.ySizeFloat = (float)p_73863_2_;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      this.mc.renderEngine.bindTexture(this.background);
      this.drawDefaultBackground();
      GL11.glPushMatrix();
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      int x = (this.width - this.xSize) / 2;
      int y = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
      float scaledXp = 240.0F * ((float)this.golem.getSubLevel() / this.golem.getRequiredXP());
      this.drawTexturedModalRect(x + 9, y + 20, 0, 227, (int)scaledXp, 11);
      this.fr.drawStringWithShadow(this.golem.getGuardianName(), x + (this.xSize / 2 - this.fr.getStringWidth(this.golem.getGuardianName()) / 2), y + 7, 182844);
      this.fr.drawString("Niveau " + this.golem.getLevel(), x + (this.xSize / 2 - this.fr.getStringWidth("Niveau " + this.golem.getLevel()) / 2), y + 21, 16777215, true);
      GL11.glPopMatrix();
      this.mc.renderEngine.bindTexture(this.disabledSlots);

      for(int i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            if (i * 9 + j > this.golem.getMaxChestSlots()) {
               this.drawTexturedModalRect(x + j * 18 + 38, y + i * 18 + 38, 0, 0, 18, 18);
            }
         }
      }

   }

   protected void actionPerformed(GuiButton button) {
      super.actionPerformed(button);
      if (button.id == GuiGuardianGolem.CHEST) {
         PacketOpenGui packet = new PacketOpenGui();
         packet.setInformations((byte)7, this.golem.getEntityId(), 0, 0);
         PalaMod var10000 = PalaMod.instance;
         PalaMod.proxy.packetPipeline.sendToServer(packet);
      }

   }
}
