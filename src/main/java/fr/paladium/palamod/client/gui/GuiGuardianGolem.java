package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.client.gui.button.ButtonGuardianChest;
import fr.paladium.palamod.client.gui.button.ButtonGuardianUpgrade;
import fr.paladium.palamod.common.gui.ContainerGuardianGolem;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.network.packets.PacketOpenGui;
import fr.paladium.palamod.util.DisplayHelper;
import fr.paladium.palamod.util.GuardianHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiGuardianGolem extends GuiContainer {
   static int CHEST = 0;
   static int UPGRADE = 1;
   private EntityGuardianGolem golem;
   private final ResourceLocation background = new ResourceLocation("palamod:textures/gui/GuardianGolem.png");
   private FontRenderer fr;
   public float xSizeFloat;
   public float ySizeFloat;

   public GuiGuardianGolem(EntityGuardianGolem entity, InventoryPlayer inventory) {
      super(new ContainerGuardianGolem(entity, inventory));
      this.fr = Minecraft.getMinecraft().fontRenderer;
      this.golem = entity;
      this.xSize = 238;
      this.ySize = 227;
   }

   public void initGui() {
      super.initGui();
      int x = (this.width - this.xSize) / 2;
      int y = (this.height - this.ySize) / 2;
      this.buttonList.add(new ButtonGuardianChest(CHEST, x + 210, y + 202, "", this.golem));
      this.buttonList.add(new ButtonGuardianUpgrade(UPGRADE, x + 140, y + 53, "", this.golem));
   }

   public void drawScreen(int p_73863_1_, int p_73863_2_, float p_73863_3_) {
      super.drawScreen(p_73863_1_, p_73863_2_, p_73863_3_);
      this.xSizeFloat = (float)p_73863_1_;
      this.ySizeFloat = (float)p_73863_2_;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      this.mc.renderEngine.bindTexture(this.background);
      this.drawDefaultBackground();
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      int x = (this.width - this.xSize) / 2;
      int y = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
      float scaledXp = 240.0F * ((float)this.golem.getSubLevel() / this.golem.getRequiredXP());
      this.drawTexturedModalRect(x + 9, y + 20, 0, 227, (int)scaledXp, 11);
      DisplayHelper.renderEntity(x + 42, y + 130, 30, (float)(x + 51) - this.xSizeFloat, (float)(y + 75 - 50) - this.ySizeFloat, this.golem, false);
      this.fr.drawStringWithShadow(this.golem.getGuardianName(), x + (this.xSize / 2 - this.fr.getStringWidth(this.golem.getGuardianName()) / 2), y + 7, 182844);
      this.fr.drawStringWithShadow("Ameliorations", x + 83, y + 40, 16777215);
      this.fr.drawStringWithShadow("Ameliorations Cosmetiques", x + 83, y + 76, 16777215);
      this.fr.drawStringWithShadow("Whitelist", x + 177, y + 40, 16777215);
      this.fr.drawString("Niveau " + this.golem.getLevel(), x + (this.xSize / 2 - this.fr.getStringWidth("Niveau " + this.golem.getLevel()) / 2), y + 21, 16777215, true);
      this.fr.drawString(this.golem.getSubLevel() + "/" + (int)this.golem.getRequiredXP(), x + 11, y + 38, 16777215, true);
      this.fr.drawString("" + this.golem.getSpeed(), x + 103, y + 120, 16777215, true);
      this.fr.drawString("" + this.golem.getDamages(), x + 151, y + 120, 16777215, true);
      this.fr.drawString("" + this.golem.getLife(), x + 195, y + 120, 16777215, true);
   }

   protected void actionPerformed(GuiButton button) {
      super.actionPerformed(button);
      PalaMod var10000;
      PacketOpenGui packet;
      if (GuardianHelper.hasUpgrade(this.golem, 5) && button.id == CHEST) {
         packet = new PacketOpenGui();
         packet.setInformations((byte)24, this.golem.getEntityId(), 0, 0);
         var10000 = PalaMod.instance;
         PalaMod.proxy.packetPipeline.sendToServer(packet);
      }

      if (GuardianHelper.hasUpgrade(this.golem, 6) && button.id == UPGRADE) {
         packet = new PacketOpenGui();
         packet.setInformations((byte)25, this.golem.getEntityId(), 0, 0);
         var10000 = PalaMod.instance;
         PalaMod.proxy.packetPipeline.sendToServer(packet);
      }

   }
}
