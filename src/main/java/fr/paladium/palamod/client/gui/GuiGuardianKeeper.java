package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerGuardianKeeper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiGuardianKeeper extends GuiContainer {
   ResourceLocation background = new ResourceLocation("palamod", "textures/gui/GuardianKeeper.png");
   FontRenderer fr;

   public GuiGuardianKeeper(EntityPlayer player) {
      super(new ContainerGuardianKeeper(player));
      this.xSize = 176;
      this.ySize = 122;
      this.fr = Minecraft.getMinecraft().fontRenderer;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      GL11.glColor4f(1.0F, 1.5F, 1.0F, 1.0F);
      this.mc.getTextureManager().bindTexture(this.background);
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
      this.fr.drawStringWithShadow("Guardian Keeper", k + 5, l + 5, 16777215);
   }
}
