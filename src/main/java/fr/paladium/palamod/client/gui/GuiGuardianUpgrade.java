package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerGuardianUpgrade;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiGuardianUpgrade extends GuiContainer {
   private EntityGuardianGolem golem;
   private final ResourceLocation background = new ResourceLocation("palamod:textures/gui/GuardianUpgrade.png");
   private FontRenderer fr;
   public float xSizeFloat;
   public float ySizeFloat;

   public GuiGuardianUpgrade(EntityGuardianGolem entity, InventoryPlayer inventory) {
      super(new ContainerGuardianUpgrade(entity, inventory));
      this.fr = Minecraft.getMinecraft().fontRenderer;
      this.golem = entity;
      this.xSize = 238;
      this.ySize = 153;
   }

   public void initGui() {
      super.initGui();
      int x = (this.width - this.xSize) / 2;
      int y = (this.height - this.ySize) / 2;
   }

   public void drawScreen(int p_73863_1_, int p_73863_2_, float p_73863_3_) {
      super.drawScreen(p_73863_1_, p_73863_2_, p_73863_3_);
      this.xSizeFloat = (float)p_73863_1_;
      this.ySizeFloat = (float)p_73863_2_;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      this.mc.renderEngine.bindTexture(this.background);
      this.drawDefaultBackground();
      GL11.glPushMatrix();
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      int x = (this.width - this.xSize) / 2;
      int y = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
      float scaledXp = 240.0F * ((float)this.golem.getSubLevel() / this.golem.getRequiredXP());
      this.drawTexturedModalRect(x + 9, y + 20, 0, 227, (int)scaledXp, 11);
      this.fr.drawStringWithShadow(this.golem.getGuardianName(), x + (this.xSize / 2 - this.fr.getStringWidth(this.golem.getGuardianName()) / 2), y + 7, 182844);
      this.fr.drawString("Niveau " + this.golem.getLevel(), x + (this.xSize / 2 - this.fr.getStringWidth("Niveau " + this.golem.getLevel()) / 2), y + 21, 16777215, true);
      this.fr.drawString("AmÃ©liorations", x + (this.xSize / 2 - this.fr.getStringWidth("AmÃ©liorations") / 2), y + 37, 16777215, true);
      GL11.glPopMatrix();
   }

   protected void actionPerformed(GuiButton button) {
      super.actionPerformed(button);
   }
}
