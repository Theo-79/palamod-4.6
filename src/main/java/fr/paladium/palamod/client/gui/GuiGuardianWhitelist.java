package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.gui.ContainerGuardianWhitelist;
import fr.paladium.palamod.network.packets.PacketGuardianWhitelist;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class GuiGuardianWhitelist extends GuiContainer {
   GuiTextField usernameInput;
   FontRenderer fontRendererObj;
   ItemStack stack;
   List nameList;
   final ResourceLocation background = new ResourceLocation("palamod:textures/gui/GuardianWhitelist.png");

   public GuiGuardianWhitelist(ItemStack stack, InventoryPlayer inventory) {
      super(new ContainerGuardianWhitelist(inventory));
      this.stack = stack;
      this.fontRendererObj = Minecraft.getMinecraft().fontRenderer;
      this.xSize = 176;
      this.ySize = 147;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      this.mc.renderEngine.bindTexture(this.background);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      int x = (this.width - this.xSize) / 2;
      int y = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
      this.usernameInput.drawTextBox();
   }

   public void initGui() {
      super.initGui();
      int x = (this.width - this.xSize) / 2;
      int y = (this.height - this.ySize) / 2;
      this.buttonList.add(new GuiButton(0, x + 4, y + 37, 50, 20, "Add"));
      this.buttonList.add(new GuiButton(1, x + 59, y + 37, 50, 20, "Remove"));
      this.usernameInput = new GuiTextField(this.fontRendererObj, x + 5, y + 20, 165, 15);
      this.usernameInput.setFocused(false);
      this.usernameInput.setCanLoseFocus(true);
      this.usernameInput.setText("");
   }

   public void onGuiClosed() {
      Keyboard.enableRepeatEvents(false);
   }

   protected void mouseClicked(int par1, int par2, int par3) {
      super.mouseClicked(par1, par2, par3);
      this.usernameInput.mouseClicked(par1, par2, par3);
   }

   protected void keyTyped(char par1, int pressedKey) {
      if (pressedKey == 1 || !this.usernameInput.isFocused() && pressedKey == this.mc.gameSettings.keyBindInventory.getKeyCode()) {
         this.mc.thePlayer.closeScreen();
      }

      this.usernameInput.textboxKeyTyped(par1, pressedKey);
   }

   protected void drawGuiContainerForegroundLayer(int param1, int param2) {
      this.fontRendererObj.drawString("Guardian Whitelist", 5, 8, 4210752);
   }

   public void updateScreen() {
      super.updateScreen();
      this.usernameInput.updateCursorCounter();
   }

   protected void actionPerformed(GuiButton button) {
      PalaMod var10000;
      switch(button.id) {
      case 0:
         PacketGuardianWhitelist packet = new PacketGuardianWhitelist();
         packet.addInformations(this.usernameInput.getText(), true);
         var10000 = PalaMod.instance;
         PalaMod.proxy.packetPipeline.sendToServer(packet);
         this.mc.thePlayer.closeScreen();
         return;
      case 1:
         PacketGuardianWhitelist packet1 = new PacketGuardianWhitelist();
         packet1.addInformations(this.usernameInput.getText(), false);
         var10000 = PalaMod.instance;
         PalaMod.proxy.packetPipeline.sendToServer(packet1);
         this.mc.thePlayer.closeScreen();
         return;
      default:
      }
   }
}
