package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerMagicalAnvilAdd;
import fr.paladium.palamod.tiles.TileEntityMagicalAnvil;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiMagicalAnvilAdd extends GuiContainer {
   private EntityPlayer player;
   private TileEntityMagicalAnvil tile;
   private ContainerMagicalAnvilAdd container;

   public GuiMagicalAnvilAdd(TileEntityMagicalAnvil tileEntityMagicalAnvil, EntityPlayer player) {
      super(new ContainerMagicalAnvilAdd(tileEntityMagicalAnvil, player));
      this.tile = tileEntityMagicalAnvil;
      this.container = (ContainerMagicalAnvilAdd)this.inventorySlots;
      this.player = player;
      this.xSize = 176;
      this.ySize = 231;
   }

   protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
      this.mc.getTextureManager().bindTexture(new ResourceLocation("palamod:textures/gui/MagicalAnvil_ADD.png"));
      this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
   }

   protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
      String s = "Magical Anvil";
      String great = "Cout du runnage : ";
      String bad = "Vous n'avez pas assez d'experience";
      this.fontRendererObj.drawString(s, 88 - this.fontRendererObj.getStringWidth(s) / 2, 2, 11665408);
      if (this.container.getinv().getStackInSlot(5) != null) {
         if (this.player.experienceLevel > this.container.maximumCost || this.player.capabilities.isCreativeMode) {
            this.fontRendererObj.drawString(great + this.container.maximumCost + " Levels", 68 - this.fontRendererObj.getStringWidth(great) / 2, 128, 5700423);
         }

         if (this.player.experienceLevel < this.container.maximumCost && !this.player.capabilities.isCreativeMode) {
            this.fontRendererObj.drawString(bad, 95 - this.fontRendererObj.getStringWidth(bad) / 2, 128, 14487054);
         }
      }

   }
}
