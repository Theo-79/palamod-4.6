package fr.paladium.palamod.client.gui;

import cpw.mods.fml.client.config.GuiSlider;
import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.network.packets.PacketMulticolorBlock;
import fr.paladium.palamod.tiles.TileEntityMulticolorBlock;
import java.awt.Color;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiMulticolorBlock extends GuiScreen {
   public static final ResourceLocation texture = new ResourceLocation("palamod", "textures/gui/container/multicolorBlock.png");
   private TileEntityMulticolorBlock tileMachine;
   private GuiButton ok;
   private GuiButton cancel;
   private GuiButton change1;
   private GuiButton change2;
   private GuiButton change3;
   private GuiButton change4;
   private GuiButton desactivate2;
   private GuiButton desactivate3;
   private GuiButton desactivate4;
   private GuiSlider red;
   private GuiSlider green;
   private GuiSlider blue;
   private GuiSlider time;
   private boolean activate2;
   private boolean activate3;
   private boolean activate4;
   private int xSize;
   private int ySize;
   private int color;
   private int color1;
   private int color2;
   private int color3;
   private int color4;
   private int timer;

   public GuiMulticolorBlock(TileEntityMulticolorBlock tile) {
      this.tileMachine = tile;
      this.allowUserInput = false;
      this.ySize = 226;
      this.xSize = 176;
      this.color = 0;
      this.color1 = this.tileMachine.getColor1();
      this.color2 = this.tileMachine.getColor2();
      this.color3 = this.tileMachine.getColor3();
      this.color4 = this.tileMachine.getColor4();
      this.timer = this.tileMachine.getTime();
      this.activate2 = this.tileMachine.isActivate2();
      this.activate3 = this.tileMachine.isActivate3();
      this.activate4 = this.tileMachine.isActivate4();
   }

   public void initGui() {
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      int posx = k + (this.xSize - 165) / 2;
      this.buttonList.add(this.red = new GuiSlider(9, posx, l + 20, 165, 20, "Red : ", "", 0.0D, 255.0D, (double)(new Color(this.color1)).getRed(), false, true));
      this.buttonList.add(this.green = new GuiSlider(10, posx, l + 50, 165, 20, "Green : ", "", 0.0D, 255.0D, (double)(new Color(this.color1)).getGreen(), false, true));
      this.buttonList.add(this.blue = new GuiSlider(11, posx, l + 80, 165, 20, "Blue : ", "", 0.0D, 255.0D, (double)(new Color(this.color1)).getBlue(), false, true));
      posx = k + (this.xSize - 150) / 2;
      this.buttonList.add(this.time = new GuiSlider(12, posx, l + 170, 150, 20, "", " seconds", 0.0D, 60.0D, (double)this.timer, false, true));
      int tmp = this.xSize / 4;
      int tmp2 = (this.xSize - 6) / 4;
      posx = k + (tmp - 20) / 2;
      GuiButton tmpbutton;
      this.buttonList.add(tmpbutton = new GuiButton(99, posx, l + 110, 20, 20, this.yesNo(true)));
      tmpbutton.enabled = false;
      posx += tmp;
      this.buttonList.add(this.desactivate2 = new GuiButton(0, posx, l + 110, 20, 20, this.yesNo(this.activate2)));
      posx += tmp;
      this.buttonList.add(this.desactivate3 = new GuiButton(1, posx, l + 110, 20, 20, this.yesNo(this.activate3)));
      posx += tmp;
      this.buttonList.add(this.desactivate4 = new GuiButton(2, posx, l + 110, 20, 20, this.yesNo(this.activate4)));
      posx = k + 2 + (tmp2 - 40) / 2;
      this.buttonList.add(this.change1 = new GuiButton(3, posx, l + 144, 40, 20, "Change"));
      this.change1.enabled = false;
      posx += tmp2;
      this.buttonList.add(this.change2 = new GuiButton(4, posx, l + 144, 40, 20, "Change"));
      if (!this.activate2) {
         this.change2.enabled = false;
      }

      posx += tmp;
      this.buttonList.add(this.change3 = new GuiButton(5, posx, l + 144, 40, 20, "Change"));
      if (!this.activate3) {
         this.change3.enabled = false;
      }

      posx += tmp;
      this.buttonList.add(this.change4 = new GuiButton(6, posx, l + 144, 40, 20, "Change"));
      if (!this.activate4) {
         this.change4.enabled = false;
      }

      posx = k + (this.xSize / 2 - 50) / 2;
      this.buttonList.add(this.ok = new GuiButton(7, posx, l + 200, 50, 20, "OK"));
      posx += this.xSize / 2;
      this.buttonList.add(this.cancel = new GuiButton(8, posx, l + 200, 50, 20, "Cancel"));
   }

   public String yesNo(boolean bool) {
      return bool ? "I" : "O";
   }

   public void drawDefaultBackground() {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      this.mc.getTextureManager().bindTexture(texture);
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
   }

   public void drawScreen(int par1, int par2, float par3) {
      this.drawDefaultBackground();
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      int tmp = this.xSize / 4;
      int posx = k + (tmp - 20) / 2;
      drawRect(posx, l + 132, posx + 20, l + 142, this.color1);
      posx += tmp;
      drawRect(posx, l + 132, posx + 20, l + 142, this.color2);
      posx += tmp;
      drawRect(posx, l + 132, posx + 20, l + 142, this.color3);
      posx += tmp;
      drawRect(posx, l + 132, posx + 20, l + 142, this.color4);
      String tileName = I18n.format("gui.multicolor_block", new Object[0]);
      this.fontRendererObj.drawString(tileName, k + (this.xSize - this.fontRendererObj.getStringWidth(tileName)) / 2, l + 5, 0);
      super.drawScreen(par1, par2, par3);
   }

   public void onGuiClosed() {
      super.onGuiClosed();
   }

   protected void actionPerformed(GuiButton button) {
      int id = button.id;
      if (id == 0) {
         if (this.activate2) {
            this.desactivate2.displayString = "O";
         } else {
            this.desactivate2.displayString = "I";
         }

         this.change2.enabled = !this.activate2;
         this.activate2 = !this.activate2;
      } else if (id == 1) {
         if (this.activate3) {
            this.desactivate3.displayString = "O";
         } else {
            this.desactivate3.displayString = "I";
         }

         this.change3.enabled = !this.activate3;
         this.activate3 = !this.activate3;
      } else if (id == 2) {
         if (this.activate4) {
            this.desactivate4.displayString = "O";
         } else {
            this.desactivate4.displayString = "I";
         }

         this.change4.enabled = !this.activate4;
         this.activate4 = !this.activate4;
      } else if (id == 3) {
         this.color = 0;
         this.change1.enabled = false;
         this.setValue(this.color1);
         if (this.activate2) {
            this.change2.enabled = true;
         }

         if (this.activate3) {
            this.change3.enabled = true;
         }

         if (this.activate4) {
            this.change4.enabled = true;
         }
      } else if (id == 4) {
         this.color = 1;
         this.change2.enabled = false;
         this.change1.enabled = true;
         this.setValue(this.color2);
         if (this.activate3) {
            this.change3.enabled = true;
         }

         if (this.activate4) {
            this.change4.enabled = true;
         }
      } else if (id == 5) {
         this.color = 2;
         this.change3.enabled = false;
         this.change1.enabled = true;
         this.setValue(this.color3);
         if (this.activate2) {
            this.change2.enabled = true;
         }

         if (this.activate4) {
            this.change4.enabled = true;
         }
      } else if (id == 6) {
         this.color = 3;
         this.change4.enabled = false;
         this.change1.enabled = true;
         this.setValue(this.color4);
         if (this.activate2) {
            this.change2.enabled = true;
         }

         if (this.activate3) {
            this.change3.enabled = true;
         }
      } else if (id == 7) {
         PacketMulticolorBlock packet = new PacketMulticolorBlock();
         packet.setInformations(this.color1, this.color2, this.color3, this.color4, this.activate2, this.activate3, this.activate4, this.timer, this.tileMachine.xCoord, this.tileMachine.yCoord, this.tileMachine.zCoord);
         PalaMod var10000 = PalaMod.instance;
         PalaMod.proxy.packetPipeline.sendToServer(packet);
         this.mc.displayGuiScreen((GuiScreen)null);
      } else if (id == 8) {
         this.mc.displayGuiScreen((GuiScreen)null);
      } else if (id != 9 && id != 10 && id != 11) {
         if (id == 12) {
            this.timer = this.time.getValueInt();
         }
      } else {
         int r = this.red.getValueInt();
         int g = this.green.getValueInt();
         int b = this.blue.getValueInt();
         Color rgb = new Color(r, g, b);
         if (this.color == 0) {
            this.color1 = rgb.getRGB();
         } else if (this.color == 1) {
            this.color2 = rgb.getRGB();
         } else if (this.color == 2) {
            this.color3 = rgb.getRGB();
         } else if (this.color == 3) {
            this.color4 = rgb.getRGB();
         }
      }

   }

   private void setValue(int colorGet) {
      this.red.setValue((double)(new Color(colorGet)).getRed());
      this.green.setValue((double)(new Color(colorGet)).getGreen());
      this.blue.setValue((double)(new Color(colorGet)).getBlue());
      this.red.displayString = "Red : " + (new Color(colorGet)).getRed();
      this.green.displayString = "Green : " + (new Color(colorGet)).getGreen();
      this.blue.displayString = "Blue : " + (new Color(colorGet)).getBlue();
   }

   public void mouseMovedOrUp(int mouseX, int mouseY, int mouseButton) {
      super.mouseMovedOrUp(mouseX, mouseY, mouseButton);
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      if (mouseY > l + 20 && mouseY < l + 100) {
         this.actionPerformed(this.red);
         if (this.color == 0) {
            this.setValue(this.color1);
         } else if (this.color == 1) {
            this.setValue(this.color2);
         } else if (this.color == 2) {
            this.setValue(this.color3);
         } else if (this.color == 3) {
            this.setValue(this.color4);
         }
      }

      if (mouseY > l + 170 && mouseY < l + 190) {
         this.actionPerformed(this.time);
      }

   }

   public boolean doesGuiPauseGame() {
      return false;
   }
}
