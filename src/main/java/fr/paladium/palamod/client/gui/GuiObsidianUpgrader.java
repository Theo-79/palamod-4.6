package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerObsidianUpgrader;
import fr.paladium.palamod.items.ItemObsidianUpgrade;
import fr.paladium.palamod.tiles.TileEntityObsidianUpgrader;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiObsidianUpgrader extends GuiContainer {
   public static final ResourceLocation texture = new ResourceLocation("palamod", "textures/gui/container/obsidianUpgrader.png");
   private TileEntityObsidianUpgrader tileMachine;
   private IInventory playerInv;

   public GuiObsidianUpgrader(TileEntityObsidianUpgrader tile, InventoryPlayer inventory) {
      super(new ContainerObsidianUpgrader(tile, inventory));
      this.tileMachine = tile;
      this.playerInv = inventory;
      this.xSize = 210;
      this.ySize = 183;
   }

   protected void drawGuiContainerBackgroundLayer(float partialRenderTick, int x, int y) {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      this.mc.getTextureManager().bindTexture(texture);
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
      if (this.tileMachine.hasObsidian()) {
         double h = (double)(this.tileMachine.getObsidian() * 55 / 64);
         int i = (int)h;
         int j = 91 - i;
         this.drawTexturedModalRect(k + 65, l + j, 0, 184, 55, i);
      }

      int i;
      if (this.tileMachine.isBurning(TileEntityObsidianUpgrader.Id.input)) {
         i = this.tileMachine.getProgress(TileEntityObsidianUpgrader.Id.input, 29);
         this.drawTexturedModalRect(k + 31, l + 53, 210, 0, i, 16);
      }

      if (this.tileMachine.isBurning(TileEntityObsidianUpgrader.Id.output)) {
         i = this.tileMachine.getProgress(TileEntityObsidianUpgrader.Id.output, 29);
         this.drawTexturedModalRect(k + 149, l + 53, 210, 0, i, 16);
      }

      if (this.tileMachine.isBurning(TileEntityObsidianUpgrader.Id.obsi)) {
         i = this.tileMachine.getProgress(TileEntityObsidianUpgrader.Id.obsi, 18);
         this.drawTexturedModalRect(k + 78, l + 13, 210, 17, 7, i);
      }

      if (this.tileMachine.isBurning(TileEntityObsidianUpgrader.Id.fuel)) {
         i = this.tileMachine.getProgress(TileEntityObsidianUpgrader.Id.fuel, 13);
         int j = 13 - i;
         this.drawTexturedModalRect(k + 91, l + 14 + j, 218, 17 + j, 14, i);
      }

   }

   protected void drawGuiContainerForegroundLayer(int x, int y) {
      String tileName = I18n.format(this.tileMachine.getInventoryName(), new Object[0]);
      this.fontRendererObj.drawString(tileName, (this.xSize - this.fontRendererObj.getStringWidth(tileName)) / 2, 5, 3087688);
      String invName = I18n.format(this.playerInv.getInventoryName(), new Object[0]);
      this.fontRendererObj.drawString(invName, (this.xSize - this.fontRendererObj.getStringWidth(invName)) / 2, this.ySize - 90, 3087688);
      this.drawToolTip(x, y);
   }

   private void drawToolTip(int x, int y) {
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      if (x >= k + 65 && y >= l + 36 && x <= k + 120 && y <= l + 91) {
         List list = new ArrayList();
         list.add(this.tileMachine.getObsidian() == 1 ? "1 Obsidian" : this.tileMachine.getObsidian() + " Obsidians");
         if (this.tileMachine.isUpgraded(ItemObsidianUpgrade.Upgrade.Explode)) {
            list.add(EnumChatFormatting.DARK_PURPLE + "Explode");
         }

         if (this.tileMachine.isUpgraded(ItemObsidianUpgrade.Upgrade.Fake)) {
            list.add(EnumChatFormatting.DARK_PURPLE + "Fake");
         }

         if (this.tileMachine.isUpgraded(ItemObsidianUpgrade.Upgrade.TwoLife)) {
            list.add(EnumChatFormatting.DARK_PURPLE + "TwoLifes");
         }

         if (this.tileMachine.isUpgraded(ItemObsidianUpgrade.Upgrade.Camouflage)) {
            list.add(EnumChatFormatting.DARK_PURPLE + "Camouflage");
         }

         this.drawHoveringText(list, x - 110, y - 30, this.fontRendererObj);
      }

   }
}
