package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.gui.ContainerEmpty;
import fr.paladium.palamod.network.packets.PacketOnlineDetector;
import fr.paladium.palamod.tiles.TileEntityOnlineDetector;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class GuiOnlineDetector extends GuiContainer {
   GuiTextField usernameInput;
   TileEntityOnlineDetector te;
   FontRenderer fontRendererObj;
   final ResourceLocation background = new ResourceLocation("palamod:textures/gui/OnlineDetector.png");

   public GuiOnlineDetector(TileEntityOnlineDetector te) {
      super(new ContainerEmpty());
      this.te = te;
      this.fontRendererObj = Minecraft.getMinecraft().fontRenderer;
      this.xSize = 161;
      this.ySize = 70;
   }

   public void initGui() {
      super.initGui();
      int x = (this.width - this.xSize) / 2;
      int y = (this.height - this.ySize) / 2;
      this.usernameInput = new GuiTextField(this.fontRendererObj, x + 5, y + 20, 150, 15);
      this.usernameInput.setFocused(false);
      this.usernameInput.setCanLoseFocus(true);
      this.usernameInput.setText(this.te.getPlayerName());
   }

   public void onGuiClosed() {
      Keyboard.enableRepeatEvents(false);
   }

   protected void mouseClicked(int par1, int par2, int par3) {
      super.mouseClicked(par1, par2, par3);
      this.usernameInput.mouseClicked(par1, par2, par3);
   }

   protected void keyTyped(char par1, int pressedKey) {
      if (pressedKey == 1 || !this.usernameInput.isFocused() && pressedKey == this.mc.gameSettings.keyBindInventory.getKeyCode()) {
         PacketOnlineDetector packet = new PacketOnlineDetector();
         packet.addInformations(this.usernameInput.getText(), this.te);
         PalaMod var10000 = PalaMod.instance;
         PalaMod.proxy.packetPipeline.sendToServer(packet);
         this.mc.thePlayer.closeScreen();
      }

      this.usernameInput.textboxKeyTyped(par1, pressedKey);
   }

   protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3) {
      this.mc.renderEngine.bindTexture(this.background);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      int x = (this.width - this.xSize) / 2;
      int y = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
      this.usernameInput.drawTextBox();
      this.fontRendererObj.drawStringWithShadow("Detecting: " + this.te.getPlayerName(), x + (this.xSize / 2 - this.fontRendererObj.getStringWidth("Detecting: " + this.te.getPlayerName()) / 2), y + 44, 16777215);
   }

   protected void drawGuiContainerForegroundLayer(int param1, int param2) {
      this.fontRendererObj.drawString("Online detector", 8, 6, 4210752);
   }

   public void updateScreen() {
      super.updateScreen();
      this.usernameInput.updateCursorCounter();
   }
}
