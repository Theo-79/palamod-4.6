package fr.paladium.palamod.client.gui;

import net.minecraft.client.settings.GameSettings.Options;

public class GuiOptionButtonPala extends GuiButtonPala {
   private final Options enumOptions;
   private static final String __OBFID = "CL_00000676";

   public GuiOptionButtonPala(int p_i45011_1_, int p_i45011_2_, int p_i45011_3_, String p_i45011_4_) {
      this(p_i45011_1_, p_i45011_2_, p_i45011_3_, (Options)null, p_i45011_4_);
   }

   public GuiOptionButtonPala(int p_i45012_1_, int p_i45012_2_, int p_i45012_3_, int p_i45012_4_, int p_i45012_5_, Options p_i45013_6_, String p_i45012_7_) {
      super(p_i45012_1_, p_i45012_2_, p_i45012_3_, p_i45012_4_, p_i45012_5_, p_i45012_7_);
      this.enumOptions = p_i45013_6_;
   }

   public GuiOptionButtonPala(int p_i45013_1_, int p_i45013_2_, int p_i45013_3_, Options p_i45013_4_, String p_i45013_5_) {
      super(p_i45013_1_, p_i45013_2_, p_i45013_3_, 150, 20, p_i45013_5_);
      this.enumOptions = p_i45013_4_;
   }

   public Options returnEnumOptions() {
      return this.enumOptions;
   }
}
