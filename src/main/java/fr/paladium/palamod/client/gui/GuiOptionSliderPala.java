package fr.paladium.palamod.client.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.util.GuiDrawer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.GameSettings.Options;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class GuiOptionSliderPala extends GuiButtonPala {
   private float field_146134_p;
   public boolean field_146135_o;
   private Options field_146133_q;
   private final float field_146132_r;
   private final float field_146131_s;
   private static final String __OBFID = "CL_00000680";
   private static ResourceLocation button = new ResourceLocation("palamod:textures/gui/sldr.png");

   public GuiOptionSliderPala(int id, int x, int y, Options options) {
      this(id, x, y, options, 0.0F, 1.0F);
   }

   public GuiOptionSliderPala(int id, int x, int y, Options options, boolean red) {
      this(id, x, y, options, 0.0F, 1.0F);
      this.red = red;
   }

   public GuiOptionSliderPala(int id, int x, int y, Options options, float par5, float par6) {
      super(id, x, y, 150, 20, "");
      this.field_146134_p = 1.0F;
      this.field_146133_q = options;
      this.field_146132_r = par5;
      this.field_146131_s = par6;
      Minecraft minecraft = Minecraft.getMinecraft();
      this.field_146134_p = options.normalizeValue(minecraft.gameSettings.getOptionFloatValue(options));
      this.displayString = minecraft.gameSettings.getKeyBinding(options);
   }

   public int getHoverState(boolean p_146114_1_) {
      return 0;
   }

   protected void mouseDragged(Minecraft p_146119_1_, int p_146119_2_, int p_146119_3_) {
      if (this.visible) {
         if (this.field_146135_o) {
            this.field_146134_p = (float)(p_146119_2_ - (this.xPosition + 4)) / (float)(this.width - 8);
            if (this.field_146134_p < 0.0F) {
               this.field_146134_p = 0.0F;
            }

            if (this.field_146134_p > 1.0F) {
               this.field_146134_p = 1.0F;
            }

            float f = this.field_146133_q.denormalizeValue(this.field_146134_p);
            p_146119_1_.gameSettings.setOptionFloatValue(this.field_146133_q, f);
            this.field_146134_p = this.field_146133_q.normalizeValue(f);
            this.displayString = p_146119_1_.gameSettings.getKeyBinding(this.field_146133_q);
         }

         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         Minecraft.getMinecraft().getTextureManager().bindTexture(button);
         GuiDrawer.drawTexturedQuadFit((double)this.xPosition, (double)this.yPosition, 45.0D, 100.0D, 0.0D);
         GuiDrawer.drawTexturedQuadFit((double)(this.xPosition + this.width - 2), (double)this.yPosition, 45.0D, 100.0D, 0.0D);
         GuiDrawer.drawTexturedQuadFit((double)(this.xPosition + (int)(this.field_146134_p * (float)(this.width - 8))), (double)this.yPosition, 120.0D, 100.0D, 0.0D);
      }

   }

   public boolean mousePressed(Minecraft minecraft, int x, int y) {
      if (super.mousePressed(minecraft, x, y)) {
         this.field_146134_p = (float)(x - (this.xPosition + 4)) / (float)(this.width - 8);
         if (this.field_146134_p < 0.0F) {
            this.field_146134_p = 0.0F;
         }

         if (this.field_146134_p > 1.0F) {
            this.field_146134_p = 1.0F;
         }

         minecraft.gameSettings.setOptionFloatValue(this.field_146133_q, this.field_146133_q.denormalizeValue(this.field_146134_p));
         this.displayString = minecraft.gameSettings.getKeyBinding(this.field_146133_q);
         this.field_146135_o = true;
         return true;
      } else {
         return false;
      }
   }

   public void mouseReleased(int p_146118_1_, int p_146118_2_) {
      this.field_146135_o = false;
   }
}
