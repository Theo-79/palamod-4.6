package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerPaladiumChest;
import fr.paladium.palamod.tiles.TileEntityPaladiumChest;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiPaladiumChest extends GuiContainer {
   ResourceLocation background = new ResourceLocation("palamod", "textures/gui/PaladiumChest.png");

   public GuiPaladiumChest(TileEntityPaladiumChest tile, InventoryPlayer inventory) {
      super(new ContainerPaladiumChest(tile, inventory));
      this.xSize = 250;
      this.ySize = 256;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      GL11.glColor4f(1.0F, 1.5F, 1.0F, 1.0F);
      this.mc.getTextureManager().bindTexture(this.background);
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
   }
}
