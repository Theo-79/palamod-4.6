package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerPaladiumFurnace;
import fr.paladium.palamod.tiles.TileEntityPaladiumFurnace;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiPaladiumFurnace extends GuiContainer {
   ResourceLocation background = new ResourceLocation("palamod", "textures/gui/PaladiumFurnace.png");
   TileEntityPaladiumFurnace tile;
   FontRenderer fr;

   public GuiPaladiumFurnace(TileEntityPaladiumFurnace tile, InventoryPlayer inventory) {
      super(new ContainerPaladiumFurnace(tile, inventory));
      this.tile = tile;
      this.ySize = 166;
      this.xSize = 176;
      this.fr = Minecraft.getMinecraft().fontRenderer;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      if (this.tile != null) {
         GL11.glColor4f(1.0F, 1.5F, 1.0F, 1.0F);
         this.mc.getTextureManager().bindTexture(this.background);
         int k = (this.width - this.xSize) / 2;
         int l = (this.height - this.ySize) / 2;
         this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
         int i1 = this.tile.getBurnTimeRemainingScaled(13) - 1;
         this.drawTexturedModalRect(k + 56, l + 36 + 12 - i1, 176, 12 - i1, 14, i1 + 1);
         if (this.tile.isBurning()) {
            i1 = this.tile.getCookProgressScaled(24);
            this.drawTexturedModalRect(k + 79, l + 34, 176, 14, i1 + 1, 16);
         }

      }
   }
}
