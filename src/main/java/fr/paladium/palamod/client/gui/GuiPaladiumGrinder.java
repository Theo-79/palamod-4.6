package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerPaladiumGrinder;
import fr.paladium.palamod.tiles.TileEntityPaladiumGrinder;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiPaladiumGrinder extends GuiContainer {
   TileEntityPaladiumGrinder tile;
   FontRenderer fr;
   public static ResourceLocation background = new ResourceLocation("palamod:textures/gui/PaladiumGrinder.png");

   public GuiPaladiumGrinder(InventoryPlayer inventory, TileEntityPaladiumGrinder tile) {
      super(new ContainerPaladiumGrinder(inventory, tile));
      this.tile = tile;
      this.ySize = 172;
      this.xSize = 210;
      this.fr = Minecraft.getMinecraft().fontRenderer;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      GL11.glColor4f(1.0F, 1.5F, 1.0F, 1.0F);
      this.mc.getTextureManager().bindTexture(background);
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
      this.drawTexturedModalRect(k + 146, l + 12, 51, 172, 53, 54 - (int)this.tile.getScaledPaladium(54));
      GL11.glPushMatrix();
      GL11.glDisable(2896);
      GL11.glEnable(3042);
      GL11.glBlendFunc(770, 771);
      this.drawTexturedModalRect(k + 147, l + 13, 0, 172, 51, 50);
      GL11.glPopMatrix();
      this.drawTexturedModalRect(k + 43, l + 27, 210, 0, this.tile.getScaledTool(31), 17);
      this.drawTexturedModalRect(k + 43, l + 67, 210, 0, this.tile.getScaledUpgrade(31), 17);
      this.drawTexturedModalRect(k + 124, l + 29, 210, 18, this.tile.getScaledProgress(19), 11);
      this.fr.drawStringWithShadow("Paladium Grinder", k + 7, l + 6, 16777215);
   }

   protected void drawGuiContainerForegroundLayer(int x, int y) {
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      if (x >= k + 147 && y >= l + 12 && x <= k + 198 && y <= l + 63) {
         List list = new ArrayList();
         list.add(EnumChatFormatting.GOLD + (this.tile.getPaladium() == 1 ? "1 Paladium" : this.tile.getPaladium() + " Paladiums"));
         list.add("Max: 100");
         this.drawHoveringText(list, x - 200, y - 30, this.fontRendererObj);
      }

   }
}
