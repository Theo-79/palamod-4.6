package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerPaladiumMachine;
import fr.paladium.palamod.tiles.TileEntityPaladiumMachine;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiPaladiumMachine extends GuiContainer {
   public static ResourceLocation background = new ResourceLocation("palamod:textures/gui/PaladiumMachine.png");
   private TileEntityPaladiumMachine tile;

   public GuiPaladiumMachine(TileEntityPaladiumMachine tile, InventoryPlayer inventory) {
      super(new ContainerPaladiumMachine(tile, inventory));
      this.tile = tile;
      this.ySize = 340;
      this.xSize = 200;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      GL11.glColor4f(1.0F, 1.5F, 1.0F, 1.0F);
      this.mc.getTextureManager().bindTexture(background);
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
      if (this.tile.isBurning()) {
         int i = this.tile.getCookProgress();
         this.drawTexturedModalRect(k + 90, l + 122, 201, 105, 20, i);
      }

   }
}
