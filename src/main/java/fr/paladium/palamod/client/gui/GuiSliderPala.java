package fr.paladium.palamod.client.gui;

import cpw.mods.fml.client.config.GuiSlider;
import cpw.mods.fml.client.config.GuiSlider.ISlider;
import fr.paladium.palamod.util.GuiDrawer;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiSliderPala extends GuiSlider {
   private static ResourceLocation button = new ResourceLocation("palamod:textures/gui/sldr.png");

   public GuiSliderPala(int id, int xPos, int yPos, String displayStr, double minVal, double maxVal, double currentVal, ISlider par) {
      super(id, xPos, yPos, displayStr, minVal, maxVal, currentVal, par);
   }

   public GuiSliderPala(int id, int xPos, int yPos, int width, int height, String prefix, String suf, double minVal, double maxVal, double currentVal, boolean showDec, boolean drawStr, ISlider par) {
      super(id, xPos, yPos, width, height, prefix, suf, minVal, maxVal, currentVal, showDec, drawStr, par);
   }

   public GuiSliderPala(int id, int xPos, int yPos, int width, int height, String prefix, String suf, double minVal, double maxVal, double currentVal, boolean showDec, boolean drawStr) {
      super(id, xPos, yPos, width, height, prefix, suf, minVal, maxVal, currentVal, showDec, drawStr);
   }

   protected void mouseDragged(Minecraft par1Minecraft, int par2, int par3) {
      if (this.visible) {
         if (this.dragging) {
            this.sliderValue = (double)((float)(par2 - (this.xPosition + 4)) / (float)(this.width - 8));
            this.updateSlider();
         }

         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         Minecraft.getMinecraft().getTextureManager().bindTexture(button);
         GuiDrawer.drawTexturedQuadFit((double)(this.xPosition + (int)((float)this.getValueInt() * (float)(this.width - 8))), (double)this.yPosition, 120.0D, 100.0D, 0.0D);
      }

   }
}
