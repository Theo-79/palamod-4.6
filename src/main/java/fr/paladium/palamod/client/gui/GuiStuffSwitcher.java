package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerStuffSwitcher;
import fr.paladium.palamod.common.inventory.InventoryStuffSwitcher;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiStuffSwitcher extends GuiContainer {
   ResourceLocation background = new ResourceLocation("palamod", "textures/gui/StuffSwitcher.png");

   public GuiStuffSwitcher(InventoryPlayer inventory, InventoryStuffSwitcher inventoryStuff) {
      super(new ContainerStuffSwitcher(inventory, inventoryStuff));
      this.xSize = 176;
      this.ySize = 117;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      GL11.glColor4f(1.0F, 1.5F, 1.0F, 1.0F);
      this.mc.getTextureManager().bindTexture(this.background);
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(k, l, 0, 1, this.xSize, this.ySize);
   }
}
