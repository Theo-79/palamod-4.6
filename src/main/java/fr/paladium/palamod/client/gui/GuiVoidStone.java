package fr.paladium.palamod.client.gui;

import fr.paladium.palamod.common.gui.ContainerVoidStone;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiVoidStone extends GuiContainer {
   ResourceLocation background = new ResourceLocation("palamod", "textures/gui/VoidStone.png");
   FontRenderer fr;

   public GuiVoidStone(InventoryPlayer invetory) {
      super(new ContainerVoidStone(invetory));
      this.xSize = 176;
      this.ySize = 122;
      this.fr = Minecraft.getMinecraft().fontRenderer;
   }

   protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
      GL11.glColor4f(1.0F, 1.5F, 1.0F, 1.0F);
      this.mc.getTextureManager().bindTexture(this.background);
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
      this.fr.drawStringWithShadow("Void Stone", k + 5, l + 5, 16777215);
   }
}
