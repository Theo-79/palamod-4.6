package fr.paladium.palamod.client.gui.button;

import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.util.DisplayHelper;
import fr.paladium.palamod.util.GuardianHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;

public class ButtonGuardianUpgrade extends GuiButton {
   ResourceLocation buttonLogo = new ResourceLocation("textures/items/bed.png");
   ResourceLocation buttonLogoHover = new ResourceLocation("textures/items/arrow.png");
   ResourceLocation buttonDisabled = new ResourceLocation("textures/items/apple.png");
   EntityGuardianGolem golem;
   int x;
   int y;

   public ButtonGuardianUpgrade(int id, int x, int y, String text, EntityGuardianGolem golem) {
      super(id, x, y, text);
      this.x = x;
      this.y = y;
      this.golem = golem;
      this.width = 16;
      this.height = 16;
      this.xPosition = x;
      this.yPosition = y;
   }

   public void drawButton(Minecraft mc, int mouseX, int mouseY) {
      if (!GuardianHelper.hasUpgrade(this.golem, 6)) {
         mc.renderEngine.bindTexture(this.buttonDisabled);
      } else {
         if (mouseX > this.x && mouseX < this.x + 16 && mouseY > this.y && mouseY < this.y + 16) {
            mc.renderEngine.bindTexture(this.buttonLogo);
         } else {
            mc.renderEngine.bindTexture(this.buttonLogoHover);
         }

         DisplayHelper.drawTexturedQuadFit((double)this.x, (double)this.y, 16.0D, 16.0D, 0.0D);
      }
   }
}
