package fr.paladium.palamod.client.gui.custom;

import fr.paladium.palamod.client.gui.GuiButtonPala;
import fr.paladium.palamod.util.GuiDrawer;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.achievement.GuiAchievements;
import net.minecraft.client.gui.achievement.GuiStats;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiInGameMenu extends GuiScreen {
   private ResourceLocation background = new ResourceLocation("palamod", "textures/gui/LOGO.png");
   private int xSize = 500;
   private int ySize = 200;

   public void initGui() {
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      this.buttonList.add(new GuiButtonPala(0, k + (this.xSize - 150) / 2, l + 170, 150, 20, !this.mc.isIntegratedServerRunning() ? I18n.format("menu.disconnect", new Object[0]) : I18n.format("menu.returnToMenu", new Object[0]), true));
      if (!this.mc.isIntegratedServerRunning()) {
         ((GuiButton)this.buttonList.get(0)).displayString = I18n.format("menu.disconnect", new Object[0]);
      }

      this.buttonList.add(new GuiButtonPala(1, k + (this.xSize - 150) / 2, l + 5, 150, 20, I18n.format("menu.returnToGame", new Object[0])));
      this.buttonList.add(new GuiButtonPala(2, k + 50, l + 90, 100, 20, I18n.format("gui.achievements", new Object[0])));
      this.buttonList.add(new GuiButtonPala(3, k + 50, l + 110, 100, 20, I18n.format("gui.stats", new Object[0])));
      this.buttonList.add(new GuiButtonPala(4, k + this.xSize - 130, l + 90, 100, 20, I18n.format("menu.options", new Object[0])));
      this.buttonList.add(new GuiButtonPala(5, k + this.xSize - 130, l + 110, 100, 20, I18n.format("TeamSpeak", new Object[0])));
   }

   protected void actionPerformed(GuiButton button) {
      switch(button.id) {
      case 0:
         button.enabled = false;
         this.mc.theWorld.sendQuittingDisconnectingPacket();
         this.mc.loadWorld((WorldClient)null);
         this.mc.displayGuiScreen(new net.minecraft.client.gui.GuiMainMenu());
         break;
      case 1:
         this.mc.displayGuiScreen((GuiScreen)null);
         this.mc.setIngameFocus();
         break;
      case 2:
         if (this.mc.thePlayer != null) {
            this.mc.displayGuiScreen(new GuiAchievements(this, this.mc.thePlayer.getStatFileWriter()));
         }
         break;
      case 3:
         if (this.mc.thePlayer != null) {
            this.mc.displayGuiScreen(new GuiStats(this, this.mc.thePlayer.getStatFileWriter()));
         }
         break;
      case 4:
         this.mc.displayGuiScreen(new net.minecraft.client.gui.GuiOptions(this, this.mc.gameSettings));
         break;
      case 5:
         try {
            Desktop.getDesktop().browse(new URI("ts3server://ts.paladium-pvp.fr/?nickname=" + Minecraft.getMinecraft().getSession().getUsername()));
         } catch (IOException var3) {
            var3.printStackTrace();
         } catch (URISyntaxException var4) {
            var4.printStackTrace();
         }
      }

   }

   public void drawDefaultBackground() {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      int k = (this.width - this.xSize) / 2;
      int l = (this.height - this.ySize) / 2;
      this.mc.getTextureManager().bindTexture(this.background);
      GuiDrawer.drawTexturedQuadFit((double)(k + 120), (double)(l + 20), 275.0D, 175.0D, 0.0D);
   }

   public void drawScreen(int x, int y, float par3) {
      this.drawDefaultBackground();
      super.drawScreen(x, y, par3);
   }
}
