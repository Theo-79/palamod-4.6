package fr.paladium.palamod.client.gui.custom;

import cpw.mods.fml.client.FMLClientHandler;
import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.client.gui.GuiButtonPala;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.FloatControl.Type;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiConfirmOpenLink;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSelectWorld;
import net.minecraft.client.gui.GuiYesNoCallback;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.ISaveFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.glu.Project;

public class GuiMainMenu extends GuiScreen implements GuiYesNoCallback {
   private static final Logger logger = LogManager.getLogger();
   private static final Random rand = new Random();
   private float updateCounter;
   private String splashText;
   private GuiButton buttonResetDemo;
   private int panoramaTimer;
   private Clip clip;
   private DynamicTexture viewportTexture;
   private final Object field_104025_t = new Object();
   private String field_92025_p;
   private String field_146972_A;
   private String field_104024_v;
   public static boolean play = true;
   public static String mute = "Mute/Play";
   private static final ResourceLocation minecraftTitleTextures = new ResourceLocation("palamod:textures/gui/LOGO.png");
   private static final ResourceLocation[] titlePanoramaPaths = new ResourceLocation[]{new ResourceLocation("palamod:textures/gui/bg.png"), new ResourceLocation("palamod:textures/gui/bg.png"), new ResourceLocation("palamod:textures/gui/bg.png"), new ResourceLocation("palamod:textures/gui/bg.png"), new ResourceLocation("palamod:textures/gui/bg.png"), new ResourceLocation("palamod:textures/gui/bg.png")};
   public static final String field_96138_a;
   private int field_92024_r;
   private int field_92023_s;
   private int field_92022_t;
   private int field_92021_u;
   private int field_92020_v;
   private int field_92019_w;
   private AudioInputStream audioIn;
   private ResourceLocation field_110351_G;
   private static final String __OBFID = "CL_00001154";

   public GuiMainMenu() {
      this.field_146972_A = field_96138_a;
      this.splashText = "missingno";
      BufferedReader bufferedreader = null;
      this.updateCounter = rand.nextFloat();
      this.field_92025_p = "";
      if (!GLContext.getCapabilities().OpenGL20 && !OpenGlHelper.func_153193_b()) {
         this.field_92025_p = I18n.format("title.oldgl1", new Object[0]);
         this.field_146972_A = I18n.format("title.oldgl2", new Object[0]);
         this.field_104024_v = "https://help.mojang.com/customer/portal/articles/325948?ref=game";
      }

   }

   private void Audio() throws IOException {
      try {
         URL url = PalaMod.class.getResource("/fr/paladium/palamod/util/Epic.wav");
         this.audioIn = AudioSystem.getAudioInputStream(url);
         this.clip = AudioSystem.getClip();
         this.clip.open(this.audioIn);
         FloatControl gainControl = (FloatControl)this.clip.getControl(Type.MASTER_GAIN);
         gainControl.setValue(-30.0F);
         this.clip.start();
         Clip var10001 = this.clip;
         this.clip.loop(-1);
      } catch (UnsupportedAudioFileException var3) {
         var3.printStackTrace();
      } catch (IOException var4) {
         var4.printStackTrace();
      } catch (LineUnavailableException var5) {
         var5.printStackTrace();
      }

   }

   public void updateScreen() {
      ++this.panoramaTimer;
   }

   public boolean doesGuiPauseGame() {
      return false;
   }

   protected void keyTyped(char p_73869_1_, int p_73869_2_) {
   }

   public void initGui() {
      this.viewportTexture = new DynamicTexture(256, 256);
      this.field_110351_G = this.mc.getTextureManager().getDynamicTextureLocation("background", this.viewportTexture);
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(new Date());
      boolean flag = true;
      int i = this.height / 4 + 48;
      if (!this.mc.isDemo()) {
         this.addSingleplayerMultiplayerButtons(i, 24);
      }

      this.buttonList.add(new GuiButtonPala(0, this.width / 2 - 100, 182, 98, 20, I18n.format("menu.options", new Object[0])));
      this.buttonList.add(new GuiButtonPala(4, this.width / 2 + 2, 182, 98, 20, I18n.format("menu.quit", new Object[0])));
      synchronized(this.field_104025_t) {
         this.field_92023_s = this.fontRendererObj.getStringWidth(this.field_92025_p);
         this.field_92024_r = this.fontRendererObj.getStringWidth(this.field_146972_A);
         int j = Math.max(this.field_92023_s, this.field_92024_r);
         this.field_92022_t = (this.width - j) / 2;
         this.field_92021_u = ((GuiButtonPala)this.buttonList.get(0)).yPosition - 24;
         this.field_92020_v = this.field_92022_t + j;
         this.field_92019_w = this.field_92021_u + 24;
      }
   }

   private void addSingleplayerMultiplayerButtons(int x, int y) {
      this.buttonList.add(new GuiButtonPala(65, this.width / 2 - 160, 110, 40, 30, "Site Web") {
         public void mouseReleased(int x, int y) {
            try {
               Desktop.getDesktop().browse(new URI("https://paladium-pvp.fr/"));
            } catch (IOException var4) {
               var4.printStackTrace();
            } catch (URISyntaxException var5) {
               var5.printStackTrace();
            }

         }
      });
      this.buttonList.add(new GuiButtonPala(64, this.width / 2 + 120, 110, 40, 30, "Teamspeak") {
         public void mouseReleased(int x, int y) {
            try {
               Desktop.getDesktop().browse(new URI("ts3server://ts.paladium-pvp.fr/?nickname=" + Minecraft.getMinecraft().getSession().getUsername()));
            } catch (IOException var4) {
               var4.printStackTrace();
            } catch (URISyntaxException var5) {
               var5.printStackTrace();
            }

         }
      });
      this.buttonList.add(new GuiButtonPala(20, this.width / 2 - 20, 30, 40, 30, I18n.format("PALADIUM", new Object[0])) {
      });
      this.buttonList.add(3, new GuiButtonPala(11, this.width / 2 - 200, 220, 40, 30, mute) {
         public void mouseReleased(int x, int y) {
            if (GuiMainMenu.play) {
               GuiMainMenu.this.clip.stop();
               GuiMainMenu.play = false;
            } else {
               GuiMainMenu.this.clip.start();
               GuiMainMenu.play = true;
            }

         }
      });
   }

   protected void actionPerformed(GuiButton button) {
      if (button.id == 0) {
         this.mc.displayGuiScreen(new net.minecraft.client.gui.GuiOptions(this, this.mc.gameSettings));
      }

      if (button.id == 5) {
      }

      Class oclass;
      Object object;
      if (button.id == 1) {
         try {
            oclass = Class.forName("java.awt.Desktop");
            object = oclass.getMethod("getDesktop").invoke((Object)null);
            oclass.getMethod("browse", URI.class).invoke(object, new URI("https://www.youtube.com/watch?v=N9ciqYfHlUY"));
         } catch (Throwable var5) {
            logger.error("Couldn't open link", var5);
         }
      }

      if (button.id == 2) {
      }

      if (button.id == 4) {
         this.mc.shutdown();
      }

      if (button.id == 6) {
      }

      if (button.id == 11) {
      }

      if (button.id == 12) {
      }

      if (button.id == 20) {
    	  this.mc.displayGuiScreen(new GuiSelectWorld(this));
      }

      if (button.id == 21) {
         try {
            oclass = Class.forName("java.awt.Desktop");
            object = oclass.getMethod("getDesktop").invoke((Object)null);
            oclass.getMethod("browse", URI.class).invoke(object, new URI("http://www.paladium-pvp.fr"));
         } catch (Throwable var4) {
            logger.error("Couldn't open link", var4);
         }
      }

   }

   public void confirmClicked(boolean p_73878_1_, int id) {
      if (p_73878_1_ && id == 12) {
         ISaveFormat isaveformat = this.mc.getSaveLoader();
         isaveformat.flushCache();
         this.mc.displayGuiScreen(this);
      } else if (id == 13) {
         if (p_73878_1_) {
            try {
               Class oclass = Class.forName("java.awt.Desktop");
               Object object = oclass.getMethod("getDesktop").invoke((Object)null);
               oclass.getMethod("browse", URI.class).invoke(object, new URI(this.field_104024_v));
            } catch (Throwable var5) {
               logger.error("Couldn't open link", var5);
            }
         }

         this.mc.displayGuiScreen(this);
      }

   }

   private void drawPanorama(int x, int y, float partialTick) {
      Tessellator tessellator = Tessellator.instance;
      GL11.glMatrixMode(5889);
      GL11.glPushMatrix();
      GL11.glLoadIdentity();
      Project.gluPerspective(120.0F, 1.0F, 0.05F, 10.0F);
      GL11.glMatrixMode(5888);
      GL11.glPushMatrix();
      GL11.glLoadIdentity();
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glEnable(3042);
      GL11.glDisable(3008);
      GL11.glDisable(2884);
      GL11.glDepthMask(false);
      OpenGlHelper.glBlendFunc(770, 771, 1, 0);
      byte b0 = 8;

      for(int k = 0; k < b0 * b0; ++k) {
         GL11.glPushMatrix();
         float f1 = ((float)(k % b0) / (float)b0 - 0.5F) / 64.0F;
         float f2 = ((float)(k / b0) / (float)b0 - 0.5F) / 64.0F;
         float f3 = 0.0F;
         GL11.glTranslatef(f1, f2, f3);
         GL11.glRotatef(MathHelper.sin(((float)this.panoramaTimer + partialTick) / 400.0F) * 25.0F + 20.0F, 1.0F, 0.0F, 0.0F);
         GL11.glRotatef(-((float)this.panoramaTimer + partialTick) * 0.1F, 0.0F, 1.0F, 0.0F);

         for(int l = 0; l < 6; ++l) {
            GL11.glPushMatrix();
            if (l == 1) {
               GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
            }

            if (l == 2) {
               GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
            }

            if (l == 3) {
               GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
            }

            if (l == 4) {
               GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
            }

            if (l == 5) {
               GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
            }

            this.mc.getTextureManager().bindTexture(titlePanoramaPaths[l]);
            tessellator.startDrawingQuads();
            tessellator.setColorRGBA_I(16777215, 255 / (k + 1));
            float f4 = 0.0F;
            tessellator.addVertexWithUV(-1.0D, -1.0D, 1.0D, (double)(0.0F + f4), (double)(0.0F + f4));
            tessellator.addVertexWithUV(1.0D, -1.0D, 1.0D, (double)(1.0F - f4), (double)(0.0F + f4));
            tessellator.addVertexWithUV(1.0D, 1.0D, 1.0D, (double)(1.0F - f4), (double)(1.0F - f4));
            tessellator.addVertexWithUV(-1.0D, 1.0D, 1.0D, (double)(0.0F + f4), (double)(1.0F - f4));
            tessellator.draw();
            GL11.glPopMatrix();
         }

         GL11.glPopMatrix();
         GL11.glColorMask(true, true, true, false);
      }

      tessellator.setTranslation(0.0D, 0.0D, 0.0D);
      GL11.glColorMask(true, true, true, true);
      GL11.glMatrixMode(5889);
      GL11.glPopMatrix();
      GL11.glMatrixMode(5888);
      GL11.glPopMatrix();
      GL11.glDepthMask(true);
      GL11.glEnable(2884);
      GL11.glEnable(2929);
   }

   private void rotateAndBlurSkybox(float partialTick) {
      this.mc.getTextureManager().bindTexture(this.field_110351_G);
      GL11.glTexParameteri(3553, 10241, 9729);
      GL11.glTexParameteri(3553, 10240, 9729);
      GL11.glCopyTexSubImage2D(3553, 0, 0, 0, 0, 0, 256, 256);
      GL11.glEnable(3042);
      OpenGlHelper.glBlendFunc(770, 771, 1, 0);
      GL11.glColorMask(true, true, true, false);
      Tessellator tessellator = Tessellator.instance;
      tessellator.startDrawingQuads();
      GL11.glDisable(3008);
      byte b0 = 3;

      for(int i = 0; i < b0; ++i) {
         tessellator.setColorRGBA_F(1.0F, 1.0F, 1.0F, 1.0F / (float)(i + 1));
         int j = this.width;
         int k = this.height;
         float f1 = (float)(i - b0 / 2) / 256.0F;
         tessellator.addVertexWithUV((double)j, (double)k, (double)this.zLevel, (double)(0.0F + f1), 1.0D);
         tessellator.addVertexWithUV((double)j, 0.0D, (double)this.zLevel, (double)(1.0F + f1), 1.0D);
         tessellator.addVertexWithUV(0.0D, 0.0D, (double)this.zLevel, (double)(1.0F + f1), 0.0D);
         tessellator.addVertexWithUV(0.0D, (double)k, (double)this.zLevel, (double)(0.0F + f1), 0.0D);
      }

      tessellator.draw();
      GL11.glEnable(3008);
      GL11.glColorMask(true, true, true, true);
   }

   private void renderSkybox(int x, int y, float partialTick) {
      this.mc.getFramebuffer().unbindFramebuffer();
      GL11.glViewport(0, 0, 256, 256);
      this.drawPanorama(x, y, partialTick);
      this.rotateAndBlurSkybox(partialTick);
      this.rotateAndBlurSkybox(partialTick);
      this.rotateAndBlurSkybox(partialTick);
      this.rotateAndBlurSkybox(partialTick);
      this.rotateAndBlurSkybox(partialTick);
      this.rotateAndBlurSkybox(partialTick);
      this.rotateAndBlurSkybox(partialTick);
      this.mc.getFramebuffer().bindFramebuffer(true);
      GL11.glViewport(0, 0, this.mc.displayWidth, this.mc.displayHeight);
      Tessellator tessellator = Tessellator.instance;
      tessellator.startDrawingQuads();
      float f1 = this.width > this.height ? 120.0F / (float)this.width : 120.0F / (float)this.height;
      float f2 = (float)this.height * f1 / 256.0F;
      float f3 = (float)this.width * f1 / 256.0F;
      tessellator.setColorRGBA_F(1.0F, 1.0F, 1.0F, 1.0F);
      int k = this.width;
      int l = this.height;
      tessellator.addVertexWithUV(0.0D, (double)l, (double)this.zLevel, (double)(0.5F - f2), (double)(0.5F + f3));
      tessellator.addVertexWithUV((double)k, (double)l, (double)this.zLevel, (double)(0.5F - f2), (double)(0.5F - f3));
      tessellator.addVertexWithUV((double)k, 0.0D, (double)this.zLevel, (double)(0.5F + f2), (double)(0.5F - f3));
      tessellator.addVertexWithUV(0.0D, 0.0D, (double)this.zLevel, (double)(0.5F + f2), (double)(0.5F + f3));
      tessellator.draw();
   }

   public static void drawTexturedQuadFit(double x, double y, double width, double height, double zLevel) {
      Tessellator tessellator = Tessellator.instance;
      tessellator.startDrawingQuads();
      tessellator.addVertexWithUV(x + 0.0D, y + height, zLevel, 0.0D, 1.0D);
      tessellator.addVertexWithUV(x + width, y + height, zLevel, 1.0D, 1.0D);
      tessellator.addVertexWithUV(x + width, y + 0.0D, zLevel, 1.0D, 0.0D);
      tessellator.addVertexWithUV(x + 0.0D, y + 0.0D, zLevel, 0.0D, 0.0D);
      tessellator.draw();
   }

   public void drawScreen(int x, int y, float partialTick) {
      GL11.glDisable(3008);
      this.renderSkybox(x, y, partialTick);
      GL11.glEnable(3008);
      Tessellator tessellator = Tessellator.instance;
      short short1 = 274;
      int k = this.width / 2 - short1 / 2;
      byte b0 = 30;
      this.drawGradientRect(0, 0, this.width, this.height, -2130706433, 16777215);
      this.drawGradientRect(0, 0, this.width, this.height, 0, Integer.MIN_VALUE);
      GL11.glPushMatrix();
      this.mc.renderEngine.bindTexture(minecraftTitleTextures);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      drawTexturedQuadFit((double)(k + 8), (double)(b0 + 25), 256.0D, 144.0D, 0.0D);
      GL11.glPopMatrix();
      tessellator.setColorOpaque_I(-1);
      GL11.glPushMatrix();
      GL11.glTranslatef((float)(this.width / 2 + 90), 70.0F, 0.0F);
      GL11.glRotatef(-20.0F, 0.0F, 0.0F, 1.0F);
      GL11.glPopMatrix();
      String s = "Paladium 4.6";
      this.drawString(this.fontRendererObj, s, this.width - this.fontRendererObj.getStringWidth(s) - 2, this.height - 20, -120000);
      String s1 = "Copyright Paladium - Mojang AB.";
      this.drawString(this.fontRendererObj, s1, this.width - this.fontRendererObj.getStringWidth(s1) - 2, this.height - 10, -25000);
      if (this.field_92025_p != null && this.field_92025_p.length() > 0) {
         drawRect(this.field_92022_t - 2, this.field_92021_u - 2, this.field_92020_v + 2, this.field_92019_w - 1, 1428160512);
      }

      super.drawScreen(x, y, partialTick);
      FontRenderer v2 = Minecraft.getMinecraft().fontRenderer;
      if (Keyboard.isKeyDown(61)) {
         v2.drawStringWithShadow("x: 666", 30, 30, 16711680);
         v2.drawStringWithShadow("y: 666", 30, 40, 16711680);
         v2.drawStringWithShadow("z: 666", 30, 50, 16711680);
         v2.drawStringWithShadow("fps: 666", 30, 60, 16711680);
         if (Keyboard.isKeyDown(50) && Keyboard.isKeyDown(25)) {
            v2.drawStringWithShadow("Sam54 et HeavenIsALie sont des beau gosses", 50, 100, 16711680);
         }
      }

      GuiMainMenu.Staff[] staffs = new GuiMainMenu.Staff[]{new GuiMainMenu.Staff(new int[]{19, 18, 32}, new String[]{"Redshift est le plus beau", "C'est la blague que Sam a fait", "Et Heaven a beaucoup ri"}), new GuiMainMenu.Staff(new int[]{25, 46, 23}, new String[]{"PCI: L'api prï¿½fï¿½rï¿½e de ceux qui ne savent pas dï¿½velopper"}), new GuiMainMenu.Staff(new int[]{35, 23, 31}, new String[]{"Oh hiss la saucisse", "C'est la blague que heaven a fait", "Et Sam a beaucoup ri"})};
      GuiMainMenu.Staff[] var12 = staffs;
      int var13 = staffs.length;

      for(int var14 = 0; var14 < var13; ++var14) {
         GuiMainMenu.Staff staff = var12[var14];
         boolean correct = true;
         int[] var17 = staff.keys;
         int var18 = var17.length;

         for(int var19 = 0; var19 < var18; ++var19) {
            int key = var17[var19];
            if (!Keyboard.isKeyDown(key)) {
               correct = false;
            }
         }

         if (correct) {
            for(int i = 0; i < staff.phrases.length; ++i) {
               v2.drawStringWithShadow(staff.phrases[i], 30, 30 + i * 10, 16711680);
            }
         }
      }

   }

   protected void mouseClicked(int p_73864_1_, int p_73864_2_, int p_73864_3_) {
      super.mouseClicked(p_73864_1_, p_73864_2_, p_73864_3_);
      Object object = this.field_104025_t;
      synchronized(this.field_104025_t) {
         if (this.field_92025_p.length() > 0 && p_73864_1_ >= this.field_92022_t && p_73864_1_ <= this.field_92020_v && p_73864_2_ >= this.field_92021_u && p_73864_2_ <= this.field_92019_w) {
            GuiConfirmOpenLink guiconfirmopenlink = new GuiConfirmOpenLink(this, this.field_104024_v, 13, true);
            guiconfirmopenlink.func_146358_g();
            this.mc.displayGuiScreen(guiconfirmopenlink);
         }

      }
   }

   static {
      field_96138_a = "Please click " + EnumChatFormatting.UNDERLINE + "here" + EnumChatFormatting.RESET + " for more information.";
   }

   public static class Staff {
      public int[] keys = null;
      public String[] phrases = null;

      public Staff(int[] pKeys, String[] pPhrases) {
         this.phrases = pPhrases;
         this.keys = pKeys;
      }
   }
}
