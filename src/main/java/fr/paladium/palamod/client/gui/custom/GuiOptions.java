package fr.paladium.palamod.client.gui.custom;

import fr.paladium.palamod.client.gui.GuiButtonPala;
import fr.paladium.palamod.client.gui.GuiOptionSliderPala;
import fr.paladium.palamod.util.GuiDrawer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiControls;
import net.minecraft.client.gui.GuiLanguage;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiScreenOptionsSounds;
import net.minecraft.client.gui.GuiScreenResourcePacks;
import net.minecraft.client.gui.GuiVideoSettings;
import net.minecraft.client.gui.ScreenChatOptions;
import net.minecraft.client.gui.stream.GuiStreamOptions;
import net.minecraft.client.gui.stream.GuiStreamUnavailable;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.settings.GameSettings.Options;
import net.minecraft.client.stream.IStream;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiOptions extends GuiScreen {
   private ResourceLocation background = new ResourceLocation("palamod", "textures/gui/LOGO.png");
   private GuiScreen previousGui;
   private GameSettings settings;

   public GuiOptions(GuiScreen prev, GameSettings setting) {
      this.previousGui = prev;
      this.settings = setting;
   }

   public void initGui() {
      boolean i = false;
      int centerX = this.width / 2;
      int centerY = this.height / 6;
      Options options = Options.FOV;
      this.buttonList.add(new GuiButtonPala(0, centerX - 155, centerY + 80, 150, 20, I18n.format("options.sounds", new Object[0])));
      this.buttonList.add(new GuiButtonPala(1, centerX - 155, centerY + 100, 150, 20, I18n.format("options.video", new Object[0])));
      this.buttonList.add(new GuiButtonPala(2, centerX - 155, centerY + 120, 150, 20, I18n.format("options.language", new Object[0])));
      this.buttonList.add(new GuiButtonPala(3, centerX - 155, centerY + 140, 150, 20, I18n.format("options.resourcepack", new Object[0])));
      this.buttonList.add(new GuiOptionSliderPala(4, centerX + 5, centerY + 80, options, false));
      this.buttonList.add(new GuiButtonPala(5, centerX + 5, centerY + 100, 150, 20, I18n.format("options.stream", new Object[0])));
      this.buttonList.add(new GuiButtonPala(6, centerX + 5, centerY + 120, 150, 20, I18n.format("options.controls", new Object[0])));
      this.buttonList.add(new GuiButtonPala(7, centerX + 5, centerY + 140, 150, 20, I18n.format("options.multiplayer.title", new Object[0])));
      this.buttonList.add(new GuiButtonPala(8, centerX - 50, centerY + 160, 100, 20, I18n.format("gui.done", new Object[0]), true));
   }

   protected void actionPerformed(GuiButton button) {
      switch(button.id) {
      case 0:
         this.changeOption(new GuiScreenOptionsSounds(this, this.settings));
         break;
      case 1:
         this.changeOption(new GuiVideoSettings(this, this.settings));
         break;
      case 2:
         this.changeOption(new GuiLanguage(this, this.settings, this.mc.getLanguageManager()));
         break;
      case 3:
         this.changeOption(new GuiScreenResourcePacks(this));
      case 4:
      default:
         break;
      case 5:
         this.mc.gameSettings.saveOptions();
         IStream istream = this.mc.func_152346_Z();
         if (istream.func_152936_l() && istream.func_152928_D()) {
            this.mc.displayGuiScreen(new GuiStreamOptions(this, this.settings));
         } else {
            GuiStreamUnavailable.func_152321_a(this);
         }
         break;
      case 6:
         this.changeOption(new GuiControls(this, this.settings));
         break;
      case 7:
         this.changeOption(new ScreenChatOptions(this, this.settings));
         break;
      case 8:
         this.changeOption(this.previousGui);
      }

   }

   private void changeOption(GuiScreen gui) {
      this.mc.gameSettings.saveOptions();
      this.mc.displayGuiScreen(gui);
   }

   public void drawDefaultBackground() {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      int centerX = this.width / 2;
      int centerY = this.height / 6;
      this.mc.getTextureManager().bindTexture(this.background);
      GuiDrawer.drawTexturedQuadFit((double)(centerX - 125), (double)(centerY - 40), 250.0D, 150.0D, 0.0D);
   }

   public void drawScreen(int x, int y, float par3) {
      this.drawDefaultBackground();
      super.drawScreen(x, y, par3);
   }
}
