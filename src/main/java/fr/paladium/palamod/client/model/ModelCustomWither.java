package fr.paladium.palamod.client.model;

import fr.paladium.palamod.entities.mobs.EntityCustomWither;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MathHelper;

public class ModelCustomWither extends ModelBase {
   private ModelRenderer[] field_82905_a;
   private ModelRenderer[] field_82904_b;

   public ModelCustomWither() {
      this.textureWidth = 64;
      this.textureHeight = 64;
      this.field_82905_a = new ModelRenderer[3];
      this.field_82905_a[0] = new ModelRenderer(this, 0, 16);
      this.field_82905_a[0].addBox(-10.0F, 3.9F, -0.5F, 20, 3, 3);
      this.field_82905_a[1] = (new ModelRenderer(this)).setTextureSize(this.textureWidth, this.textureHeight);
      this.field_82905_a[1].setRotationPoint(-2.0F, 6.9F, -0.5F);
      this.field_82905_a[1].setTextureOffset(0, 22).addBox(0.0F, 0.0F, 0.0F, 3, 10, 3);
      this.field_82905_a[1].setTextureOffset(24, 22).addBox(-4.0F, 1.5F, 0.5F, 11, 2, 2);
      this.field_82905_a[1].setTextureOffset(24, 22).addBox(-4.0F, 4.0F, 0.5F, 11, 2, 2);
      this.field_82905_a[1].setTextureOffset(24, 22).addBox(-4.0F, 6.5F, 0.5F, 11, 2, 2);
      this.field_82905_a[2] = new ModelRenderer(this, 12, 22);
      this.field_82905_a[2].addBox(0.0F, 0.0F, 0.0F, 3, 6, 3);
      this.field_82904_b = new ModelRenderer[3];
      this.field_82904_b[0] = new ModelRenderer(this, 0, 0);
      this.field_82904_b[0].addBox(-4.0F, -4.0F, -4.0F, 8, 8, 8);
      this.field_82904_b[1] = new ModelRenderer(this, 32, 0);
      this.field_82904_b[1].addBox(-4.0F, -4.0F, -4.0F, 6, 6, 6);
      this.field_82904_b[1].rotationPointX = -8.0F;
      this.field_82904_b[1].rotationPointY = 4.0F;
      this.field_82904_b[2] = new ModelRenderer(this, 32, 0);
      this.field_82904_b[2].addBox(-4.0F, -4.0F, -4.0F, 6, 6, 6);
      this.field_82904_b[2].rotationPointX = 10.0F;
      this.field_82904_b[2].rotationPointY = 4.0F;
   }

   public int func_82903_a() {
      return 32;
   }

   public void render(Entity p_render_1_, float p_render_2_, float p_render_3_, float p_render_4_, float p_render_5_, float p_render_6_, float p_render_7_) {
      this.setRotationAngles(p_render_2_, p_render_3_, p_render_4_, p_render_5_, p_render_6_, p_render_7_, p_render_1_);
      ModelRenderer[] var8 = this.field_82904_b;
      int var9 = var8.length;

      int var10;
      ModelRenderer var11;
      for(var10 = 0; var10 < var9; ++var10) {
         var11 = var8[var10];
         var11.render(p_render_7_);
      }

      var8 = this.field_82905_a;
      var9 = var8.length;

      for(var10 = 0; var10 < var9; ++var10) {
         var11 = var8[var10];
         var11.render(p_render_7_);
      }

   }

   public void setRotationAngles(float p_setRotationAngles_1_, float p_setRotationAngles_2_, float p_setRotationAngles_3_, float p_setRotationAngles_4_, float p_setRotationAngles_5_, float p_setRotationAngles_6_, Entity p_setRotationAngles_7_) {
      float var8 = MathHelper.cos(p_setRotationAngles_3_ * 0.1F);
      this.field_82905_a[1].rotateAngleX = (0.065F + 0.05F * var8) * 3.1415927F;
      this.field_82905_a[2].setRotationPoint(-2.0F, 6.9F + MathHelper.cos(this.field_82905_a[1].rotateAngleX) * 10.0F, -0.5F + MathHelper.sin(this.field_82905_a[1].rotateAngleX) * 10.0F);
      this.field_82905_a[2].rotateAngleX = (0.265F + 0.1F * var8) * 3.1415927F;
      this.field_82904_b[0].rotateAngleY = p_setRotationAngles_4_ / 57.295776F;
      this.field_82904_b[0].rotateAngleX = p_setRotationAngles_5_ / 57.295776F;
   }

   public void setLivingAnimations(EntityLivingBase p_setLivingAnimations_1_, float p_setLivingAnimations_2_, float p_setLivingAnimations_3_, float p_setLivingAnimations_4_) {
      EntityCustomWither var5 = (EntityCustomWither)p_setLivingAnimations_1_;

      for(int var6 = 1; var6 < 3; ++var6) {
         this.field_82904_b[var6].rotateAngleY = (var5.func_82207_a(var6 - 1) - p_setLivingAnimations_1_.renderYawOffset) / 57.295776F;
         this.field_82904_b[var6].rotateAngleX = var5.func_82210_r(var6 - 1) / 57.295776F;
      }

   }
}
