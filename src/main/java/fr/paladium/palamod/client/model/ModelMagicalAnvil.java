package fr.paladium.palamod.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelMagicalAnvil extends ModelBase {
   public ModelRenderer Elem19;
   public ModelRenderer Elem18;
   public ModelRenderer Elem17;
   public ModelRenderer Elem16;
   public ModelRenderer Elem15;
   public ModelRenderer Elem14;
   public ModelRenderer Elem24;
   public ModelRenderer Elem13;
   public ModelRenderer Elem23;
   public ModelRenderer Elem12;
   public ModelRenderer Elem22;
   public ModelRenderer Elem11;
   public ModelRenderer Elem21;
   public ModelRenderer Elem10;
   public ModelRenderer Elem20;
   public ModelRenderer Elem9;
   public ModelRenderer Elem7;
   public ModelRenderer Elem8;
   public ModelRenderer Elem5;
   public ModelRenderer Elem6;
   public ModelRenderer Elem4;

   public ModelMagicalAnvil() {
      this.textureWidth = 64;
      this.textureHeight = 64;
      this.Elem13 = new ModelRenderer(this, 26, 15);
      this.Elem13.setRotationPoint(-2.5F, 20.5F, -4.0F);
      this.Elem13.addBox(0.0F, 0.0F, 0.0F, 5, 4, 8, 0.0F);
      this.Elem24 = new ModelRenderer(this, 0, 0);
      this.Elem24.setRotationPoint(-1.0F, 22.0F, 5.0F);
      this.Elem24.addBox(0.0F, 0.0F, 0.0F, 2, 1, 1, 0.0F);
      this.Elem12 = new ModelRenderer(this, 14, 4);
      this.Elem12.setRotationPoint(-5.0F, 11.5F, 7.0F);
      this.Elem12.addBox(0.0F, 0.0F, 0.0F, 10, 2, 1, 0.0F);
      this.Elem16 = new ModelRenderer(this, 51, 2);
      this.Elem16.setRotationPoint(-2.0F, 21.5F, -5.0F);
      this.Elem16.addBox(0.0F, 0.0F, 0.0F, 4, 3, 1, 0.0F);
      this.Elem18 = new ModelRenderer(this, 46, 0);
      this.Elem18.setRotationPoint(-2.0F, 11.0F, -2.5F);
      this.Elem18.addBox(0.0F, 0.0F, 0.0F, 4, 1, 1, 0.0F);
      this.Elem15 = new ModelRenderer(this, 0, 3);
      this.Elem15.setRotationPoint(-2.5F, 11.0F, -2.0F);
      this.Elem15.addBox(0.0F, 0.0F, 0.0F, 5, 1, 4, 0.0F);
      this.Elem14 = new ModelRenderer(this, 0, 8);
      this.Elem14.setRotationPoint(-2.5F, 13.5F, -4.0F);
      this.Elem14.addBox(0.0F, 0.0F, 0.0F, 5, 1, 8, 0.0F);
      this.Elem5 = new ModelRenderer(this, 32, 30);
      this.Elem5.setRotationPoint(-7.5F, 11.5F, -6.0F);
      this.Elem5.addBox(0.0F, 0.0F, 0.0F, 15, 2, 1, 0.0F);
      this.Elem9 = new ModelRenderer(this, 0, 23);
      this.Elem9.setRotationPoint(-6.0F, 11.5F, -7.5F);
      this.Elem9.addBox(0.0F, 0.0F, 0.0F, 12, 2, 1, 0.0F);
      this.Elem7 = new ModelRenderer(this, 28, 27);
      this.Elem7.setRotationPoint(-6.5F, 11.5F, -6.5F);
      this.Elem7.addBox(0.0F, 0.0F, 0.0F, 13, 2, 1, 0.0F);
      this.Elem23 = new ModelRenderer(this, 6, 0);
      this.Elem23.setRotationPoint(-1.0F, 23.0F, 5.0F);
      this.Elem23.addBox(0.0F, 0.0F, 0.0F, 2, 1, 1, 0.0F);
      this.Elem10 = new ModelRenderer(this, 0, 20);
      this.Elem10.setRotationPoint(-6.0F, 11.5F, 6.5F);
      this.Elem10.addBox(0.0F, 0.0F, 0.0F, 12, 2, 1, 0.0F);
      this.Elem19 = new ModelRenderer(this, 36, 0);
      this.Elem19.setRotationPoint(-2.0F, 11.0F, 1.5F);
      this.Elem19.addBox(0.0F, 0.0F, 0.0F, 4, 1, 1, 0.0F);
      this.Elem21 = new ModelRenderer(this, 18, 0);
      this.Elem21.setRotationPoint(-1.0F, 23.0F, -6.0F);
      this.Elem21.addBox(0.0F, 0.0F, 0.0F, 2, 1, 1, 0.0F);
      this.Elem8 = new ModelRenderer(this, 0, 27);
      this.Elem8.setRotationPoint(-6.5F, 11.5F, 5.5F);
      this.Elem8.addBox(0.0F, 0.0F, 0.0F, 13, 2, 1, 0.0F);
      this.Elem17 = new ModelRenderer(this, 36, 2);
      this.Elem17.setRotationPoint(-2.0F, 14.5F, -3.5F);
      this.Elem17.addBox(0.0F, 0.0F, 0.0F, 4, 6, 7, 0.0F);
      this.Elem11 = new ModelRenderer(this, 0, 17);
      this.Elem11.setRotationPoint(-5.0F, 11.5F, -8.0F);
      this.Elem11.addBox(0.0F, 0.0F, 0.0F, 10, 2, 1, 0.0F);
      this.Elem6 = new ModelRenderer(this, 0, 30);
      this.Elem6.setRotationPoint(-7.5F, 11.5F, 5.0F);
      this.Elem6.addBox(0.0F, 0.0F, 0.0F, 15, 2, 1, 0.0F);
      this.Elem4 = new ModelRenderer(this, 0, 33);
      this.Elem4.setRotationPoint(-8.0F, 11.5F, -5.0F);
      this.Elem4.addBox(0.0F, 0.0F, 0.0F, 16, 2, 10, 0.0F);
      this.Elem22 = new ModelRenderer(this, 12, 0);
      this.Elem22.setRotationPoint(-1.0F, 22.0F, -6.0F);
      this.Elem22.addBox(0.0F, 0.0F, 0.0F, 2, 1, 1, 0.0F);
      this.Elem20 = new ModelRenderer(this, 26, 0);
      this.Elem20.setRotationPoint(-2.0F, 21.5F, 4.0F);
      this.Elem20.addBox(0.0F, 0.0F, 0.0F, 4, 3, 1, 0.0F);
   }

   public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
      this.Elem13.render(f5);
      this.Elem24.render(f5);
      this.Elem12.render(f5);
      this.Elem16.render(f5);
      this.Elem18.render(f5);
      this.Elem15.render(f5);
      this.Elem14.render(f5);
      this.Elem5.render(f5);
      this.Elem9.render(f5);
      this.Elem7.render(f5);
      this.Elem23.render(f5);
      this.Elem10.render(f5);
      this.Elem19.render(f5);
      this.Elem21.render(f5);
      this.Elem8.render(f5);
      this.Elem17.render(f5);
      this.Elem11.render(f5);
      this.Elem6.render(f5);
      this.Elem4.render(f5);
      this.Elem22.render(f5);
      this.Elem20.render(f5);
   }

   public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
      modelRenderer.rotateAngleX = x;
      modelRenderer.rotateAngleY = y;
      modelRenderer.rotateAngleZ = z;
   }

   public void renderModel(float f5) {
      this.Elem24.render(f5);
      this.Elem23.render(f5);
      this.Elem22.render(f5);
      this.Elem21.render(f5);
      this.Elem20.render(f5);
      this.Elem19.render(f5);
      this.Elem18.render(f5);
      this.Elem17.render(f5);
      this.Elem16.render(f5);
      this.Elem15.render(f5);
      this.Elem14.render(f5);
      this.Elem13.render(f5);
      this.Elem12.render(f5);
      this.Elem11.render(f5);
      this.Elem10.render(f5);
      this.Elem9.render(f5);
      this.Elem8.render(f5);
      this.Elem7.render(f5);
      this.Elem6.render(f5);
      this.Elem5.render(f5);
      this.Elem4.render(f5);
   }
}
