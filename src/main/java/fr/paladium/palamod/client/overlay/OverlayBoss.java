package fr.paladium.palamod.client.overlay;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.client.CustomBossStatus;
import fr.paladium.palamod.util.DisplayHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderGameOverlayEvent.Pre;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class OverlayBoss {
   Minecraft mc = Minecraft.getMinecraft();
   private ResourceLocation boss = new ResourceLocation("palamod:textures/gui/bossBar.png");

   @SubscribeEvent(
      priority = EventPriority.NORMAL
   )
   public void onRender(Pre event) {
      if (event.type == ElementType.BOSSHEALTH) {
         if (CustomBossStatus.bossName == null) {
            return;
         }

         CustomBossStatus.update();
         event.setCanceled(true);
         FontRenderer fr = Minecraft.getMinecraft().fontRenderer;
         ScaledResolution scaledresolution = new ScaledResolution(this.mc, this.mc.displayWidth, this.mc.displayHeight);
         int i = scaledresolution.getScaledWidth();
         short short1 = 182;
         int j = i / 2 - short1 / 2;
         int k = (int)(CustomBossStatus.healthScale * (float)(short1 + 1));
         byte b0 = 12;
         String s = CustomBossStatus.bossName;
         fr.drawStringWithShadow(s, i / 2 - fr.getStringWidth(s) / 2, b0 + 7, 16777215);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         this.mc.getTextureManager().bindTexture(Gui.icons);
         this.mc.getTextureManager().bindTexture(this.boss);
         int size = (int)(181.0F * CustomBossStatus.healthScale);
         DisplayHelper.drawTexturedModalRect(i / 2 - 92, 3, 1.0F, 0, 0, 185, 15);
         DisplayHelper.drawTexturedModalRect(i / 2 - 90, 7, 2.0F, 0, 15, k, 7);
      }

   }
}
