package fr.paladium.palamod.client.overlay;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent.Pre;

public class OverlayBow extends Gui {
   Minecraft mc = Minecraft.getMinecraft();
   final ResourceLocation tex = new ResourceLocation("palamod", "textures/overlay/Crosshair_0.png");

   @SubscribeEvent(
      priority = EventPriority.NORMAL
   )
   public void onRender(Pre event) {
   }
}
