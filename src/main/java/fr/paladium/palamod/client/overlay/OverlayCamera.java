package fr.paladium.palamod.client.overlay;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import fr.paladium.palamod.blocks.BlockCamera;
import fr.paladium.palamod.entities.mobs.EntityCamera;
import fr.paladium.palamod.items.ItemCameraTablet;
import fr.paladium.palamod.tiles.TileEntityCamera;
import java.util.Calendar;
import java.util.Date;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderGameOverlayEvent.Pre;
import org.lwjgl.opengl.GL11;

public class OverlayCamera extends Gui {
   FontRenderer fontRender;
   private Minecraft mc = Minecraft.getMinecraft();
   int fire = 20;

   public OverlayCamera() {
      this.fontRender = this.mc.fontRenderer;
   }

   @SubscribeEvent(
      priority = EventPriority.NORMAL
   )
   public void onRender(Pre event) {
      if (event.type == ElementType.HOTBAR) {
         ItemStack stack = this.mc.thePlayer.getHeldItem();
         if (stack != null && stack.getItem() instanceof ItemCameraTablet && this.mc.theWorld.isRemote && !this.mc.thePlayer.isSneaking()) {
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glDisable(2896);
            GL11.glEnable(3042);
            GL11.glBlendFunc(770, 771);
            NBTTagCompound compound = stack.getTagCompound();
            if (compound != null) {
               boolean flag = false;
               int x = 0;
               int y = 0;
               int z = 0;
               boolean yaw = false;
               int index = compound.getInteger("Index");
               int yaw1;
               if (index == 0) {
                  if (compound.getBoolean("cam1")) {
                     x = compound.getInteger("cam1X");
                     y = compound.getInteger("cam1Y");
                     z = compound.getInteger("cam1Z");
                     yaw1 = compound.getInteger("cam1Yaw");
                     flag = false;
                  } else {
                     compound.setInteger("Index", 1);
                     index = 1;
                     flag = true;
                  }
               }

               if (index == 1) {
                  if (compound.getBoolean("cam2")) {
                     x = compound.getInteger("cam2X");
                     y = compound.getInteger("cam2Y");
                     z = compound.getInteger("cam2Z");
                     yaw1 = compound.getInteger("cam2Yaw");
                     flag = false;
                  } else {
                     compound.setInteger("Index", 2);
                     index = 2;
                     flag = true;
                  }
               }

               if (index == 2) {
                  if (compound.getBoolean("cam3")) {
                     x = compound.getInteger("cam3X");
                     y = compound.getInteger("cam3Y");
                     z = compound.getInteger("cam3Z");
                     yaw1 = compound.getInteger("cam3Yaw");
                     flag = false;
                  } else {
                     compound.setInteger("Index", 0);
                     index = 0;
                     flag = true;
                  }
               }

               flag = index != 0 && index != 1 && index != 2;
               if (flag) {
                  EntityCamera.destroyCamera();
                  return;
               }

               this.mc.thePlayer.worldObj.getChunkProvider().loadChunk(x, z);
               if (this.mc.thePlayer.worldObj.getBlock(x, y, z) instanceof BlockCamera) {
                  Calendar calendar = Calendar.getInstance();
                  calendar.setTime(new Date());
                  String hour = this.dbl(calendar.get(11)) + ":" + this.dbl(calendar.get(12)) + ":" + this.dbl(calendar.get(13));
                  String date = this.dbl(calendar.get(5)) + "/" + this.dbl(calendar.get(2) + 1) + "/" + calendar.get(1);
                  String time = hour + " - " + date;
                  if (this.fire <= 10) {
                     this.fontRender.drawString("Recording â€¢", 10, 10, 16711680);
                  } else {
                     this.fontRender.drawString("Recording", 10, 10, 16711680);
                  }

                  --this.fire;
                  if (this.fire <= 0) {
                     this.fire = 20;
                  }

                  this.fontRender.drawString(time, event.resolution.getScaledWidth() - 10 - this.fontRender.getStringWidth(time), 10, 16711680);
                  float rotation = 0.0F;
                  switch(Minecraft.getMinecraft().theWorld.getBlockMetadata(x, y - 2, z)) {
                  case 0:
                     rotation = 0.0F;
                     break;
                  case 1:
                     rotation = 0.0F;
                     break;
                  case 2:
                     rotation = 0.0F;
                     break;
                  case 3:
                     rotation = 0.0F;
                  }

                  TileEntity tile = Minecraft.getMinecraft().theWorld.getTileEntity(x, y, z);
                  x = (int)((double)x + 1.5D);
                  z = (int)((double)z + 1.5D);
                  if (tile != null && tile instanceof TileEntityCamera) {
                     EntityCamera.createCamera((double)x, (double)y, (double)z, (TileEntityCamera)tile, 40.0F, rotation * 90.0F);
                  } else {
                     EntityCamera.destroyCamera();
                  }

                  event.setCanceled(true);
                  return;
               }

               switch(index) {
               case 0:
                  compound.setBoolean("cam1", false);
                  break;
               case 1:
                  compound.setBoolean("cam2", false);
                  break;
               case 2:
                  compound.setBoolean("cam3", false);
               }
            }
         }

         EntityCamera.destroyCamera();
      }
   }

   private String dbl(int a) {
      return a <= 9 ? "0" + a : "" + a;
   }

   private float calcYaw(int metadata) {
      switch(metadata) {
      case 0:
         return 5.0F;
      case 1:
         return 6.0F;
      case 2:
         return 7.0F;
      case 3:
         return 0.0F;
      case 4:
         return 1.0F;
      case 5:
         return 2.0F;
      case 6:
         return 3.0F;
      case 7:
         return 4.0F;
      default:
         return 0.0F;
      }
   }
}
