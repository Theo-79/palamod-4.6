package fr.paladium.palamod.client.overlay;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.items.ItemGuardianWand;
import fr.paladium.palamod.util.DisplayHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderGameOverlayEvent.Pre;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class OverlayGolem {
   Minecraft mc = Minecraft.getMinecraft();
   FontRenderer fr;
   ResourceLocation background;

   public OverlayGolem() {
      this.fr = Minecraft.getMinecraft().fontRenderer;
      this.background = new ResourceLocation("palamod:textures/gui/GolemOverlay.png");
   }

   @SubscribeEvent(
      priority = EventPriority.NORMAL
   )
   public void onRender(Pre event) {
      if (event.type == ElementType.HOTBAR) {
         if (this.mc.thePlayer.getHeldItem() == null || !(this.mc.thePlayer.getHeldItem().getItem() instanceof ItemGuardianWand)) {
            return;
         }

         ItemStack wand = this.mc.thePlayer.getHeldItem();
         if (!ItemGuardianWand.isGolemLoaded(wand, this.mc.thePlayer.worldObj)) {
            return;
         }

         ScaledResolution res = new ScaledResolution(this.mc, this.mc.displayWidth, this.mc.displayHeight);
         int width = res.getScaledWidth();
         int height = res.getScaledHeight();
         int x = width - 45;
         int y = 60;
         int size = 20;
         float rotX = 20.0F;
         float rotY = 0.0F;
         GL11.glPushMatrix();
         GL11.glTranslatef((float)x, (float)y, 50.0F);
         EntityGuardianGolem golem = ItemGuardianWand.getGolem(wand, this.mc.theWorld);
         this.renderGolem(0, 0, size, rotX, rotY, golem);
         GL11.glScaled(0.6D, 0.6D, 0.0D);
         this.fr.drawStringWithShadow(golem.getName(), -35, 10, 16777215);
         GL11.glScaled(1.6666666666666667D, 1.6666666666666667D, 0.0D);
         this.mc.renderEngine.bindTexture(this.background);
         GL11.glScalef(0.85F, 0.85F, 0.0F);
         GL11.glEnable(3042);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 0.5F);
         DisplayHelper.drawTexturedModalRect(-35, -66, 0.0F, 0, 21, 71, 90);
         GL11.glPopMatrix();
      }

   }

   public void renderGolem(int x, int y, int size, float rotX, float rotY, EntityGuardianGolem golem) {
      GL11.glEnable(2903);
      GL11.glPushMatrix();
      GL11.glTranslatef((float)x, (float)y, 50.0F);
      GL11.glScalef((float)(-size), (float)size, (float)size);
      GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
      float f2 = golem.renderYawOffset;
      float f3 = golem.rotationYaw;
      float f4 = golem.rotationPitch;
      float f5 = golem.prevRotationYawHead;
      float f6 = golem.rotationYawHead;
      GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
      GL11.glRotatef(-135.0F, 0.0F, 1.0F, 0.0F);
      GL11.glRotatef(-((float)Math.atan((double)(rotX / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
      golem.renderYawOffset = (float)Math.atan((double)(rotX / 40.0F)) * 20.0F;
      golem.rotationYaw = (float)Math.atan((double)(rotY / 40.0F)) * 40.0F;
      golem.rotationPitch = -((float)Math.atan((double)(rotY / 40.0F))) * 20.0F;
      golem.rotationYawHead = golem.rotationYaw;
      golem.prevRotationYawHead = golem.rotationYaw;
      GL11.glTranslatef(0.0F, golem.yOffset, 0.0F);
      RenderManager.instance.playerViewY = 180.0F;
      RenderManager.instance.renderEntityWithPosYaw(golem, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
      golem.renderYawOffset = f2;
      golem.rotationYaw = f3;
      golem.rotationPitch = f4;
      golem.prevRotationYawHead = f5;
      golem.rotationYawHead = f6;
      GL11.glPopMatrix();
      GL11.glDisable(32826);
      OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
      GL11.glDisable(3553);
      OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
   }
}
