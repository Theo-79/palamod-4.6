package fr.paladium.palamod.client.overlay;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.client.DisplayMessage;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderGameOverlayEvent.Pre;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class OverlayMessage {
   public static long time;
   public static ArrayList<DisplayMessage> messages = new ArrayList();
   public static Minecraft mc = Minecraft.getMinecraft();
   public static FontRenderer fr;

   public static void addMessageToQueue(DisplayMessage message) {
      messages.add(message);
   }

   public static void removeLast() {
      if (messages.size() >= 1) {
         messages.remove(messages.size() - 1);
      }
   }

   public static DisplayMessage getLast() {
      return messages.size() < 1 ? null : (DisplayMessage)messages.get(messages.size() - 1);
   }

   public static void reset() {
      time = 0L;
      removeLast();
   }

   @SubscribeEvent(
      priority = EventPriority.NORMAL
   )
   public void onMessage(Pre event) {
      if (event.type == ElementType.CROSSHAIRS) {
         if (!messages.isEmpty()) {
            DisplayMessage message = getLast();
            if (time == 0L) {
               time = System.currentTimeMillis() + (long)(message.time * 1000);
            }

            event.setCanceled(true);
            String title = message.title;
            String subTitle = message.subTitle;
            title = title.replaceAll("&", "§").replaceAll("_", " ");
            subTitle = subTitle.replaceAll("&", "§").replaceAll("_", " ");
            ScaledResolution scaledresolution = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
            int i = scaledresolution.getScaledWidth();
            int j = scaledresolution.getScaledHeight();
            int x1 = (i - fr.getStringWidth(title) * 3) / 2;
            int y1 = (j - 35) / 2;
            GL11.glPushMatrix();
            GL11.glTranslated((double)x1, (double)y1, 0.0D);
            GL11.glScaled(3.0D, 3.0D, 3.0D);
            fr.drawStringWithShadow(title, 0, 0, 16777215);
            GL11.glPopMatrix();
            int x2 = (i - fr.getStringWidth(subTitle)) / 2;
            int y2 = (int)((double)j / 1.8D);
            GL11.glPushMatrix();
            GL11.glTranslated((double)x2, (double)y2, 0.0D);
            fr.drawStringWithShadow(subTitle, 0, 0, 16777215);
            GL11.glPopMatrix();
            if (System.currentTimeMillis() >= time) {
               reset();
            }

         }
      }
   }

   static {
      fr = Minecraft.getMinecraft().fontRenderer;
   }
}
