package fr.paladium.palamod.client.overlay;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.client.commands.CommandHideBoss;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderGameOverlayEvent.Pre;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class OverlayMessageBoss {
   public static int currentTime = 0;
   public static long ms;
   public static int x;
   public static int z;
   FontRenderer fr;
   Minecraft mc;

   public OverlayMessageBoss() {
      this.fr = Minecraft.getMinecraft().fontRenderer;
      this.mc = Minecraft.getMinecraft();
   }

   @SubscribeEvent(
      priority = EventPriority.NORMAL
   )
   public void display(Pre event) {
      if (event.type == ElementType.HOTBAR) {
         if (currentTime != 0) {
            if (System.currentTimeMillis() >= ms + 1000L) {
               --currentTime;
               ms = System.currentTimeMillis();
            }

            if (!CommandHideBoss.hide) {
               GL11.glPushMatrix();
               this.fr.drawStringWithShadow("Prochain Boss dans ", 5, 5, 16777215);
               this.fr.drawStringWithShadow(this.getMinutes(currentTime) + ":" + this.getSeconds(currentTime), 109, 5, 1028394);
               this.fr.drawStringWithShadow("En x:" + x + " z:" + z, 5, 17, 16777215);
               GL11.glPopMatrix();
            }
         }
      }
   }

   public static void startCd(int x, int z, int time) {
      currentTime = time;
      OverlayMessageBoss.x = x;
      OverlayMessageBoss.z = z;
   }

   public int getMinutes(int time) {
      return time / 60;
   }

   public int getSeconds(int time) {
      return time % 60;
   }

   public static void reset() {
      currentTime = 0;
      ms = 0L;
   }
}
