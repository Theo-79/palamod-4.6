package fr.paladium.palamod.client.render.block;

import fr.paladium.palamod.client.model.ModelAlchemyCreator;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;

public class RenderAlchemyCreator extends TileEntitySpecialRenderer {
   private final ModelAlchemyCreator model = new ModelAlchemyCreator();
   private final ResourceLocation textures = new ResourceLocation("palamod:textures/blocks/AlchemyCreator.png");

   private void adjustRotatePivotViaMeta(World world, int x, int y, int z) {
      int meta = world.getBlockMetadata(x, y, z);
      GL11.glPushMatrix();
      GL11.glRotatef((float)(meta * -90), 0.0F, 0.0F, 1.0F);
      GL11.glPopMatrix();
   }

   public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float f) {
      GL11.glPushMatrix();
      GL11.glTranslatef((float)x + 0.5F, (float)y + 1.5F, (float)z + 0.5F);
      Minecraft.getMinecraft().renderEngine.bindTexture(this.textures);
      GL11.glPushMatrix();
      int meta = Minecraft.getMinecraft().theWorld.getBlockMetadata(tile.xCoord, tile.yCoord, tile.zCoord);
      int id = 0;
      switch(meta) {
      case 0:
         id = 0;
      case 1:
      case 2:
      default:
         break;
      case 3:
         id = 2;
         break;
      case 4:
         id = 3;
         break;
      case 5:
         id = 1;
      }

      GL11.glRotatef((float)(id * -90), 0.0F, 1.0F, 0.0F);
      GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
      this.model.render((Entity)null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
      GL11.glPopMatrix();
   }
}
