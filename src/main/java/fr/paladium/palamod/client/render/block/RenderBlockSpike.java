package fr.paladium.palamod.client.render.block;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.client.ClientProxy;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.Facing;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class RenderBlockSpike implements ISimpleBlockRenderingHandler {
   public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {
      Tessellator t = Tessellator.instance;
      GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
      this.renderSpikeBlock(Minecraft.getMinecraft().theWorld, 0, 0, 0, 1, 0, block, renderer, -1);
      GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.5F, 0.5F, 0.5F);
   }

   public boolean renderSpikeBlock(IBlockAccess world, int x, int y, int z, int side, int type, Block block, RenderBlocks renderer, int brightness) {
      float ax = 0.0F;
      float ay = 0.0F;
      float az = 0.0F;
      float bx = 1.0F;
      float by = 0.0F;
      float bz = 0.0F;
      float cx = 0.0F;
      float cy = 0.0F;
      float cz = 1.0F;
      float dx = 1.0F;
      float dy = 0.0F;
      float dz = 1.0F;
      float ex = 0.0F;
      float ey = 1.0F;
      float ez = 0.0F;
      float fx = 1.0F;
      float fy = 1.0F;
      float fz = 0.0F;
      float gx = 0.0F;
      float gy = 1.0F;
      float gz = 1.0F;
      float hx = 1.0F;
      float hy = 1.0F;
      float hz = 1.0F;
      switch(side) {
      case 0:
         dz = 0.5F;
         dx = 0.5F;
         cz = 0.5F;
         cx = 0.5F;
         bz = 0.5F;
         bx = 0.5F;
         az = 0.5F;
         ax = 0.5F;
         break;
      case 1:
         hz = 0.5F;
         hx = 0.5F;
         gz = 0.5F;
         gx = 0.5F;
         fz = 0.5F;
         fx = 0.5F;
         ez = 0.5F;
         ex = 0.5F;
         break;
      case 2:
         fx = 0.5F;
         ex = 0.5F;
         bx = 0.5F;
         ax = 0.5F;
         fy = 0.5F;
         ey = 0.5F;
         by = 0.5F;
         ay = 0.5F;
         break;
      case 3:
         hx = 0.5F;
         gx = 0.5F;
         dx = 0.5F;
         cx = 0.5F;
         hy = 0.5F;
         gy = 0.5F;
         dy = 0.5F;
         cy = 0.5F;
         break;
      case 4:
         gz = 0.5F;
         ez = 0.5F;
         cz = 0.5F;
         az = 0.5F;
         gy = 0.5F;
         ey = 0.5F;
         cy = 0.5F;
         ay = 0.5F;
         break;
      case 5:
         hz = 0.5F;
         fz = 0.5F;
         dz = 0.5F;
         bz = 0.5F;
         hy = 0.5F;
         fy = 0.5F;
         dy = 0.5F;
         by = 0.5F;
         break;
      default:
         return false;
      }

      IIcon texture = block.getIcon(side, type);
      if (renderer.hasOverrideBlockTexture()) {
         texture = renderer.overrideBlockTexture;
      }

      Tessellator tessellator = Tessellator.instance;
      if (brightness >= 0) {
         tessellator.setBrightness(brightness);
      }

      boolean inventory = brightness < 0;
      if (brightness >= 0) {
         tessellator.setColorOpaque_F(0.5F, 0.5F, 0.5F);
      }

      if (!renderer.hasOverrideBlockTexture()) {
         texture = block.getIcon(0, side + type * 6);
      }

      if (inventory) {
         tessellator.startDrawingQuads();
         tessellator.setNormal(0.0F, -1.0F, 0.0F);
      }

      float[] u;
      float[] v;
      int rotation;
      if (side != 0) {
         u = new float[]{ax, bx, dx, cx};
         v = new float[]{az, bz, dz, cz};
         rotation = this.calcRotation(0, side);
         tessellator.addVertexWithUV((double)((float)x + ax), (double)((float)y + ay), (double)((float)z + az), (double)this.getU(0, texture, rotation, u, v), (double)this.getV(0, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + bx), (double)((float)y + by), (double)((float)z + bz), (double)this.getU(1, texture, rotation, u, v), (double)this.getV(1, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + dx), (double)((float)y + dy), (double)((float)z + dz), (double)this.getU(2, texture, rotation, u, v), (double)this.getV(2, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + cx), (double)((float)y + cy), (double)((float)z + cz), (double)this.getU(3, texture, rotation, u, v), (double)this.getV(3, texture, rotation, u, v));
      }

      if (inventory) {
         tessellator.draw();
      }

      if (inventory) {
         tessellator.startDrawingQuads();
         tessellator.setNormal(0.0F, 1.0F, 0.0F);
      }

      if (brightness >= 0) {
         tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
      }

      if (!renderer.hasOverrideBlockTexture()) {
         texture = block.getIcon(1, side + type * 6);
      }

      if (side != 1) {
         u = new float[]{ex, gx, hx, fx};
         v = new float[]{ez, gz, hz, fz};
         rotation = this.calcRotation(1, side);
         tessellator.addVertexWithUV((double)((float)x + ex), (double)((float)y + ey), (double)((float)z + ez), (double)this.getU(0, texture, rotation, u, v), (double)this.getV(0, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + gx), (double)((float)y + gy), (double)((float)z + gz), (double)this.getU(1, texture, rotation, u, v), (double)this.getV(1, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + hx), (double)((float)y + hy), (double)((float)z + hz), (double)this.getU(2, texture, rotation, u, v), (double)this.getV(2, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + fx), (double)((float)y + fy), (double)((float)z + fz), (double)this.getU(3, texture, rotation, u, v), (double)this.getV(3, texture, rotation, u, v));
      }

      if (inventory) {
         tessellator.draw();
      }

      if (brightness >= 0) {
         if (side == 0) {
            tessellator.setColorOpaque_F(0.65F, 0.65F, 0.65F);
         } else if (side == 1) {
            tessellator.setColorOpaque_F(0.9F, 0.9F, 0.9F);
         } else {
            tessellator.setColorOpaque_F(0.8F, 0.8F, 0.8F);
         }
      }

      if (!renderer.hasOverrideBlockTexture()) {
         texture = block.getIcon(2, side + type * 6);
      }

      if (inventory) {
         tessellator.startDrawingQuads();
         tessellator.setNormal(0.0F, 0.445F, 0.894F);
      }

      if (side != 2) {
         u = new float[]{1.0F - ax, 1.0F - ex, 1.0F - fx, 1.0F - bx};
         v = new float[]{1.0F - ay, 1.0F - ey, 1.0F - fy, 1.0F - by};
         rotation = this.calcRotation(2, side);
         tessellator.addVertexWithUV((double)((float)x + ax), (double)((float)y + ay), (double)((float)z + az), (double)this.getU(0, texture, rotation, u, v), (double)this.getV(0, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + ex), (double)((float)y + ey), (double)((float)z + ez), (double)this.getU(1, texture, rotation, u, v), (double)this.getV(1, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + fx), (double)((float)y + fy), (double)((float)z + fz), (double)this.getU(2, texture, rotation, u, v), (double)this.getV(2, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + bx), (double)((float)y + by), (double)((float)z + bz), (double)this.getU(3, texture, rotation, u, v), (double)this.getV(3, texture, rotation, u, v));
      }

      if (inventory) {
         tessellator.draw();
      }

      if (brightness >= 0) {
         if (side == 0) {
            tessellator.setColorOpaque_F(0.65F, 0.65F, 0.65F);
         } else if (side == 1) {
            tessellator.setColorOpaque_F(0.9F, 0.9F, 0.9F);
         } else {
            tessellator.setColorOpaque_F(0.8F, 0.8F, 0.8F);
         }
      }

      if (!renderer.hasOverrideBlockTexture()) {
         texture = block.getIcon(3, side + type * 6);
      }

      if (inventory) {
         tessellator.startDrawingQuads();
         tessellator.setNormal(0.0F, 0.445F, -0.894F);
      }

      if (side != 3) {
         u = new float[]{dx, hx, gx, cx};
         v = new float[]{1.0F - dy, 1.0F - hy, 1.0F - gy, 1.0F - cy};
         rotation = this.calcRotation(3, side);
         tessellator.addVertexWithUV((double)((float)x + dx), (double)((float)y + dy), (double)((float)z + dz), (double)this.getU(0, texture, rotation, u, v), (double)this.getV(0, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + hx), (double)((float)y + hy), (double)((float)z + hz), (double)this.getU(1, texture, rotation, u, v), (double)this.getV(1, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + gx), (double)((float)y + gy), (double)((float)z + gz), (double)this.getU(2, texture, rotation, u, v), (double)this.getV(2, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + cx), (double)((float)y + cy), (double)((float)z + cz), (double)this.getU(3, texture, rotation, u, v), (double)this.getV(3, texture, rotation, u, v));
      }

      if (inventory) {
         tessellator.draw();
      }

      if (brightness >= 0) {
         if (side == 0) {
            tessellator.setColorOpaque_F(0.55F, 0.55F, 0.55F);
         } else if (side == 1) {
            tessellator.setColorOpaque_F(0.7F, 0.7F, 0.7F);
         } else {
            tessellator.setColorOpaque_F(0.6F, 0.6F, 0.6F);
         }
      }

      if (!renderer.hasOverrideBlockTexture()) {
         texture = block.getIcon(4, side + type * 6);
      }

      if (inventory) {
         tessellator.startDrawingQuads();
         tessellator.setNormal(0.894F, 0.445F, 0.0F);
      }

      if (side != 4) {
         u = new float[]{cz, gz, ez, az};
         v = new float[]{1.0F - cy, 1.0F - gy, 1.0F - ey, 1.0F - ay};
         rotation = this.calcRotation(4, side);
         tessellator.addVertexWithUV((double)((float)x + cx), (double)((float)y + cy), (double)((float)z + cz), (double)this.getU(0, texture, rotation, u, v), (double)this.getV(0, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + gx), (double)((float)y + gy), (double)((float)z + gz), (double)this.getU(1, texture, rotation, u, v), (double)this.getV(1, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + ex), (double)((float)y + ey), (double)((float)z + ez), (double)this.getU(2, texture, rotation, u, v), (double)this.getV(2, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + ax), (double)((float)y + ay), (double)((float)z + az), (double)this.getU(3, texture, rotation, u, v), (double)this.getV(3, texture, rotation, u, v));
      }

      if (inventory) {
         tessellator.draw();
      }

      if (brightness >= 0) {
         if (side == 0) {
            tessellator.setColorOpaque_F(0.55F, 0.55F, 0.55F);
         } else if (side == 1) {
            tessellator.setColorOpaque_F(0.7F, 0.7F, 0.7F);
         } else {
            tessellator.setColorOpaque_F(0.6F, 0.6F, 0.6F);
         }
      }

      if (!renderer.hasOverrideBlockTexture()) {
         texture = block.getIcon(5, side + type * 6);
      }

      if (inventory) {
         tessellator.startDrawingQuads();
         tessellator.setNormal(-0.894F, 0.445F, 0.0F);
      }

      if (side != 5) {
         u = new float[]{1.0F - bz, 1.0F - fz, 1.0F - hz, 1.0F - dz};
         v = new float[]{1.0F - by, 1.0F - fy, 1.0F - hy, 1.0F - dy};
         rotation = this.calcRotation(5, side);
         tessellator.addVertexWithUV((double)((float)x + bx), (double)((float)y + by), (double)((float)z + bz), (double)this.getU(0, texture, rotation, u, v), (double)this.getV(0, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + fx), (double)((float)y + fy), (double)((float)z + fz), (double)this.getU(1, texture, rotation, u, v), (double)this.getV(1, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + hx), (double)((float)y + hy), (double)((float)z + hz), (double)this.getU(2, texture, rotation, u, v), (double)this.getV(2, texture, rotation, u, v));
         tessellator.addVertexWithUV((double)((float)x + dx), (double)((float)y + dy), (double)((float)z + dz), (double)this.getU(3, texture, rotation, u, v), (double)this.getV(3, texture, rotation, u, v));
      }

      if (inventory) {
         tessellator.draw();
      }

      return true;
   }

   public float getU(int i, IIcon texture, int rotation, float[] u, float[] v) {
      switch(rotation % 4) {
      case 0:
         return texture.getInterpolatedU((double)(u[i % 4] * 16.0F));
      case 1:
         return texture.getInterpolatedU((double)(v[i % 4] * 16.0F));
      case 2:
         return texture.getInterpolatedU((double)(16.0F - u[i % 4] * 16.0F));
      case 3:
         return texture.getInterpolatedU((double)(16.0F - v[i % 4] * 16.0F));
      default:
         return 0.0F;
      }
   }

   public float getV(int i, IIcon texture, int rotation, float[] u, float[] v) {
      switch(rotation % 4) {
      case 0:
         return texture.getInterpolatedV((double)(v[i % 4] * 16.0F));
      case 1:
         return texture.getInterpolatedV((double)(16.0F - u[i % 4] * 16.0F));
      case 2:
         return texture.getInterpolatedV((double)(16.0F - v[i % 4] * 16.0F));
      case 3:
         return texture.getInterpolatedV((double)(u[i % 4] * 16.0F));
      default:
         return 0.0F;
      }
   }

   public int calcRotation(int side, int direction) {
      if (side == direction) {
         return 0;
      } else if (side == Facing.oppositeSide[direction]) {
         return 0;
      } else if (direction == 1) {
         return 0;
      } else if (direction == 0) {
         return 2;
      } else {
         return side != 0 && side != 1 ? 1 + (side + direction + direction / 2) % 2 * 2 : (new int[]{0, 2, 3, 1})[direction - 2];
      }
   }

   public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
      int side = world.getBlockMetadata(x, y, z) % 6;
      int type = (world.getBlockMetadata(x, y, z) - side) / 6;
      int brightness = block.getMixedBrightnessForBlock(world, x, y, z);
      return this.renderSpikeBlock(world, x, y, z, side, type, block, renderer, brightness);
   }

   public boolean shouldRender3DInInventory(int modelId) {
      return true;
   }

   public int getRenderId() {
      return ClientProxy.renderBlockSpikeId;
   }
}
