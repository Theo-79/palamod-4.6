package fr.paladium.palamod.client.render.block;

import fr.paladium.palamod.client.model.ModelMagicalAnvil;
import fr.paladium.palamod.items.weapons.ItemBroadsword;
import fr.paladium.palamod.items.weapons.ItemFastsword;
import fr.paladium.palamod.tiles.TileEntityMagicalAnvil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemSword;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderMagicalAnvil extends TileEntitySpecialRenderer {
   private ModelMagicalAnvil model = new ModelMagicalAnvil();
   private ResourceLocation texture = new ResourceLocation("palamod:textures/blocks/MagicalAnvil.png");
   EntityItem entItem = null;
   int i = 0;

   public void renderTileEntityAt(TileEntity entity, double x, double y, double z, float f) {
      GL11.glPushMatrix();
      GL11.glTranslated((double)((float)x + 0.5F), (double)((float)y + 1.5F), (double)((float)z + 0.5F));
      Minecraft.getMinecraft().renderEngine.bindTexture(this.texture);
      GL11.glPushMatrix();
      int meta = Minecraft.getMinecraft().theWorld.getBlockMetadata(entity.xCoord, entity.yCoord, entity.zCoord);
      int id = 0;
      switch(meta) {
      case 0:
         id = 0;
      case 1:
      case 2:
      default:
         break;
      case 3:
         id = 2;
         break;
      case 4:
         id = 3;
         break;
      case 5:
         id = 1;
      }

      GL11.glRotatef((float)(id * -90), 0.0F, 1.0F, 0.0F);
      GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
      this.model.renderModel(0.0625F);
      GL11.glPopMatrix();
      GL11.glPopMatrix();
      int slot = 0;
      TileEntityMagicalAnvil tileEntity = (TileEntityMagicalAnvil)entity;
      if (tileEntity.getStackInSlot(slot) != null) {
         this.entItem = new EntityItem(tileEntity.getWorldObj(), x, y, z, tileEntity.getStackInSlot(slot));
         GL11.glPushMatrix();
         this.entItem.hoverStart = 0.0F;
         RenderItem.renderInFrame = true;
         if (!(this.entItem.getEntityItem().getItem() instanceof ItemSword) && !(this.entItem.getEntityItem().getItem() instanceof ItemBroadsword) && !(this.entItem.getEntityItem().getItem() instanceof ItemFastsword)) {
            if (this.entItem.getEntityItem().getItem() instanceof ItemArmor) {
               GL11.glTranslatef((float)x + 0.5F, (float)y + 1.02F, (float)z + 0.5F);
               GL11.glRotatef((float)this.i, 0.0F, 1.0F, 0.0F);
               if (this.i == 360) {
                  this.i = 0;
               } else {
                  ++this.i;
               }
            }
         } else {
            GL11.glTranslatef((float)x + 0.65F, (float)y + 1.05F, (float)z + 0.5F);
            GL11.glRotatef(45.0F, 0.0F, 0.0F, 1.0F);
            GL11.glRotatef((float)this.i, 0.0F, 1.0F, 0.0F);
            if (this.i == 360) {
               this.i = 0;
            } else {
               ++this.i;
            }
         }

         RenderManager.instance.renderEntityWithPosYaw(this.entItem, 0.0D, 0.0D, 0.0D, 0.0F, 0.0F);
         RenderItem.renderInFrame = false;
         GL11.glPopMatrix();
      }

   }
}
