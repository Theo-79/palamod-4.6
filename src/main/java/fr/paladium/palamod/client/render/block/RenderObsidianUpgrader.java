package fr.paladium.palamod.client.render.block;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import fr.paladium.palamod.client.ClientProxy;
import fr.paladium.palamod.tiles.TileEntityObsidianUpgrader;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;

public class RenderObsidianUpgrader implements ISimpleBlockRenderingHandler {
   public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer) {
      renderer.setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
      this.renderInInventory(Tessellator.instance, renderer, block, metadata);
   }

   public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
      TileEntity tile = world.getTileEntity(x, y, z);
      if (tile instanceof TileEntityObsidianUpgrader) {
         TileEntityObsidianUpgrader te = (TileEntityObsidianUpgrader)tile;
         if (te.hasMaster()) {
            TileEntityObsidianUpgrader master = (TileEntityObsidianUpgrader)world.getTileEntity(te.getMasterX(), te.getMasterY(), te.getMasterZ());
            boolean flag = this.isSame(x + 1, y, z, te);
            boolean flag1 = this.isSame(x, y, z + 1, te);
            boolean flag2 = this.isSame(x - 1, y, z, te);
            boolean flag3 = this.isSame(x, y, z - 1, te);
            if (flag && flag1) {
               renderer.setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 0.10000000149011612D, 1.0D);
               renderer.renderStandardBlock(block, x, y, z);
               renderer.setRenderBounds(0.0D, 0.10000000149011612D, 0.0D, 0.10000000149011612D, 1.0D, 1.0D);
               renderer.renderStandardBlock(block, x, y, z);
               renderer.setRenderBounds(0.0D, 0.10000000149011612D, 0.0D, 1.0D, 1.0D, 0.10000000149011612D);
               renderer.renderStandardBlock(block, x, y, z);
               renderer.setRenderBounds(0.10000000149011612D, 0.10000000149011612D, 0.10000000149011612D, 1.0D, (double)((float)master.getObsidian() / 64.0F), 1.0D);
               renderer.renderStandardBlock(block, x, y, z);
               return true;
            }

            if (flag2 && flag1) {
               renderer.setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 0.10000000149011612D, 1.0D);
               renderer.renderStandardBlock(block, x, y, z);
               renderer.setRenderBounds(0.8999999761581421D, 0.10000000149011612D, 0.0D, 1.0D, 1.0D, 1.0D);
               renderer.renderStandardBlock(block, x, y, z);
               renderer.setRenderBounds(0.0D, 0.10000000149011612D, 0.0D, 1.0D, 1.0D, 0.10000000149011612D);
               renderer.renderStandardBlock(block, x, y, z);
               renderer.setRenderBounds(0.0D, 0.10000000149011612D, 0.10000000149011612D, 0.8999999761581421D, (double)((float)master.getObsidian() / 64.0F), 1.0D);
               renderer.renderStandardBlock(block, x, y, z);
               return true;
            }

            if (flag && flag3) {
               renderer.setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 0.10000000149011612D, 1.0D);
               renderer.renderStandardBlock(block, x, y, z);
               renderer.setRenderBounds(0.0D, 0.10000000149011612D, 0.0D, 0.10000000149011612D, 1.0D, 1.0D);
               renderer.renderStandardBlock(block, x, y, z);
               renderer.setRenderBounds(0.0D, 0.10000000149011612D, 0.8999999761581421D, 1.0D, 1.0D, 1.0D);
               renderer.renderStandardBlock(block, x, y, z);
               renderer.setRenderBounds(0.10000000149011612D, 0.10000000149011612D, 0.0D, 1.0D, (double)((float)master.getObsidian() / 64.0F), 0.8999999761581421D);
               renderer.renderStandardBlock(block, x, y, z);
               return true;
            }

            if (flag2 && flag3) {
               renderer.setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 0.10000000149011612D, 1.0D);
               renderer.renderStandardBlock(block, x, y, z);
               renderer.setRenderBounds(0.8999999761581421D, 0.10000000149011612D, 0.0D, 1.0D, 1.0D, 1.0D);
               renderer.renderStandardBlock(block, x, y, z);
               renderer.setRenderBounds(0.0D, 0.10000000149011612D, 0.8999999761581421D, 1.0D, 1.0D, 1.0D);
               renderer.renderStandardBlock(block, x, y, z);
               renderer.setRenderBounds(0.0D, 0.10000000149011612D, 0.0D, 0.8999999761581421D, (double)((float)master.getObsidian() / 64.0F), 0.8999999761581421D);
               renderer.renderStandardBlock(block, x, y, z);
               return true;
            }
         }
      }

      renderer.setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
      renderer.renderStandardBlock(block, x, y, z);
      return true;
   }

   public boolean shouldRender3DInInventory(int modelId) {
      return true;
   }

   public int getRenderId() {
      return ClientProxy.renderObsidianUpgraderId;
   }

   private void renderInInventory(Tessellator tessellator, RenderBlocks renderer, Block block, int metadata) {
      GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
      tessellator.startDrawingQuads();
      tessellator.setNormal(0.0F, -1.0F, 0.0F);
      renderer.renderFaceYNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(0, metadata));
      tessellator.draw();
      tessellator.startDrawingQuads();
      tessellator.setNormal(0.0F, 1.0F, 0.0F);
      renderer.renderFaceYPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(1, metadata));
      tessellator.draw();
      tessellator.startDrawingQuads();
      tessellator.setNormal(0.0F, 0.0F, -1.0F);
      renderer.renderFaceZNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(2, metadata));
      tessellator.draw();
      tessellator.startDrawingQuads();
      tessellator.setNormal(0.0F, 0.0F, 1.0F);
      renderer.renderFaceZPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(3, metadata));
      tessellator.draw();
      tessellator.startDrawingQuads();
      tessellator.setNormal(-1.0F, 0.0F, 0.0F);
      renderer.renderFaceXNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(4, metadata));
      tessellator.draw();
      tessellator.startDrawingQuads();
      tessellator.setNormal(1.0F, 0.0F, 0.0F);
      renderer.renderFaceXPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(5, metadata));
      tessellator.draw();
      GL11.glTranslatef(0.5F, 0.5F, 0.5F);
   }

   private boolean isSame(int x, int y, int z, TileEntityObsidianUpgrader te) {
      World world = te.getWorldObj();
      TileEntity tile = world.getTileEntity(x, y, z);
      if (!(tile instanceof TileEntityObsidianUpgrader)) {
         return false;
      } else {
         TileEntityObsidianUpgrader te2 = (TileEntityObsidianUpgrader)tile;
         return te.getMasterX() == te2.getMasterX() && te.getMasterY() == te2.getMasterY() && te.getMasterZ() == te2.getMasterZ();
      }
   }
}
