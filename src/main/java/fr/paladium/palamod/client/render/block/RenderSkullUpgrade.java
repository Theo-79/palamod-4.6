package fr.paladium.palamod.client.render.block;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import net.minecraft.block.Block;
import net.minecraft.client.model.ModelSkeletonHead;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.IBlockAccess;
import org.lwjgl.opengl.GL11;

public class RenderSkullUpgrade extends TileEntitySpecialRenderer implements ISimpleBlockRenderingHandler {
   private static final ResourceLocation wither = new ResourceLocation("textures/entity/skeleton/wither_skeleton.png");
   private static final ResourceLocation life = new ResourceLocation("palamod:textures/models/WitherHeadLife.png");
   private static final ResourceLocation damage = new ResourceLocation("palamod:textures/models/WitherHeadDamage.png");
   private static final ResourceLocation nofly = new ResourceLocation("palamod:textures/models/WitherHeadNoFly.png");
   private static final ResourceLocation nosound = new ResourceLocation("palamod:textures/models/WitherHeadNoSound.png");
   private static final ResourceLocation nofake = new ResourceLocation("palamod:textures/models/WitherHeadNoFake.png");
   private static final ResourceLocation angry = new ResourceLocation("palamod:textures/models/WitherHeadAngry.png");
   private static final ResourceLocation explosion = new ResourceLocation("palamod:textures/models/WitherHeadExplosion.png");
   private static final ResourceLocation anvil = new ResourceLocation("palamod:textures/models/WitherHeadAnvil.png");
   private static final ResourceLocation remote = new ResourceLocation("palamod:textures/models/WitherHeadRemote.png");
   private final ModelSkeletonHead modelskeletonhead = new ModelSkeletonHead();

   public void renderTileEntityAt(TileEntity tileEntity, double v, double v1, double v2, float v3) {
   }

   public void renderInventoryBlock(Block block, int i, int i1, RenderBlocks renderBlocks) {
   }

   public boolean renderWorldBlock(IBlockAccess iBlockAccess, int i, int i1, int i2, Block block, int i3, RenderBlocks renderBlocks) {
      try {
         this.bindTexture(wither);
         float rotation = (float)iBlockAccess.getBlockMetadata(i, i1, i2) * 45.0F;
         GL11.glTranslatef((float)i + 0.5F, (float)i1, (float)i2 + 0.5F);
         GL11.glEnable(32826);
         GL11.glScalef(-1.0F, -1.0F, 1.0F);
         GL11.glEnable(3008);

         try {
            this.modelskeletonhead.render((Entity)null, 0.0F, 0.0F, 0.0F, rotation, 0.0F, 0.0625F);
         } catch (Exception var10) {
            var10.printStackTrace();
         }

         GL11.glPopMatrix();
         return true;
      } catch (Exception var11) {
         var11.printStackTrace();
         return false;
      }
   }

   public boolean shouldRender3DInInventory(int i) {
      return false;
   }

   public int getRenderId() {
      return 45;
   }
}
