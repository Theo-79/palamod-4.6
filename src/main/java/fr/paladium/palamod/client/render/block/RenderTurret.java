package fr.paladium.palamod.client.render.block;

import fr.paladium.palamod.client.model.ModelTurret;
import fr.paladium.palamod.tiles.TileEntityTurret;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderTurret extends TileEntitySpecialRenderer {
   private final ModelTurret model = new ModelTurret();
   private final ResourceLocation textures = new ResourceLocation("palamod:textures/models/Turret.png");

   public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float f) {
      GL11.glPushMatrix();
      GL11.glTranslatef((float)x + 0.5F, (float)y + 1.5F, (float)z + 0.5F);
      this.bindTexture(this.textures);
      GL11.glPushMatrix();
      GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
      TileEntityTurret tileTurret = (TileEntityTurret)tile;
      float yaw = (float)(-Math.atan2((double)tileTurret.targetOffX, (double)tileTurret.targetOffZ));
      this.model.render(0.0625F, 0.0F, yaw);
      GL11.glPopMatrix();
      GL11.glPopMatrix();
   }
}
