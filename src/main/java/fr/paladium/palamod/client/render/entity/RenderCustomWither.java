package fr.paladium.palamod.client.render.entity;

import fr.paladium.palamod.client.model.ModelCustomWither;
import fr.paladium.palamod.entities.mobs.EntityCustomWither;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.BossStatus;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderCustomWither extends RenderLiving {
   private static final ResourceLocation invulnerableWitherTextures = new ResourceLocation("textures/entity/wither/wither_invulnerable.png");
   private static final ResourceLocation witherTextures = new ResourceLocation("textures/entity/wither/wither.png");
   private int field_82419_a;

   public RenderCustomWither() {
      super(new ModelCustomWither(), 1.0F);
      this.field_82419_a = ((ModelCustomWither)this.mainModel).func_82903_a();
   }

   protected ResourceLocation getEntityTexture(Entity entity) {
      return this.getEntityTexturew((EntityCustomWither)entity);
   }

   public void doRender(EntityCustomWither p_doRender_1_, double p_doRender_2_, double p_doRender_4_, double p_doRender_6_, float p_doRender_8_, float p_doRender_9_) {
      BossStatus.setBossStatus(p_doRender_1_, true);
      int var10 = ((ModelCustomWither)this.mainModel).func_82903_a();
      if (var10 != this.field_82419_a) {
         this.field_82419_a = var10;
         this.mainModel = new ModelCustomWither();
      }

      super.doRender(p_doRender_1_, p_doRender_2_, p_doRender_4_, p_doRender_6_, p_doRender_8_, p_doRender_9_);
   }

   protected ResourceLocation getEntityTexturew(EntityCustomWither wither) {
      int var2 = wither.func_82212_n();
      return var2 <= 0 || var2 <= 80 && var2 / 5 % 2 == 1 ? witherTextures : invulnerableWitherTextures;
   }

   protected void preRenderCallback(EntityLivingBase p_77041_1_, float p_77041_2_) {
      this.preRenderCallback((EntityWither)p_77041_1_, p_77041_2_);
   }

   protected void preRenderCallback(EntityWither p_77041_1_, float p_77041_2_) {
      int i = p_77041_1_.func_82212_n();
      if (i > 0) {
         float f1 = 2.0F - ((float)i - p_77041_2_) / 220.0F * 0.5F;
         GL11.glScalef(f1, f1, f1);
      } else {
         GL11.glScalef(2.0F, 2.0F, 2.0F);
      }

   }

   protected int shouldRenderPass(EntityCustomWither p_shouldRenderPass_1_, int p_shouldRenderPass_2_, float p_shouldRenderPass_3_) {
      if (p_shouldRenderPass_1_.isArmored()) {
         if (p_shouldRenderPass_1_.isInvisible()) {
            GL11.glDepthMask(false);
         } else {
            GL11.glDepthMask(true);
         }

         if (p_shouldRenderPass_2_ == 1) {
            float var4 = (float)p_shouldRenderPass_1_.ticksExisted + p_shouldRenderPass_3_;
            this.bindTexture(invulnerableWitherTextures);
            GL11.glMatrixMode(5890);
            GL11.glLoadIdentity();
            float var5 = MathHelper.cos(var4 * 0.02F) * 3.0F;
            float var6 = var4 * 0.01F;
            GL11.glTranslatef(var5, var6, 0.0F);
            this.setRenderPassModel(this.mainModel);
            GL11.glMatrixMode(5888);
            GL11.glEnable(3042);
            float var7 = 0.5F;
            GL11.glColor4f(var7, var7, var7, 1.0F);
            GL11.glDisable(2896);
            GL11.glBlendFunc(1, 1);
            GL11.glTranslatef(0.0F, -0.01F, 0.0F);
            GL11.glScalef(1.1F, 1.1F, 1.1F);
            return 1;
         }

         if (p_shouldRenderPass_2_ == 2) {
            GL11.glMatrixMode(5890);
            GL11.glLoadIdentity();
            GL11.glMatrixMode(5888);
            GL11.glEnable(2896);
            GL11.glDisable(3042);
         }
      }

      return -1;
   }

   protected int inheritRenderPass(EntityCustomWither p_inheritRenderPass_1_, int p_inheritRenderPass_2_, float p_inheritRenderPass_3_) {
      return -1;
   }
}
