package fr.paladium.palamod.client.render.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.entities.projectiles.EntitySplashPotion;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class RenderEntityPotion extends Render {
   private static ResourceLocation nausea = new ResourceLocation("palamod:textures/entity/SicknessPotion.png");
   private static ResourceLocation web = new ResourceLocation("palamod:textures/entity/WebPotion.png");
   private float scale = 1.0F;

   public void renderProjectile(EntityThrowable projectile, double x, double y, double z) {
      GL11.glPushMatrix();
      this.bindEntityTexture(projectile);
      GL11.glTranslatef((float)x, (float)y, (float)z);
      GL11.glEnable(32826);
      GL11.glScalef(this.scale * 0.5F, this.scale * 0.5F, this.scale * 0.5F);
      Tessellator tessellator = Tessellator.instance;
      float minU = 0.0F;
      float maxU = 1.0F;
      float minV = 0.0F;
      float maxV = 1.0F;
      float f7 = 1.0F;
      float f8 = 0.5F;
      float f9 = 0.25F;
      GL11.glRotatef(180.0F - this.renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
      GL11.glRotatef(-this.renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
      tessellator.startDrawingQuads();
      tessellator.setNormal(0.0F, 1.0F, 0.0F);
      tessellator.addVertexWithUV((double)(0.0F - f8), (double)(0.0F - f9), 0.0D, (double)minU, (double)maxV);
      tessellator.addVertexWithUV((double)(f7 - f8), (double)(0.0F - f9), 0.0D, (double)maxU, (double)maxV);
      tessellator.addVertexWithUV((double)(f7 - f8), (double)(1.0F - f9), 0.0D, (double)maxU, (double)minV);
      tessellator.addVertexWithUV((double)(0.0F - f8), (double)(1.0F - f9), 0.0D, (double)minU, (double)minV);
      tessellator.draw();
      GL11.glDisable(32826);
      GL11.glPopMatrix();
   }

   public void doRender(Entity par1Entity, double par2, double par4, double par6, float par8, float par9) {
      this.renderProjectile((EntityThrowable)par1Entity, par2, par4, par6);
   }

   protected ResourceLocation getEntityTexture(Entity entity) {
      EntitySplashPotion e = (EntitySplashPotion)entity;
      switch(e.getDamageValue()) {
      case 0:
         return nausea;
      case 1:
         return web;
      default:
         return nausea;
      }
   }
}
