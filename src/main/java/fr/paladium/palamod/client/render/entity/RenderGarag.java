package fr.paladium.palamod.client.render.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.client.model.ModelGarag;
import fr.paladium.palamod.entities.mobs.EntityGarag;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class RenderGarag extends RenderLiving {
   private static final ResourceLocation EntityTexture = new ResourceLocation("palamod:textures/entity/Garag.png");
   protected ModelGarag model;
   private float scale;

   public RenderGarag(ModelBase par1ModelBase, float par2) {
      super(par1ModelBase, par2);
      this.model = (ModelGarag)this.mainModel;
      this.scale = 2.0F;
   }

   protected void preRenderCallback(ModelGarag par1EntityLivingBase, float par2) {
      this.preRenderCallback(par1EntityLivingBase, par2);
   }

   public void renderGarag(EntityGarag var1, double var2, double var4, double var6, float var8, float var9) {
      super.doRender(var1, var2, var4, var6, var8, var9);
   }

   public void doRender(EntityLiving var1, double var2, double var4, double var6, float var8, float var9) {
      this.renderGarag((EntityGarag)var1, var2, var4, var6, var8, var9);
   }

   public void doRender(Entity var1, double var2, double var4, double var6, float var8, float var9) {
      this.renderGarag((EntityGarag)var1, var2, var4, var6, var8, var9);
   }

   protected void preRenderCallback(EntityLivingBase entity, float f) {
      GL11.glScalef(1.6F, 1.6F, 1.6F);
   }

   protected ResourceLocation getEntityTexture(Entity entity) {
      return EntityTexture;
   }
}
