package fr.paladium.palamod.client.render.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.client.model.ModelGuardianGolem;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class RenderGuardianGolem extends RenderLiving {
   private static final ResourceLocation ironGolemTextures = new ResourceLocation("palamod:textures/entity/PaladiumGuardian.png");
   private final ModelGuardianGolem ironGolemModel;
   private static final String __OBFID = "CL_00001031";

   public RenderGuardianGolem() {
      super(new ModelGuardianGolem(), 0.5F);
      this.ironGolemModel = (ModelGuardianGolem)this.mainModel;
   }

   public void doRender(EntityGuardianGolem p_76986_1_, double p_76986_2_, double p_76986_4_, double p_76986_6_, float p_76986_8_, float p_76986_9_) {
      super.doRender(p_76986_1_, p_76986_2_, p_76986_4_, p_76986_6_, p_76986_8_, p_76986_9_);
   }

   protected ResourceLocation getEntityTexture(EntityGuardianGolem p_110775_1_) {
      return ironGolemTextures;
   }

   protected void rotateCorpse(EntityGuardianGolem p_77043_1_, float p_77043_2_, float p_77043_3_, float p_77043_4_) {
      super.rotateCorpse(p_77043_1_, p_77043_2_, p_77043_3_, p_77043_4_);
      if ((double)p_77043_1_.limbSwingAmount >= 0.01D) {
         float f3 = 13.0F;
         float f4 = p_77043_1_.limbSwing - p_77043_1_.limbSwingAmount * (1.0F - p_77043_4_) + 6.0F;
         float f5 = (Math.abs(f4 % f3 - f3 * 0.5F) - f3 * 0.25F) / (f3 * 0.25F);
         GL11.glRotatef(6.5F * f5, 0.0F, 0.0F, 1.0F);
      }

   }

   public void doRender(EntityLiving p_76986_1_, double p_76986_2_, double p_76986_4_, double p_76986_6_, float p_76986_8_, float p_76986_9_) {
      this.doRender((EntityGuardianGolem)p_76986_1_, p_76986_2_, p_76986_4_, p_76986_6_, p_76986_8_, p_76986_9_);
   }

   protected void rotateCorpse(EntityLivingBase p_77043_1_, float p_77043_2_, float p_77043_3_, float p_77043_4_) {
      this.rotateCorpse((EntityGuardianGolem)p_77043_1_, p_77043_2_, p_77043_3_, p_77043_4_);
   }

   public void doRender(EntityLivingBase p_76986_1_, double p_76986_2_, double p_76986_4_, double p_76986_6_, float p_76986_8_, float p_76986_9_) {
      this.doRender(p_76986_1_, p_76986_2_, p_76986_4_, p_76986_6_, p_76986_8_, p_76986_9_);
   }

   protected ResourceLocation getEntityTexture(Entity p_110775_1_) {
      return this.getEntityTexture((EntityGuardianGolem)p_110775_1_);
   }

   public void doRender(Entity p_76986_1_, double p_76986_2_, double p_76986_4_, double p_76986_6_, float p_76986_8_, float p_76986_9_) {
      this.doRender((EntityGuardianGolem)p_76986_1_, p_76986_2_, p_76986_4_, p_76986_6_, p_76986_8_, p_76986_9_);
   }

   protected void bindEntityTexture(Entity p_110777_1_) {
      super.bindEntityTexture(p_110777_1_);
   }

   protected void renderEquippedItems(EntityLivingBase p_77029_1_, float p_77029_2_) {
      this.renderEquippedItems((EntityGuardianGolem)p_77029_1_, p_77029_2_);
   }

   protected void renderEquippedItems(EntityGuardianGolem p_77029_1_, float p_77029_2_) {
      super.renderEquippedItems(p_77029_1_, p_77029_2_);
      ItemStack itemstack = p_77029_1_.getWeapon();
      if (itemstack != null) {
         GL11.glEnable(32826);
         GL11.glPushMatrix();
         GL11.glRotatef(5.0F + 180.0F * this.ironGolemModel.ironGolemRightArm.rotateAngleX / 3.1415927F, 1.0F, 0.0F, 0.0F);
         GL11.glTranslatef(-0.6275F, 1.0F, -0.1F);
         GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
         GL11.glRotatef(39.0F, 0.0F, 1.0F, 0.0F);
         float f1 = 0.8F;
         GL11.glScalef(f1, -f1, f1);
         int i = p_77029_1_.getBrightnessForRender(p_77029_2_);
         int j = i % 65536;
         int k = i / 65536;
         OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float)j / 1.0F, (float)k / 1.0F);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         this.bindTexture(TextureMap.locationBlocksTexture);
         i = itemstack.getItem().getColorFromItemStack(itemstack, 0);
         float f4 = (float)(i >> 16 & 255) / 255.0F;
         float f5 = (float)(i >> 8 & 255) / 255.0F;
         float f2 = (float)(i & 255) / 255.0F;
         GL11.glColor4f(f4, f5, f2, 1.0F);
         this.renderManager.itemRenderer.renderItem(p_77029_1_, itemstack, 0);
         GL11.glPopMatrix();
         GL11.glDisable(32826);
      }
   }
}
