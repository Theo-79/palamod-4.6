package fr.paladium.palamod.client.render.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.client.model.ModelTobalt;
import fr.paladium.palamod.entities.mobs.EntityTobalt;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class RenderTobalt extends RenderLiving {
   private static final ResourceLocation EntityTexture = new ResourceLocation("palamod:textures/entity/Tobalt.png");
   protected ModelTobalt model;

   public RenderTobalt(ModelBase par1ModelBase, float par2) {
      super(par1ModelBase, par2);
      this.model = (ModelTobalt)this.mainModel;
   }

   public void renderSilverfoot(EntityTobalt var1, double var2, double var4, double var6, float var8, float var9) {
      super.doRender(var1, var2, var4, var6, var8, var9);
   }

   protected void preRenderCallback(EntityLivingBase entity, float f) {
      GL11.glScalef(1.6F, 1.6F, 1.6F);
   }

   public void doRender(EntityLiving var1, double var2, double var4, double var6, float var8, float var9) {
      this.renderSilverfoot((EntityTobalt)var1, var2, var4, var6, var8, var9);
   }

   public void doRender(Entity var1, double var2, double var4, double var6, float var8, float var9) {
      this.renderSilverfoot((EntityTobalt)var1, var2, var4, var6, var8, var9);
   }

   protected ResourceLocation getEntityTexture(Entity entity) {
      return EntityTexture;
   }
}
