package fr.paladium.palamod.client.render.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.util.BowHelper;
import fr.paladium.palamod.util.DisplayHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.IItemRenderer.ItemRendererHelper;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class ItemBowRenderer implements IItemRenderer {
   private final RenderItem renderItem = new RenderItem();
   private final ResourceLocation speed = new ResourceLocation("palamod:textures/items/upgrade_speed.png");
   private final ResourceLocation range = new ResourceLocation("palamod:textures/items/upgrade_range.png");

   public boolean handleRenderType(ItemStack stack, ItemRenderType type) {
      ItemStack usingItem = Minecraft.getMinecraft().thePlayer.getItemInUse();
      return type == ItemRenderType.INVENTORY;
   }

   public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack stack, ItemRendererHelper helper) {
      return false;
   }

   public void renderItem(ItemRenderType type, ItemStack stack, Object... data) {
      TextureManager textureManager = Minecraft.getMinecraft().getTextureManager();
      ResourceLocation resource = textureManager.getResourceLocation(stack.getItemSpriteNumber());
      EntityPlayer player = Minecraft.getMinecraft().thePlayer;
      ItemStack usingItem = player.getItemInUse();
      int useRemaining = player.getItemInUseCount();
      ItemBow bow = (ItemBow)stack.getItem();
      IIcon icon = bow.getIcon(stack, 0);
      int pulling1 = 18;
      int pulling2 = 13;
      if (!BowHelper.canApply(stack, 1)) {
         pulling1 = 8;
         pulling2 = 7;
      }

      if (usingItem != null && usingItem == stack) {
         int charge = stack.getMaxItemUseDuration() - useRemaining;
         if (charge >= pulling1) {
            icon = bow.getItemIconForUseDuration(2);
         } else if (charge > pulling2) {
            icon = bow.getItemIconForUseDuration(1);
         } else if (charge > 0) {
            icon = bow.getItemIconForUseDuration(0);
         }
      }

      if (icon == null) {
         icon = ((TextureMap)textureManager.getTexture(resource)).getAtlasSprite("missingno");
      }

      GL11.glPushMatrix();
      textureManager.bindTexture(resource);
      GL11.glDisable(2896);
      GL11.glEnable(3008);
      GL11.glEnable(3042);
      OpenGlHelper.glBlendFunc(770, 771, 1, 0);
      this.renderItem.renderIcon(0, 0, (IIcon)icon, 16, 16);
      GL11.glDisable(3042);
      GL11.glDisable(3008);
      if (stack.hasEffect(0)) {
         this.renderItem.renderEffect(textureManager, 0, 0);
      }

      GL11.glPopMatrix();
      GL11.glPushMatrix();
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glDisable(2896);
      GL11.glEnable(3042);
      GL11.glBlendFunc(770, 771);
      if (!BowHelper.canApply(stack, 1)) {
         textureManager.bindTexture(this.speed);
         DisplayHelper.drawTexturedQuadFit(0.0D, 0.0D, 16.0D, 16.0D, 0.0D);
      }

      if (!BowHelper.canApply(stack, 0)) {
         textureManager.bindTexture(this.range);
         DisplayHelper.drawTexturedQuadFit(0.0D, 0.0D, 16.0D, 16.0D, 0.0D);
      }

      GL11.glDisable(3042);
      GL11.glDisable(3008);
      GL11.glPopMatrix();
   }
}
