package fr.paladium.palamod.client.render.item;

import fr.paladium.palamod.tiles.TileEntityPaladiumChest;
import net.minecraft.client.model.ModelChest;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.IItemRenderer.ItemRendererHelper;
import org.lwjgl.opengl.GL11;

public class ItemChestRender implements IItemRenderer {
   private ModelChest chestModel = new ModelChest();

   public boolean handleRenderType(ItemStack item, ItemRenderType type) {
      return true;
   }

   public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
      return true;
   }

   public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
      GL11.glPushMatrix();
      GL11.glRotated(90.0D, 0.0D, 1.0D, 0.0D);
      if (type != ItemRenderType.ENTITY) {
         GL11.glTranslatef(-1.0F, -0.1F, 0.0F);
      } else {
         GL11.glTranslatef(-0.5F, -0.45F, -0.5F);
      }

      TileEntityRendererDispatcher.instance.renderTileEntityAt(new TileEntityPaladiumChest(), 0.0D, 0.0D, 0.0D, 0.0F);
      GL11.glPopMatrix();
   }
}
