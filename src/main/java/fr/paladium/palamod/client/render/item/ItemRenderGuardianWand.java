package fr.paladium.palamod.client.render.item;

import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.IItemRenderer.ItemRendererHelper;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import org.lwjgl.opengl.GL11;

public class ItemRenderGuardianWand implements IItemRenderer {
   private ResourceLocation guardianWandTewture = new ResourceLocation("palamodtextures/models/GuardianWand.png");
   private ResourceLocation guardianWandModel = new ResourceLocation("palamod", "models/GuardianWand.obj");
   public IModelCustom model;

   public ItemRenderGuardianWand() {
      this.model = AdvancedModelLoader.loadModel(this.guardianWandModel);
   }

   public boolean handleRenderType(ItemStack item, ItemRenderType type) {
      return true;
   }

   public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
      return false;
   }

   public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
      GL11.glPushMatrix();
      float scale = 1.2F;
      GL11.glScalef(scale, scale, scale);
      GL11.glRotatef(210.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(270.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(0.0F, 0.0F, 1.0F, 0.0F);
      Minecraft.getMinecraft().renderEngine.bindTexture(this.guardianWandTewture);
      this.model.renderAll();
      GL11.glPopMatrix();
   }
}
