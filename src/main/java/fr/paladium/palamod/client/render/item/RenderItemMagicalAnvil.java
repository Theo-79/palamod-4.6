package fr.paladium.palamod.client.render.item;

import fr.paladium.palamod.client.model.ModelMagicalAnvil;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.IItemRenderer.ItemRendererHelper;
import org.lwjgl.opengl.GL11;

public class RenderItemMagicalAnvil implements IItemRenderer {
   private ModelMagicalAnvil model;
   private TileEntitySpecialRenderer render;
   private TileEntity entity;

   public RenderItemMagicalAnvil(TileEntitySpecialRenderer render, TileEntity entity) {
      this.render = render;
      this.entity = entity;
      this.model = new ModelMagicalAnvil();
   }

   public boolean handleRenderType(ItemStack item, ItemRenderType type) {
      return true;
   }

   public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
      return true;
   }

   public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
      if (type == ItemRenderType.ENTITY) {
         GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
      }

      if (type == ItemRenderType.INVENTORY) {
         GL11.glTranslated(0.0D, -0.8D, 0.0D);
      }

      if (type == ItemRenderType.EQUIPPED) {
         GL11.glScaled(1.5D, 1.5D, 1.5D);
         GL11.glTranslated(0.6D, 0.0D, 0.8D);
      }

      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      this.render.renderTileEntityAt(this.entity, 0.0D, 0.0D, 0.0D, 0.0F);
   }
}
