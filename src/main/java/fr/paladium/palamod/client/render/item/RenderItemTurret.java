package fr.paladium.palamod.client.render.item;

import fr.paladium.palamod.client.model.ModelTurret;
import fr.paladium.palamod.tiles.TileEntityTurret;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.IItemRenderer.ItemRendererHelper;
import org.lwjgl.opengl.GL11;

public class RenderItemTurret implements IItemRenderer {
   private ModelTurret model = new ModelTurret();

   public boolean handleRenderType(ItemStack item, ItemRenderType type) {
      return true;
   }

   public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
      return true;
   }

   public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
      GL11.glPushMatrix();
      GL11.glRotated(90.0D, 0.0D, 1.0D, 0.0D);
      if (type != ItemRenderType.ENTITY) {
         GL11.glTranslatef(-1.0F, -0.1F, 0.0F);
      } else {
         GL11.glTranslatef(-0.5F, -0.45F, -0.5F);
      }

      if (type == ItemRenderType.EQUIPPED || type == ItemRenderType.EQUIPPED_FIRST_PERSON) {
         GL11.glScaled(2.0D, 2.0D, 2.0D);
         GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
         GL11.glTranslated(-0.5D, 0.0D, -0.8D);
      }

      TileEntityRendererDispatcher.instance.renderTileEntityAt(new TileEntityTurret(), 0.0D, 0.0D, 0.0D, 0.0F);
      GL11.glPopMatrix();
   }
}
