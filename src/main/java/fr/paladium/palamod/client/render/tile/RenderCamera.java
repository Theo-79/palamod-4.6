package fr.paladium.palamod.client.render.tile;

import fr.paladium.palamod.client.model.ModelSecurityCamera;
import fr.paladium.palamod.tiles.TileEntityCamera;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderCamera extends TileEntitySpecialRenderer {
   public static ModelSecurityCamera model = new ModelSecurityCamera();
   public static ResourceLocation texture = new ResourceLocation("palamod", "textures/blocks/securityCamera.png");

   public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float partialRenderTick) {
      this.renderTileEntityCameraAt((TileEntityCamera)tile, x, y, z, partialRenderTick);
   }

   private void renderTileEntityCameraAt(TileEntityCamera tile, double x, double y, double z, float partialRenderTick) {
      GL11.glPushMatrix();
      GL11.glTranslated(x + 0.5D, y + 1.5D, z + 0.5D);
      GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
      int rotation = 0;
      switch(Minecraft.getMinecraft().theWorld.getBlockMetadata(tile.xCoord, tile.yCoord, tile.zCoord)) {
      case 0:
         rotation = 0;
         break;
      case 1:
         rotation = -2;
         break;
      case 2:
         rotation = 4;
         break;
      case 3:
         rotation = 2;
      }

      GL11.glRotatef((float)(rotation * -45), 0.0F, 1.0F, 0.0F);
      float rot = tile.getRotation();
      model.setCameraRotation(rot);
      Minecraft.getMinecraft().renderEngine.bindTexture(texture);
      model.renderAll();
      GL11.glPopMatrix();
   }
}
