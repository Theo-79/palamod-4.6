package fr.paladium.palamod.client.render.tile;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.tiles.TileEntityPaladiumChest;
import net.minecraft.client.model.ModelChest;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class TileEntityPaladiumChestRender extends TileEntitySpecialRenderer {
   public float field_145972_a;
   public float field_145975_i;
   public int field_145973_j;
   private int field_145974_k;
   private static final ResourceLocation texture = new ResourceLocation("palamod:textures/models/PaladiumChest.png");
   private ModelChest model = new ModelChest();

   public void renderTileEntityAt(TileEntityPaladiumChest tile, double p_147500_2_, double p_147500_4_, double p_147500_6_, float p_147500_8_) {
      int i = 0;
      if (tile.hasWorldObj()) {
         i = tile.getBlockMetadata();
      }

      this.bindTexture(texture);
      GL11.glPushMatrix();
      GL11.glEnable(32826);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glTranslatef((float)p_147500_2_, (float)p_147500_4_ + 1.0F, (float)p_147500_6_ + 1.0F);
      GL11.glScalef(1.0F, -1.0F, -1.0F);
      GL11.glTranslatef(0.5F, 0.5F, 0.5F);
      short short1 = 0;
      if (i == 2) {
         short1 = 180;
      }

      if (i == 3) {
         short1 = 0;
      }

      if (i == 4) {
         short1 = 90;
      }

      if (i == 5) {
         short1 = -90;
      }

      GL11.glRotatef((float)short1, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
      if (tile.openning) {
         if (!(tile.lid >= 1.0F) && !tile.closing) {
            tile.lid += 0.05F;
         } else {
            tile.openning = false;
         }
      }

      if (tile.closing) {
         if (!(tile.lid <= 0.0F) && !tile.openning) {
            tile.lid -= 0.05F;
         } else {
            tile.closing = false;
         }
      }

      float f1 = tile.lid;
      f1 = 1.0F - f1;
      f1 = 1.0F - f1 * f1 * f1;
      if (f1 < 0.0F) {
         f1 = 0.0F;
      }

      this.model.chestLid.rotateAngleX = -(f1 * 3.1415927F / 2.0F);
      this.model.renderAll();
      GL11.glPopMatrix();
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public void renderTileEntityAt(TileEntity p_147500_1_, double p_147500_2_, double p_147500_4_, double p_147500_6_, float p_147500_8_) {
      this.renderTileEntityAt((TileEntityPaladiumChest)p_147500_1_, p_147500_2_, p_147500_4_, p_147500_6_, p_147500_8_);
   }
}
