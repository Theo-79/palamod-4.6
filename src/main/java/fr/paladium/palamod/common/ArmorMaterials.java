package fr.paladium.palamod.common;

import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraftforge.common.util.EnumHelper;

public class ArmorMaterials {
   public static ArmorMaterial armorPaladium;
   public static ArmorMaterial armorTitane;
   public static ArmorMaterial armorAmethyst;
   public static ArmorMaterial armorEndium;

   public static void init() {
      armorPaladium = EnumHelper.addArmorMaterial("paladiumArmor", 320, new int[]{4, 7, 5, 6}, 21);
      armorTitane = EnumHelper.addArmorMaterial("titaneArmor", 190, new int[]{3, 7, 6, 3}, 19);
      armorAmethyst = EnumHelper.addArmorMaterial("amethystArmor", 160, new int[]{3, 7, 6, 3}, 21);
      armorEndium = EnumHelper.addArmorMaterial("endiumArmor", 370, new int[]{4, 7, 5, 6}, 30);
   }
}
