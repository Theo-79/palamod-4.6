package fr.paladium.palamod.common;

public class BossHandler {
   public static long ms;
   public static long tick;
   public static int time;
   public static int x;
   public static int z;

   public static void reset() {
      ms = 0L;
   }

   public static void enable(int time, int x, int z) {
      ms = System.currentTimeMillis() + (long)time * 1000L;
      BossHandler.time = time;
      BossHandler.x = x;
      BossHandler.z = z;
   }

   public static boolean check() {
      boolean flag;
      if (System.currentTimeMillis() >= ms && ms != 0L) {
         flag = false;
         reset();
      } else if (ms == 0L) {
         flag = false;
      } else {
         flag = true;
      }

      return flag;
   }

   public static void tick() {
      if (ms != 0L) {
         if (tick <= System.currentTimeMillis()) {
            --time;
            tick = System.currentTimeMillis() + 1000L;
            if (time <= 0) {
               reset();
            }
         }

      }
   }
}
