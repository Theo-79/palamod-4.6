package fr.paladium.palamod.common;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Vec3;

public class CameraHandler {
   public static CameraHandler.CameraData[] cameras = new CameraHandler.CameraData[256];

   public static int createCamera(double x, double y, double z, float pitch, float yaw) {
      CameraHandler.CameraData cameraData = new CameraHandler.CameraData(Vec3.createVectorHelper(x, y, z), pitch, yaw);

      for(int i = 0; i < cameras.length; ++i) {
         if (cameras[i] == null) {
            cameras[i] = cameraData;
            return i;
         }
      }

      return -1;
   }

   public static int createCamera(EntityPlayer player) {
      CameraHandler.CameraData cameraData = new CameraHandler.CameraData(Vec3.createVectorHelper(player.posX, player.posY + (double)player.getEyeHeight(), player.posZ), player.rotationPitch, player.rotationYawHead);

      for(int i = 0; i < cameras.length; ++i) {
         if (cameras[i] == null) {
            cameras[i] = cameraData;
            return i;
         }
      }

      return -1;
   }

   public static CameraHandler.CameraData getCamera(int id) {
      return id >= 0 && id < cameras.length ? cameras[id] : null;
   }

   public static void removeCamera(int id) {
      if (id >= 0 && id < cameras.length) {
         cameras[id] = null;
      }
   }

   public static final class CameraData {
      public final Vec3 position;
      public final float pitch;
      public final float yaw;
      public final int dimension;

      public CameraData(Vec3 position, float pitch, float yaw) {
         this.position = position;
         this.pitch = pitch;
         this.yaw = yaw;
         this.dimension = Minecraft.getMinecraft().theWorld.provider.dimensionId;
      }
   }
}
