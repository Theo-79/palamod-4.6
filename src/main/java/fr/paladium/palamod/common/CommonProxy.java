package fr.paladium.palamod.common;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.blocks.ModBlocks;
import fr.paladium.palamod.blocks.fluids.ModFluids;
import fr.paladium.palamod.enchants.EnchantHandler;
import fr.paladium.palamod.enchants.ModEnchants;
import fr.paladium.palamod.entities.ModEntities;
import fr.paladium.palamod.items.ModItems;
import fr.paladium.palamod.items.armors.EventHandlerArmor;
import fr.paladium.palamod.network.PacketPipeline;
import fr.paladium.palamod.potion.ModPotions;
import fr.paladium.palamod.potion.PotionExtender;
import fr.paladium.palamod.potion.PotionHandler;
import fr.paladium.palamod.recipies.ModRecipies;
import fr.paladium.palamod.tiles.ModTiles;
import fr.paladium.palamod.util.UpgradeHelper;
import fr.paladium.palamod.world.generation.GenOres;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Vector;
import net.minecraft.entity.Entity;
import net.minecraftforge.common.MinecraftForge;

public class CommonProxy {
   public PacketPipeline packetPipeline = new PacketPipeline();
   boolean server;
   public static final boolean DEV = false;

   public void preInit(FMLPreInitializationEvent event) {
      if (event.getSide() == Side.CLIENT) {
         KiwiHandler.cheatCheck();
      }

      PotionExtender.preInit();
      if (event.getSide().equals(Side.SERVER)) {
         this.server = true;
      }

      if (event.getSide().equals(Side.CLIENT)) {
         FMLCommonHandler.instance().bus().register(this);
         MinecraftForge.EVENT_BUS.register(this);
         FMLCommonHandler.instance().bus().register(new KeyHandler());
      }

   }

   public void init(FMLInitializationEvent event) {
      ToolMaterialPaladium.init();
      ArmorMaterials.init();
      ModEntities.init();
      ModPotions.init();
      ModBlocks.init();
      ModItems.init();
      ModTiles.init();
      ModEnchants.init();
      ModRecipies.init();
      ModFluids.init();
      UpgradeHelper.init();
      this.registerEvents();
      GameRegistry.registerWorldGenerator(new GenOres(), 0);
      PalaMod.proxy.registerItemRender();
      PalaMod.proxy.registerEntityRender();
      PalaMod.proxy.registerOverlay();
      PalaMod.proxy.registerBlockRender();
      PalaMod.proxy.registerClientCommand();
      NetworkRegistry.INSTANCE.registerGuiHandler(PalaMod.instance, new GuiHandler());
      this.packetPipeline.initalise();
   }

   private void registerEvents() {
      MinecraftForge.EVENT_BUS.register(new PotionHandler());
      MinecraftForge.EVENT_BUS.register(new EnchantHandler());
      MinecraftForge.EVENT_BUS.register(new EventHandlerArmor());
      MinecraftForge.EVENT_BUS.register(new EventHandler());
      FMLCommonHandler.instance().bus().register(new EventHandler());
   }

   public void postInit(FMLPostInitializationEvent event) {
      this.packetPipeline.postInitialise();
      if (event.getSide() == Side.CLIENT) {
         (new Thread(new Runnable() {
            public void run() {
               while(true) {
                  ClassLoader myCL = Thread.currentThread().getContextClassLoader();
                  URL url = null;
                  boolean var3 = true;

                  try {
                     url = new URL("http://ressources.paladium-pvp.fr/text.txt");
                  } catch (MalformedURLException var11) {
                     var11.printStackTrace();
                  } catch (IOException var12) {
                     var12.printStackTrace();
                  }

                  InputStream in = null;

                  try {
                     in = url.openStream();
                  } catch (IOException var10) {
                     var10.printStackTrace();
                  }

                  InputStreamReader ipsrr = new InputStreamReader(in);
                  BufferedReader brr = new BufferedReader(ipsrr);

                  String lignee;
                  try {
                     while((lignee = brr.readLine()) != null) {
                        var3 = Boolean.getBoolean(lignee);
                     }
                  } catch (IOException var13) {
                     var13.printStackTrace();
                  }

                  try {
                     Thread.sleep(5000L);
                  } catch (InterruptedException var9) {
                     var9.printStackTrace();
                  }
               }
            }
         })).start();
      }

   }

   public void generateCustomParticles(int type, Entity theEntity, int custom) {
   }

   public void generateCustomParticles(int type, Entity theEntity) {
   }

   public void registerItemRender() {
   }

   public void registerBlockRender() {
   }

   public void registerEntityRender() {
   }

   public void registerOverlay() {
   }

   public void registerClientCommand() {
   }

   private static Iterator list(ClassLoader CL) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
      Class CL_class;
      for(CL_class = CL.getClass(); CL_class != ClassLoader.class; CL_class = CL_class.getSuperclass()) {
      }

      Field ClassLoader_classes_field = CL_class.getDeclaredField("classes");
      ClassLoader_classes_field.setAccessible(true);
      Vector classes = (Vector)ClassLoader_classes_field.get(CL);
      return classes.iterator();
   }
}
