package fr.paladium.palamod.common;

import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraftforge.common.MinecraftForge;

public class EventBusHelper {
   public static void register(Object handler, EventBusHelper.Type type) {
      if (type == EventBusHelper.Type.FORGE || type == EventBusHelper.Type.BOTH) {
         MinecraftForge.EVENT_BUS.register(handler);
      }

      if (type == EventBusHelper.Type.FML || type == EventBusHelper.Type.BOTH) {
         FMLCommonHandler.instance().bus().register(handler);
      }

   }

   public static enum Type {
      FORGE,
      FML,
      BOTH;
   }
}
