package fr.paladium.palamod.common;

import cpw.mods.fml.client.GuiIngameModOptions;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.eventhandler.Event.Result;
import cpw.mods.fml.common.gameevent.TickEvent.ClientTickEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.blocks.ModBlocks;
import fr.paladium.palamod.client.gui.GuiButtonPala;
import fr.paladium.palamod.client.gui.custom.GuiMainMenu;
import fr.paladium.palamod.entities.mobs.EntityCamera;
import fr.paladium.palamod.entities.mobs.EntityCustomWither;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.entities.projectiles.EntityCustomArrow;
import fr.paladium.palamod.items.ItemCameraTablet;
import fr.paladium.palamod.items.ItemChestExplorer;
import fr.paladium.palamod.items.ItemWitherUnSummoner;
import fr.paladium.palamod.network.packets.PacketMessageBoss;
import fr.paladium.palamod.util.GuardianHelper;
import fr.paladium.palamod.util.WitherData;
import java.awt.Desktop;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiShareToLan;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.GuiScreenEvent.ActionPerformedEvent;
import net.minecraftforge.client.event.GuiScreenEvent.InitGuiEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderGameOverlayEvent.Pre;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.EntityInteractEvent;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;
import net.minecraftforge.event.world.ExplosionEvent;
import org.lwjgl.input.Keyboard;

public class EventHandler {
   public static Map<Block, Item> buckets = new HashMap();
   public static long timeHurt;
   public static final String[] directions = new String[]{"Sud", "Ouest", "Nord", "Est"};
   private static long v2 = 0L;

   @SubscribeEvent
   public void onLivingHurt(LivingHurtEvent event) {
      DamageSource source = event.source;
      if (source.isProjectile()) {
         Entity entity = source.getSourceOfDamage();
         if (entity instanceof EntityCustomArrow && event.entity instanceof EntityLivingBase) {
            EntityCustomArrow arrow = (EntityCustomArrow)entity;
            EntityLivingBase base = (EntityLivingBase)event.entity;
            int type = arrow.getType();
            switch(type) {
            case 0:
               base.addPotionEffect(new PotionEffect(Potion.poison.id, 100, 2));
               break;
            case 1:
               base.addPotionEffect(new PotionEffect(Potion.wither.id, 100, 2));
               break;
            case 2:
               base.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 100, 2));
               break;
            case 3:
               event.ammount = 0.0F;
               arrow.getPlayer().addPotionEffect(new PotionEffect(Potion.confusion.id, 250, 255));
               double x1 = arrow.getPlayer().posX;
               double y1 = arrow.getPlayer().posY;
               double z1 = arrow.getPlayer().posZ;
               double x2 = base.posX;
               double y2 = base.posY;
               double z2 = base.posZ;
               if (!(base instanceof EntityPlayer)) {
                  arrow.getPlayer().setPositionAndUpdate(x2, y2, z2);
                  base.setPosition(x1, y1, z1);
                  event.ammount = 0.0F;
               }

               event.setCanceled(true);
            }
         }
      }

   }

   @SubscribeEvent
   public void onDeath(LivingDeathEvent event) {
      if (event.entity instanceof EntityPlayer) {
         EntityPlayer player = (EntityPlayer)event.entity;
         NBTTagCompound tag = player.getEntityData();
         player.closeScreen();
         if (tag.hasKey("contentBackpack")) {
            NBTTagList nbtlist = tag.getTagList("contentBackpack", 10);

            int i;
            for(int i1 = 0; i1 < nbtlist.tagCount(); ++i1) {
               NBTTagCompound comp1 = nbtlist.getCompoundTagAt(i1);
               i1 = comp1.getInteger("Slot");
               ItemStack toDrop = ItemStack.loadItemStackFromNBT(comp1);
               EntityItem entityItem = new EntityItem(player.worldObj, player.posX, player.posY + 1.0D, player.posZ, toDrop);
               player.worldObj.spawnEntityInWorld(entityItem);
            }

            ItemStack[] content = new ItemStack[54];
            NBTTagList nbtlist2 = new NBTTagList();

            for(i = 0; i < content.length; ++i) {
               if (content[i] != null) {
                  NBTTagCompound comp1 = new NBTTagCompound();
                  comp1.setInteger("Slot", i);
                  content[i].writeToNBT(comp1);
                  nbtlist2.appendTag(comp1);
               }
            }

            tag.setTag("contentBackpack", nbtlist2);
         }
      }

   }

   @SubscribeEvent
   public void onAttack(AttackEntityEvent event) {
      if (event.entity.worldObj.isRemote) {
         if (EntityCamera.isActive()) {
            event.setCanceled(true);
            return;
         }

         if (event.entityLiving instanceof EntityPlayer && (EntityPlayer)event.entityLiving == Minecraft.getMinecraft().thePlayer && ((EntityPlayer)event.entityLiving == Minecraft.getMinecraft().thePlayer || event.entityPlayer == Minecraft.getMinecraft().thePlayer)) {
            timeHurt = System.currentTimeMillis() + 5000L;
         }
      }

   }

   @SubscribeEvent
   public void onPlayerMove(LivingUpdateEvent e) {
      if (e.entity instanceof EntityPlayer) {
         EntityPlayer p = (EntityPlayer)e.entity;
         if (p.getHeldItem() != null && p.getHeldItem().getItem() instanceof ItemCameraTablet && ItemCameraTablet.isWorking(p.getHeldItem()) && !p.isSneaking() && p.worldObj.isRemote) {
            EntityCamera.rotateCamera(p.motionX);
         }
      }

   }

   @SubscribeEvent
   public void worldTick(ClientTickEvent event) {
      if (timeHurt != 0L && timeHurt <= System.currentTimeMillis()) {
         timeHurt = 0L;
      }

   }

   @SideOnly(Side.CLIENT)
   @SubscribeEvent(
      priority = EventPriority.HIGHEST
   )
   public void onOverlay(Pre event) {
      Minecraft mc = Minecraft.getMinecraft();
      FontRenderer v2;
      if (event.type == ElementType.DEBUG) {
         v2 = Minecraft.getMinecraft().fontRenderer;
         v2.drawStringWithShadow("x: " + (int)mc.thePlayer.posX, 30, 30, Keyboard.isKeyDown(19) ? 16711680 : 16777215);
         v2.drawStringWithShadow("y: " + (int)mc.thePlayer.posY, 30, 40, 16777215);
         v2.drawStringWithShadow("z: " + (int)mc.thePlayer.posZ, 30, 50, Keyboard.isKeyDown(19) ? 16711680 : 16777215);
         v2.drawStringWithShadow("yawl: " + (int)mc.thePlayer.rotationYaw, 120, 30, 16777215);
         v2.drawStringWithShadow("pitch: " + ((int)mc.thePlayer.rotationPitch == -90 ? "666" : (int)mc.thePlayer.rotationPitch), 120, 40, 16777215);
         v2.drawStringWithShadow(Minecraft.getMinecraft().debug, 30, 60, 16777215);
         v2.drawStringWithShadow("direction: " + directions[MathHelper.floor_double((double)(Minecraft.getMinecraft().thePlayer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3], 120, 50, 16777215);
         if (Keyboard.isKeyDown(50) && Keyboard.isKeyDown(25)) {
            v2.drawStringWithShadow("Sam54 et HeavenIsALie sont des beau gosses", 50, 100, 16711680);
         }

         event.setCanceled(true);
      }

      if (event.type == ElementType.DEBUG) {
         v2 = Minecraft.getMinecraft().fontRenderer;
         v2.drawStringWithShadow("x: " + (int)mc.thePlayer.posX, 30, 30, Keyboard.isKeyDown(19) ? 16711680 : 16777215);
         v2.drawStringWithShadow("y: " + (int)mc.thePlayer.posY, 30, 40, 16777215);
         v2.drawStringWithShadow("z: " + (int)mc.thePlayer.posZ, 30, 50, Keyboard.isKeyDown(19) ? 16711680 : 16777215);
         v2.drawStringWithShadow("yawl: " + (int)mc.thePlayer.rotationYaw, 120, 30, 16777215);
         v2.drawStringWithShadow("pitch: " + ((int)mc.thePlayer.rotationPitch == -90 ? "666" : (int)mc.thePlayer.rotationPitch), 120, 40, 16777215);
         v2.drawStringWithShadow(Minecraft.getMinecraft().debug, 30, 60, 16777215);
         v2.drawStringWithShadow("direction: " + directions[MathHelper.floor_double((double)(Minecraft.getMinecraft().thePlayer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3], 120, 50, 16777215);
         if (Keyboard.isKeyDown(50) && Keyboard.isKeyDown(25)) {
            v2.drawStringWithShadow("Sam54 et HeavenIsALie sont des beau gosses", 50, 100, 16711680);
         }

         event.setCanceled(true);
      }

   }

   @SideOnly(Side.CLIENT)
   @SubscribeEvent(
      priority = EventPriority.HIGHEST
   )
   public void initGui(InitGuiEvent v1) {
      Minecraft mc = Minecraft.getMinecraft();
      if (v1.gui instanceof GuiMainMenu) {
         GuiScreen v2 = v1.gui;
         int[] v3 = new int[]{mc.getSession().getToken() == "FML" ? -1 : 1, 14, 6, 2};
         Iterator var5 = v1.buttonList.iterator();

         while(var5.hasNext()) {
            Object o = var5.next();
            GuiButtonPala v4 = (GuiButtonPala)o;
            int[] var8 = v3;
            int var9 = v3.length;

            for(int var10 = 0; var10 < var9; ++var10) {
               int v = var8[var10];
               if (v == v4.id) {
                  v4.visible = false;
               }
            }

            Iterator var15 = v1.buttonList.iterator();

            while(var15.hasNext()) {
               Object o1 = var15.next();
               GuiButtonPala btn1 = (GuiButtonPala)o1;
               int[] var18 = v3;
               int var12 = v3.length;

               for(int var13 = 0; var13 < var12; ++var13) {
                  int v = var18[var13];
                  if (v == btn1.id) {
                     btn1.visible = false;
                  }
               }
            }
         }
      }

   }

   @SubscribeEvent
   public void onClick(PlayerInteractEvent e) {
      EntityPlayer player = e.entityPlayer;
      if (e.action == Action.LEFT_CLICK_BLOCK && player.isSneaking() && player.getHeldItem() != null && player.getHeldItem().getItem() instanceof ItemChestExplorer) {
         e.setCanceled(true);
      }

      if (e.action == Action.RIGHT_CLICK_AIR && player.getHeldItem() != null && player.getHeldItem().equals(new ItemStack(ModBlocks.invisibleBlock))) {
         ModBlocks.invisibleBlock.setBlockTextureName("palamod:InvisibleBlock_Crea");
      }

   }

   @SideOnly(Side.CLIENT)
   @SubscribeEvent(
      priority = EventPriority.HIGHEST
   )
   public void onAction(ActionPerformedEvent v1) {
      if (v1.gui instanceof GuiShareToLan) {
         v1.gui.mc.shutdown();
      }

   }

   @SideOnly(Side.CLIENT)
   @SubscribeEvent(
      priority = EventPriority.HIGHEST
   )
   public void openGuiEvent(GuiOpenEvent v1) {
      if (v1.gui instanceof GuiIngameModOptions) {
         v1.setCanceled(true);
      }

      if (v1.gui instanceof net.minecraft.client.gui.GuiMainMenu) {
         v1.gui = new GuiMainMenu();
      }

      if (v1.gui instanceof GuiIngameMenu) {
         v1.gui = new GuiIngameMenu() {
            private int counter = 0;

            protected void actionPerformed(GuiButton v2) {
               if (v2.id == 12) {
                  try {
                     Desktop.getDesktop().browse(new URI("ts3server://ts.paladium-pvp.fr/?nickname=" + Minecraft.getMinecraft().getSession().getUsername()));
                  } catch (Exception var3) {
                     var3.printStackTrace();
                  }
               } else {
                  super.actionPerformed(v2);
               }

            }

            public void initGui() {
               super.initGui();
               GuiButton v3 = (GuiButton)this.buttonList.get(0);
               v3.displayString = "(Patientez 5 secondes)";
               v3.enabled = false;
               GuiButton v4 = (GuiButton)this.buttonList.get(3);
               v4.displayString = "Teamspeak";
            }

            public void updateScreen() {
               super.updateScreen();
               GuiButton v3 = (GuiButton)this.buttonList.get(0);
               ++this.counter;
               if (this.counter < 20) {
                  v3.displayString = "(Patientez 5 secondes)";
                  v3.enabled = false;
               } else if (this.counter > 20 && this.counter < 40) {
                  v3.displayString = "(Patientez 4 secondes)";
                  v3.enabled = false;
               } else if (this.counter > 40 && this.counter < 60) {
                  v3.displayString = "(Patientez 3 secondes)";
                  v3.enabled = false;
               } else if (this.counter > 60 && this.counter < 80) {
                  v3.displayString = "(Patientez 2 secondes)";
                  v3.enabled = false;
               } else if (this.counter > 80 && this.counter < 100) {
                  v3.displayString = "(Patientez 1 secondes)";
                  v3.enabled = false;
               } else if (this.counter > 100) {
                  v3.displayString = I18n.format("menu.disconnect", new Object[0]);
                  v3.enabled = true;
               }

            }
         };
      }

   }

   @SubscribeEvent
   public void onBucketFill(FillBucketEvent event) {
      ItemStack result = this.fillCustomBucket(event.world, event.target, event.entityPlayer);
      if (result != null) {
         event.result = result;
         event.setResult(Result.ALLOW);
      }
   }

   private ItemStack fillCustomBucket(World world, MovingObjectPosition pos, EntityPlayer player) {
      Block block = world.getBlock(pos.blockX, pos.blockY, pos.blockZ);
      if (!player.canHarvestBlock(block)) {
         return null;
      } else {
         Item bucket = (Item)buckets.get(block);
         if (bucket != null && world.getBlockMetadata(pos.blockX, pos.blockY, pos.blockZ) == 0) {
            world.setBlockToAir(pos.blockX, pos.blockY, pos.blockZ);
            return new ItemStack(bucket);
         } else {
            return null;
         }
      }
   }

   @SubscribeEvent
   public void onJoin(EntityJoinWorldEvent event) {
      if (event.entity instanceof EntityPlayer) {
         EntityPlayer entityPlayer = (EntityPlayer)event.entity;
         if (BossHandler.check()) {
            PacketMessageBoss packet = new PacketMessageBoss();
            packet.addInformations(BossHandler.x, BossHandler.z, (byte)0, BossHandler.time);
            PalaMod var10000 = PalaMod.instance;
            PalaMod.proxy.packetPipeline.sendTo(packet, (EntityPlayerMP)event.entity);
         }

      }
   }

   @SubscribeEvent
   public void onTick(ServerTickEvent event) {
      if (event.phase == Phase.END) {
         BossHandler.tick();
      }

   }

   @SubscribeEvent
   public void onItemToolTip(ItemTooltipEvent event) {
      if ((event.itemStack.getItem() == Item.getItemFromBlock(ModBlocks.upgraded_obsidian) || event.itemStack.getItem() == Item.getItemFromBlock(Blocks.obsidian)) && event.itemStack.hasTagCompound()) {
         NBTTagCompound compound = event.itemStack.getTagCompound();
         if (compound.getBoolean("Explode")) {
            event.toolTip.add(EnumChatFormatting.DARK_PURPLE + "Explode");
         }

         if (compound.getBoolean("Fake")) {
            event.toolTip.add(EnumChatFormatting.DARK_PURPLE + "Fake");
         }

         if (compound.getBoolean("TwoLife")) {
            event.toolTip.add(EnumChatFormatting.DARK_PURPLE + "TwoLifes");
         }

         if (compound.getBoolean("Camouflage")) {
            event.toolTip.add(EnumChatFormatting.DARK_PURPLE + "Camouflage");
         }
      }

   }

   @SubscribeEvent
   public void onEntityInteractEvent(EntityInteractEvent event) {
      if (event.entityPlayer.getHeldItem() != null && event.entityPlayer.getHeldItem().getItem() instanceof ItemWitherUnSummoner) {
         if (event.entityPlayer.capabilities.isCreativeMode) {
            if (event.target instanceof EntityCustomWither || event.target instanceof EntityWither) {
               event.target.setRotationYawHead(0.0F);
               event.target.setDead();
            }
         } else if (event.target instanceof EntityCustomWither && ((EntityCustomWither)event.target).getWitherData().remote) {
            event.target.setRotationYawHead(0.0F);
            event.target.setDead();
         }

      }
   }

   @SubscribeEvent
   public void onEntityHurt(LivingHurtEvent event) {
      EntityLivingBase entityLiving = event.entityLiving;
      if (entityLiving instanceof EntityCustomWither) {
         WitherData witherData = ((EntityCustomWither)entityLiving).getWitherData();
         if (event.source.isExplosion() && witherData.explosion != 0) {
            switch(witherData.explosion) {
            case 1:
               event.ammount /= 2.0F;
               break;
            case 2:
               event.ammount /= 10.0F;
            }
         }

         if (event.source == DamageSource.anvil && witherData.anvil != 0) {
            switch(witherData.anvil) {
            case 1:
               event.ammount /= 2.0F;
               break;
            case 2:
               event.ammount = 0.0F;
            }
         }
      }

   }

   @SubscribeEvent
   public void onExplode(ExplosionEvent event) {
      Explosion explosion = event.explosion;
      if (explosion.getExplosivePlacedBy() == null || !(explosion.getExplosivePlacedBy() instanceof EntityWither)) {
         double x = explosion.explosionX;
         double y = explosion.explosionY;
         double z = explosion.explosionZ;
         List e = event.world.getEntitiesWithinAABB(EntityGuardianGolem.class, AxisAlignedBB.getBoundingBox(x - 6.0D, y - 6.0D, z - 6.0D, x + 6.0D, y + 6.0D, z + 6.0D));
         if (e.size() != 0) {
            EntityGuardianGolem golem = (EntityGuardianGolem)e.get(0);
            if (golem == null || !GuardianHelper.hasUpgrade(golem, 16)) {
               return;
            }

            explosion.explosionSize = 0.0F;
            Minecraft.getMinecraft().getIntegratedServer().worldServerForDimension(golem.dimension).func_147487_a("explode", golem.posX, golem.posY, golem.posZ, 32, 0.0D, 0.0D, 0.0D, 1.0D);
         }

      }
   }

   @SubscribeEvent
   public void onJump(LivingJumpEvent event) {
      if (event.entityLiving instanceof EntityCustomWither && ((EntityCustomWither)event.entityLiving).getWitherData().nofly) {
         EntityCustomWither var10000 = (EntityCustomWither)event.entityLiving;
         var10000.motionY += 0.25D;
      }

   }
}
