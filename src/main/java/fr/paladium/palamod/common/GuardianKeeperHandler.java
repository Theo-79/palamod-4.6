package fr.paladium.palamod.common;

import fr.paladium.palamod.common.inventory.InventoryGuardianKeeper;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.World;
import net.minecraft.world.WorldSavedData;

public class GuardianKeeperHandler extends WorldSavedData {
   private static final String DATA_NAME = "palamod_GuardianKeeper";
   HashMap<String, InventoryGuardianKeeper> guardianMap = new HashMap();

   public GuardianKeeperHandler(String name) {
      super(name);
   }

   public GuardianKeeperHandler() {
      super("palamod_GuardianKeeper");
   }

   public void readFromNBT(NBTTagCompound tag) {
      NBTTagList list = tag.getTagList("data", 10);

      for(int i = 0; i < list.tagCount(); ++i) {
         NBTTagCompound entry = list.getCompoundTagAt(i);
         String uuid = entry.getString("uuid");
         ItemStack stack = ItemStack.loadItemStackFromNBT(entry.getCompoundTag("item"));
         InventoryGuardianKeeper inventory = new InventoryGuardianKeeper();
         inventory.setInventorySlotContents(0, stack);
         this.guardianMap.put(uuid, inventory);
      }

   }

   public void writeToNBT(NBTTagCompound tag) {
      NBTTagList list = new NBTTagList();
      Iterator var3 = this.guardianMap.entrySet().iterator();

      while(var3.hasNext()) {
         Entry<String, InventoryGuardianKeeper> entry = (Entry)var3.next();
         NBTTagCompound tag2 = new NBTTagCompound();
         tag2.setString("uuid", (String)entry.getKey());
         new NBTTagCompound();
         NBTTagCompound stackComp = new NBTTagCompound();
         ItemStack stack = ((InventoryGuardianKeeper)entry.getValue()).getStackInSlot(0);
         if (stack != null) {
            stack.writeToNBT(stackComp);
         }

         tag2.setTag("item", stackComp);
         list.appendTag(tag2);
      }

      tag.setTag("data", list);
   }

   public InventoryGuardianKeeper getStoneFromUUID(String uuid) {
      if (!this.guardianMap.containsKey(uuid)) {
         this.guardianMap.put(uuid, new InventoryGuardianKeeper());
      }

      this.markDirty();
      return (InventoryGuardianKeeper)this.guardianMap.get(uuid);
   }

   public static GuardianKeeperHandler get(World worldObj) {
      GuardianKeeperHandler handler = (GuardianKeeperHandler)worldObj.loadItemData(GuardianKeeperHandler.class, "palamod_GuardianKeeper");
      if (handler == null) {
         handler = new GuardianKeeperHandler();
         worldObj.setItemData("palamod_GuardianKeeper", handler);
      }

      return handler;
   }
}
