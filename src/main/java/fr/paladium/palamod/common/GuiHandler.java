package fr.paladium.palamod.common;

import cpw.mods.fml.common.network.IGuiHandler;
import fr.paladium.palamod.client.gui.GuiAlchemyCreatorArrow;
import fr.paladium.palamod.client.gui.GuiAlchemyCreatorPotion;
import fr.paladium.palamod.client.gui.GuiAlchemyStacker;
import fr.paladium.palamod.client.gui.GuiArmorCompressor;
import fr.paladium.palamod.client.gui.GuiBackpack;
import fr.paladium.palamod.client.gui.GuiBowMachine;
import fr.paladium.palamod.client.gui.GuiCameraTablet;
import fr.paladium.palamod.client.gui.GuiChestExplorer;
import fr.paladium.palamod.client.gui.GuiFAnnounce;
import fr.paladium.palamod.client.gui.GuiFurnaceInfo;
import fr.paladium.palamod.client.gui.GuiGuardianAnchor;
import fr.paladium.palamod.client.gui.GuiGuardianChest;
import fr.paladium.palamod.client.gui.GuiGuardianGolem;
import fr.paladium.palamod.client.gui.GuiGuardianKeeper;
import fr.paladium.palamod.client.gui.GuiGuardianUpgrade;
import fr.paladium.palamod.client.gui.GuiGuardianWhitelist;
import fr.paladium.palamod.client.gui.GuiMagicalAnvilAdd;
import fr.paladium.palamod.client.gui.GuiMulticolorBlock;
import fr.paladium.palamod.client.gui.GuiObsidianUpgrader;
import fr.paladium.palamod.client.gui.GuiOnlineDetector;
import fr.paladium.palamod.client.gui.GuiPaladiumChest;
import fr.paladium.palamod.client.gui.GuiPaladiumFurnace;
import fr.paladium.palamod.client.gui.GuiPaladiumGrinder;
import fr.paladium.palamod.client.gui.GuiPaladiumMachine;
import fr.paladium.palamod.client.gui.GuiPaladiumOven;
import fr.paladium.palamod.client.gui.GuiStuffSwitcher;
import fr.paladium.palamod.client.gui.GuiVoidStone;
import fr.paladium.palamod.common.gui.ContainerAlchemyCreatorArrow;
import fr.paladium.palamod.common.gui.ContainerAlchemyCreatorPotion;
import fr.paladium.palamod.common.gui.ContainerAlchemyStacker;
import fr.paladium.palamod.common.gui.ContainerArmorCompressor;
import fr.paladium.palamod.common.gui.ContainerBackpack;
import fr.paladium.palamod.common.gui.ContainerBowMachine;
import fr.paladium.palamod.common.gui.ContainerChestExplorer;
import fr.paladium.palamod.common.gui.ContainerEmpty;
import fr.paladium.palamod.common.gui.ContainerGuardianChest;
import fr.paladium.palamod.common.gui.ContainerGuardianGolem;
import fr.paladium.palamod.common.gui.ContainerGuardianKeeper;
import fr.paladium.palamod.common.gui.ContainerGuardianUpgrade;
import fr.paladium.palamod.common.gui.ContainerGuardianWhitelist;
import fr.paladium.palamod.common.gui.ContainerMagicalAnvilAdd;
import fr.paladium.palamod.common.gui.ContainerObsidianUpgrader;
import fr.paladium.palamod.common.gui.ContainerPaladiumChest;
import fr.paladium.palamod.common.gui.ContainerPaladiumFurnace;
import fr.paladium.palamod.common.gui.ContainerPaladiumGrinder;
import fr.paladium.palamod.common.gui.ContainerPaladiumMachine;
import fr.paladium.palamod.common.gui.ContainerPaladiumOven;
import fr.paladium.palamod.common.gui.ContainerStuffSwitcher;
import fr.paladium.palamod.common.gui.ContainerVoidStone;
import fr.paladium.palamod.common.inventory.InventoryBackpack;
import fr.paladium.palamod.common.inventory.InventoryStuffSwitcher;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.tiles.TileEntityAlchemyCreator;
import fr.paladium.palamod.tiles.TileEntityAlchemyStacker;
import fr.paladium.palamod.tiles.TileEntityArmorCompressor;
import fr.paladium.palamod.tiles.TileEntityBowMachine;
import fr.paladium.palamod.tiles.TileEntityGuardianAnchor;
import fr.paladium.palamod.tiles.TileEntityMagicalAnvil;
import fr.paladium.palamod.tiles.TileEntityMulticolorBlock;
import fr.paladium.palamod.tiles.TileEntityObsidianUpgrader;
import fr.paladium.palamod.tiles.TileEntityOnlineDetector;
import fr.paladium.palamod.tiles.TileEntityPaladiumChest;
import fr.paladium.palamod.tiles.TileEntityPaladiumFurnace;
import fr.paladium.palamod.tiles.TileEntityPaladiumGrinder;
import fr.paladium.palamod.tiles.TileEntityPaladiumMachine;
import fr.paladium.palamod.tiles.TileEntityPaladiumOven;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class GuiHandler implements IGuiHandler {
   public static final int PALADIUM_MACHINE = 0;
   public static final int ALCHEMY_CREATOR_POTION = 1;
   public static final int ALCHEMY_CREATOR_ARROW = 2;
   public static final int PALADIUM_FURNACE = 3;
   public static final int PALADIUM_CHEST = 4;
   public static final int BOW_MACHINE = 5;
   public static final int ALCHEMY_STACKER = 6;
   public static final int GUARDIAN_GOLEM = 7;
   public static final int BACKPACK = 8;
   public static final int ONLINE_DETECTOR = 9;
   public static final int VOID_STONE = 10;
   public static final int BOOK = 11;
   public static final int PALADIUM_GRINDER = 12;
   public static final int CHEST_EXPLORER = 13;
   public static final int STUFF_SWITCHER = 14;
   public static final int GUARDIAN_WHITELIST = 15;
   public static final int GUARDIAN_KEEPER = 16;
   public static final int PALADIUM_OVEN = 17;
   public static final int FURNACE_INFO = 18;
   public static final int FACTION_INFO = 19;
   public static final int MAGICAL_ANVIL_ADD = 20;
   public static final int ARMOR_COMPRESSOR = 21;
   public static final int MULTICOLOR_BLOCK = 22;
   public static final int GUARDIAN_ANCHOR = 23;
   public static final int GUARDIAN_CHEST = 24;
   public static final int GUARDIAN_UPGRADE = 25;
   public static final int OBSIDIAN_UPGRADER = 26;
   public static final int CAMERA_TABLET = 27;

   public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
      switch(ID) {
      case 0:
         return new ContainerPaladiumMachine((TileEntityPaladiumMachine)world.getTileEntity(x, y, z), player.inventory);
      case 1:
         return new ContainerAlchemyCreatorPotion((TileEntityAlchemyCreator)world.getTileEntity(x, y, z), player);
      case 2:
         return new ContainerAlchemyCreatorArrow((TileEntityAlchemyCreator)world.getTileEntity(x, y, z), player);
      case 3:
         return new ContainerPaladiumFurnace((TileEntityPaladiumFurnace)world.getTileEntity(x, y, z), player.inventory);
      case 4:
         return new ContainerPaladiumChest((TileEntityPaladiumChest)world.getTileEntity(x, y, z), player.inventory);
      case 5:
         return new ContainerBowMachine((TileEntityBowMachine)world.getTileEntity(x, y, z), player.inventory);
      case 6:
         return new ContainerAlchemyStacker((TileEntityAlchemyStacker)world.getTileEntity(x, y, z), player.inventory);
      case 7:
         return new ContainerGuardianGolem((EntityGuardianGolem)world.getEntityByID(x), player.inventory);
      case 8:
         return new ContainerBackpack(player.inventory, new InventoryBackpack(player));
      case 9:
         return new ContainerEmpty();
      case 10:
         return new ContainerVoidStone(player.inventory);
      case 11:
      default:
         return null;
      case 12:
         return new ContainerPaladiumGrinder(player.inventory, (TileEntityPaladiumGrinder)world.getTileEntity(x, y, z));
      case 13:
         return new ContainerChestExplorer(world.getTileEntity(x, y, z));
      case 14:
         return new ContainerStuffSwitcher(player.inventory, new InventoryStuffSwitcher(player.getHeldItem()));
      case 15:
         return new ContainerGuardianWhitelist(player.inventory);
      case 16:
         return new ContainerGuardianKeeper(player);
      case 17:
         return new ContainerPaladiumOven((TileEntityPaladiumOven)world.getTileEntity(x, y, z), player.inventory);
      case 18:
         return new ContainerEmpty();
      case 19:
         return new ContainerEmpty();
      case 20:
         return new ContainerMagicalAnvilAdd((TileEntityMagicalAnvil)world.getTileEntity(x, y, z), player);
      case 21:
         return new ContainerArmorCompressor((TileEntityArmorCompressor)world.getTileEntity(x, y, z), player);
      case 22:
         return null;
      case 23:
         return new ContainerEmpty();
      case 24:
         return new ContainerGuardianChest((EntityGuardianGolem)world.getEntityByID(x), player.inventory);
      case 25:
         return new ContainerGuardianUpgrade((EntityGuardianGolem)world.getEntityByID(x), player.inventory);
      case 26:
         return new ContainerObsidianUpgrader((TileEntityObsidianUpgrader)world.getTileEntity(x, y, z), player.inventory);
      case 27:
         return null;
      }
   }

   public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
      switch(ID) {
      case 0:
         return new GuiPaladiumMachine((TileEntityPaladiumMachine)world.getTileEntity(x, y, z), player.inventory);
      case 1:
         return new GuiAlchemyCreatorPotion((TileEntityAlchemyCreator)world.getTileEntity(x, y, z), player);
      case 2:
         return new GuiAlchemyCreatorArrow((TileEntityAlchemyCreator)world.getTileEntity(x, y, z), player);
      case 3:
         return new GuiPaladiumFurnace((TileEntityPaladiumFurnace)world.getTileEntity(x, y, z), player.inventory);
      case 4:
         return new GuiPaladiumChest((TileEntityPaladiumChest)world.getTileEntity(x, y, z), player.inventory);
      case 5:
         return new GuiBowMachine((TileEntityBowMachine)world.getTileEntity(x, y, z), player.inventory);
      case 6:
         return new GuiAlchemyStacker((TileEntityAlchemyStacker)world.getTileEntity(x, y, z), player.inventory);
      case 7:
         return new GuiGuardianGolem((EntityGuardianGolem)world.getEntityByID(x), player.inventory);
      case 8:
         return new GuiBackpack(player.inventory, new InventoryBackpack(player));
      case 9:
         return new GuiOnlineDetector((TileEntityOnlineDetector)world.getTileEntity(x, y, z));
      case 10:
         return new GuiVoidStone(player.inventory);
      case 11:
      default:
         return null;
      case 12:
         return new GuiPaladiumGrinder(player.inventory, (TileEntityPaladiumGrinder)world.getTileEntity(x, y, z));
      case 13:
         return new GuiChestExplorer(world.getTileEntity(x, y, z));
      case 14:
         return new GuiStuffSwitcher(player.inventory, new InventoryStuffSwitcher(player.getHeldItem()));
      case 15:
         return new GuiGuardianWhitelist(player.getHeldItem(), player.inventory);
      case 16:
         return new GuiGuardianKeeper(player);
      case 17:
         return new GuiPaladiumOven((TileEntityPaladiumOven)world.getTileEntity(x, y, z), player.inventory);
      case 18:
         return new GuiFurnaceInfo();
      case 19:
         return new GuiFAnnounce();
      case 20:
         return new GuiMagicalAnvilAdd((TileEntityMagicalAnvil)world.getTileEntity(x, y, z), player);
      case 21:
         return new GuiArmorCompressor((TileEntityArmorCompressor)world.getTileEntity(x, y, z), player);
      case 22:
         return new GuiMulticolorBlock((TileEntityMulticolorBlock)world.getTileEntity(x, y, z));
      case 23:
         return new GuiGuardianAnchor((TileEntityGuardianAnchor)world.getTileEntity(x, y, z));
      case 24:
         return new GuiGuardianChest((EntityGuardianGolem)world.getEntityByID(x), player.inventory);
      case 25:
         return new GuiGuardianUpgrade((EntityGuardianGolem)world.getEntityByID(x), player.inventory);
      case 26:
         return new GuiObsidianUpgrader((TileEntityObsidianUpgrader)world.getTileEntity(x, y, z), player.inventory);
      case 27:
         return new GuiCameraTablet(player);
      }
   }
}
