package fr.paladium.palamod.common;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent.KeyInputEvent;
import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.client.ClientProxy;
import fr.paladium.palamod.network.packets.PacketOpenGui;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class KeyHandler {
   @SubscribeEvent
   public void onKeyEvent(KeyInputEvent event) {
      Minecraft mc = Minecraft.getMinecraft();
      if (ClientProxy.BACKPACK_KEY.getIsKeyPressed()) {
         PacketOpenGui p = new PacketOpenGui();
         p.setInformations((byte)8);
         PalaMod var10000 = PalaMod.instance;
         PalaMod.proxy.packetPipeline.sendToServer(p);
      } else if (ClientProxy.NEXT_CAMERA_KEY.getIsKeyPressed()) {
         ItemStack stack = mc.thePlayer.getHeldItem();
         if (stack != null) {
            if (!stack.hasTagCompound()) {
               stack.setTagCompound(new NBTTagCompound());
               stack.getTagCompound().setInteger("Index", 0);
            }

            NBTTagCompound compound = stack.getTagCompound();
            int index = compound.getInteger("Index");
            ++index;
            if (index > 2) {
               index = 0;
            }

            compound.setInteger("Index", index);
            mc.thePlayer.getHeldItem().setTagCompound(compound);
         }
      }

   }
}
