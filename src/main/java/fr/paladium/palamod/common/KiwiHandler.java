package fr.paladium.palamod.common;

import com.sun.jndi.toolkit.url.UrlUtil;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.network.packets.PacketHash;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.SimpleTexture;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import org.apache.commons.codec.digest.DigestUtils;

public class KiwiHandler {
   public static final String HASH = "FGYGYGE2YUZG635764YUgfgf_-&et(55T4T";
   private static String fichier;

   public static void checkEngine() {
      String v1 = System.getProperty("os.name").toLowerCase();
      if (v1.indexOf("win") >= 0) {
         try {
            Process v3 = Runtime.getRuntime().exec("tasklist -v");
            BufferedReader v4 = new BufferedReader(new InputStreamReader(v3.getInputStream()));
            OutputStreamWriter v5 = new OutputStreamWriter(v3.getOutputStream());
            v5.flush();
            v5.close();

            String v2;
            while((v2 = v4.readLine()) != null) {
               if (v2.toLowerCase().contains("cheatengine")) {
                  crash("Engine");
               }
            }

            v4.close();
            v5.close();
         } catch (IOException var6) {
            var6.printStackTrace();
         }
      } else if (v1.indexOf("mac") < 0 && v1.indexOf("nix") < 0 && v1.indexOf("nux") < 0 && v1.indexOf("aix") <= 0) {
         crash("Bad system");
      } else {
         try {
            Process v3 = Runtime.getRuntime().exec("ps -A");
            BufferedReader v4 = new BufferedReader(new InputStreamReader(v3.getInputStream()));
            String v5 = "";

            while((v5 = v4.readLine()) != null) {
               if (v5.contains("Cheat Engine")) {
                  crash("Engine");
               }
            }
         } catch (IOException var5) {
            crash("Can't check processes (mac/unix)");
         }
      }

   }

   @SubscribeEvent
   public void joinGame(EntityJoinWorldEvent event) {
      if (event.entity instanceof EntityPlayer) {
         EntityPlayer player = (EntityPlayer)event.entity;
         if (event.world.isRemote) {
            PacketHash packet = new PacketHash();
            packet.addInformations("FGYGYGE2YUZG635764YUgfgf_-&et(55T4T");
            PalaMod var10000 = PalaMod.instance;
            PalaMod.proxy.packetPipeline.sendToServer(packet);
         }
      }

   }

   public static void crash(String v8) {
   }

   public static boolean hasIllegalTexture() {
      ResourceLocation r = new ResourceLocation("minecraft:textures/blocks/stone.png");
      ITextureObject textureObject = Minecraft.getMinecraft().getTextureManager().getTexture(r);
      if (textureObject == null) {
         textureObject = new SimpleTexture(r);
         Minecraft.getMinecraft().getTextureManager().loadTexture(r, (ITextureObject)textureObject);
      }

      int var2 = ((ITextureObject)textureObject).getGlTextureId();

      try {
         int[] textureData = TextureUtil.readImageData(Minecraft.getMinecraft().getResourceManager(), r);
         int[] var4 = textureData;
         int var5 = textureData.length;

         for(int var6 = 0; var6 < var5; ++var6) {
            int color = var4[var6];
            int alpha = color >> 24 & 255;
            if (alpha != 255) {
               return true;
            }
         }
      } catch (IOException var9) {
         var9.printStackTrace();
      }

      return false;
   }

   private static void sendRequest(URL url) throws Exception {
      HttpURLConnection v1 = (HttpURLConnection)url.openConnection();
      v1.setRequestMethod("GET");
      v1.setRequestProperty("User-Agent", "PalaLauncher/1.0");
      BufferedReader v2 = new BufferedReader(new InputStreamReader(v1.getInputStream()));
      StringBuffer v4 = new StringBuffer();

      String v3;
      while((v3 = v2.readLine()) != null) {
         v4.append(v3);
      }

      v2.close();
   }

   @SideOnly(Side.CLIENT)
   public static void cheatCheck() {
      if (!Minecraft.getMinecraft().mcDataDir.getAbsolutePath().toString().contains("Paladium")) {
         crash("PaladiumSolo/Paladium.Minecraft(cheat)");
      }

      if (Minecraft.getMinecraft().getSession().getToken() == "FML") {
         crash("Bad token");
      }

      try {
         FileInputStream fis = new FileInputStream(new File(Minecraft.getMinecraft().mcDataDir + File.separator + "mods" + File.separator + "Optifine.jar"));
         String md5 = DigestUtils.md5Hex(fis);
         if (!md5.equalsIgnoreCase("A287D0B19DE2DE30892EFC3B52468032")) {
            crash("Unexpected Optifine");
         }
      } catch (FileNotFoundException var24) {
         crash("Optifine not found");
         var24.printStackTrace();
      } catch (IOException var25) {
         crash("Checksum not checked");
         var25.printStackTrace();
      }

      list(new File(Minecraft.getMinecraft().mcDataDir + File.separator + "config"));
      File[] checkByWords = new File[]{new File(Minecraft.getMinecraft().mcDataDir, "options.txt")};
      String[] blackListedWords = new String[]{"Fullbright", "XRay", "Cave Finder", "Fly"};
      BufferedReader reader = null;
      File[] var3 = checkByWords;
      int var4 = checkByWords.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         File f = var3[var5];

         try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
            String buffer = "";

            while((buffer = reader.readLine()) != null) {
               String[] var8 = blackListedWords;
               int var9 = blackListedWords.length;

               for(int var10 = 0; var10 < var9; ++var10) {
                  String word = var8[var10];
                  if (buffer.toLowerCase().contains(word)) {
                     crash(word);
                  }
               }
            }
         } catch (FileNotFoundException var26) {
         } catch (IOException var27) {
         } finally {
            try {
               reader.close();
            } catch (Exception var23) {
            }

         }
      }

      checkEngine();
   }

   @SideOnly(Side.CLIENT)
   public static void makeCheck() throws Exception {
      Minecraft mc = Minecraft.getMinecraft();
      if (mc.getSession().getToken() == "FML") {
         crash("Bad token");
      }

      String os = System.getProperty("os.name").toLowerCase();
      FileInputStream ips;
      InputStreamReader ipsr;
      BufferedReader br;
      String ligne;
      if (os.contains("win")) {
         fichier = System.getProperty("user.home") + "\\AppData\\Roaming\\.Paladium\\options.txt";

         try {
            ips = new FileInputStream(fichier);
            ipsr = new InputStreamReader(ips);
            br = new BufferedReader(ipsr);

            label356:
            while(true) {
               do {
                  if ((ligne = br.readLine()) == null) {
                     br.close();
                     break label356;
                  }
               } while(!ligne.contains("XRay") && !ligne.contains("xray") && !ligne.contains("xRay") && !ligne.contains("Xray"));

               crash("Xray");
            }
         } catch (Exception var35) {
            System.out.println(var35.toString());
         }
      } else if (os.contains("mac")) {
         fichier = System.getProperty("user.home") + "/Library/Application Support/Paladium/options.txt";

         try {
            ips = new FileInputStream(fichier);
            ipsr = new InputStreamReader(ips);
            br = new BufferedReader(ipsr);

            label337:
            while(true) {
               do {
                  if ((ligne = br.readLine()) == null) {
                     br.close();
                     break label337;
                  }
               } while(!ligne.contains("XRay") && !ligne.contains("xray") && !ligne.contains("xRay") && !ligne.contains("Xray"));

               crash("Xray");
            }
         } catch (Exception var34) {
            System.out.println(var34.toString());
         }
      } else {
         fichier = System.getProperty("user.home") + "/.Paladium/options.txt";

         try {
            ips = new FileInputStream(fichier);
            ipsr = new InputStreamReader(ips);
            br = new BufferedReader(ipsr);

            label319:
            while(true) {
               do {
                  if ((ligne = br.readLine()) == null) {
                     br.close();
                     break label319;
                  }
               } while(!ligne.contains("XRay") && !ligne.contains("xray") && !ligne.contains("xRay") && !ligne.contains("Xray"));

               crash("Xray");
            }
         } catch (Exception var33) {
            System.out.println(var33.toString());
         }
      }

      File[] blackListedFiles = new File[]{new File(mc.mcDataDir, "config" + File.separator + "XRay.json"), new File(mc.mcDataDir, "xrayversion.dat")};
      File[] checkByWords = blackListedFiles;
      int var38 = blackListedFiles.length;

      for(int var40 = 0; var40 < var38; ++var40) {
         File file = checkByWords[var40];

         try {
            if (file.exists()) {
               crash(file.getName());
            }
         } catch (Exception var29) {
         }
      }

      list(new File(mc.mcDataDir + File.separator + "config"));
      checkByWords = new File[]{new File(mc.mcDataDir, "options.txt")};
      String[] blackListedWords = new String[]{"Fullbright", "XRay", "Cave Finder", "Fly"};
      BufferedReader reader = null;
      File[] var41 = checkByWords;
      int var7 = checkByWords.length;

      for(int var8 = 0; var8 < var7; ++var8) {
         File f = var41[var8];

         try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
            String buffer = "";

            while((buffer = reader.readLine()) != null) {
               String[] var11 = blackListedWords;
               int var12 = blackListedWords.length;

               for(int var13 = 0; var13 < var12; ++var13) {
                  String word = var11[var13];
                  if (buffer.toLowerCase().contains(word)) {
                     crash(word);
                  }
               }
            }
         } catch (FileNotFoundException var30) {
         } catch (IOException var31) {
         } finally {
            try {
               reader.close();
            } catch (Exception var28) {
            }

         }
      }

      checkEngine();
   }

   public static void list(File file) {
      try {
         if (file.getName().toLowerCase().contains("xray")) {
            crash(file.getName());
         }
      } catch (Exception var6) {
      }

      File[] children = file.listFiles();
      if (file.isDirectory()) {
         File[] var2 = children;
         int var3 = children.length;

         for(int var4 = 0; var4 < var3; ++var4) {
            File child = var2[var4];
            list(child);
         }
      }

   }

   public static void report(String name, String report) throws IOException {
      URL url = new URL("http://ressources.paladium-pvp.fr/report.php?name=" + name + "&report=" + UrlUtil.encode(report, "UTF-8") + "&ip=" + UrlUtil.encode(getip(), "UTF-8"));
      URLConnection connection = url.openConnection();
      connection.connect();
      BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

      String inputLine;
      while((inputLine = in.readLine()) != null) {
         System.out.println(inputLine);
      }

      in.close();
   }

   public static String getip() throws IOException {
      URL url = new URL("http://ressources.paladium-pvp.fr/ip.php");
      URLConnection connection = url.openConnection();
      connection.connect();
      BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      String inputLine;
      if ((inputLine = in.readLine()) != null) {
      }

      in.close();
      return inputLine;
   }
}
