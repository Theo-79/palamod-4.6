package fr.paladium.palamod.common;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;

public class PlayerInformation implements IExtendedEntityProperties {
   public static final String IDENTIFIER = "Paladium_data";
   private final EntityPlayer player;

   public PlayerInformation(EntityPlayer player) {
      this.player = player;
   }

   public void saveNBTData(NBTTagCompound compound) {
   }

   public void loadNBTData(NBTTagCompound compound) {
   }

   public void init(Entity entity, World world) {
   }
}
