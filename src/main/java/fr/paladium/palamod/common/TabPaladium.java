package fr.paladium.palamod.common;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class TabPaladium extends CreativeTabs {
   public static final TabPaladium INSTANCE = new TabPaladium();

   public TabPaladium() {
      super("palamod");
      this.setNoTitle();
   }

   @SideOnly(Side.CLIENT)
   public Item getTabIconItem() {
      return ModItems.compressedPaladium;
   }
}
