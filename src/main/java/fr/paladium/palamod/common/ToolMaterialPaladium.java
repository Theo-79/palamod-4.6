package fr.paladium.palamod.common;

import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.util.EnumHelper;

public class ToolMaterialPaladium {
   public static ToolMaterial toolTypePaladium;
   public static ToolMaterial toolTypeAmethyst;
   public static ToolMaterial toolTypeTitane;
   public static ToolMaterial toolTypeEndium;

   public static void init() {
      toolTypePaladium = EnumHelper.addToolMaterial("paladiumTool", 3, 4999, 30.0F, 6.0F, 35);
      toolTypeAmethyst = EnumHelper.addToolMaterial("amethystTool", 3, 1999, 20.0F, 4.0F, 20);
      toolTypeTitane = EnumHelper.addToolMaterial("titaneTool", 3, 2999, 23.0F, 5.0F, 25);
      toolTypeEndium = EnumHelper.addToolMaterial("endiumTool", 4, 4999, 28.0F, 7.0F, 35);
   }
}
