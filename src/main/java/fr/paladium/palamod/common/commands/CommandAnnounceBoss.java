package fr.paladium.palamod.common.commands;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.BossHandler;
import fr.paladium.palamod.network.packets.PacketMessageBoss;
import java.util.List;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentTranslation;

public class CommandAnnounceBoss implements ICommand {
   public int compareTo(Object arg0) {
      return 0;
   }

   public String getCommandName() {
      return "aboss";
   }

   public String getCommandUsage(ICommandSender sender) {
      return "aboss [x] [z] [time]";
   }

   public List getCommandAliases() {
      return null;
   }

   public void processCommand(ICommandSender sender, String[] command) {
      if (command.length <= 3) {
         sender.addChatMessage(new ChatComponentTranslation("Mauvais usage", new Object[0]));
      } else {
         PacketMessageBoss packet = new PacketMessageBoss();
         boolean time = false;
         boolean x = false;
         boolean var6 = false;

         EntityPlayerMP player;
         int time1;
         int x1;
         int z;
         try {
            time1 = Integer.parseInt(command[3]);
            x1 = Integer.parseInt(command[1]);
            z = Integer.parseInt(command[2]);
            player = MinecraftServer.getServer().getConfigurationManager().func_152612_a(command[0]);
         } catch (NumberFormatException var9) {
            sender.addChatMessage(new ChatComponentTranslation("Mauvais usage", new Object[0]));
            return;
         }

         packet.addInformations(x1, z, (byte)0, time1);
         PalaMod var10000 = PalaMod.instance;
         PalaMod.proxy.packetPipeline.sendTo(packet, player);
         BossHandler.enable(time1, x1, z);
      }
   }

   public boolean canCommandSenderUseCommand(ICommandSender sender) {
      return true;
   }

   public List addTabCompletionOptions(ICommandSender p_71516_1_, String[] p_71516_2_) {
      return null;
   }

   public boolean isUsernameIndex(String[] p_82358_1_, int p_82358_2_) {
      return false;
   }
}
