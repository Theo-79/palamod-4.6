package fr.paladium.palamod.common.commands;

import java.util.List;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class CommandFurnace implements ICommand {
   public int compareTo(Object arg0) {
      return 0;
   }

   public String getCommandName() {
      return "furnace";
   }

   public String getCommandUsage(ICommandSender sender) {
      return "furnace";
   }

   public List getCommandAliases() {
      return null;
   }

   public void processCommand(ICommandSender sender, String[] command) {
      if (sender instanceof EntityPlayer) {
         EntityPlayer player = (EntityPlayer)sender;
         ItemStack item = player.getHeldItem();
         if (item == null) {
            player.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Vous devez avoir l'item que vous voulez cuire dans votre main !"));
            return;
         }

         ItemStack itemResult = FurnaceRecipes.smelting().getSmeltingResult(item);
         if (itemResult == null) {
            player.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Cet item ne peux pas Ãªtre cuit !"));
            return;
         }

         itemResult.stackSize = player.inventory.getStackInSlot(player.inventory.currentItem).stackSize;
         player.inventory.setInventorySlotContents(player.inventory.currentItem, (ItemStack)null);
         player.inventory.addItemStackToInventory(itemResult);
         player.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Le contenu de votre main a ete cuit !"));
      }

   }

   public boolean canCommandSenderUseCommand(ICommandSender sender) {
      return true;
   }

   public List addTabCompletionOptions(ICommandSender p_71516_1_, String[] p_71516_2_) {
      return null;
   }

   public boolean isUsernameIndex(String[] p_82358_1_, int p_82358_2_) {
      return false;
   }
}
