package fr.paladium.palamod.common.commands;

import fr.paladium.palamod.util.PlayerHelper;
import java.util.List;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class CommandFurnaceInfo implements ICommand {
   public int compareTo(Object arg0) {
      return 0;
   }

   public String getCommandName() {
      return "finfo";
   }

   public String getCommandUsage(ICommandSender sender) {
      return "finfo [pseudo]";
   }

   public List getCommandAliases() {
      return null;
   }

   public void processCommand(ICommandSender sender, String[] command) {
      if (command.length == 0) {
         sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Mauvais usage !"));
      }

      EntityPlayer p = PlayerHelper.getPlayerByName(command[0]);
      if (p != null) {
         p.openGui("palamod", 18, p.getEntityWorld(), 0, 0, 0);
      }

   }

   public boolean canCommandSenderUseCommand(ICommandSender sender) {
      return true;
   }

   public List addTabCompletionOptions(ICommandSender p_71516_1_, String[] p_71516_2_) {
      return null;
   }

   public boolean isUsernameIndex(String[] p_82358_1_, int p_82358_2_) {
      return false;
   }
}
