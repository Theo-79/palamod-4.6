package fr.paladium.palamod.common.commands;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.network.packets.PacketSendMessage;
import fr.paladium.palamod.util.PlayerHelper;
import java.util.List;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatComponentTranslation;

public class CommandSendMessage implements ICommand {
   public int compareTo(Object arg0) {
      return 0;
   }

   public String getCommandName() {
      return "overlay";
   }

   public String getCommandUsage(ICommandSender sender) {
      return "overlay [title] [subtitle] [time] [player/@all]";
   }

   public List getCommandAliases() {
      return null;
   }

   public void processCommand(ICommandSender sender, String[] command) {
      if (command.length <= 3) {
         sender.addChatMessage(new ChatComponentTranslation("Mauvais usage", new Object[0]));
      } else {
         PacketSendMessage packet = new PacketSendMessage();
         boolean var4 = false;

         int time;
         try {
            time = Integer.parseInt(command[2]);
         } catch (NumberFormatException var6) {
            sender.addChatMessage(new ChatComponentTranslation("Mauvais usage", new Object[0]));
            return;
         }

         command[0].replaceAll("_", " ");
         command[1].replaceAll("_", " ");
         packet.addInformations(command[0], command[1], time);
         PalaMod var10000;
         if (command[3].equalsIgnoreCase("@all")) {
            var10000 = PalaMod.instance;
            PalaMod.proxy.packetPipeline.sendToAll(packet);
         } else {
            EntityPlayer p = PlayerHelper.getPlayerByName(command[3]);
            if (p == null) {
               return;
            }

            var10000 = PalaMod.instance;
            PalaMod.proxy.packetPipeline.sendTo(packet, (EntityPlayerMP)p);
         }

      }
   }

   public boolean canCommandSenderUseCommand(ICommandSender sender) {
      return true;
   }

   public List addTabCompletionOptions(ICommandSender p_71516_1_, String[] p_71516_2_) {
      return null;
   }

   public boolean isUsernameIndex(String[] p_82358_1_, int p_82358_2_) {
      return false;
   }
}
