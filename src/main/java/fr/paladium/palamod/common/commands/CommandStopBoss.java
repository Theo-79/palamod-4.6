package fr.paladium.palamod.common.commands;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.BossHandler;
import fr.paladium.palamod.network.packets.PacketMessageBoss;
import java.util.List;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;

public class CommandStopBoss implements ICommand {
   public int compareTo(Object arg0) {
      return 0;
   }

   public String getCommandName() {
      return "sboss";
   }

   public String getCommandUsage(ICommandSender sender) {
      return "sboss";
   }

   public List getCommandAliases() {
      return null;
   }

   public void processCommand(ICommandSender sender, String[] command) {
      PacketMessageBoss packet = new PacketMessageBoss();
      packet.addInformations(0, 0, (byte)1, 0);
      PalaMod var10000 = PalaMod.instance;
      PalaMod.proxy.packetPipeline.sendToAll(packet);
      BossHandler.reset();
   }

   public boolean canCommandSenderUseCommand(ICommandSender sender) {
      return true;
   }

   public List addTabCompletionOptions(ICommandSender p_71516_1_, String[] p_71516_2_) {
      return null;
   }

   public boolean isUsernameIndex(String[] p_82358_1_, int p_82358_2_) {
      return false;
   }
}
