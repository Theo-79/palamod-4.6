package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.common.slot.SlotSize;
import fr.paladium.palamod.tiles.TileEntityAlchemyCreator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerAlchemyCreatorPotion extends Container {
   InventoryPlayer inventory;
   TileEntityAlchemyCreator tile;

   public ContainerAlchemyCreatorPotion(TileEntityAlchemyCreator tile, EntityPlayer player) {
      this.inventory = player.inventory;
      this.tile = tile;
      this.addSlotToContainer(new Slot(tile, 0, 50, 35));
      this.addSlotToContainer(new Slot(tile, 1, 79, 17));
      this.addSlotToContainer(new Slot(tile, 2, 108, 35));
      this.addSlotToContainer(new SlotSize(tile, 3, 79, 53, 1));
      this.bindPlayerInventory();
   }

   private void bindPlayerInventory() {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(this.inventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(this.inventory, i, 8 + i * 18, 142));
      }

   }

   public ItemStack transferStackInSlot(EntityPlayer player, int quantity) {
      return null;
   }

   public void onContainerClosed(EntityPlayer player) {
      super.onContainerClosed(player);
      if (TileEntityAlchemyCreator.oppenedGui.containsKey(player)) {
         TileEntityAlchemyCreator.oppenedGui.remove(player);
      }

   }

   public boolean canInteractWith(EntityPlayer player) {
      if (!TileEntityAlchemyCreator.oppenedGui.containsKey(player)) {
         TileEntityAlchemyCreator.oppenedGui.put(player, this.tile);
      }

      return this.tile.isUseableByPlayer(player);
   }
}
