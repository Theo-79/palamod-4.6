package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.common.slot.SlotPotion;
import fr.paladium.palamod.items.ItemPotion;
import fr.paladium.palamod.items.ItemSplashPotion;
import fr.paladium.palamod.tiles.TileEntityAlchemyStacker;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerAlchemyStacker extends Container {
   public TileEntityAlchemyStacker tile;

   public ContainerAlchemyStacker(TileEntityAlchemyStacker tile, InventoryPlayer inventory) {
      this.tile = tile;
      this.addSlotToContainer(new SlotPotion(tile, 0, 44, 39));
      this.addSlotToContainer(new SlotPotion(tile, 1, 104, 39));
      this.bindPlayerInventory(inventory);
   }

   private void bindPlayerInventory(InventoryPlayer inventory) {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(inventory, i, 8 + i * 18, 142));
      }

   }

   public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
      ItemStack stack = null;
      Slot slots = (Slot)this.inventorySlots.get(slot);
      if (slots != null && slots.getHasStack()) {
         ItemStack stack1 = slots.getStack();
         stack = stack1.copy();
         if (slot < 2) {
            if (!this.mergeItemStack(stack1, 3, 38, true)) {
               return null;
            }

            slots.onSlotChange(stack1, stack);
         }

         System.out.println(stack.getItemDamage());
         if (slot >= 2 && (stack.getItem() instanceof ItemPotion || stack.getItem() instanceof ItemSplashPotion || stack.getItem() instanceof net.minecraft.item.ItemPotion && stack.getItemDamage() != 0 && stack.getItemDamage() != 16) && !this.mergeItemStack(stack1, 0, 1, true)) {
            return null;
         }

         if (stack1.stackSize == 0) {
            slots.putStack((ItemStack)null);
         } else {
            slots.onSlotChanged();
         }

         if (stack1.stackSize == stack.stackSize) {
            return null;
         }

         slots.onPickupFromSlot(player, stack1);
      }

      return stack;
   }

   public void onContainerClosed(EntityPlayer player) {
      super.onContainerClosed(player);
      this.tile.closeInventory();
   }

   public boolean canInteractWith(EntityPlayer player) {
      return this.tile.isUseableByPlayer(player);
   }
}
