package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.tiles.TileEntityArmorCompressor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerArmorCompressor extends Container {
   private InventoryPlayer inventory;
   private TileEntityArmorCompressor tile;

   public ContainerArmorCompressor(TileEntityArmorCompressor tile, EntityPlayer player) {
      this.inventory = player.inventory;
      this.tile = tile;
      this.bindPlayerInventory();
      this.addSlotToContainer(new Slot(tile, 0, 79, 22));
      this.addSlotToContainer(new Slot(tile, 1, 79, 93) {
         public boolean isItemValid(ItemStack stack) {
            return false;
         }
      });
   }

   public ItemStack transferStackInSlot(EntityPlayer player, int quantity) {
      return null;
   }

   public boolean canInteractWith(EntityPlayer player) {
      return this.tile.isUseableByPlayer(player);
   }

   private void bindPlayerInventory() {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(this.inventory, j + i * 9 + 9, 8 + j * 18, 149 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(this.inventory, i, 8 + i * 18, 207));
      }

   }
}
