package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.common.inventory.InventoryBackpack;
import fr.paladium.palamod.common.slot.SlotBackpack;
import fr.paladium.palamod.items.ItemBackpack;
import fr.paladium.palamod.items.ModItems;
import fr.paladium.palamod.util.PlayerHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChatComponentText;

public class ContainerBackpack extends Container {
   int rows;
   InventoryBackpack inventoryBackpack;

   public ContainerBackpack(InventoryPlayer inventory, InventoryBackpack inventoryBackpack) {
      this.inventoryBackpack = inventoryBackpack;
      this.rows = inventoryBackpack.getSizeInventory() / 9;
      System.out.println(this.rows);

      for(int j = 0; j < this.rows; ++j) {
         for(int k = 0; k < 9; ++k) {
            this.addSlotToContainer(new SlotBackpack(inventoryBackpack, k + j * 9, 8 + k * 18, 18 + j * 18));
         }
      }

      this.bindPlayerInventory(inventory);
   }

   private void bindPlayerInventory(InventoryPlayer inventory) {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, 8 + j * 18, 140 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(inventory, i, 8 + i * 18, 198));
      }

   }

   public void writeToNBT(NBTTagCompound stack) {
      if (stack != null) {
         this.inventoryBackpack.writeToNBT(stack);
      }
   }

   public boolean canInteractWith(EntityPlayer player) {
      this.writeToNBT(player.getEntityData());
      if (!PlayerHelper.hasItem(ModItems.backpack, player)) {
         player.addChatMessage(new ChatComponentText("Vous n'avez pas de Backpack"));
      }

      return PlayerHelper.hasItem(ModItems.backpack, player);
   }

   public void onContainerClosed(EntityPlayer player) {
      this.writeToNBT(player.getEntityData());
      player.getEntityData().setBoolean("Open", false);
      super.onContainerClosed(player);
   }

   protected boolean mergeItemStack(ItemStack stack, int p_75135_2_, int p_75135_3_, boolean p_75135_4_) {
      return super.mergeItemStack(stack, p_75135_2_, p_75135_3_, p_75135_4_);
   }

   public ItemStack transferStackInSlot(EntityPlayer player, int index) {
      ItemStack itemstack = null;
      Slot slot = (Slot)this.inventorySlots.get(index);
      if (slot != null && slot.getHasStack()) {
         ItemStack itemstack1 = slot.getStack();
         itemstack = itemstack1.copy();
         if (itemstack.getItem() instanceof ItemBackpack) {
            this.writeToNBT(player.getEntityData());
            return null;
         }

         if (index < this.inventoryBackpack.getSizeInventory()) {
            if (!this.mergeItemStack(itemstack1, this.inventoryBackpack.getSizeInventory(), this.inventorySlots.size(), true)) {
               return null;
            }
         } else if (!this.mergeItemStack(itemstack1, 0, this.inventoryBackpack.getSizeInventory(), false)) {
            return null;
         }

         if (itemstack1.stackSize == 0) {
            slot.putStack((ItemStack)null);
         } else {
            slot.onSlotChanged();
         }
      }

      this.writeToNBT(player.getEntityData());
      return itemstack;
   }

   public ItemStack slotClick(int slotIndex, int buttonPressed, int flag, EntityPlayer player) {
      this.writeToNBT(player.getEntityData());
      if (flag == 2 && buttonPressed == player.inventory.currentItem) {
         return null;
      } else {
         return slotIndex - this.inventoryBackpack.getSizeInventory() - 27 == player.inventory.currentItem ? null : super.slotClick(slotIndex, buttonPressed, flag, player);
      }
   }
}
