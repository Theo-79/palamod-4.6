package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.common.slot.SlotSingle;
import fr.paladium.palamod.items.ModItems;
import fr.paladium.palamod.tiles.TileEntityBowMachine;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerBowMachine extends Container {
   public TileEntityBowMachine tile;

   public ContainerBowMachine(TileEntityBowMachine tile, InventoryPlayer inventory) {
      this.tile = tile;
      this.addSlotToContainer(new Slot(tile, 0, 81, 35));
      this.addSlotToContainer(new SlotSingle(tile, 1, 150, 35, ModItems.paladiumBow));
      this.bindPlayerInventory(inventory);
   }

   private void bindPlayerInventory(InventoryPlayer inventory) {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(inventory, i, 8 + i * 18, 142));
      }

   }

   public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
      ItemStack stack = null;
      Slot slots = (Slot)this.inventorySlots.get(slot);
      if (slots != null && slots.getHasStack()) {
         ItemStack stack1 = slots.getStack();
         stack = stack1.copy();
         if (slot < 2) {
            if (!this.mergeItemStack(stack1, 3, 38, true)) {
               return null;
            }

            slots.onSlotChange(stack1, stack);
         }

         if (slot >= 2 && !this.mergeItemStack(stack1, 0, 1, true)) {
            return null;
         }

         if (stack1.stackSize == 0) {
            slots.putStack((ItemStack)null);
         } else {
            slots.onSlotChanged();
         }

         if (stack1.stackSize == stack.stackSize) {
            return null;
         }

         slots.onPickupFromSlot(player, stack1);
      }

      return stack;
   }

   public void onContainerClosed(EntityPlayer player) {
      super.onContainerClosed(player);
      this.tile.closeInventory();
   }

   public boolean canInteractWith(EntityPlayer player) {
      return this.tile.isUseableByPlayer(player);
   }
}
