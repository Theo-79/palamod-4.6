package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.common.inventory.InventoryDummy;
import fr.paladium.palamod.common.slot.SlotChestExplorer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;

public class ContainerChestExplorer extends Container {
   IInventory inventory;

   public ContainerChestExplorer(TileEntity tile) {
      this.inventory = (IInventory)tile;
      boolean i = false;
      int j = 0;

      for(int i1 = 0; i1 < 108; ++i1) {
         if (i1 % 12 == 0) {
            ++j;
         }

         int u = i1 % 12 + 1;
         if (i1 < this.inventory.getSizeInventory()) {
            this.addSlotToContainer(new SlotChestExplorer(this.inventory, i1, u * 18 - 6, j * 18 - 10));
         } else {
            this.addSlotToContainer(new SlotChestExplorer(new InventoryDummy(), 0, u * 18 - 6, j * 18 - 10));
         }
      }

   }

   public boolean canInteractWith(EntityPlayer player) {
      return this.inventory.isUseableByPlayer(player);
   }

   public ItemStack slotClick(int slotIndex, int buttonPressed, int flag, EntityPlayer player) {
      return null;
   }

   public ItemStack transferStackInSlot(EntityPlayer player, int quantity) {
      return null;
   }
}
