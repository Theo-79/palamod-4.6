package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.common.inventory.InventoryGuardianChest;
import fr.paladium.palamod.common.slot.SlotGuardianChest;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.items.ItemBackpack;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerGuardianChest extends Container {
   private EntityGuardianGolem golem;
   private InventoryGuardianChest inventoryGuardian;

   public ContainerGuardianChest(EntityGuardianGolem entity, InventoryPlayer inventory) {
      this.golem = entity;
      this.inventoryGuardian = new InventoryGuardianChest(this.golem);
      this.bindPlayerInventory(inventory);

      for(int i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            if (i * 9 + j > this.golem.getMaxChestSlots()) {
               return;
            }

            this.addSlotToContainer(new SlotGuardianChest(this.inventoryGuardian, i * 9 + j, 39 + j * 18, 39 + i * 18, this.golem));
         }
      }

   }

   private void bindPlayerInventory(InventoryPlayer inventory) {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, j * 18 + 39, 99 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(inventory, i, i * 18 + 39, 157));
      }

   }

   public void onContainerClosed(EntityPlayer player) {
      super.onContainerClosed(player);
      this.inventoryGuardian.writeToNBT(this.golem.getEntityData());
   }

   public boolean canInteractWith(EntityPlayer player) {
      return this.golem.isUseableByPlayer(player);
   }

   public ItemStack transferStackInSlot(EntityPlayer player, int index) {
      ItemStack itemstack = null;
      Slot slot = (Slot)this.inventorySlots.get(index);
      if (slot != null && slot.getHasStack()) {
         ItemStack itemstack1 = slot.getStack();
         itemstack = itemstack1.copy();
         if (itemstack.getItem() instanceof ItemBackpack) {
            this.inventoryGuardian.writeToNBT(player.getEntityData());
            return null;
         }

         if (index < this.inventoryGuardian.getSizeInventory()) {
            if (!this.mergeItemStack(itemstack1, this.inventoryGuardian.getSizeInventory(), this.inventorySlots.size(), true)) {
               return null;
            }
         } else if (!this.mergeItemStack(itemstack1, 0, this.inventoryGuardian.getSizeInventory(), false)) {
            return null;
         }

         if (itemstack1.stackSize == 0) {
            slot.putStack((ItemStack)null);
         } else {
            slot.onSlotChanged();
         }
      }

      this.inventoryGuardian.writeToNBT(player.getEntityData());
      return itemstack;
   }

   public ItemStack slotClick(int slotIndex, int buttonPressed, int flag, EntityPlayer player) {
      this.inventoryGuardian.writeToNBT(player.getEntityData());
      return slotIndex - this.inventoryGuardian.getSizeInventory() - 27 == player.inventory.currentItem ? null : super.slotClick(slotIndex, buttonPressed, flag, player);
   }
}
