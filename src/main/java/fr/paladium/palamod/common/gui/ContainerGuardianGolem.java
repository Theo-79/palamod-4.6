package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.common.slot.SlotGuardianCosmeticUpgrade;
import fr.paladium.palamod.common.slot.SlotGuardianUpgrade;
import fr.paladium.palamod.common.slot.SlotSingle;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerGuardianGolem extends Container {
   private EntityGuardianGolem golem;

   public ContainerGuardianGolem(EntityGuardianGolem entity, InventoryPlayer inventory) {
      this.golem = entity;
      this.addSlotToContainer(new SlotGuardianUpgrade(entity, 0, 84, 53, this.golem));
      this.addSlotToContainer(new SlotGuardianUpgrade(entity, 1, 102, 53, this.golem));
      this.addSlotToContainer(new SlotGuardianUpgrade(entity, 2, 120, 53, this.golem));
      this.addSlotToContainer(new SlotGuardianCosmeticUpgrade(entity, 3, 84, 88, this.golem));
      this.addSlotToContainer(new SlotGuardianCosmeticUpgrade(entity, 4, 102, 88, this.golem));
      this.addSlotToContainer(new SlotGuardianCosmeticUpgrade(entity, 5, 120, 88, this.golem));
      this.addSlotToContainer(new SlotSingle(entity, 6, 200, 53, ModItems.guardianWhitelist));
      this.bindPlayerInventory(inventory);
   }

   private void bindPlayerInventory(InventoryPlayer inventory) {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, j * 18 + 39, 145 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(inventory, i, i * 18 + 39, 203));
      }

   }

   public boolean canInteractWith(EntityPlayer player) {
      return this.golem.isUseableByPlayer(player);
   }

   public ItemStack transferStackInSlot(EntityPlayer player, int slotId) {
      return null;
   }
}
