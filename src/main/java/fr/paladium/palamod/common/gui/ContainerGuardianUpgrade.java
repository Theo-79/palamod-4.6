package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.common.inventory.InventoryGuardianMore;
import fr.paladium.palamod.common.slot.SlotGuardianUpgrade;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.items.ItemBackpack;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerGuardianUpgrade extends Container {
   private EntityGuardianGolem golem;
   private InventoryGuardianMore inventoryGuardian;

   public ContainerGuardianUpgrade(EntityGuardianGolem entity, InventoryPlayer inventory) {
      this.golem = entity;
      this.inventoryGuardian = new InventoryGuardianMore(this.golem);
      this.bindPlayerInventory(inventory);

      for(int j = 0; j < 9; ++j) {
         this.addSlotToContainer(new SlotGuardianUpgrade(this.inventoryGuardian, j, 39 + j * 18, 50, this.golem));
      }

   }

   private void bindPlayerInventory(InventoryPlayer inventory) {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, j * 18 + 39, 71 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(inventory, i, i * 18 + 39, 129));
      }

   }

   public void onContainerClosed(EntityPlayer player) {
      super.onContainerClosed(player);
      this.inventoryGuardian.writeToNBT(this.golem.getEntityData());
   }

   public boolean canInteractWith(EntityPlayer player) {
      return this.golem.isUseableByPlayer(player);
   }

   public ItemStack transferStackInSlot(EntityPlayer player, int index) {
      ItemStack itemstack = null;
      Slot slot = (Slot)this.inventorySlots.get(index);
      if (slot != null && slot.getHasStack()) {
         ItemStack itemstack1 = slot.getStack();
         itemstack = itemstack1.copy();
         if (itemstack.getItem() instanceof ItemBackpack) {
            this.inventoryGuardian.writeToNBT(player.getEntityData());
            return null;
         }

         if (index < this.inventoryGuardian.getSizeInventory()) {
            if (!this.mergeItemStack(itemstack1, this.inventoryGuardian.getSizeInventory(), this.inventorySlots.size(), true)) {
               return null;
            }
         } else if (!this.mergeItemStack(itemstack1, 0, this.inventoryGuardian.getSizeInventory(), false)) {
            return null;
         }

         if (itemstack1.stackSize == 0) {
            slot.putStack((ItemStack)null);
         } else {
            slot.onSlotChanged();
         }
      }

      this.inventoryGuardian.writeToNBT(player.getEntityData());
      return itemstack;
   }

   public ItemStack slotClick(int slotIndex, int buttonPressed, int flag, EntityPlayer player) {
      this.inventoryGuardian.writeToNBT(player.getEntityData());
      return slotIndex - this.inventoryGuardian.getSizeInventory() - 27 == player.inventory.currentItem ? null : super.slotClick(slotIndex, buttonPressed, flag, player);
   }
}
