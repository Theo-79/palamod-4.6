package fr.paladium.palamod.common.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerGuardianWhitelist extends Container {
   public ContainerGuardianWhitelist(InventoryPlayer inventory) {
      this.bindPlayerInventory(inventory);
   }

   private void bindPlayerInventory(InventoryPlayer inventory) {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, 8 + j * 18, 65 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(inventory, i, 8 + i * 18, 123));
      }

   }

   public boolean canInteractWith(EntityPlayer player) {
      return true;
   }

   public ItemStack transferStackInSlot(EntityPlayer player, int quantity) {
      return null;
   }
}
