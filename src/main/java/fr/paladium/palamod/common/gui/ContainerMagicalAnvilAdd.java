package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.items.ItemRune;
import fr.paladium.palamod.tiles.TileEntityMagicalAnvil;
import fr.paladium.palamod.util.RuneItemSlot;
import fr.paladium.palamod.util.SingleItemSlot;
import java.util.UUID;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.inventory.InventoryCraftResult;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ContainerMagicalAnvilAdd extends Container {
   InventoryPlayer inventory;
   TileEntityMagicalAnvil tile;
   private IInventory inputSlots = new InventoryBasic("Rune", true, 5) {
      public void markDirty() {
         super.markDirty();
         ContainerMagicalAnvilAdd.this.onCraftMatrixChanged(this);
      }
   };
   private IInventory outputSlot = new InventoryCraftResult();
   public int maximumCost = 20;
   private World theWorld;

   public ContainerMagicalAnvilAdd(TileEntityMagicalAnvil tileEntityMagicalAnvil, EntityPlayer player) {
      this.inventory = player.inventory;
      this.tile = tileEntityMagicalAnvil;
      this.theWorld = player.worldObj;
      this.tile.markDirty();
      this.bindPlayerInventory();
      this.addSlotToContainer(new SingleItemSlot(this.inputSlots, 0, 69, 60));
      this.addSlotToContainer(new RuneItemSlot(this.inputSlots, 1, 25, 61));
      this.addSlotToContainer(new RuneItemSlot(this.inputSlots, 2, 109, 61));
      this.addSlotToContainer(new RuneItemSlot(this.inputSlots, 3, 68, 104));
      this.addSlotToContainer(new RuneItemSlot(this.inputSlots, 4, 69, 20));
      this.addSlotToContainer(new Slot(this.outputSlot, 5, 152, 60) {
         public boolean isItemValid(ItemStack stack) {
            return false;
         }

         public boolean canTakeStack(EntityPlayer player) {
            return (player.capabilities.isCreativeMode || player.experienceLevel >= ContainerMagicalAnvilAdd.this.maximumCost) && ContainerMagicalAnvilAdd.this.maximumCost > 0 && this.getHasStack();
         }

         public void onPickupFromSlot(EntityPlayer player, ItemStack stack) {
            if (!player.capabilities.isCreativeMode) {
               player.addExperienceLevel(-ContainerMagicalAnvilAdd.this.maximumCost);
            }

            ContainerMagicalAnvilAdd.this.inputSlots.setInventorySlotContents(0, (ItemStack)null);
            ContainerMagicalAnvilAdd.this.inputSlots.setInventorySlotContents(1, (ItemStack)null);
            ContainerMagicalAnvilAdd.this.inputSlots.setInventorySlotContents(2, (ItemStack)null);
            ContainerMagicalAnvilAdd.this.inputSlots.setInventorySlotContents(3, (ItemStack)null);
            ContainerMagicalAnvilAdd.this.inputSlots.setInventorySlotContents(4, (ItemStack)null);
         }
      });
   }

   public boolean canInteractWith(EntityPlayer player) {
      return this.tile.isUseableByPlayer(player);
   }

   public IInventory getinv() {
      return this.outputSlot;
   }

   private void bindPlayerInventory() {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(this.inventory, j + i * 9 + 9, 8 + j * 18, 149 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(this.inventory, i, 8 + i * 18, 207));
      }

   }

   public void onCraftMatrixChanged(IInventory p_75130_1_) {
      super.onCraftMatrixChanged(p_75130_1_);
      if (p_75130_1_ == this.inputSlots) {
         this.onUpdate();
      }

   }

   private void onUpdate() {
      if (this.inputSlots.getStackInSlot(0) != null) {
         if (this.inputSlots.getStackInSlot(1) == null && this.inputSlots.getStackInSlot(2) == null && this.inputSlots.getStackInSlot(3) == null && this.inputSlots.getStackInSlot(4) == null) {
            this.outputSlot.setInventorySlotContents(5, (ItemStack)null);
         } else {
            NBTTagCompound modifierNBT2 = null;
            if (this.inputSlots.getStackInSlot(0) == null) {
               return;
            }

            ItemStack item = this.inputSlots.getStackInSlot(0).copy();
            int armortype = 4;
            UUID MODIFIER_UUID = null;
            if (item.getItem() instanceof ItemArmor) {
               ItemArmor itemm = (ItemArmor)item.getItem();
               armortype = itemm.armorType;
            }

            if (armortype == 0) {
               MODIFIER_UUID = UUID.fromString("3dc93f48-3a53-409a-906b-0e0623d086fc");
            } else if (armortype == 1) {
               MODIFIER_UUID = UUID.fromString("4e9c4559-2664-4e6f-85d8-1a850d105404");
            } else if (armortype == 2) {
               MODIFIER_UUID = UUID.fromString("c5906777-6649-46f5-820d-559e9665276a");
            } else if (armortype == 3) {
               MODIFIER_UUID = UUID.fromString("686ca306-261c-4d70-bd53-9056f69bdbcd");
            } else if (armortype == 4) {
               MODIFIER_UUID = UUID.fromString("d36a8f31-ecdf-4b53-a946-1c60f9462a21");
               String STR = item.getAttributeModifiers().get("generic.attackDamage").toString();
               String strl = STR.substring(26, 29);
               AttributeModifier attackModifier2 = new AttributeModifier(MODIFIER_UUID, "generic.attack", Double.parseDouble(strl), 0);
               modifierNBT2 = writeAttributeModifierToNBT(SharedMonsterAttributes.attackDamage, attackModifier2, armortype);
            }

            NBTTagList list = new NBTTagList();
            NBTTagList lore = new NBTTagList();
            NBTTagCompound stackTagCompound;
            if (item.stackTagCompound != null) {
               stackTagCompound = item.stackTagCompound;
            } else {
               stackTagCompound = new NBTTagCompound();
            }

            if (armortype == 4) {
               list.appendTag(modifierNBT2);
            }

            this.maximumCost = 0;

            for(int i = 1; i <= 4; ++i) {
               if (this.inputSlots.getStackInSlot(i) != null) {
                  ItemStack ite = this.inputSlots.getStackInSlot(i);
                  int levelRune = ite.stackTagCompound.getInteger("Level");
                  double bonus = ite.stackTagCompound.getDouble("Bonus");
                  double malus = ite.stackTagCompound.getDouble("Malus");
                  int type = ite.stackTagCompound.getInteger("Type");
                  String mType = ite.stackTagCompound.getString("MalusType");
                  String bType = ite.stackTagCompound.getString("BonusType");
                  AttributeModifier attackModifierB = new AttributeModifier(UUID.randomUUID(), bType, bonus / 100.0D, 1);
                  NBTTagCompound modifierNBTB = writeAttributeModifierToNBT(this.getb(bType), attackModifierB, armortype);
                  AttributeModifier attackModifierM = new AttributeModifier(UUID.randomUUID(), mType, malus / 100.0D, 1);
                  NBTTagCompound modifierNBTM = writeAttributeModifierToNBT(this.getb(mType), attackModifierM, armortype);
                  this.setmaxcost(levelRune);
                  list.appendTag(modifierNBTB);
                  list.appendTag(modifierNBTM);
                  String displayLevel = "Niveau " + levelRune;
                  lore.appendTag(new NBTTagString(EnumChatFormatting.BLUE + "Rune " + i + ": " + displayLevel));
                  lore.appendTag(new NBTTagString(EnumChatFormatting.GREEN + bType + " : " + bonus + ItemRune.getDisplayType(type)));
                  lore.appendTag(new NBTTagString(EnumChatFormatting.RED + mType + " : " + malus + ItemRune.getDisplayType(type)));
                  lore.appendTag(new NBTTagString(" "));
               }
            }

            stackTagCompound.setTag("AttributeModifiers", list);
            NBTTagCompound display = new NBTTagCompound();
            display.setTag("Lore", lore);
            stackTagCompound.setTag("display", display);
            item.setTagCompound(stackTagCompound);
            this.outputSlot.setInventorySlotContents(5, item);
         }
      } else {
         this.outputSlot.setInventorySlotContents(5, (ItemStack)null);
      }

   }

   private void setmaxcost(int level) {
      if (level == 1) {
         this.maximumCost += 10;
      } else if (level == 2) {
         this.maximumCost += 20;
      } else if (level == 3) {
         this.maximumCost += 30;
      } else if (level == 4) {
         this.maximumCost += 40;
      }

   }

   public IAttribute getb(String s) {
      if (s.equals("Degat")) {
         return SharedMonsterAttributes.attackDamage;
      } else if (s.equals("Vie")) {
         return SharedMonsterAttributes.maxHealth;
      } else if (s.equals("Knockback Resistance")) {
         return SharedMonsterAttributes.knockbackResistance;
      } else {
         return s.equals("Vitesse") ? SharedMonsterAttributes.movementSpeed : null;
      }
   }

   private static NBTTagCompound writeAttributeModifierToNBT(IAttribute attribute, AttributeModifier modifier, int i) {
      NBTTagCompound nbttagcompound = new NBTTagCompound();
      String slot = "";
      nbttagcompound.setString("AttributeName", attribute.getAttributeUnlocalizedName());
      nbttagcompound.setString("Name", modifier.getName());
      nbttagcompound.setDouble("Amount", modifier.getAmount());
      nbttagcompound.setInteger("Operation", modifier.getOperation());
      nbttagcompound.setLong("UUIDMost", modifier.getID().getMostSignificantBits());
      nbttagcompound.setLong("UUIDLeast", modifier.getID().getLeastSignificantBits());
      if (i == 0) {
         slot = "head";
      } else if (i == 1) {
         slot = "torso";
      } else if (i == 2) {
         slot = "legs";
      } else if (i == 3) {
         slot = "feet";
      } else if (i == 4) {
         slot = "mainhand";
      }

      nbttagcompound.setString("Slot", slot);
      return nbttagcompound;
   }

   public void onContainerClosed(EntityPlayer player) {
      super.onContainerClosed(player);
      if (!this.theWorld.isRemote) {
         for(int i = 0; i < this.inputSlots.getSizeInventory(); ++i) {
            ItemStack itemstack = this.inputSlots.getStackInSlotOnClosing(i);
            if (itemstack != null) {
               player.dropPlayerItemWithRandomChoice(itemstack, false);
            }
         }
      }

   }

   public ItemStack transferStackInSlot(EntityPlayer playerIn, int fromSlot) {
      return null;
   }
}
