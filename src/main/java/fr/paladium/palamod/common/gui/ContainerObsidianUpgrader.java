package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.blocks.ModBlocks;
import fr.paladium.palamod.common.slot.SlotFuel;
import fr.paladium.palamod.common.slot.SlotItemList;
import fr.paladium.palamod.common.slot.SlotResult;
import fr.paladium.palamod.items.ItemFurnaceUpgrade;
import fr.paladium.palamod.items.ModItems;
import fr.paladium.palamod.tiles.TileEntityObsidianUpgrader;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.tileentity.TileEntityFurnace;

public class ContainerObsidianUpgrader extends Container {
   private TileEntityObsidianUpgrader tileMachine;

   public ContainerObsidianUpgrader(TileEntityObsidianUpgrader tile, InventoryPlayer inventory) {
      this.tileMachine = tile;
      List<Item> items0 = new ArrayList();
      items0.add(Item.getItemFromBlock(Blocks.obsidian));
      items0.add(Item.getItemFromBlock(ModBlocks.upgraded_obsidian));
      List<Item> items1 = new ArrayList();
      items1.add(ModItems.explode_obsidian_upgrade);
      items1.add(ModItems.fake_obsidian_upgrade);
      items1.add(ModItems.twoLife_obsidian_upgrade);
      items1.add(ModItems.camouflage_obsidian_upgrade);
      this.addSlotToContainer(new SlotItemList(tile, 0, 60, 14, items0));
      this.addSlotToContainer(new SlotItemList(tile, 1, 11, 54, items1));
      this.addSlotToContainer(new Slot(tile, 2, 129, 45));
      this.addSlotToContainer(new Slot(tile, 3, 129, 63));
      this.addSlotToContainer(new SlotResult(tile, 4, 183, 54));
      this.addSlotToContainer(new SlotFuel(tile, 5, 108, 14));
      this.bindPlayerInventory(inventory);
   }

   public boolean canInteractWith(EntityPlayer player) {
      return this.tileMachine.isUseableByPlayer(player);
   }

   private void bindPlayerInventory(InventoryPlayer inventory) {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, 23 + j * 18, 103 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(inventory, i, 23 + i * 18, 161));
      }

   }

   public ItemStack transferStackInSlot(EntityPlayer player, int slotIndex) {
      ItemStack itemstack = null;
      Slot slot = (Slot)this.inventorySlots.get(slotIndex);
      if (slot != null && slot.getHasStack()) {
         ItemStack itemstack1 = slot.getStack();
         itemstack = itemstack1.copy();
         if (slotIndex < this.tileMachine.getSizeInventory()) {
            if (!this.mergeItemStack(itemstack1, this.tileMachine.getSizeInventory(), this.inventorySlots.size(), true)) {
               return null;
            }
         } else if (!this.mergeItemStack(itemstack1, 0, this.tileMachine.getSizeInventory(), false)) {
            if (FurnaceRecipes.smelting().getSmeltingResult(itemstack1) != null && !this.mergeItemStack(itemstack1, 1, 2, true)) {
               return null;
            }

            if (TileEntityFurnace.isItemFuel(itemstack1) && !this.mergeItemStack(itemstack1, 2, 3, true)) {
               return null;
            }

            if (itemstack.getItem() instanceof ItemFurnaceUpgrade && !this.mergeItemStack(itemstack1, 0, 1, true)) {
               return null;
            }
         }

         if (itemstack1.stackSize == 0) {
            slot.putStack((ItemStack)null);
         } else {
            slot.onSlotChanged();
         }
      }

      return itemstack;
   }

   public void onContainerClosed(EntityPlayer player) {
      super.onContainerClosed(player);
      this.tileMachine.closeInventory();
   }
}
