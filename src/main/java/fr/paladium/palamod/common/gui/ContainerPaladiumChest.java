package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.common.slot.SlotRing;
import fr.paladium.palamod.tiles.TileEntityPaladiumChest;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerPaladiumChest extends Container {
   TileEntityPaladiumChest tile;
   InventoryPlayer inventory;

   public ContainerPaladiumChest(TileEntityPaladiumChest tile, InventoryPlayer inventory) {
      this.inventory = inventory;
      this.tile = tile;
      tile.openInventory();
      boolean i = false;

      for(int i1 = 0; i1 < 9; ++i1) {
         for(int j = 0; j < 12; ++j) {
            this.addSlotToContainer(new Slot(tile, j + i1 * 12, 12 + j * 18, 8 + i1 * 18));
         }
      }

      this.addSlotToContainer(new SlotRing(tile, 108, 228, 8));
      this.addSlotToContainer(new SlotRing(tile, 109, 228, 26));
      this.addSlotToContainer(new SlotRing(tile, 110, 228, 44));
      this.addSlotToContainer(new SlotRing(tile, 111, 228, 62));
      this.bindPlayerInventory();
   }

   private void bindPlayerInventory() {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(this.inventory, j + i * 9 + 9, 39 + j * 18, 174 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(this.inventory, i, 39 + i * 18, 232));
      }

   }

   public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
      ItemStack stack = null;
      Slot slots = (Slot)this.inventorySlots.get(slot);
      if (slots != null && slots.getHasStack()) {
         ItemStack stack1 = slots.getStack();
         stack = stack1.copy();
         if (slot < 112) {
            if (!this.mergeItemStack(stack1, 112, 148, true)) {
               return null;
            }

            slots.onSlotChange(stack1, stack);
         }

         if (slot >= 112) {
            if (!this.mergeItemStack(stack1, 0, 108, false)) {
               return null;
            }

            slots.onSlotChange(stack1, stack);
         }

         if (stack1.stackSize == 0) {
            slots.putStack((ItemStack)null);
         } else {
            slots.onSlotChanged();
         }

         if (stack1.stackSize == stack.stackSize) {
            return null;
         }

         slots.onPickupFromSlot(player, stack1);
      }

      return stack;
   }

   public void onContainerClosed(EntityPlayer player) {
      super.onContainerClosed(player);
      this.tile.closeInventory();
   }

   public boolean canInteractWith(EntityPlayer player) {
      return this.tile.isUseableByPlayer(player);
   }
}
