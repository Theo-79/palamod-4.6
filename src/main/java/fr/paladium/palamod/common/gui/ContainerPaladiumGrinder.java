package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.blocks.BlockPaladiumGrinder;
import fr.paladium.palamod.common.slot.SlotGrinder;
import fr.paladium.palamod.common.slot.SlotResult;
import fr.paladium.palamod.common.slot.SlotUpgradeGrinder;
import fr.paladium.palamod.tiles.TileEntityPaladiumGrinder;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerPaladiumGrinder extends Container {
   TileEntityPaladiumGrinder tile;

   public ContainerPaladiumGrinder(InventoryPlayer inventory, TileEntityPaladiumGrinder tile) {
      this.tile = tile;
      this.addSlotToContainer(new SlotResult(tile, 0, 77, 29));
      this.addSlotToContainer(new Slot(tile, 1, 23, 20));
      this.addSlotToContainer(new Slot(tile, 2, 23, 38));
      this.addSlotToContainer(new Slot(tile, 4, 23, 69));
      this.addSlotToContainer(new SlotUpgradeGrinder(tile, 5, 77, 69));
      this.addSlotToContainer(new SlotGrinder(tile, 6, 125, 12, tile));
      this.bindPlayerInventory(inventory);
   }

   private void bindPlayerInventory(InventoryPlayer inventory) {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, 23 + j * 18, 91 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(inventory, i, 23 + i * 18, 149));
      }

   }

   public boolean canInteractWith(EntityPlayer player) {
      if (!this.tile.isUseableByPlayer(player)) {
         return false;
      } else {
         return player.worldObj.getWorldTime() % 20L == 0L ? ((BlockPaladiumGrinder)player.worldObj.getBlock(this.tile.xCoord, this.tile.yCoord, this.tile.zCoord)).isMultiBlockStructure(player.worldObj, this.tile.xCoord, this.tile.yCoord, this.tile.zCoord) : true;
      }
   }

   public ItemStack transferStackInSlot(EntityPlayer player, int quantity) {
      return null;
   }
}
