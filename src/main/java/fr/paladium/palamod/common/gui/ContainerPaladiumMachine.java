package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.common.slot.SlotResult;
import fr.paladium.palamod.tiles.TileEntityPaladiumMachine;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerPaladiumMachine extends Container {
   private TileEntityPaladiumMachine tilePaladiumMachine;

   public ContainerPaladiumMachine(TileEntityPaladiumMachine tile, InventoryPlayer inventory) {
      this.tilePaladiumMachine = tile;
      this.addSlotToContainer(new Slot(tile, 0, 89, 95));
      this.addSlotToContainer(new Slot(tile, 1, 25, 103));
      this.addSlotToContainer(new Slot(tile, 2, 53, 123));
      this.addSlotToContainer(new Slot(tile, 3, 153, 103));
      this.addSlotToContainer(new Slot(tile, 4, 125, 123));
      this.addSlotToContainer(new SlotResult(tile, 5, 89, 146));
      this.bindPlayerInventory(inventory);
   }

   private void bindPlayerInventory(InventoryPlayer inventory) {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, 17 + j * 18, 171 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(inventory, i, 17 + i * 18, 229));
      }

   }

   public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
      ItemStack stack = null;
      Slot slots = (Slot)this.inventorySlots.get(slot);
      if (slots != null && slots.getHasStack()) {
         ItemStack stack1 = slots.getStack();
         stack = stack1.copy();
         if (slot < 6) {
            if (!this.mergeItemStack(stack1, 6, 42, true)) {
               return null;
            }

            slots.onSlotChange(stack1, stack);
         }

         if (slot >= 6) {
            if (!this.mergeItemStack(stack1, 0, 5, false)) {
               return null;
            }

            slots.onSlotChange(stack1, stack);
         }

         if (stack1.stackSize == 0) {
            slots.putStack((ItemStack)null);
         } else {
            slots.onSlotChanged();
         }

         if (stack1.stackSize == stack.stackSize) {
            return null;
         }

         slots.onPickupFromSlot(player, stack1);
      }

      return stack;
   }

   public void onContainerClosed(EntityPlayer player) {
      super.onContainerClosed(player);
      this.tilePaladiumMachine.closeInventory();
   }

   public boolean canInteractWith(EntityPlayer player) {
      return this.tilePaladiumMachine.isUseableByPlayer(player);
   }
}
