package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.common.slot.SlotResult;
import fr.paladium.palamod.common.slot.SlotSingle;
import fr.paladium.palamod.items.ItemFurnaceUpgrade;
import fr.paladium.palamod.items.ModItems;
import fr.paladium.palamod.tiles.TileEntityPaladiumOven;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.tileentity.TileEntityFurnace;

public class ContainerPaladiumOven extends Container {
   public TileEntityPaladiumOven tile;

   public ContainerPaladiumOven(TileEntityPaladiumOven tile, InventoryPlayer inventory) {
      this.tile = tile;
      this.addSlotToContainer(new SlotSingle(tile, 0, 19, 35, ModItems.furnaceUpgrade));
      this.addSlotToContainer(new Slot(tile, 1, 56, 17));
      this.addSlotToContainer(new Slot(tile, 2, 56, 53));
      this.addSlotToContainer(new SlotResult(tile, 3, 116, 35));
      this.bindPlayerInventory(inventory);
   }

   private void bindPlayerInventory(InventoryPlayer inventory) {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(inventory, i, 8 + i * 18, 142));
      }

   }

   public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
      ItemStack stack = null;
      Slot slots = (Slot)this.inventorySlots.get(slot);
      if (slots != null && slots.getHasStack()) {
         ItemStack stack1 = slots.getStack();
         stack = stack1.copy();
         if (slot < 4) {
            if (!this.mergeItemStack(stack1, 4, 39, true)) {
               return null;
            }

            slots.onSlotChange(stack1, stack);
         }

         if (slot >= 4) {
            if (FurnaceRecipes.smelting().getSmeltingResult(stack1) != null && !this.mergeItemStack(stack1, 1, 2, true)) {
               return null;
            }

            if (TileEntityFurnace.isItemFuel(stack1) && !this.mergeItemStack(stack1, 2, 3, true)) {
               return null;
            }

            if (stack.getItem() instanceof ItemFurnaceUpgrade && !this.mergeItemStack(stack1, 0, 1, true)) {
               return null;
            }
         }

         if (stack1.stackSize == 0) {
            slots.putStack((ItemStack)null);
         } else {
            slots.onSlotChanged();
         }

         if (stack1.stackSize == stack.stackSize) {
            return null;
         }

         slots.onPickupFromSlot(player, stack1);
      }

      return stack;
   }

   public void onContainerClosed(EntityPlayer player) {
      super.onContainerClosed(player);
      this.tile.closeInventory();
   }

   public boolean canInteractWith(EntityPlayer player) {
      return this.tile.isUseableByPlayer(player);
   }
}
