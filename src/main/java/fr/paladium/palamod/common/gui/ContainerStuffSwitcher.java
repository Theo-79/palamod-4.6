package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.common.inventory.InventoryStuffSwitcher;
import fr.paladium.palamod.common.slot.SlotStuffSwitcher;
import fr.paladium.palamod.items.ItemStuffSwitcher;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class ContainerStuffSwitcher extends Container {
   int rows;
   InventoryStuffSwitcher inventoryStuff;

   public ContainerStuffSwitcher(InventoryPlayer inventory, InventoryStuffSwitcher inventoryStuff) {
      this.inventoryStuff = inventoryStuff;
      this.addSlotToContainer(new SlotStuffSwitcher(inventoryStuff, 0, 44, 11, 0));
      this.addSlotToContainer(new SlotStuffSwitcher(inventoryStuff, 1, 67, 11, 1));
      this.addSlotToContainer(new SlotStuffSwitcher(inventoryStuff, 2, 91, 11, 2));
      this.addSlotToContainer(new SlotStuffSwitcher(inventoryStuff, 3, 116, 11, 3));
      this.bindPlayerInventory(inventory);
   }

   private void bindPlayerInventory(InventoryPlayer inventory) {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, 8 + j * 18, 35 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(inventory, i, 8 + i * 18, 93));
      }

   }

   public void writeToNBT(ItemStack stack) {
      if (stack != null) {
         if (stack.getItem() != null) {
            if (stack.getItem() instanceof ItemStuffSwitcher) {
               if (!stack.hasTagCompound()) {
                  stack.setTagCompound(new NBTTagCompound());
               }

               if (stack.getTagCompound().hasKey("Open") && stack.getTagCompound().getBoolean("Open")) {
                  this.inventoryStuff.writeToNBT(stack.getTagCompound());
               }
            }
         }
      }
   }

   public boolean canInteractWith(EntityPlayer player) {
      this.writeToNBT(player.getHeldItem());
      boolean open = false;
      if (player.getHeldItem() == null || !(player.getHeldItem().getItem() instanceof ItemStuffSwitcher) && player.getHeldItem().stackSize <= 1) {
         player.closeScreen();
      }

      if (player.getHeldItem().hasTagCompound() && player.getHeldItem().getTagCompound().hasKey("Open")) {
         open = player.getHeldItem().getTagCompound().getBoolean("Open");
      }

      if (!open) {
         player.closeScreen();
      }

      return open;
   }

   public void onContainerClosed(EntityPlayer player) {
      this.writeToNBT(player.getHeldItem());
      super.onContainerClosed(player);
   }

   public ItemStack transferStackInSlot(EntityPlayer player, int index) {
      return null;
   }

   protected boolean mergeItemStack(ItemStack stack, int p_75135_2_, int p_75135_3_, boolean p_75135_4_) {
      return super.mergeItemStack(stack, p_75135_2_, p_75135_3_, p_75135_4_);
   }

   public ItemStack slotClick(int slotIndex, int buttonPressed, int flag, EntityPlayer player) {
      if (flag == 2 && buttonPressed == player.inventory.currentItem) {
         return null;
      } else if (slotIndex - this.inventoryStuff.getSizeInventory() - 27 == player.inventory.currentItem) {
         return null;
      } else {
         this.writeToNBT(player.getHeldItem());
         return super.slotClick(slotIndex, buttonPressed, flag, player);
      }
   }
}
