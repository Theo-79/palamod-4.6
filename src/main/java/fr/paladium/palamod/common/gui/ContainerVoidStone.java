package fr.paladium.palamod.common.gui;

import fr.paladium.palamod.common.inventory.InventoryDummy;
import fr.paladium.palamod.items.ItemVoidStone;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerVoidStone extends Container {
   public ContainerVoidStone(InventoryPlayer inventory) {
      this.addSlotToContainer(new Slot(new InventoryDummy(), 0, 80, 18));
      this.bindPlayerInventory(inventory);
   }

   private void bindPlayerInventory(InventoryPlayer inventory) {
      int i;
      for(i = 0; i < 3; ++i) {
         for(int j = 0; j < 9; ++j) {
            this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, 8 + j * 18, 40 + i * 18));
         }
      }

      for(i = 0; i < 9; ++i) {
         this.addSlotToContainer(new Slot(inventory, i, 8 + i * 18, 98));
      }

   }

   public boolean canInteractWith(EntityPlayer player) {
      return true;
   }

   public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
      ItemStack stack = null;
      Slot slots = (Slot)this.inventorySlots.get(slot);
      if (slots != null && slots.getHasStack()) {
         ItemStack stack1 = slots.getStack();
         stack = stack1.copy();
         System.out.println(slot);
         if (slot >= 1 && !(stack.getItem() instanceof ItemVoidStone) && !this.mergeItemStack(stack1, 0, 1, true)) {
            return null;
         }

         if (stack1.stackSize == 0) {
            slots.putStack((ItemStack)null);
         } else {
            slots.onSlotChanged();
         }

         if (stack1.stackSize == stack.stackSize) {
            return null;
         }

         slots.onPickupFromSlot(player, stack1);
      }

      return stack;
   }

   public ItemStack slotClick(int slotIndex, int buttonPressed, int flag, EntityPlayer player) {
      if (flag == 2 && buttonPressed == player.inventory.currentItem) {
         return null;
      } else {
         return slotIndex - 1 - 27 == player.inventory.currentItem ? null : super.slotClick(slotIndex, buttonPressed, flag, player);
      }
   }
}
