package fr.paladium.palamod.common.slot;

import fr.paladium.palamod.items.ItemBackpack;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotBackpack extends Slot {
   public SlotBackpack(IInventory inventory, int id, int x, int y) {
      super(inventory, id, x, y);
   }

   public boolean isItemValid(ItemStack stack) {
      return !(stack.getItem() instanceof ItemBackpack);
   }
}
