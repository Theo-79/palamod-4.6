package fr.paladium.palamod.common.slot;

import fr.paladium.palamod.blocks.ModBlocks;
import fr.paladium.palamod.items.armors.ItemArmorPaladium;
import fr.paladium.palamod.items.ores.ItemPaladium;
import fr.paladium.palamod.items.tools.ItemPaladiumPickaxe;
import fr.paladium.palamod.items.weapons.ItemPaladiumSword;
import fr.paladium.palamod.tiles.TileEntityPaladiumGrinder;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class SlotGrinder extends Slot {
   TileEntityPaladiumGrinder grinder;

   public SlotGrinder(IInventory inventory, int id, int x, int y, TileEntityPaladiumGrinder grinder) {
      super(inventory, id, x, y);
      this.grinder = grinder;
   }

   public boolean isItemValid(ItemStack stack) {
      if (this.grinder.getPaladium() == 100) {
         return false;
      } else if (stack.getItem() instanceof ItemPaladium) {
         return true;
      } else if (stack.getItem() == Item.getItemFromBlock(ModBlocks.paladiumBlock)) {
         return true;
      } else if (stack.getItem() instanceof ItemArmorPaladium) {
         return true;
      } else if (stack.getItem() instanceof ItemPaladiumPickaxe) {
         return true;
      } else {
         return stack.getItem() instanceof ItemPaladiumSword;
      }
   }

   public ItemStack decrStackSize(int amount) {
      return super.decrStackSize(amount);
   }

   public int getSlotStackLimit() {
      return 64;
   }

   public void onPickupFromSlot(EntityPlayer player, ItemStack stack) {
      super.onCrafting(stack);
      super.onPickupFromSlot(player, stack);
   }
}
