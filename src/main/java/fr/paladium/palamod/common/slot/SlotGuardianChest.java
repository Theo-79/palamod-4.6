package fr.paladium.palamod.common.slot;

import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class SlotGuardianChest extends Slot {
   ResourceLocation disabled = new ResourceLocation("palamod:textures/gui/SlotsGui.png");
   EntityGuardianGolem golem;
   int id;

   public SlotGuardianChest(IInventory inventory, int id, int x, int y, EntityGuardianGolem golem) {
      super(inventory, id, x, y);
      this.id = id;
      this.golem = golem;
      if (this.id > golem.getMaxChestSlots()) {
         this.setBackgroundIconTexture(this.disabled);
      }

   }

   public boolean isItemValid(ItemStack stack) {
      return this.id <= this.golem.getMaxChestSlots();
   }

   public ResourceLocation getBackgroundIconTexture() {
      return super.getBackgroundIconTexture();
   }
}
