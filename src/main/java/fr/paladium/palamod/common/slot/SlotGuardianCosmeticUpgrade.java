package fr.paladium.palamod.common.slot;

import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.items.ItemGuardianCosmeticUpgrade;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotGuardianCosmeticUpgrade extends Slot {
   EntityGuardianGolem golem;

   public SlotGuardianCosmeticUpgrade(IInventory inventory, int id, int x, int y, EntityGuardianGolem golem) {
      super(inventory, id, x, y);
      this.golem = golem;
   }

   public boolean isItemValid(ItemStack stack) {
      return stack.getItem() instanceof ItemGuardianCosmeticUpgrade && this.golem.getLevel() >= ((ItemGuardianCosmeticUpgrade)stack.getItem()).getRequiredLevel();
   }
}
