package fr.paladium.palamod.common.slot;

import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.items.ItemGuardianUpgrade;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotGuardianUpgrade extends Slot {
   EntityGuardianGolem golem;

   public SlotGuardianUpgrade(IInventory inventory, int id, int x, int y, EntityGuardianGolem golem) {
      super(inventory, id, x, y);
      this.golem = golem;
   }

   public boolean isItemValid(ItemStack stack) {
      return stack.getItem() instanceof ItemGuardianUpgrade && this.golem.getLevel() >= ((ItemGuardianUpgrade)stack.getItem()).getRequiredLevel();
   }
}
