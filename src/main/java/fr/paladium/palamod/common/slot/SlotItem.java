package fr.paladium.palamod.common.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class SlotItem extends Slot {
   Item itemYes;

   public SlotItem(IInventory inventory, int id, int x, int y, Item item) {
      super(inventory, id, x, y);
      this.itemYes = item;
   }

   public boolean isItemValid(ItemStack stack) {
      return stack.getItem() == this.itemYes;
   }
}
