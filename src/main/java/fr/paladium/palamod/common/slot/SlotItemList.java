package fr.paladium.palamod.common.slot;

import java.util.List;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class SlotItemList extends Slot {
   List<Item> itemsYes;

   public SlotItemList(IInventory inventory, int id, int x, int y, List<Item> items) {
      super(inventory, id, x, y);
      this.itemsYes = items;
   }

   public boolean isItemValid(ItemStack stack) {
      for(int i = 0; i < this.itemsYes.size(); ++i) {
         if (stack.getItem() == this.itemsYes.get(i)) {
            return true;
         }
      }

      return false;
   }
}
