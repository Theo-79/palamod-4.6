package fr.paladium.palamod.common.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotKeeper extends Slot {
   public SlotKeeper(IInventory inventory, int id, int x, int y) {
      super(inventory, id, x, y);
   }

   public boolean isItemValid(ItemStack stack) {
      return false;
   }
}
