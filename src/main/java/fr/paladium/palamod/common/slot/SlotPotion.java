package fr.paladium.palamod.common.slot;

import fr.paladium.palamod.items.ItemSplashPotion;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;

public class SlotPotion extends Slot {
   public SlotPotion(IInventory inventory, int id, int x, int y) {
      super(inventory, id, x, y);
   }

   public boolean isItemValid(ItemStack stack) {
      return stack.getItem() instanceof ItemPotion && stack.getItemDamage() != 0 && stack.getItemDamage() != 16 && stack.getItemDamage() != 16393 && stack.getItemDamage() != 16425 && stack.getItemDamage() != 16457 || stack.getItem() instanceof fr.paladium.palamod.items.ItemPotion || stack.getItem() instanceof ItemSplashPotion;
   }

   public ItemStack decrStackSize(int amount) {
      return super.decrStackSize(amount);
   }

   public void onPickupFromSlot(EntityPlayer player, ItemStack stack) {
      super.onCrafting(stack);
      super.onPickupFromSlot(player, stack);
   }
}
