package fr.paladium.palamod.common.slot;

import fr.paladium.palamod.items.ItemRingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotRing extends Slot {
   public SlotRing(IInventory inventory, int id, int x, int y) {
      super(inventory, id, x, y);
   }

   public boolean isItemValid(ItemStack stack) {
      return stack.getItem() instanceof ItemRingBase;
   }

   public ItemStack decrStackSize(int amount) {
      return super.decrStackSize(amount);
   }

   public void onPickupFromSlot(EntityPlayer player, ItemStack stack) {
      super.onCrafting(stack);
      super.onPickupFromSlot(player, stack);
   }
}
