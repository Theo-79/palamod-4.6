package fr.paladium.palamod.common.slot;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class SlotSingle extends Slot {
   ItemStack item;

   public SlotSingle(IInventory inventory, int id, int x, int y, Item item) {
      super(inventory, id, x, y);
      this.item = new ItemStack(item);
   }

   public boolean isItemValid(ItemStack stack) {
      return stack.getItem() == this.item.getItem();
   }

   public ItemStack decrStackSize(int amount) {
      return super.decrStackSize(amount);
   }

   public void onPickupFromSlot(EntityPlayer player, ItemStack stack) {
      super.onCrafting(stack);
      super.onPickupFromSlot(player, stack);
   }
}
