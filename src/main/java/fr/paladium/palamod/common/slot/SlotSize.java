package fr.paladium.palamod.common.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;

public class SlotSize extends Slot {
   int size;

   public SlotSize(IInventory inventory, int id, int x, int y, int size) {
      super(inventory, id, x, y);
      this.size = size;
   }

   public int getSlotStackLimit() {
      return this.size;
   }
}
