package fr.paladium.palamod.common.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class SlotStuffSwitcher extends Slot {
   public int type;

   public SlotStuffSwitcher(IInventory inventory, int id, int x, int y, int type) {
      super(inventory, id, x, y);
      this.type = type;
   }

   public boolean isItemValid(ItemStack stack) {
      if (stack.getItem() instanceof ItemArmor) {
         ItemArmor armor = (ItemArmor)stack.getItem();
         return armor.armorType == this.type;
      } else {
         return false;
      }
   }
}
