package fr.paladium.palamod.common.slot;

import fr.paladium.palamod.items.tools.ItemPaladiumHammer;
import fr.paladium.palamod.items.weapons.ItemPaladiumBroadsword;
import fr.paladium.palamod.items.weapons.ItemPaladiumFastsword;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotUpgradeGrinder extends Slot {
   public SlotUpgradeGrinder(IInventory inventory, int id, int x, int y) {
      super(inventory, id, x, y);
   }

   public boolean isItemValid(ItemStack stack) {
      if (stack.getItem() instanceof ItemPaladiumHammer) {
         return true;
      } else if (stack.getItem() instanceof ItemPaladiumBroadsword) {
         return true;
      } else {
         return stack.getItem() instanceof ItemPaladiumFastsword;
      }
   }

   public ItemStack decrStackSize(int amount) {
      return super.decrStackSize(amount);
   }

   public void onPickupFromSlot(EntityPlayer player, ItemStack stack) {
      super.onCrafting(stack);
      super.onPickupFromSlot(player, stack);
   }
}
