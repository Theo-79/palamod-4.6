package fr.paladium.palamod.enchants;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnumEnchantmentType;

public class EnchantDeathbringer extends Enchantment {
   protected EnchantDeathbringer(int id, int weight) {
      super(id, weight, EnumEnchantmentType.weapon);
      this.setName("deathbringer");
   }

   public int getMinEnchantability(int par1) {
      return 13;
   }

   public int getMinLevel() {
      return 1;
   }

   public int getMaxLevel() {
      return 3;
   }
}
