package fr.paladium.palamod.enchants;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import java.util.Map;
import java.util.Random;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class EnchantHandler {
   @SubscribeEvent
   public void onMobDamage(LivingHurtEvent event) {
      if (event.entityLiving != null && !event.entityLiving.worldObj.isRemote && event.source != null && event.source.getSourceOfDamage() != null) {
         Entity player = event.source.getSourceOfDamage();
         EntityLivingBase victim = event.entityLiving;
         if (player instanceof EntityPlayer) {
            ItemStack currentItem = ((EntityPlayer)player).getCurrentEquippedItem();
            this.weaponEnchants((EntityPlayer)player, victim, currentItem, event);
         }

         if (victim instanceof EntityPlayer) {
            this.armorEnchants((EntityPlayer)victim, player, event);
         }
      }

   }

   private void armorEnchants(EntityPlayer player, Entity attacker, LivingHurtEvent event) {
      int wither = countEnchantLevels(player, ModEnchants.witherHunter.effectId);
      if (wither > 0 && attacker instanceof EntityWither) {
         event.ammount /= (float)wither;
      }

      int implants = countEnchantLevels(player, ModEnchants.implants.effectId);
      if (implants > 0) {
         Random rand = player.worldObj.rand;
         if (rand.nextInt(150) >= 149 - implants) {
            if (event.ammount > 7.0F) {
               player.heal(2.0F);
            }

            event.ammount = 0.0F;
         }
      }

   }

   private void weaponEnchants(EntityPlayer player, EntityLivingBase mob, ItemStack currentItem, LivingHurtEvent event) {
      if (currentItem != null && EnchantmentHelper.getEnchantmentLevel(ModEnchants.deathBringer.effectId, currentItem) > 0) {
         int enchLevel = EnchantmentHelper.getEnchantmentLevel(ModEnchants.deathBringer.effectId, currentItem);
         Random rand = player.worldObj.rand;
         if (rand.nextInt(15) >= 14 - enchLevel) {
            event.ammount *= 2.0F;
         }
      }

   }

   public static int countEnchantLevels(EntityPlayer player, int enchant) {
      if (player != null) {
         int count = 0;
         ItemStack[] var3 = player.inventory.armorInventory;
         int var4 = var3.length;

         for(int var5 = 0; var5 < var4; ++var5) {
            ItemStack stack = var3[var5];
            if (stack != null) {
               Map<Integer, Integer> enchantments = EnchantmentHelper.getEnchantments(stack);
               Integer ench = (Integer)enchantments.get(enchant);
               if (ench != null) {
                  count += ench;
               }
            }
         }

         return count;
      } else {
         return 0;
      }
   }
}
