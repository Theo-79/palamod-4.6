package fr.paladium.palamod.enchants;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnumEnchantmentType;

public class EnchantImplants extends Enchantment {
   protected EnchantImplants(int id, int weight) {
      super(id, weight, EnumEnchantmentType.weapon);
      this.setName("implants");
   }

   public int getMinEnchantability(int par1) {
      return 20;
   }

   public int getMinLevel() {
      return 1;
   }

   public int getMaxLevel() {
      return 4;
   }
}
