package fr.paladium.palamod.enchants;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnumEnchantmentType;

public class EnchantWither extends Enchantment {
   protected EnchantWither(int id, int weight) {
      super(id, weight, EnumEnchantmentType.armor);
      this.setName("witherhunter");
   }

   public int getMinEnchantability(int par1) {
      return 5;
   }

   public int getMinLevel() {
      return 1;
   }

   public int getMaxLevel() {
      return 4;
   }
}
