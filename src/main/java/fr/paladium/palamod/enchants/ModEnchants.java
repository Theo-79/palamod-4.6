package fr.paladium.palamod.enchants;

import net.minecraft.enchantment.Enchantment;

public class ModEnchants {
   static Enchantment deathBringer;
   static Enchantment witherHunter;
   static Enchantment implants;

   public static void init() {
      deathBringer = new EnchantDeathbringer(200, 4);
      witherHunter = new EnchantWither(202, 2);
      implants = new EnchantImplants(203, 4);
      Enchantment.addToBookList(deathBringer);
      Enchantment.addToBookList(witherHunter);
      Enchantment.addToBookList(implants);
   }
}
