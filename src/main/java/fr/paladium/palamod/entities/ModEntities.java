package fr.paladium.palamod.entities;

import cpw.mods.fml.common.registry.EntityRegistry;
import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.entities.mobs.EntityCamera;
import fr.paladium.palamod.entities.mobs.EntityCustomWither;
import fr.paladium.palamod.entities.mobs.EntityCustomWitherSkull;
import fr.paladium.palamod.entities.mobs.EntityGarag;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.entities.mobs.EntityTobalt;
import fr.paladium.palamod.entities.projectiles.EntityCustomArrow;
import fr.paladium.palamod.entities.projectiles.EntityDynamite;
import fr.paladium.palamod.entities.projectiles.EntityDynamiteNinja;
import fr.paladium.palamod.entities.projectiles.EntityPotionGun;
import fr.paladium.palamod.entities.projectiles.EntitySplashPotion;
import fr.paladium.palamod.entities.projectiles.EntityTurretBullet;

public class ModEntities {
   public static void init() {
      EntityRegistry.registerModEntity(EntityPotionGun.class, "Gun Potion", 0, PalaMod.instance, 64, 10, true);
      EntityRegistry.registerModEntity(EntityCustomArrow.class, "Custom Arrow", 1, PalaMod.instance, 64, 10, true);
      EntityRegistry.registerModEntity(EntitySplashPotion.class, "EntityPotion", 2, PalaMod.instance, 80, 3, true);
      EntityRegistry.registerModEntity(EntityGuardianGolem.class, "GuardianGolem", 3, PalaMod.instance, 80, 3, true);
      EntityRegistry.registerModEntity(EntityGarag.class, "Garag", 4, PalaMod.instance, 80, 3, true);
      EntityRegistry.registerModEntity(EntityTobalt.class, "Tobalt", 5, PalaMod.instance, 80, 3, true);
      EntityRegistry.registerModEntity(EntityDynamite.class, "Dynamite", 6, PalaMod.instance, 80, 3, true);
      EntityRegistry.registerModEntity(EntityDynamiteNinja.class, "DynamiteNinja", 7, PalaMod.instance, 80, 3, true);
      EntityRegistry.registerModEntity(EntityCamera.class, "Camera", 8, PalaMod.instance, 80, 3, true);
      EntityRegistry.registerModEntity(EntityCustomWither.class, "palawither", 9, PalaMod.instance, 16, 2, true);
      EntityRegistry.registerModEntity(EntityCustomWitherSkull.class, "palawitherskull", 10, PalaMod.instance, 80, 2, true);
      EntityRegistry.registerModEntity(EntityTurretBullet.class, "turretBullet", 11, PalaMod.instance, 16, 20, true);
   }
}
