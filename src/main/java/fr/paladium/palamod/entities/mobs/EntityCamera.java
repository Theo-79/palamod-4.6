package fr.paladium.palamod.entities.mobs;

import fr.paladium.palamod.tiles.TileEntityCamera;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class EntityCamera extends EntityLivingBase {
   private static EntityCamera activeCamera;
   private static EntityLivingBase activePlayer;
   public static int activeCameraId = -1;
   static TileEntityCamera tileCamera;
   static int xCam;
   static int yCam;
   static int zCam;
   static float prevYaw;
   static boolean positive;

   public static boolean isActive() {
      return activeCamera != null;
   }

   public static void createCamera(double x, double y, double z, TileEntityCamera tile, float pitch, float yaw) {
      createCamera(tile);
      xCam = (int)x;
      yCam = (int)y;
      zCam = (int)z;
      moveCamera(x, y, z, pitch, yaw);
   }

   public static void createCamera(TileEntityCamera tile) {
      if (activeCamera == null) {
         activeCamera = new EntityCamera(Minecraft.getMinecraft().renderViewEntity.worldObj);
         activePlayer = Minecraft.getMinecraft().renderViewEntity;
         activeCamera.worldObj.spawnEntityInWorld(activeCamera);
         Minecraft.getMinecraft().renderViewEntity = activeCamera;
         tileCamera = tile;
      }

   }

   public static void moveCamera(double x, double y, double z, float pitch, float yaw) {
      if (activeCamera != null) {
         float tempYaw = prevYaw;
         if (positive) {
            tempYaw += 0.1F;
         } else {
            tempYaw -= 0.1F;
         }

         if (tempYaw > 360.0F) {
            positive = false;
         }

         if (tempYaw < 0.0F) {
            positive = true;
         }

         activeCamera.setPositionAndRotation(x, y, z, yaw + tempYaw, 0.0F);
         prevYaw = tempYaw;
         if (tileCamera != null) {
            tileCamera.setRotation(yaw);
         }
      }

   }

   public static void destroyCamera() {
      if (activeCamera != null) {
         Minecraft.getMinecraft().renderViewEntity = activePlayer;
         activeCamera.worldObj.removeEntity(activeCamera);
         activeCamera.setDead();
         activeCamera = null;
      }

   }

   public EntityCamera(World world) {
      super(world);
      this.width = 0.0F;
      this.height = 0.0F;
   }

   public void onEntityUpdate() {
      this.motionX = this.motionY = this.motionZ = 0.0D;
      super.onEntityUpdate();
   }

   public ItemStack getHeldItem() {
      return null;
   }

   public ItemStack getEquipmentInSlot(int var1) {
      return null;
   }

   public void setCurrentItemOrArmor(int var1, ItemStack var2) {
   }

   public ItemStack[] getLastActiveItems() {
      return new ItemStack[0];
   }

   public static void rotateCamera(double d) {
   }
}
