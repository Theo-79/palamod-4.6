package fr.paladium.palamod.entities.mobs;

import fr.paladium.palamod.util.WitherData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.boss.BossStatus;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.boss.IBossDisplayData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class EntityCustomWither extends EntityWither implements IBossDisplayData {
   WitherData data;

   public EntityCustomWither(World world) {
      super(world);
      this.data = new WitherData();
      this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue((double)this.data.maxlife);
   }

   public EntityCustomWither(World world, WitherData witherData) {
      super(world);
      this.data = witherData;
      this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue((double)this.data.maxlife);
   }

   protected void applyEntityAttributes() {
      super.applyEntityAttributes();
   }

   public void onLivingUpdate() {
      if (this.worldObj.isRemote) {
         BossStatus.setBossStatus(this, true);
      }

      super.onLivingUpdate();
   }

   public WitherData getWitherData() {
      return this.data;
   }

   private void func_82209_a(int p_82209_1_, double p_82209_2_, double p_82209_4_, double p_82209_6_, boolean p_82209_8_) {
      this.worldObj.playAuxSFXAtEntity((EntityPlayer)null, 1014, (int)this.posX, (int)this.posY, (int)this.posZ, 0);
      double d3 = this.func_82214_u(p_82209_1_);
      double d4 = this.func_82208_v(p_82209_1_);
      double d5 = this.func_82213_w(p_82209_1_);
      double d6 = p_82209_2_ - d3;
      double d7 = p_82209_4_ - d4;
      double d8 = p_82209_6_ - d5;
      EntityCustomWitherSkull entitywitherskull = new EntityCustomWitherSkull(this.worldObj, this, d6, d7, d8);
      if (p_82209_8_) {
         entitywitherskull.setInvulnerable(true);
      }

      entitywitherskull.posY = d4;
      entitywitherskull.posX = d3;
      entitywitherskull.posZ = d5;
      this.worldObj.spawnEntityInWorld(entitywitherskull);
   }

   private double func_82214_u(int p_82214_1_) {
      if (p_82214_1_ <= 0) {
         return this.posX;
      } else {
         float f = (this.renderYawOffset + (float)(180 * (p_82214_1_ - 1))) / 180.0F * 3.1415927F;
         float f1 = MathHelper.cos(f);
         return this.posX + (double)f1 * 1.3D;
      }
   }

   private double func_82208_v(int p_82208_1_) {
      return p_82208_1_ <= 0 ? this.posY + 3.0D : this.posY + 2.2D;
   }

   private double func_82213_w(int p_82213_1_) {
      if (p_82213_1_ <= 0) {
         return this.posZ;
      } else {
         float f = (this.renderYawOffset + (float)(180 * (p_82213_1_ - 1))) / 180.0F * 3.1415927F;
         float f1 = MathHelper.sin(f);
         return this.posZ + (double)f1 * 1.3D;
      }
   }

   public void playLivingSound() {
      if (!this.data.nosound) {
         super.playLivingSound();
      }

   }

   protected String getLivingSound() {
      return this.data.nosound ? "" : "mob.wither.idle";
   }

   protected String getHurtSound() {
      return this.data.nosound ? "" : "mob.wither.hurt";
   }

   protected String getDeathSound() {
      return this.data.nosound ? "" : "mob.wither.death";
   }

   public boolean isArmored() {
      return this.isAngry() ? true : super.isArmored();
   }

   public boolean isAngry() {
      return (int)((double)(this.getHealth() * 100.0F) / this.getEntityAttribute(SharedMonsterAttributes.maxHealth).getBaseValue()) - this.data.angrylevel * 25 <= 50;
   }
}
