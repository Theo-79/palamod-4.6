package fr.paladium.palamod.entities.mobs;

import fr.paladium.palamod.common.SilentExplosion;
import fr.paladium.palamod.util.WitherData;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityWitherSkull;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityCustomWitherSkull extends EntityWitherSkull {
   private WitherData data = new WitherData();

   public EntityCustomWitherSkull(World p_i1793_1_) {
      super(p_i1793_1_);
   }

   public EntityCustomWitherSkull(World p_i1794_1_, EntityLivingBase p_i1794_2_, double p_i1794_3_, double p_i1794_5_, double p_i1794_7_) {
      super(p_i1794_1_, p_i1794_2_, p_i1794_3_, p_i1794_5_, p_i1794_7_);
   }

   protected void onImpact(MovingObjectPosition p_70227_1_) {
      super.onImpact(p_70227_1_);
      if (this.data.nosound) {
         SilentExplosion.newExplosion(this, this.posX, this.posY, this.posZ, this.data.impactDamage, false, this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"), this.worldObj);
      } else {
         this.worldObj.newExplosion(this, this.posX, this.posY, this.posZ, this.data.impactDamage, false, this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"));
      }

      this.setDead();
   }

   public void playSound(String p_85030_1_, float p_85030_2_, float p_85030_3_) {
      if (!this.data.nosound) {
         super.playSound(p_85030_1_, p_85030_2_, p_85030_3_);
      }

   }

   public void setWitherData(WitherData witherData) {
      this.data = witherData;
   }

   public WitherData getWitherData() {
      return this.data;
   }
}
