package fr.paladium.palamod.entities.mobs;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.ai.EntityAIGuardian;
import fr.paladium.palamod.client.CustomBossStatus;
import fr.paladium.palamod.common.GuardianKeeperHandler;
import fr.paladium.palamod.common.inventory.InventoryGuardianKeeper;
import fr.paladium.palamod.items.ItemGuardianCosmeticUpgrade;
import fr.paladium.palamod.items.ItemGuardianUpgrade;
import fr.paladium.palamod.items.ItemGuardianWand;
import fr.paladium.palamod.items.ItemGuardianWhitelist;
import fr.paladium.palamod.items.ModItems;
import fr.paladium.palamod.network.packets.PacketGolem;
import fr.paladium.palamod.tiles.TileEntityGuardianAnchor;
import fr.paladium.palamod.util.GuardianHelper;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIMoveTowardsRestriction;
import net.minecraft.entity.ai.EntityAIMoveTowardsTarget;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.boss.IBossDisplayData;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class EntityGuardianGolem extends EntityTameable implements IAnimals, IInventory, IBossDisplayData {
   public ItemStack[] content;
   public ItemStack[] contentChest;
   private String name;
   private int attackCooldown;
   private int attackTimer;
   private boolean isUseable;
   private int level;
   private int subLevel;
   private int requiredXP;
   public String ownerUUID;
   private double life;
   private double speed;
   private double damages;
   private int textureid;
   private int xpmodifier;
   private TileEntityGuardianAnchor anchor;
   private int anchorX;
   private int anchorY;
   private int anchorZ;
   private boolean anchorInitialized = true;
   private int regenModifier = 1;
   private ItemStack weapon;
   private long lastDrop;
   private long lastXP;
   public final double HEALTH_UP = 100.0D;
   public final double SPEED_UP = 0.02D;
   public final double DAMAGE_UP = 1.0D;
   public final double HEALTH_BASE = 100.0D;
   public final double SPEED_BASE = 0.25D;
   public final double DAMAGE_BASE = 1.0D;
   public final int TEXTURE_DEFAULT = 0;

   public EntityGuardianGolem(World world) {
      super(world);
      this.setSize(1.4F, 2.9F);
      this.getNavigator().setAvoidsWater(true);
      this.tasks.addTask(1, new EntityAIAttackOnCollide(this, 1.0D, true));
      this.tasks.addTask(2, new EntityAIMoveTowardsTarget(this, 0.9D, 32.0F));
      this.tasks.addTask(4, new EntityAIMoveTowardsRestriction(this, 1.0D));
      this.tasks.addTask(6, new EntityAIWander(this, 0.6D));
      this.targetTasks.addTask(8, new EntityAIGuardian(this, EntityPlayer.class, 0, true));
      this.tasks.addTask(7, new EntityAIWatchClosest(this, EntityWither.class, 15.0F));
      this.tasks.addTask(7, new EntityAIWatchClosest(this, EntityGuardianGolem.class, 15.0F));
      this.tasks.addTask(9, new EntityAILookIdle(this));
      this.requiredXP = 10;
      this.content = new ItemStack[7];
      this.isUseable = true;
      this.textureid = 0;
      this.name = "Guardian Golem";
      this.xpmodifier = 1;
      this.ignoreFrustumCheck = true;
      this.damages = 1.0D;
      this.setCurrentItemOrArmor(0, new ItemStack(ModItems.paladiumSword));
   }

   public void addInformations(ItemStack[] content, int level, int subLevel, String player) {
      this.content = content;
      this.level = level;
      this.subLevel = subLevel;
      this.ownerUUID = player;
      this.setHealth(100.0F);
      this.setRequiredXP(level);
   }

   @SideOnly(Side.CLIENT)
   public void setClientInfos(int level, int subLevel, ItemStack weapon) {
      this.level = level;
      this.subLevel = subLevel;
      this.weapon = weapon;
      this.setRequiredXP(level);
   }

   public void sync() {
      PacketGolem packet = new PacketGolem();
      packet.addInformations(this.level, this.subLevel, this.getEntityId(), this.weapon);
      PalaMod var10000 = PalaMod.instance;
      PalaMod.proxy.packetPipeline.sendToAll(packet);
      this.damages = 1.0D + 1.0D * (double)this.level;
      this.speed = 0.25D + 0.02D * (double)this.level;
      this.life = 100.0D + 100.0D * (double)this.level;
   }

   public void onLivingUpdate() {
      super.onLivingUpdate();
      this.checkForUpgardes();
      if (this.worldObj.isRemote) {
         CustomBossStatus.setBossStatus(this, true);
      }

      if (this.attackTimer > 0) {
         --this.attackTimer;
      }

      if (!this.worldObj.isRemote && (double)this.getHealth() < this.life && this.rand.nextInt(20) == 0 && this.isUseable) {
         if (GuardianHelper.hasUpgrade(this, 2)) {
            this.regenModifier = 2;
         }

         if (GuardianHelper.hasUpgrade(this, 3)) {
            this.regenModifier = 3;
         }

         if (GuardianHelper.hasUpgrade(this, 4)) {
            this.regenModifier = 4;
         }

         this.setHealth(this.getHealth() + (float)this.regenModifier);
      }

      int i;
      if (!this.worldObj.isRemote && GuardianHelper.hasUpgrade(this, 21) && this.lastDrop + 300000L >= System.currentTimeMillis()) {
         this.lastDrop = System.currentTimeMillis();
         i = this.worldObj.rand.nextInt(4);
         switch(i) {
         case 0:
            new EntityItem(this.worldObj, this.posX, this.posY, this.posZ, new ItemStack(ModItems.amethyst, 1));
            break;
         case 1:
            new EntityItem(this.worldObj, this.posX, this.posY, this.posZ, new ItemStack(ModItems.titane, 1));
            break;
         case 2:
            new EntityItem(this.worldObj, this.posX, this.posY, this.posZ, new ItemStack(ModItems.paladium, 1));
            break;
         case 3:
            new EntityItem(this.worldObj, this.posX, this.posY, this.posZ, new ItemStack(Items.iron_ingot, 1));
            break;
         case 4:
            new EntityItem(this.worldObj, this.posX, this.posY, this.posZ, new ItemStack(Items.diamond, 1));
         }
      }

      if (GuardianHelper.hasUpgrade(this, 21) && this.lastXP + 300000L >= System.currentTimeMillis()) {
         i = this.worldObj.rand.nextInt(100);
         this.lastXP = System.currentTimeMillis();
         this.addXp(i);
      }

      if (this.motionX * this.motionX + this.motionZ * this.motionZ > 2.500000277905201E-7D && this.rand.nextInt(5) == 0) {
         i = MathHelper.floor_double(this.posX);
         int j = MathHelper.floor_double(this.posY - 0.20000000298023224D - (double)this.yOffset);
         int k = MathHelper.floor_double(this.posZ);
         Block block = this.worldObj.getBlock(i, j, k);
         if (block.getMaterial() != Material.air) {
            this.worldObj.spawnParticle("blockcrack_" + Block.getIdFromBlock(block) + "_" + this.worldObj.getBlockMetadata(i, j, k), this.posX + ((double)this.rand.nextFloat() - 0.5D) * (double)this.width, this.boundingBox.minY + 0.1D, this.posZ + ((double)this.rand.nextFloat() - 0.5D) * (double)this.width, 4.0D * ((double)this.rand.nextFloat() - 0.5D), 0.5D, ((double)this.rand.nextFloat() - 0.5D) * 4.0D);
         }
      }

      if (GuardianHelper.hasUpgrade(this, 7)) {
         this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(this.life * 1.1D);
      }

      if (GuardianHelper.hasUpgrade(this, 8)) {
         this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(this.life * 1.3D);
      }

      if (GuardianHelper.hasUpgrade(this, 9)) {
         this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(this.life * 1.5D);
      }

      if (!GuardianHelper.hasUpgrade(this, 9) && !GuardianHelper.hasUpgrade(this, 8) && !GuardianHelper.hasUpgrade(this, 7)) {
         this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(this.life);
      }

      if (!this.anchorInitialized) {
         this.setAnchor(this.anchorX, this.anchorY, this.anchorZ);
         this.weapon = this.getEquipmentInSlot(0);
         this.anchorInitialized = true;
      }

      if (GuardianHelper.hasUpgrade(this, 1) && this.anchor != null && !this.anchor.inRadius(this.posX, this.posY, this.posZ)) {
         this.worldObj.playSoundEffect(this.posX, this.posY, this.posZ, "mob.endermen.portal", 0.1F, 1.0F);
         this.setPositionAndUpdate((double)this.anchor.xCoord, (double)(this.anchor.yCoord + 2), (double)this.anchor.zCoord);
      }

      if (!GuardianHelper.hasUpgrade(this, 20) && this.weapon != null) {
         ItemStack weaponCopy = this.weapon.copy();
         this.weapon = null;
         this.setCurrentItemOrArmor(0, (ItemStack)null);
         EntityItem weaponEntity = new EntityItem(this.worldObj, this.posX, this.posY, this.posZ, weaponCopy);
         if (!this.worldObj.isRemote) {
            this.worldObj.spawnEntityInWorld(weaponEntity);
         }
      }

   }

   public void onDeath(DamageSource damage) {
      this.isUseable = false;
      ItemStack stack = this.fromGuardianToStone();
      GuardianKeeperHandler handler = GuardianKeeperHandler.get(this.worldObj);
      InventoryGuardianKeeper inventory = handler.getStoneFromUUID(this.ownerUUID);
      inventory.setInventorySlotContents(0, stack);
      ItemStack[] contentChest = new ItemStack[26];
      NBTTagList nbtlist = this.getEntityData().getTagList("contentChest", 10);

      int slot;
      for(int i = 0; i < nbtlist.tagCount(); ++i) {
         NBTTagCompound comp1 = nbtlist.getCompoundTagAt(i);
         slot = comp1.getInteger("Slot");
         contentChest[slot] = ItemStack.loadItemStackFromNBT(comp1);
      }

      ItemStack[] var12 = contentChest;
      int var13 = contentChest.length;

      for(slot = 0; slot < var13; ++slot) {
         ItemStack content = var12[slot];
         if (content != null) {
            EntityItem itemEntity = new EntityItem(this.worldObj, this.posX, this.posY + 1.0D, this.posZ, content);
            this.worldObj.spawnEntityInWorld(itemEntity);
         }
      }

   }

   public boolean attackEntityAsMob(Entity entity) {
      if (entity instanceof EntityPlayer && this.checkWhitelist((EntityPlayer)entity)) {
         return false;
      } else {
         if (this.attackTimer == 0) {
            this.attackTimer = this.attackCooldown;
         }

         this.worldObj.setEntityState(this, (byte)4);
         float damageMultiplier = 1.0F;
         float damageAdder = 0.0F;
         if (GuardianHelper.hasUpgrade(this, 10)) {
            damageMultiplier = 1.1F;
         }

         if (GuardianHelper.hasUpgrade(this, 11)) {
            damageMultiplier = 1.3F;
         }

         if (GuardianHelper.hasUpgrade(this, 12)) {
            damageMultiplier = 1.5F;
         }

         if (GuardianHelper.hasUpgrade(this, 13)) {
            damageAdder = 0.5F;
         }

         if (GuardianHelper.hasUpgrade(this, 14)) {
            damageAdder = 1.0F;
         }

         if (GuardianHelper.hasUpgrade(this, 15)) {
            damageAdder = 1.5F;
         }

         boolean flag = entity.attackEntityFrom(DamageSource.causeMobDamage(this), (float)(this.damages + (double)this.rand.nextInt(5)) * damageMultiplier + damageAdder);
         if (flag) {
            entity.motionY += 0.4000000059604645D;
         }

         this.playSound("mob.irongolem.throw", 1.0F, 1.0F);
         return flag;
      }
   }

   public boolean checkWhitelist(EntityPlayer entity) {
      if (entity == null) {
         return false;
      } else if (this.ownerUUID != null && entity.getUniqueID().toString().equals(this.ownerUUID)) {
         return true;
      } else {
         return ItemGuardianWhitelist.check(this.content[6], entity.getDisplayName());
      }
   }

   public boolean interact(EntityPlayer player) {
      if (player.isSneaking() && player.getHeldItem() == null && this.weapon != null && GuardianHelper.hasUpgrade(this, 20)) {
         player.setCurrentItemOrArmor(0, this.weapon.copy());
         this.weapon = null;
         this.setCurrentItemOrArmor(0, this.weapon);
         this.sync();
         return true;
      } else if (player.getHeldItem() != null && (player.getHeldItem().getItem() instanceof ItemSword || player.getHeldItem().getItem() instanceof ItemAxe) && GuardianHelper.hasUpgrade(this, 20)) {
         ItemStack stack = player.getHeldItem();
         this.weapon = stack.copy();
         this.setCurrentItemOrArmor(0, this.weapon);
         player.setCurrentItemOrArmor(0, (ItemStack)null);
         this.sync();
         return true;
      } else if (player.getHeldItem() != null && GuardianHelper.checkXPStuff(player.getHeldItem().getItem())) {
         if (this.level < 80) {
            this.consumeXP(player);
            this.worldObj.spawnParticle("happyVillager", this.posX + (double)(this.rand.nextFloat() * this.width * 2.0F) - (double)this.width, this.posY + 0.5D + (double)(this.rand.nextFloat() * this.height), this.posZ + (double)(this.rand.nextFloat() * this.width * 2.0F) - (double)this.width, this.motionX, this.motionY, this.motionZ);
         }

         this.sync();
         return true;
      } else if (player.getHeldItem() != null && player.getHeldItem().getItem() instanceof ItemGuardianWand && player.isSneaking()) {
         ItemGuardianWand.setGolem(player.getHeldItem(), this);
         this.sync();
         return true;
      } else if (!this.worldObj.isRemote && this.checkWhitelist(player)) {
         player.openGui(PalaMod.instance, 7, this.worldObj, this.getEntityId(), 0, 0);
         this.sync();
         return true;
      } else {
         return super.interact(player);
      }
   }

   protected void collideWithEntity(Entity entity) {
      if (entity instanceof IMob && this.getRNG().nextInt(20) == 0) {
         this.setAttackTarget((EntityLivingBase)entity);
      }

      super.collideWithEntity(entity);
   }

   protected void applyEntityAttributes() {
      super.applyEntityAttributes();
      this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(100.0D);
      this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.25D);
   }

   protected boolean isAIEnabled() {
      return true;
   }

   protected boolean canDespawn() {
      return false;
   }

   public void checkForUpgardes() {
      this.xpmodifier = 1;

      int i;
      int type;
      for(i = 0; i < 3; ++i) {
         if (this.content[i] != null) {
            type = ((ItemGuardianUpgrade)this.content[i].getItem()).getType();
            switch(type) {
            case 0:
               this.xpmodifier = 2;
            }
         }
      }

      this.name = "Guardian Golem";

      for(i = 3; i < 6; ++i) {
         if (this.content[i] != null) {
            type = ((ItemGuardianCosmeticUpgrade)this.content[i].getItem()).getType();
            switch(type) {
            case 0:
               if (this.content[i].hasDisplayName()) {
                  this.name = this.content[i].getDisplayName();
               }
            }
         }
      }

   }

   public int getSizeInventory() {
      return this.content.length;
   }

   public ItemStack getStackInSlot(int slot) {
      return this.content[slot];
   }

   public ItemStack decrStackSize(int slotIndex, int amount) {
      if (this.content[slotIndex] != null) {
         ItemStack itemstack;
         if (this.content[slotIndex].stackSize <= amount) {
            itemstack = this.content[slotIndex];
            this.content[slotIndex] = null;
            this.markDirty();
            return itemstack;
         } else {
            itemstack = this.content[slotIndex].splitStack(amount);
            if (this.content[slotIndex].stackSize == 0) {
               this.content[slotIndex] = null;
            }

            this.markDirty();
            return itemstack;
         }
      } else {
         return null;
      }
   }

   public ItemStack getStackInSlotOnClosing(int slotIndex) {
      if (this.content[slotIndex] != null) {
         ItemStack itemstack = this.content[slotIndex];
         this.content[slotIndex] = null;
         return itemstack;
      } else {
         return null;
      }
   }

   public void setInventorySlotContents(int slotIndex, ItemStack stack) {
      this.content[slotIndex] = stack;
      if (stack != null && stack.stackSize > this.getInventoryStackLimit()) {
         stack.stackSize = this.getInventoryStackLimit();
      }

      this.markDirty();
   }

   public String getInventoryName() {
      return "Entity.GuardianGolem";
   }

   public boolean hasCustomInventoryName() {
      return false;
   }

   public int getInventoryStackLimit() {
      return 1;
   }

   public void markDirty() {
   }

   public boolean isUseableByPlayer(EntityPlayer player) {
      if (!this.isUseable) {
         return false;
      } else {
         return player.getDistanceSq(this.posX + 0.5D, this.posY + 0.5D, this.posZ + 0.5D) <= 64.0D;
      }
   }

   public void openInventory() {
   }

   public void closeInventory() {
   }

   public boolean isItemValidForSlot(int slot, ItemStack stack) {
      return true;
   }

   public EntityAgeable createChild(EntityAgeable p_90011_1_) {
      return null;
   }

   public boolean isTamed() {
      return true;
   }

   public void writeToNBT(NBTTagCompound compound) {
      super.writeToNBT(compound);
      NBTTagList nbttaglist = new NBTTagList();

      for(int i = 0; i < this.content.length; ++i) {
         if (this.content[i] != null) {
            NBTTagCompound nbttagcompound1 = new NBTTagCompound();
            nbttagcompound1.setByte("Slot", (byte)i);
            this.content[i].writeToNBT(nbttagcompound1);
            nbttaglist.appendTag(nbttagcompound1);
         }
      }

      if (this.weapon != null) {
         NBTTagCompound weaponComp = new NBTTagCompound();
         this.weapon.writeToNBT(weaponComp);
         compound.setTag("Weapon", weaponComp);
      }

      compound.setTag("Items", nbttaglist);
      compound.setInteger("Levels", this.level);
      compound.setInteger("SubLevels", this.subLevel);
      compound.setString("player", this.ownerUUID);
      compound.setBoolean("isUseable", this.isUseable);
      compound.setLong("LastDrop", this.lastDrop);
      compound.setLong("LastXP", this.lastXP);
      if (this.anchor != null) {
         compound.setInteger("anchorX", this.anchor.xCoord);
         compound.setInteger("anchorY", this.anchor.yCoord);
         compound.setInteger("anchorZ", this.anchor.zCoord);
      }

   }

   public void readFromNBT(NBTTagCompound compound) {
      super.readFromNBT(compound);
      NBTTagList nbttaglist = compound.getTagList("Items", 10);
      this.content = new ItemStack[this.getSizeInventory()];

      for(int i = 0; i < nbttaglist.tagCount(); ++i) {
         NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
         int j = nbttagcompound1.getByte("Slot") & 255;
         if (j >= 0 && j < this.content.length) {
            this.content[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
         }
      }

      this.level = compound.getInteger("Levels");
      this.subLevel = compound.getInteger("SubLevels");
      this.ownerUUID = compound.getString("player");
      this.setRequiredXP(this.level);
      if (compound.hasKey("Weapon")) {
         this.weapon = ItemStack.loadItemStackFromNBT((NBTTagCompound)compound.getTag("Weapon"));
      }

      this.anchorInitialized = false;
      this.lastDrop = compound.getLong("LastDrop");
      this.lastXP = compound.getLong("LastXP");
      this.anchorX = compound.getInteger("anchorX");
      this.anchorY = compound.getInteger("anchorY");
      this.anchorZ = compound.getInteger("anchorZ");
   }

   public int getLevel() {
      return this.level;
   }

   @SideOnly(Side.CLIENT)
   public int getLife() {
      return (int)this.life;
   }

   @SideOnly(Side.CLIENT)
   public int getSpeed() {
      return (int)(this.speed * 10.0D);
   }

   @SideOnly(Side.CLIENT)
   public int getDamages() {
      return (int)this.damages;
   }

   @SideOnly(Side.CLIENT)
   public String getGuardianName() {
      return this.name;
   }

   @SideOnly(Side.CLIENT)
   public float getScaledHealth() {
      return this.getHealth() / this.getMaxHealth();
   }

   @SideOnly(Side.CLIENT)
   public float getRequiredXP() {
      return (float)this.requiredXP;
   }

   @SideOnly(Side.CLIENT)
   public int getTextureId() {
      return this.textureid;
   }

   @SideOnly(Side.CLIENT)
   public int getAttackTimer() {
      return this.attackTimer;
   }

   @SideOnly(Side.CLIENT)
   public int getSubLevel() {
      return this.subLevel;
   }

   @SideOnly(Side.CLIENT)
   public void handleHealthUpdate(byte byt) {
      if (byt == 4 && this.attackTimer == 0) {
         this.attackTimer = 10;
         this.playSound("mob.irongolem.throw", 1.0F, 1.0F);
      } else {
         super.handleHealthUpdate(byt);
      }

   }

   public void setOwner(EntityPlayer player) {
      this.ownerUUID = player.getUniqueID().toString();
   }

   public void setRequiredXP(int level) {
      this.requiredXP = 1 * level * level + 10;
   }

   public void addXp(int xp) {
      this.subLevel += xp * this.xpmodifier;
      if (this.subLevel >= this.requiredXP) {
         ++this.level;
         this.subLevel -= this.requiredXP;
         this.setRequiredXP(this.level);
         this.onLevelUp();
         this.addXp(0);
      }

   }

   public void onLevelUp() {
      this.speed += 0.02D;
      ++this.damages;
      this.life += 100.0D;
      this.applyLevelModifiers();
   }

   public void applyLevelModifiers() {
      this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(this.life);
      this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(this.speed);
   }

   private void consumeXP(EntityPlayer player) {
      ItemStack stack = player.getHeldItem();
      this.addXp(GuardianHelper.getXpFromItem(stack.getItem()));
      --stack.stackSize;
      if (stack.stackSize <= 0) {
         stack = null;
      }

   }

   public ItemStack fromGuardianToStone() {
      ItemStack stack = new ItemStack(ModItems.guardianStone);
      NBTTagCompound compound = new NBTTagCompound();
      NBTTagList nbttaglist = new NBTTagList();

      for(int i = 0; i < this.content.length; ++i) {
         if (this.content[i] != null) {
            NBTTagCompound nbttagcompound1 = new NBTTagCompound();
            nbttagcompound1.setByte("Slot", (byte)i);
            this.content[i].writeToNBT(nbttagcompound1);
            nbttaglist.appendTag(nbttagcompound1);
         }
      }

      compound.setTag("Items", nbttaglist);
      compound.setInteger("Levels", this.level);
      compound.setInteger("SubLevels", this.subLevel);
      compound.setString("player", this.ownerUUID);
      compound.setBoolean("isUseable", this.isUseable);
      stack.setTagCompound(compound);
      return stack;
   }

   public void setAnchor(TileEntityGuardianAnchor anchor) {
      this.anchor = anchor;
   }

   public void setAnchor(int x, int y, int z) {
      TileEntity tile = this.worldObj.getTileEntity(x, y, z);
      if (tile != null && tile instanceof TileEntityGuardianAnchor) {
         this.anchor = (TileEntityGuardianAnchor)tile;
      }

   }

   public String getName() {
      return this.name;
   }

   public int getMaxChestSlots() {
      return this.level / 3;
   }

   protected void damageEntity(DamageSource source, float ammount) {
      super.damageEntity(source, ammount);
      if (source.getSourceOfDamage() != null && source.getSourceOfDamage() instanceof EntityPlayer) {
         EntityPlayer player = (EntityPlayer)source.getSourceOfDamage();
         if (player != null) {
            float back = 0.0F;
            if (GuardianHelper.hasUpgrade(this, 17)) {
               back = ammount * 0.1F;
            }

            if (GuardianHelper.hasUpgrade(this, 18)) {
               back = ammount * 0.15F;
            }

            if (GuardianHelper.hasUpgrade(this, 19)) {
               back = ammount * 0.2F;
            }

            player.attackEntityFrom(DamageSource.causeMobDamage(this), back);
         }
      }
   }

   public ItemStack getWeapon() {
      return this.weapon;
   }
}
