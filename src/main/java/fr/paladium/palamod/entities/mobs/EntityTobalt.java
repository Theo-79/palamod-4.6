package fr.paladium.palamod.entities.mobs;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import fr.paladium.palamod.items.ModItems;
import java.util.HashMap;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.boss.BossStatus;
import net.minecraft.entity.boss.IBossDisplayData;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.item.Item;
import net.minecraft.world.World;

public class EntityTobalt extends EntityMob implements IBossDisplayData {
   public EntityTobalt(World world) {
      super(world);
      this.setSize(3.0F, 3.0F);
   }

   public void applyEntityAttributes() {
      super.applyEntityAttributes();
      this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(10000.0D);
      this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(25.0D);
      this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.7D);
   }

   public void onLivingUpdate() {
      super.onLivingUpdate();
      if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
         BossStatus.setBossStatus(this, true);
      }

   }

   protected String getLivingSound() {
      return "mob.blaze.breathe";
   }

   protected String getHurtSound() {
      return "mob.blaze.hit";
   }

   protected String getDeathSound() {
      return "mob.enderman.death";
   }

   protected void dropFewItems(boolean par1, int par2) {
      HashMap<Integer, Item> itemEndium = new HashMap();
      itemEndium.put(1, ModItems.endiumChestplate);
      itemEndium.put(2, ModItems.endiumBoots);
      itemEndium.put(3, ModItems.endiumAxe);
      itemEndium.put(4, ModItems.endiumHelmet);
      itemEndium.put(5, ModItems.endiumLeggings);
      itemEndium.put(6, ModItems.endiumSword);
      itemEndium.put(7, ModItems.endiumPickaxe);
      HashMap<Integer, Item> autreLoot = new HashMap();
      autreLoot.put(1, ModItems.paladiumBoots);
      autreLoot.put(2, ModItems.paladiumChestplate);
      autreLoot.put(3, ModItems.paladiumHelmet);
      autreLoot.put(4, ModItems.paladiumLeggings);
      autreLoot.put(5, ModItems.paladiumSword);
      autreLoot.put(6, ModItems.paladiumPickaxe);
      autreLoot.put(7, ModItems.paladiumShovel);
      autreLoot.put(8, ModItems.titaneBoots);
      autreLoot.put(9, ModItems.titaneChestplate);
      autreLoot.put(10, ModItems.titaneLeggings);
      autreLoot.put(11, ModItems.titaneHelmet);
      autreLoot.put(12, ModItems.titaneSword);
      int autreL = autreLoot.size();
      int EndiumL = itemEndium.size();
      double loot = Math.random();
      boolean nombreloot = false;
      byte nombreloot1;
      if (loot < 0.5D) {
         nombreloot1 = 2;
      } else if (loot < 0.7D) {
         nombreloot1 = 3;
      } else if (loot < 0.9D) {
         nombreloot1 = 4;
      } else {
         nombreloot1 = 5;
      }

      double endiumDrop = Math.random() * (double)nombreloot1;
      int autreDrop = nombreloot1 - (int)endiumDrop;
      int i;
      double z;
      if ((int)endiumDrop > 0) {
         for(i = 0; (double)i <= endiumDrop; ++i) {
            z = Math.random() * (double)EndiumL;
            this.dropItem((Item)itemEndium.get((int)z), 1);
         }
      }

      if (autreDrop > 0) {
         for(i = 0; i <= autreDrop; ++i) {
            z = Math.random() * (double)autreL;
            this.dropItem((Item)autreLoot.get((int)z), 1);
         }
      }

   }
}
