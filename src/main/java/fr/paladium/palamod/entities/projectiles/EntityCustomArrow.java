package fr.paladium.palamod.entities.projectiles;

import fr.paladium.palamod.util.BowHelper;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityCustomArrow extends EntityArrow {
   public static final int POISON = 0;
   public static final int WITHER = 1;
   public static final int SLOWNESS = 2;
   public static final int SWITCH = 3;
   EntityPlayer player;
   int type;
   private boolean inGround;
   private int field_145791_d = -1;
   private int field_145792_e = -1;
   private int field_145789_f = -1;
   private Block field_145790_g;
   private int inData;
   private boolean canBePickedUp;
   public int arrowShake;
   public Entity shootingEntity;
   private int ticksInGround;
   private int ticksInAir;
   private double damage = 2.0D;
   private int knockbackStrength;

   public EntityCustomArrow(World world) {
      super(world);
   }

   public EntityCustomArrow(World world, int type, EntityPlayer player) {
      super(world);
      this.type = type;
      this.player = player;
   }

   public EntityCustomArrow(World world, EntityLivingBase entity, float power, int type, boolean infiniteAmmo) {
      super(world, entity, power);
      this.type = type;
      this.player = (EntityPlayer)entity;
      this.canBePickedUp = infiniteAmmo;
   }

   public void readEntityFromNBT(NBTTagCompound nbt) {
      super.readEntityFromNBT(nbt);
      this.canBePickedUp = nbt.getBoolean("canBePickedUp");
      if (nbt.hasKey("typearrow")) {
         this.setType(nbt.getInteger("typearrow"));
      }

   }

   public EntityPlayer getPlayer() {
      return this.player;
   }

   public void writeEntityToNBT(NBTTagCompound nbt) {
      super.writeEntityToNBT(nbt);
      nbt.setBoolean("canBePickedUp", this.canBePickedUp);
      nbt.setInteger("typearrow", this.getType());
   }

   public int getType() {
      return this.type;
   }

   public void onEntityUpdate() {
      if (this.ticksExisted >= 2000) {
         this.setDead();
      }

      Vec3 vec3d = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
      Vec3 vec3d1 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
      MovingObjectPosition movingobjectposition = this.worldObj.func_147447_a(vec3d, vec3d1, false, true, false);
      List<Entity> list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox.addCoord(this.motionX, this.motionY, this.motionZ).expand(1.0D, 1.0D, 1.0D));
      double d = 0.0D;
      Entity entity = null;

      for(int l = 0; l < list.size(); ++l) {
         Entity entity1 = (Entity)list.get(l);
         if (entity1.canBeCollidedWith() && (entity1 != this.shootingEntity || this.ticksInAir >= 5)) {
            float f4 = 0.3F;
            AxisAlignedBB axisalignedbb1 = entity1.boundingBox.expand((double)f4, (double)f4, (double)f4);
            MovingObjectPosition movingobjectposition1 = axisalignedbb1.calculateIntercept(vec3d, vec3d1);
            if (movingobjectposition1 != null) {
               double d1 = vec3d.distanceTo(movingobjectposition1.hitVec);
               if (d1 < d || d == 0.0D) {
                  entity = entity1;
                  d = d1;
               }
            }
         }
      }

      if (entity != null) {
         movingobjectposition = new MovingObjectPosition(entity);
      }

      if (movingobjectposition != null && movingobjectposition.entityHit == null) {
         this.inGround = true;
      }

      super.onEntityUpdate();
   }

   public void setType(int type) {
      this.type = type;
   }

   public EntityItem entityDropItem(ItemStack p_70099_1_, float p_70099_2_) {
      return super.entityDropItem(p_70099_1_, p_70099_2_);
   }

   public void onCollideWithPlayer(EntityPlayer player) {
      if (this.canBePickedUp) {
         if (this.inGround) {
            if (BowHelper.getItem(this.type) == null) {
               return;
            }

            EntityItem arrow = new EntityItem(player.worldObj, player.posX, player.posY, player.posZ, new ItemStack(BowHelper.getItem(this.type)));
            if (!player.worldObj.isRemote) {
               player.worldObj.spawnEntityInWorld(arrow);
            }

            this.setDead();
         }

      }
   }
}
