package fr.paladium.palamod.entities.projectiles;

import cpw.mods.fml.common.registry.IThrowableEntity;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.common.SilentExplosion;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.S2BPacketChangeGameState;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityDynamiteNinja extends EntityDynamite implements IThrowableEntity {
   public static final int NO_PICKUP = 0;
   public static final int PICKUP_ALL = 1;
   public static final int PICKUP_CREATIVE = 2;
   public static final int PICKUP_OWNER = 3;
   protected int xTile;
   protected int yTile;
   protected int zTile;
   protected Block inTile;
   protected int inData;
   protected boolean inGround;
   public int pickupMode;
   protected int ticksInGround;
   protected int ticksInAir;
   public boolean beenInGround;
   public float extraDamage;
   public int knockBack;
   private int explodefuse;
   private boolean extinguished;
   public boolean stuck;
   public static int DEFAULT = 0;
   public static int NINJA = 1;
   public static int BIG = 2;
   int type;

   public EntityDynamiteNinja(World world) {
      super(world);
      this.stuck = true;
      this.xTile = -1;
      this.yTile = -1;
      this.zTile = -1;
      this.inTile = null;
      this.inData = 0;
      this.inGround = false;
      this.arrowShake = 0;
      this.ticksInAir = 0;
      this.yOffset = 0.0F;
      this.pickupMode = 0;
      this.setPickupMode(0);
      this.extinguished = false;
      this.explodefuse = 500;
      this.extraDamage = 0.0F;
      this.knockBack = 0;
      this.setSize(0.5F, 0.5F);
   }

   public EntityDynamiteNinja(World world, double d, double d1, double d2) {
      this(world);
      this.setPosition(d, d1, d2);
   }

   public EntityDynamiteNinja(World world, EntityLivingBase entityliving, int i, int type) {
      this(world);
      this.shootingEntity = entityliving;
      this.setLocationAndAngles(entityliving.posX, entityliving.posY + (double)entityliving.getEyeHeight(), entityliving.posZ, entityliving.rotationYaw, entityliving.rotationPitch);
      this.posX -= (double)(MathHelper.cos(this.rotationYaw / 180.0F * 3.141593F) * 0.16F);
      this.posY -= 0.1D;
      this.posZ -= (double)(MathHelper.sin(this.rotationYaw / 180.0F * 3.141593F) * 0.16F);
      this.setPosition(this.posX, this.posY, this.posZ);
      this.motionX = (double)(-MathHelper.sin(this.rotationYaw / 180.0F * 3.141593F) * MathHelper.cos(this.rotationPitch / 180.0F * 3.141593F));
      this.motionZ = (double)(MathHelper.cos(this.rotationYaw / 180.0F * 3.141593F) * MathHelper.cos(this.rotationPitch / 180.0F * 3.141593F));
      this.motionY = (double)(-MathHelper.sin(this.rotationPitch / 180.0F * 3.141593F));
      this.setThrowableHeading(this.motionX, this.motionY, this.motionZ, 0.7F, 4.0F);
      this.explodefuse = i;
      this.type = type;
   }

   protected void entityInit() {
   }

   public Entity getThrower() {
      return this.shootingEntity;
   }

   public void setThrower(Entity entity) {
      this.shootingEntity = entity;
   }

   protected void setPickupModeFromEntity(EntityLivingBase entityliving) {
      if (entityliving instanceof EntityPlayer) {
         if (((EntityPlayer)entityliving).capabilities.isCreativeMode) {
            this.setPickupMode(2);
         }
      } else {
         this.setPickupMode(0);
      }

   }

   public void setThrowableHeading(double x, double y, double z, float speed, float deviation) {
      float f2 = MathHelper.sqrt_double(x * x + y * y + z * z);
      x /= (double)f2;
      y /= (double)f2;
      z /= (double)f2;
      x += this.rand.nextGaussian() * 0.007499999832361937D * (double)deviation;
      y += this.rand.nextGaussian() * 0.007499999832361937D * (double)deviation;
      z += this.rand.nextGaussian() * 0.007499999832361937D * (double)deviation;
      x *= (double)speed;
      y *= (double)speed;
      z *= (double)speed;
      this.motionX = x;
      this.motionY = y;
      this.motionZ = z;
      float f3 = MathHelper.sqrt_double(x * x + z * z);
      this.prevRotationYaw = this.rotationYaw = (float)(Math.atan2(x, z) * 180.0D / 3.141592653589793D);
      this.prevRotationPitch = this.rotationPitch = (float)(Math.atan2(y, (double)f3) * 180.0D / 3.141592653589793D);
      this.ticksInGround = 0;
   }

   public void setVelocity(double d, double d1, double d2) {
      this.motionX = d;
      this.motionY = d1;
      this.motionZ = d2;
      if (this.aimRotation() && this.prevRotationPitch == 0.0F && this.prevRotationYaw == 0.0F) {
         float f = MathHelper.sqrt_double(d * d + d2 * d2);
         this.prevRotationYaw = this.rotationYaw = (float)(Math.atan2(d, d2) * 180.0D / 3.141592653589793D);
         this.prevRotationPitch = this.rotationPitch = (float)(Math.atan2(d1, (double)f) * 180.0D / 3.141592653589793D);
         this.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, this.rotationPitch);
         this.ticksInGround = 0;
      }

   }

   public void onUpdate() {
      this.onEntityUpdate();
      if (!this.inGround && !this.beenInGround) {
         this.rotationPitch -= 50.0F;
      } else {
         this.rotationPitch = 180.0F;
      }

      if (this.isInWater() && !this.extinguished) {
         this.extinguished = true;
         if (this.type != NINJA) {
            this.worldObj.playSoundAtEntity(this, "random.fizz", 1.0F, 1.2F / (this.rand.nextFloat() * 0.2F + 0.9F));
         }

         for(int k = 0; k < 8; ++k) {
            float f6 = 0.25F;
            this.worldObj.spawnParticle("explode", this.posX - this.motionX * (double)f6, this.posY - this.motionY * (double)f6, this.posZ - this.motionZ * (double)f6, this.motionX, this.motionY, this.motionZ);
         }
      }

      --this.explodefuse;
      if (this.explodefuse <= 0) {
         this.detonate();
         this.setDead();
      } else if (this.explodefuse > 0) {
         this.worldObj.spawnParticle("smoke", this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);
      }

   }

   public void onEntityUpdate() {
      super.onEntityUpdate();
      if (this.aimRotation()) {
         float f = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
         this.prevRotationYaw = this.rotationYaw = (float)(Math.atan2(this.motionX, this.motionZ) * 180.0D / 3.141592653589793D);
         this.prevRotationPitch = this.rotationPitch = (float)(Math.atan2(this.motionY, (double)f) * 180.0D / 3.141592653589793D);
      }

      Block i = this.worldObj.getBlock(this.xTile, this.yTile, this.zTile);
      if (i != null) {
         i.setBlockBoundsBasedOnState(this.worldObj, this.xTile, this.yTile, this.zTile);
         AxisAlignedBB axisalignedbb = i.getCollisionBoundingBoxFromPool(this.worldObj, this.xTile, this.yTile, this.zTile);
         if (axisalignedbb != null && axisalignedbb.isVecInside(Vec3.createVectorHelper(this.posX, this.posY, this.posZ))) {
            this.inGround = true;
         }
      }

      if (this.arrowShake > 0) {
         --this.arrowShake;
      }

      if (this.inGround) {
         Block j = this.worldObj.getBlock(this.xTile, this.yTile, this.zTile);
         int k = this.worldObj.getBlockMetadata(this.xTile, this.yTile, this.zTile);
         if (j == this.inTile && k == this.inData) {
            ++this.ticksInGround;
            int t = this.getMaxLifetime();
            if (t != 0 && this.ticksInGround >= t) {
               this.setDead();
            }
         } else {
            this.inGround = false;
            this.motionX *= (double)(this.rand.nextFloat() * 0.2F);
            this.motionY *= (double)(this.rand.nextFloat() * 0.2F);
            this.motionZ *= (double)(this.rand.nextFloat() * 0.2F);
            this.ticksInGround = 0;
            this.ticksInAir = 0;
         }

      } else {
         ++this.ticksInAir;
         Vec3 vec3d = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
         Vec3 vec3d1 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
         MovingObjectPosition movingobjectposition = this.worldObj.func_147447_a(vec3d, vec3d1, false, true, false);
         vec3d = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
         vec3d1 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
         if (movingobjectposition != null) {
            vec3d1 = Vec3.createVectorHelper(movingobjectposition.hitVec.xCoord, movingobjectposition.hitVec.yCoord, movingobjectposition.hitVec.zCoord);
         }

         Entity entity = null;
         List<Entity> list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox.addCoord(this.motionX, this.motionY, this.motionZ).expand(1.0D, 1.0D, 1.0D));
         double d = 0.0D;

         int i1;
         for(i1 = 0; i1 < list.size(); ++i1) {
            Entity entity1 = (Entity)list.get(i1);
            if (entity1.canBeCollidedWith() && (entity1 != this.shootingEntity || this.ticksInAir >= 5)) {
               float f4 = 0.3F;
               AxisAlignedBB axisalignedbb1 = entity1.boundingBox.expand((double)f4, (double)f4, (double)f4);
               MovingObjectPosition movingobjectposition1 = axisalignedbb1.calculateIntercept(vec3d, vec3d1);
               if (movingobjectposition1 != null) {
                  double d1 = vec3d.distanceTo(movingobjectposition1.hitVec);
                  if (d1 < d || d == 0.0D) {
                     entity = entity1;
                     d = d1;
                  }
               }
            }
         }

         if (entity != null) {
            movingobjectposition = new MovingObjectPosition(entity);
         }

         if (movingobjectposition != null) {
            if (movingobjectposition.entityHit != null) {
               this.onEntityHit(movingobjectposition.entityHit);
            } else {
               this.onGroundHit(movingobjectposition);
            }
         }

         if (this.getIsCritical()) {
            for(i1 = 0; i1 < 2; ++i1) {
               this.worldObj.spawnParticle("crit", this.posX + this.motionX * (double)i1 / 4.0D, this.posY + this.motionY * (double)i1 / 4.0D, this.posZ + this.motionZ * (double)i1 / 4.0D, -this.motionX, -this.motionY + 0.2D, -this.motionZ);
            }
         }

         this.posX += this.motionX;
         this.posY += this.motionY;
         this.posZ += this.motionZ;
         float res;
         if (this.aimRotation()) {
            res = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
            this.rotationYaw = (float)(Math.atan2(this.motionX, this.motionZ) * 180.0D / 3.141592653589793D);

            for(this.rotationPitch = (float)(Math.atan2(this.motionY, (double)res) * 180.0D / 3.141592653589793D); this.rotationPitch - this.prevRotationPitch < -180.0F; this.prevRotationPitch -= 360.0F) {
            }

            while(this.rotationPitch - this.prevRotationPitch >= 180.0F) {
               this.prevRotationPitch += 360.0F;
            }

            while(this.rotationYaw - this.prevRotationYaw < -180.0F) {
               this.prevRotationYaw -= 360.0F;
            }

            while(this.rotationYaw - this.prevRotationYaw >= 180.0F) {
               this.prevRotationYaw += 360.0F;
            }

            this.rotationPitch = this.prevRotationPitch + (this.rotationPitch - this.prevRotationPitch) * 0.2F;
            this.rotationYaw = this.prevRotationYaw + (this.rotationYaw - this.prevRotationYaw) * 0.2F;
         }

         res = this.getAirResistance();
         float grav = this.getGravity();
         if (this.isInWater()) {
            this.beenInGround = true;

            for(int i11 = 0; i11 < 4; ++i11) {
               float f6 = 0.25F;
               this.worldObj.spawnParticle("bubble", this.posX - this.motionX * (double)f6, this.posY - this.motionY * (double)f6, this.posZ - this.motionZ * (double)f6, this.motionX, this.motionY, this.motionZ);
            }

            res *= 0.8080808F;
         }

         this.motionX *= (double)res;
         this.motionY *= (double)res;
         this.motionZ *= (double)res;
         this.motionY -= (double)grav;
         this.setPosition(this.posX, this.posY, this.posZ);
         this.func_145775_I();
      }
   }

   public void onEntityHit(Entity entity) {
      this.bounceBack();
      this.applyEntityHitEffects(entity);
   }

   public void applyEntityHitEffects(Entity entity) {
      if (this.isBurning() && !(entity instanceof EntityEnderman)) {
         entity.setFire(5);
      }

      if (entity instanceof EntityLivingBase) {
         EntityLivingBase entityliving = (EntityLivingBase)entity;
         if (this.knockBack > 0) {
            float f = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
            if (f > 0.0F) {
               entity.addVelocity(this.motionX * (double)this.knockBack * 0.6D / (double)f, 0.1D, this.motionZ * (double)this.knockBack * 0.6D / (double)f);
            }
         }

         if (this.shootingEntity instanceof EntityLivingBase) {
            EnchantmentHelper.func_151384_a(entityliving, this.shootingEntity);
            EnchantmentHelper.func_151385_b((EntityLivingBase)this.shootingEntity, entityliving);
         }

         if (this.shootingEntity instanceof EntityPlayerMP && this.shootingEntity != entity && entity instanceof EntityPlayer) {
            ((EntityPlayerMP)this.shootingEntity).playerNetServerHandler.sendPacket(new S2BPacketChangeGameState(6, 0.0F));
         }
      }

   }

   public void onGroundHit(MovingObjectPosition mop) {
      this.xTile = mop.blockX;
      this.yTile = mop.blockY;
      this.zTile = mop.blockZ;
      this.inTile = this.worldObj.getBlock(this.xTile, this.yTile, this.zTile);
      this.motionX = (double)((float)(mop.hitVec.xCoord - this.posX));
      this.motionY = (double)((float)(mop.hitVec.yCoord - this.posY));
      this.motionZ = (double)((float)(mop.hitVec.zCoord - this.posZ));
      float f1 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionY * this.motionY + this.motionZ * this.motionZ);
      this.posX -= this.motionX / (double)f1 * 0.05D;
      this.posY -= this.motionY / (double)f1 * 0.05D;
      this.posZ -= this.motionZ / (double)f1 * 0.05D;
      this.motionX *= -0.2D;
      this.motionZ *= -0.2D;
      if (mop.sideHit == 1) {
         this.inGround = true;
         this.beenInGround = true;
      } else {
         this.inGround = false;
         if (this.type != NINJA) {
            this.worldObj.playSoundAtEntity(this, "random.fizz", 1.0F, 1.2F / (this.rand.nextFloat() * 0.2F + 0.9F));
         }
      }

      if (this.inTile != null) {
         this.inTile.onEntityCollidedWithBlock(this.worldObj, this.xTile, this.yTile, this.zTile, this);
      }

   }

   private void detonate() {
      float f = 3.0F;
      if (this.type == BIG) {
         f = 4.5F;
      }

      if (this.type == DEFAULT) {
         f = 2.5F;
      }

      if (this.type != NINJA) {
         this.worldObj.createExplosion(this, this.posX, this.posY, this.posZ, f, true);
      } else {
         SilentExplosion.newExplosion(this, this.posX, this.posY, this.posZ, f, false, false, this.worldObj);
      }

   }

   protected void bounceBack() {
      this.motionX *= -0.1D;
      this.motionY *= -0.1D;
      this.motionZ *= -0.1D;
      this.rotationYaw += 180.0F;
      this.prevRotationYaw += 180.0F;
      this.ticksInAir = 0;
   }

   public int getType() {
      return this.type;
   }

   public boolean aimRotation() {
      return false;
   }

   public int getMaxLifetime() {
      return 1200;
   }

   public ItemStack getPickupItem() {
      return null;
   }

   public float getAirResistance() {
      return 0.99F;
   }

   public float getGravity() {
      return 0.05F;
   }

   public int getMaxArrowShake() {
      return 0;
   }

   public void playHitSound() {
      if (this.type != NINJA) {
         this.worldObj.playSoundAtEntity(this, "random.fizz", 1.0F, 1.2F / (this.rand.nextFloat() * 0.2F + 0.9F));
      }

   }

   public boolean canBeCritical() {
      return false;
   }

   public void setIsCritical(boolean flag) {
      if (this.canBeCritical()) {
         this.dataWatcher.updateObject(16, (byte)(flag ? 1 : 0));
      }

   }

   public boolean getIsCritical() {
      return this.canBeCritical() && this.dataWatcher.getWatchableObjectByte(16) != 0;
   }

   public void setExtraDamage(float f) {
      this.extraDamage = f;
   }

   public void setKnockbackStrength(int i) {
      this.knockBack = i;
   }

   public void setPickupMode(int i) {
      this.pickupMode = i;
   }

   public int getPickupMode() {
      return this.pickupMode;
   }

   public boolean canPickup(EntityPlayer entityplayer) {
      if (this.pickupMode == 1) {
         return true;
      } else if (this.pickupMode == 2) {
         return entityplayer.capabilities.isCreativeMode;
      } else if (this.pickupMode == 3) {
         return entityplayer == this.shootingEntity;
      } else {
         return false;
      }
   }

   public void onCollideWithPlayer(EntityPlayer entityplayer) {
      if (this.inGround && this.arrowShake <= 0 && this.canPickup(entityplayer) && !this.worldObj.isRemote) {
         ItemStack item = this.getPickupItem();
         if (item == null) {
            return;
         }

         if (this.pickupMode == 2 && entityplayer.capabilities.isCreativeMode || entityplayer.inventory.addItemStackToInventory(item)) {
            if (this.type != NINJA) {
               this.worldObj.playSoundAtEntity(this, "random.pop", 0.2F, ((this.rand.nextFloat() - this.rand.nextFloat()) * 0.7F + 1.0F) * 2.0F);
            }

            this.onItemPickup(entityplayer);
            this.setDead();
         }
      }

   }

   protected void onItemPickup(EntityPlayer entityplayer) {
      entityplayer.onItemPickup(this, 1);
   }

   @SideOnly(Side.CLIENT)
   public float getShadowSize() {
      return 0.0F;
   }

   protected boolean canTriggerWalking() {
      return false;
   }

   public void writeEntityToNBT(NBTTagCompound nbttagcompound) {
      nbttagcompound.setShort("xTile", (short)this.xTile);
      nbttagcompound.setShort("yTile", (short)this.yTile);
      nbttagcompound.setShort("zTile", (short)this.zTile);
      nbttagcompound.setByte("inTile", (byte)Block.getIdFromBlock(this.inTile));
      nbttagcompound.setByte("inData", (byte)this.inData);
      nbttagcompound.setByte("shake", (byte)this.arrowShake);
      nbttagcompound.setBoolean("inGround", this.inGround);
      nbttagcompound.setBoolean("beenInGround", this.beenInGround);
      nbttagcompound.setByte("pickup", (byte)this.pickupMode);
      nbttagcompound.setByte("fuse", (byte)this.explodefuse);
      nbttagcompound.setBoolean("off", this.extinguished);
   }

   public void readEntityFromNBT(NBTTagCompound nbttagcompound) {
      this.xTile = nbttagcompound.getShort("xTile");
      this.yTile = nbttagcompound.getShort("yTile");
      this.zTile = nbttagcompound.getShort("zTile");
      this.inTile = Block.getBlockById(nbttagcompound.getByte("inTile") & 255);
      this.inData = nbttagcompound.getByte("inData") & 255;
      this.arrowShake = nbttagcompound.getByte("shake") & 255;
      this.inGround = nbttagcompound.getBoolean("inGround");
      this.beenInGround = nbttagcompound.getBoolean("beenInGrond");
      this.pickupMode = nbttagcompound.getByte("pickup");
      this.explodefuse = nbttagcompound.getByte("fuse");
      this.extinguished = nbttagcompound.getBoolean("off");
   }
}
