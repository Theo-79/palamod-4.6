package fr.paladium.palamod.entities.projectiles;

import fr.paladium.palamod.blocks.ModBlocks;
import java.util.Iterator;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.Blocks;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntitySplashPotion extends EntityThrowable {
   public int damageValue;
   public boolean isGun;
   public static final int NAUSEA = 0;
   public static final int WEB = 1;

   public EntitySplashPotion(World world) {
      super(world);
   }

   public EntitySplashPotion(World world, EntityPlayer player, int damageValue) {
      super(world, player);
      this.setThrowableHeading(this.motionX, this.motionY, this.motionZ, 0.4F, 0.3F);
      this.setDamageValue(damageValue);
   }

   public EntitySplashPotion(World world, EntityPlayer player, int damageValue, boolean isGun) {
      super(world, player);
      this.setThrowableHeading(this.motionX, this.motionY, this.motionZ, 0.4F, 0.3F);
      this.setDamageValue(damageValue);
      this.isGun = isGun;
   }

   protected void entityInit() {
      super.entityInit();
      this.dataWatcher.addObject(2, 0);
   }

   public void setDamageValue(int damageValue) {
      this.damageValue = damageValue;
      if (!this.worldObj.isRemote) {
         this.dataWatcher.updateObject(2, damageValue);
      }

   }

   public int getDamageValue() {
      return this.dataWatcher.getWatchableObjectInt(2);
   }

   protected void onImpact(MovingObjectPosition p_70184_1_) {
      if (!this.worldObj.isRemote) {
         switch(this.damageValue) {
         case 0:
            this.nauseaEffect(p_70184_1_);
            break;
         case 1:
            this.spawnWeb(p_70184_1_);
         }
      }

      this.setDead();
      this.setDead();
   }

   private static boolean setBlockIfNotSolid(World world, int x, int y, int z, Block block) {
      return setBlockIfNotSolid(world, x, y, z, block, 0);
   }

   private static boolean setBlockIfNotSolid(World world, int x, int y, int z, Block block, int metadata) {
      if (!world.getBlock(x, y, z).isAir(world, x, y, z) && world.getBlock(x, y, z) != ModBlocks.customWeb) {
         return false;
      } else {
         world.setBlock(x, y, z, block, metadata, 3);
         return true;
      }
   }

   private void spawnWeb(MovingObjectPosition mop) {
      switch(mop.typeOfHit) {
      case ENTITY:
         this.worldObj.setBlock((int)mop.entityHit.posX, (int)mop.entityHit.posY, (int)mop.entityHit.posZ, Blocks.web);
         break;
      case BLOCK:
         if (this.worldObj.getBlock(mop.blockX, mop.blockY, mop.blockZ) == Blocks.snow) {
            --mop.blockY;
            mop.sideHit = 1;
         }

         switch(mop.sideHit) {
         case 0:
            setBlockIfNotSolid(this.worldObj, mop.blockX, mop.blockY - 1, mop.blockZ, ModBlocks.customWeb);
            break;
         case 1:
            setBlockIfNotSolid(this.worldObj, mop.blockX, mop.blockY + 1, mop.blockZ, ModBlocks.customWeb);
            break;
         case 2:
            setBlockIfNotSolid(this.worldObj, mop.blockX - 1, mop.blockY, mop.blockZ, ModBlocks.customWeb);
            break;
         case 3:
            setBlockIfNotSolid(this.worldObj, mop.blockX + 1, mop.blockY, mop.blockZ, ModBlocks.customWeb);
            break;
         case 4:
            setBlockIfNotSolid(this.worldObj, mop.blockX, mop.blockY, mop.blockZ - 1, ModBlocks.customWeb);
            break;
         case 5:
            setBlockIfNotSolid(this.worldObj, mop.blockX, mop.blockY, mop.blockZ + 1, ModBlocks.customWeb);
         }
      }

   }

   protected float getGravityVelocity() {
      return this.isGun ? 0.01F : super.getGravityVelocity();
   }

   protected float func_70182_d() {
      return this.isGun ? 2.0F : super.func_70182_d();
   }

   protected float func_70183_g() {
      return this.isGun ? 2.0F : super.func_70183_g();
   }

   public void nauseaEffect(MovingObjectPosition p_70184_1_) {
      AxisAlignedBB axisalignedbb = this.boundingBox.expand(4.0D, 2.0D, 4.0D);
      List list1 = this.worldObj.getEntitiesWithinAABB(EntityPlayer.class, axisalignedbb);
      if (list1 != null && !list1.isEmpty()) {
         Iterator iterator = list1.iterator();

         while(iterator.hasNext()) {
            EntityPlayer entitylivingbase = (EntityPlayer)iterator.next();
            double d0 = this.getDistanceSqToEntity(entitylivingbase);
            if (d0 < 16.0D) {
               double d1 = 1.0D - Math.sqrt(d0) / 4.0D;
               if (entitylivingbase == p_70184_1_.entityHit) {
                  d1 = 1.0D;
               }

               entitylivingbase.addPotionEffect(new PotionEffect(Potion.confusion.id, 400, 100));
            }
         }
      }

      this.worldObj.playAuxSFX(2002, (int)Math.round(this.posX), (int)Math.round(this.posY), (int)Math.round(this.posZ), 4);
   }
}
