package fr.paladium.palamod.entities.projectiles;

import fr.paladium.palamod.entities.mobs.EntityCustomWither;
import fr.paladium.palamod.tiles.TileEntityTurret;
import net.minecraft.entity.Entity;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityTurretBullet extends EntityThrowable {
   static final float damage = 5.0F;

   public EntityTurretBullet(World world) {
      super(world);
   }

   protected void onImpact(MovingObjectPosition object) {
      if (!this.worldObj.isRemote) {
         if (this.worldObj.getTileEntity(object.blockX, object.blockY, object.blockZ) instanceof TileEntityTurret) {
            return;
         }

         this.setDead();
         Entity entity = object.entityHit;
         if (entity == null) {
            return;
         }

         if (entity instanceof EntityWither || entity instanceof EntityCustomWither) {
            entity.attackEntityFrom(DamageSource.magic, 5.0F);
         }
      }

   }

   protected float getGravityVelocity() {
      return 0.0F;
   }
}
