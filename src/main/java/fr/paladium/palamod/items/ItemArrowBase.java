package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemArrowBase extends Item {
   int effect;

   public ItemArrowBase(String name, String texture, int effect) {
      this.effect = effect;
      this.setMaxStackSize(64);
      this.setUnlocalizedName(name);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:" + texture);
   }

   public int getEffect() {
      return this.effect;
   }
}
