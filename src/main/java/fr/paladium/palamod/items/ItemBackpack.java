package fr.paladium.palamod.items;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.TabPaladium;
import java.util.List;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ItemBackpack extends Item {
   public ItemBackpack() {
      this.setMaxStackSize(1);
      this.setUnlocalizedName("backpack");
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:Backpack");
   }

   public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
      if (!world.isRemote) {
         player.openGui(PalaMod.instance, 8, world, 0, 0, 0);
      }

      return stack;
   }

   public void onUpdate(ItemStack stack, World world, Entity e, int in, boolean b) {
      if (stack.hasTagCompound() && stack.getTagCompound().hasKey("Open") && !b) {
         stack.getTagCompound().setBoolean("Open", false);
      }

      super.onUpdate(stack, world, e, in, b);
   }

   public void onCreated(ItemStack ItemStack, World World, EntityPlayer EntityPlayer) {
      ItemStack.stackTagCompound = new NBTTagCompound();
      ItemStack.stackTagCompound.setBoolean("Enderchest", false);
      ItemStack.stackTagCompound.setString("Interdit", "Interdit dans les enderchest");
   }

   public void addInformation(ItemStack itemStack, EntityPlayer player, List list, boolean par4) {
      if (itemStack.stackTagCompound != null) {
         String owner = itemStack.stackTagCompound.getString("Interdit");
         list.add(EnumChatFormatting.RED + owner);
      }

   }
}
