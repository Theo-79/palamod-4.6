package fr.paladium.palamod.items;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class ItemBlockSkullUpgrade extends ItemBlock {
   public ItemBlockSkullUpgrade(Block block) {
      super(block);
      this.setTextureName("minecraft:skull_wither");
   }

   public int getSpriteNumber() {
      return 1;
   }

   public boolean placeBlockAt(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int metadata) {
      metadata = this.getMetaByYawl(player.rotationYaw);
      if (!world.setBlock(x, y, z, this.field_150939_a, metadata, 3)) {
         return false;
      } else {
         if (world.getBlock(x, y, z) == this.field_150939_a) {
            this.field_150939_a.onBlockPlacedBy(world, x, y, z, player, stack);
            this.field_150939_a.onPostBlockPlaced(world, x, y, z, metadata);
         }

         return true;
      }
   }

   public int getMetaByYawl(float yawl) {
      while(yawl < 0.0F) {
         yawl += 360.0F;
      }

      while(yawl > 360.0F) {
         yawl -= 360.0F;
      }

      if (!(yawl <= 22.5F) && !(yawl > 337.5F)) {
         if (yawl <= 67.5F) {
            return 1;
         } else if (yawl <= 112.5F) {
            return 2;
         } else if (yawl <= 157.5F) {
            return 3;
         } else if (yawl <= 202.5F) {
            return 4;
         } else if (yawl <= 247.5F) {
            return 5;
         } else if (yawl <= 292.5F) {
            return 6;
         } else {
            return 7;
         }
      } else {
         return 0;
      }
   }

   public IIcon getIconFromDamage(int p_77617_1_) {
      return this.itemIcon;
   }
}
