package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.Block;
import net.minecraft.item.ItemBucket;

public class ItemBucketBase extends ItemBucket {
   public ItemBucketBase(Block block, String name, String texture) {
      super(block);
      this.setUnlocalizedName(name);
      this.setMaxStackSize(1);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:" + texture);
   }
}
