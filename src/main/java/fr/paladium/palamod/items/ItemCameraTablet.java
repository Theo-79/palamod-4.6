package fr.paladium.palamod.items;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.blocks.BlockCamera;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

public class ItemCameraTablet extends Item {
   public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
      if (player.isSneaking() && world.getBlock(x, y, z) instanceof BlockCamera) {
         if (!stack.hasTagCompound()) {
            stack.setTagCompound(new NBTTagCompound());
            stack.getTagCompound().setBoolean("cam1", false);
            stack.getTagCompound().setBoolean("cam2", false);
            stack.getTagCompound().setBoolean("cam3", false);
            stack.getTagCompound().setInteger("Index", 0);
         }

         NBTTagCompound compound = stack.getTagCompound();
         boolean flag = false;
         if (!compound.getBoolean("cam1")) {
            compound.setBoolean("cam1", true);
            compound.setInteger("cam1X", x);
            compound.setInteger("cam1Y", y);
            compound.setInteger("cam1Z", z);
            compound.setInteger("cam1Yaw", world.getBlockMetadata(x, y, z));
            flag = true;
         } else if (!compound.getBoolean("cam2")) {
            compound.setBoolean("cam2", true);
            compound.setInteger("cam2X", x);
            compound.setInteger("cam2Y", y);
            compound.setInteger("cam2Z", z);
            compound.setInteger("cam2Yaw", world.getBlockMetadata(x, y, z));
            flag = true;
         } else if (!compound.getBoolean("cam3")) {
            compound.setBoolean("cam3", true);
            compound.setInteger("cam3X", x);
            compound.setInteger("cam3Y", y);
            compound.setInteger("cam3Z", z);
            compound.setInteger("cam3Yaw", world.getBlockMetadata(x, y, z));
            flag = true;
         }

         if (flag) {
            stack.setTagCompound(compound);
            return true;
         }
      }

      return false;
   }

   public static boolean isWorking(ItemStack stack) {
      if (stack != null && stack.getItem() instanceof ItemCameraTablet && stack.hasTagCompound()) {
         return stack.getTagCompound().getBoolean("cam1") || stack.getTagCompound().getBoolean("cam2") || stack.getTagCompound().getBoolean("cam3");
      } else {
         return false;
      }
   }

   public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
      if (!player.isSneaking()) {
         player.openGui(PalaMod.instance, 27, world, 0, 0, 0);
      }

      return stack;
   }

   public boolean onBlockDestroyed(ItemStack stack, World world, Block block, int x, int y, int z, EntityLivingBase entity) {
      if (stack != null && stack.hasTagCompound() && entity instanceof EntityPlayer) {
         ((EntityPlayer)entity).addChatComponentMessage(new ChatComponentText(stack.getTagCompound().toString()));
         return true;
      } else {
         return false;
      }
   }
}
