package fr.paladium.palamod.items;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class ItemChestExplorer extends Item {
   public ItemChestExplorer() {
      this.setMaxStackSize(1);
      this.setUnlocalizedName("chestexplorer");
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:ChestExplorer");
   }

   public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
      if (!world.isRemote) {
         TileEntity tile = world.getTileEntity(x, y, z);
         if (tile == null) {
            return false;
         }

         if (tile instanceof IInventory && tile.getClass().toString().contains("Chest") && !tile.getClass().toString().contains("PaladiumChest")) {
            player.openGui(PalaMod.instance, 13, world, x, y, z);
            return true;
         }
      }

      return true;
   }
}
