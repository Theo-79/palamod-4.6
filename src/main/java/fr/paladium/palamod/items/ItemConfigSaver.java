package fr.paladium.palamod.items;

import fr.paladium.palamod.blocks.MulticolorBlock;
import fr.paladium.palamod.blocks.MulticolorSlab;
import fr.paladium.palamod.blocks.MulticolorStairs;
import fr.paladium.palamod.tiles.TileEntityMulticolorBlock;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class ItemConfigSaver extends Item {
   IIcon itemIcon2;

   public IIcon getIcon(ItemStack stack, int pass) {
      return stack.hasTagCompound() && !stack.getTagCompound().getBoolean("FirstUse") ? this.itemIcon2 : this.itemIcon;
   }

   public IIcon getIconIndex(ItemStack stack) {
      return this.getIcon(stack, 0);
   }

   public void registerIcons(IIconRegister register) {
      this.itemIcon = register.registerIcon("palamod:config_saver_0");
      this.itemIcon2 = register.registerIcon("palamod:config_saver_1");
   }

   public void onUpdate(ItemStack stack, World world, Entity entity, int par4, boolean par5) {
      if (stack.hasTagCompound()) {
         int tmp;
         if (stack.getTagCompound().getInteger("Counter") > 0) {
            tmp = stack.getTagCompound().getInteger("Counter");
            --tmp;
            stack.getTagCompound().setInteger("Counter", tmp);
         }

         if (stack.getTagCompound().getInteger("Counter") == 0) {
            if (stack.getTagCompound().getInteger("Index") == 1) {
               if (stack.getTagCompound().getBoolean("Activate2")) {
                  stack.getTagCompound().setInteger("CurrentColor", stack.getTagCompound().getInteger("Color2"));
               } else {
                  tmp = stack.getTagCompound().getInteger("Index");
                  ++tmp;
                  stack.getTagCompound().setInteger("Index", tmp);
               }
            }

            if (stack.getTagCompound().getInteger("Index") == 2) {
               if (stack.getTagCompound().getBoolean("Activate3")) {
                  stack.getTagCompound().setInteger("CurrentColor", stack.getTagCompound().getInteger("Color3"));
               } else {
                  tmp = stack.getTagCompound().getInteger("Index");
                  ++tmp;
                  stack.getTagCompound().setInteger("Index", tmp);
               }
            }

            if (stack.getTagCompound().getInteger("Index") == 3) {
               if (stack.getTagCompound().getBoolean("Activate4")) {
                  stack.getTagCompound().setInteger("CurrentColor", stack.getTagCompound().getInteger("Color4"));
               } else {
                  tmp = stack.getTagCompound().getInteger("Index");
                  ++tmp;
                  stack.getTagCompound().setInteger("Index", tmp);
               }
            }

            if (stack.getTagCompound().getInteger("Index") > 3) {
               stack.getTagCompound().setInteger("Index", 0);
            }

            if (stack.getTagCompound().getInteger("Index") == 0) {
               stack.getTagCompound().setInteger("CurrentColor", stack.getTagCompound().getInteger("Color1"));
            }

            tmp = stack.getTagCompound().getInteger("Index");
            ++tmp;
            stack.getTagCompound().setInteger("Index", tmp);
            stack.getTagCompound().setInteger("Counter", stack.getTagCompound().getInteger("Time") * 20);
         }
      }

   }

   public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
      if (!(world.getBlock(x, y, z) instanceof MulticolorBlock) && !(world.getBlock(x, y, z) instanceof MulticolorStairs) && !(world.getBlock(x, y, z) instanceof MulticolorSlab)) {
         if (player.isSneaking()) {
            this.setCompound(stack, (TileEntityMulticolorBlock)null, true, world, player, x, y, z);
            if (world.isRemote) {
               player.addChatMessage(new ChatComponentTranslation("chat.reset_config_saver", new Object[0]));
            }
         }
      } else if (world.getTileEntity(x, y, z) instanceof TileEntityMulticolorBlock) {
         this.setCompound(stack, (TileEntityMulticolorBlock)world.getTileEntity(x, y, z), false, world, player, x, y, z);
      }

      return false;
   }

   private ItemStack setCompound(ItemStack stack, TileEntityMulticolorBlock tile, Boolean empty, World world, EntityPlayer player, int x, int y, int z) {
      if (!stack.hasTagCompound() || empty) {
         stack.setTagCompound(new NBTTagCompound());
         stack.getTagCompound().setBoolean("FirstUse", true);
      }

      if (empty) {
         return stack;
      } else {
         NBTTagCompound compound = stack.getTagCompound();
         if (compound.getBoolean("FirstUse")) {
            compound.setInteger("Color1", tile.getColor1());
            compound.setInteger("Color2", tile.getColor2());
            compound.setInteger("Color3", tile.getColor3());
            compound.setInteger("Color4", tile.getColor4());
            compound.setInteger("CurrentColor", tile.getCurrentColor());
            compound.setBoolean("Activate2", tile.isActivate2());
            compound.setBoolean("Activate3", tile.isActivate3());
            compound.setBoolean("Activate4", tile.isActivate4());
            compound.setInteger("Time", tile.getTime());
            compound.setInteger("Counter", tile.getCounter());
            compound.setInteger("Index", tile.getIndex());
            compound.setBoolean("FirstUse", false);
            if (world.isRemote) {
               player.addChatMessage(new ChatComponentTranslation("chat.succesful_config_saver", new Object[0]));
            }
         } else {
            tile.setColor1(compound.getInteger("Color1"));
            tile.setColor2(compound.getInteger("Color2"));
            tile.setColor3(compound.getInteger("Color3"));
            tile.setColor4(compound.getInteger("Color4"));
            tile.setCurrentColor(compound.getInteger("CurrentColor"));
            tile.setActivate2(compound.getBoolean("Activate2"));
            tile.setActivate3(compound.getBoolean("Activate3"));
            tile.setActivate4(compound.getBoolean("Activate4"));
            tile.setTime(compound.getInteger("Time"));
            tile.setCounter(compound.getInteger("Counter"));
            tile.setIndex(compound.getInteger("Index"));
            world.markBlockForUpdate(x, y, z);
            if (world.isRemote) {
               player.addChatMessage(new ChatComponentTranslation("chat.config_saver_applied", new Object[0]));
            }
         }

         stack.setTagCompound(compound);
         return stack;
      }
   }
}
