package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemExperienceBerry extends Item {
   public ItemExperienceBerry() {
      this.setMaxStackSize(64);
      this.setUnlocalizedName("experienceberry");
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:ExperienceBerry");
   }

   public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
      --stack.stackSize;
      player.addExperience(10);
      return stack;
   }
}
