package fr.paladium.palamod.items;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemExplosiveStoneBase extends Item {
   public ItemExplosiveStoneBase(String name, String texture, int color) {
      this.setMaxStackSize(16);
      this.setUnlocalizedName(name);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:" + texture);
   }

   public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
      PalaMod.proxy.generateCustomParticles(0, player, 0);
      return stack;
   }
}
