package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;
import net.minecraft.world.World;

public class ItemExtrapolatedBucket extends Item {
   public ItemExtrapolatedBucket() {
      this.setMaxStackSize(1);
      this.setMaxDamage(16);
      this.setUnlocalizedName("extrapolatedbucket");
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:ExtrapolatedBucket");
   }

   public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
      MovingObjectPosition movingobjectposition = this.getMovingObjectPositionFromPlayer(par2World, par3EntityPlayer, true);
      if (movingobjectposition == null) {
         return par1ItemStack;
      } else {
         if (movingobjectposition.typeOfHit == MovingObjectType.BLOCK) {
            int i = movingobjectposition.blockX;
            int j = movingobjectposition.blockY;
            int k = movingobjectposition.blockZ;
            if (!par2World.canMineBlock(par3EntityPlayer, i, j, k)) {
               return par1ItemStack;
            }

            if (!par3EntityPlayer.canPlayerEdit(i, j, k, movingobjectposition.sideHit, par1ItemStack)) {
               return par1ItemStack;
            }

            Material material = par2World.getBlock(i, j, k).getMaterial();
            int l = par2World.getBlockMetadata(i, j, k);
            if (material == Material.water && l == 0) {
               par2World.setBlockToAir(i, j, k);
               par1ItemStack.damageItem(1, par3EntityPlayer);
               return par1ItemStack;
            }

            if (material == Material.lava && l == 0) {
               par2World.setBlockToAir(i, j, k);
               par1ItemStack.damageItem(1, par3EntityPlayer);
               return par1ItemStack;
            }
         }

         return par1ItemStack;
      }
   }
}
