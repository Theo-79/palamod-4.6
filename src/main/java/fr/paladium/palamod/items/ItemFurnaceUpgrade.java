package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemFurnaceUpgrade extends Item {
   public ItemFurnaceUpgrade() {
      this.setMaxStackSize(16);
      this.setUnlocalizedName("furnaceupgrade");
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:FurnaceUpgrade");
   }
}
