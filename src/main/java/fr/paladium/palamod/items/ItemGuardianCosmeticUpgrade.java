package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemGuardianCosmeticUpgrade extends Item {
   public static final int NAMETAG = 0;
   public int type;
   public int level;

   public ItemGuardianCosmeticUpgrade(String name, String texture, int type, int level) {
      this.type = type;
      this.level = level;
      this.setMaxStackSize(1);
      this.setUnlocalizedName(name);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:" + texture);
   }

   public int getRequiredLevel() {
      return this.level;
   }

   public int getType() {
      return this.type;
   }
}
