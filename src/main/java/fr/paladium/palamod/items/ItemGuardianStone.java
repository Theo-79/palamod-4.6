package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class ItemGuardianStone extends Item {
   public ItemGuardianStone() {
      this.setMaxStackSize(1);
      this.setUnlocalizedName("guardianstone");
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:GuardianStone");
   }

   public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) {
      if (stack.hasTagCompound()) {
         NBTTagCompound tag = stack.getTagCompound();
         list.add("Contient un golem");
         list.add("Niveau " + tag.getInteger("Levels"));
      }
   }
}
