package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemGuardianUpgrade extends Item {
   public static final int XP = 0;
   public static final int ANCHOR = 1;
   public static final int REGEN_1 = 2;
   public static final int REGEN_2 = 3;
   public static final int REGEN_3 = 4;
   public static final int CHEST = 5;
   public static final int MORE = 6;
   public static final int LIFE_1 = 7;
   public static final int LIFE_2 = 8;
   public static final int LIFE_3 = 9;
   public static final int DAMAGE_1 = 10;
   public static final int DAMAGE_2 = 11;
   public static final int DAMAGE_3 = 12;
   public static final int PENE_1 = 13;
   public static final int PENE_2 = 14;
   public static final int PENE_3 = 15;
   public static final int EXPLOSION = 16;
   public static final int THORNS_1 = 17;
   public static final int THORNS_2 = 18;
   public static final int THORNS_3 = 19;
   public static final int WEAPON = 20;
   public static final int FARM = 21;
   public static final int AUTOXP = 22;
   public static final int WAND = 23;
   public int type;
   public int level;

   public ItemGuardianUpgrade(String name, String texture, int type, int level) {
      this.type = type;
      this.level = level;
      this.setMaxStackSize(1);
      this.setUnlocalizedName(name);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:" + texture);
   }

   public int getRequiredLevel() {
      return this.level;
   }

   public int getType() {
      return this.type;
   }

   public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) {
      super.addInformation(stack, player, list, b);
      list.add("Required level:§c " + this.level);
   }
}
