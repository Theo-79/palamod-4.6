package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import java.util.Iterator;
import java.util.List;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ItemGuardianWand extends Item {
   public ItemGuardianWand() {
      this.setMaxStackSize(1);
      this.setUnlocalizedName("guardianwand");
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:GuardianWand");
   }

   public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer) {
      return itemstack;
   }

   public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) {
      EntityGuardianGolem golem = getGolem(stack, player.worldObj);
      if (golem != null) {
         list.add("Contient: " + golem.getName());
      }

      super.addInformation(stack, player, list, b);
   }

   public static void setGolem(ItemStack stack, EntityGuardianGolem e) {
      if (e != null && e instanceof EntityGuardianGolem) {
         if (!stack.hasTagCompound()) {
            stack.setTagCompound(new NBTTagCompound());
         }

         NBTTagCompound tag = stack.getTagCompound();
         tag.setString("golemUUID", e.getUniqueID().toString());
         tag.setInteger("golemID", e.getEntityId());
      }

   }

   public static boolean isGolemLoaded(ItemStack stack, World world) {
      if (!stack.hasTagCompound()) {
         return false;
      } else {
         String uuid = stack.getTagCompound().getString("golemUUID");
         if (!world.isRemote) {
            Iterator var5 = world.getLoadedEntityList().iterator();

            Object obj;
            do {
               if (!var5.hasNext()) {
                  return false;
               }

               obj = var5.next();
            } while(!(obj instanceof EntityGuardianGolem) || !((EntityGuardianGolem)obj).getUniqueID().toString().equals(uuid));

            return true;
         } else {
            Entity e = world.getEntityByID(stack.getTagCompound().getInteger("golemID"));
            return e != null && e instanceof EntityGuardianGolem;
         }
      }
   }

   public static EntityGuardianGolem getGolem(ItemStack stack, World world) {
      if (!isGolemLoaded(stack, world)) {
         return null;
      } else {
         String uuid = stack.getTagCompound().getString("golemUUID");
         if (!world.isRemote) {
            Iterator var3 = world.getLoadedEntityList().iterator();

            while(var3.hasNext()) {
               Object obj = var3.next();
               if (obj instanceof EntityGuardianGolem && ((EntityGuardianGolem)obj).getUniqueID().toString().equals(uuid)) {
                  return (EntityGuardianGolem)obj;
               }
            }
         } else {
            Entity e = world.getEntityByID(stack.getTagCompound().getInteger("golemID"));
            if (e != null && e instanceof EntityGuardianGolem) {
               return (EntityGuardianGolem)e;
            }
         }

         return null;
      }
   }
}
