package fr.paladium.palamod.items;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.TabPaladium;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.World;

public class ItemGuardianWhitelist extends Item {
   public ItemGuardianWhitelist() {
      this.setMaxStackSize(1);
      this.setUnlocalizedName("guardianwhitelist");
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:GuardianWhitelist");
   }

   public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) {
      if (stack.hasTagCompound()) {
         List<String> players = getList(stack);
         list.add("§6Players:");

         for(int i = 0; i < players.size(); ++i) {
            list.add("§c- " + (String)players.get(i));
         }

      }
   }

   public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
      if (!world.isRemote) {
         player.openGui(PalaMod.instance, 15, world, 0, 0, 0);
      }

      return stack;
   }

   public static List<String> getList(ItemStack stack) {
      List<String> list = new ArrayList();
      if (stack != null && stack.getItem() != null) {
         if (!(stack.getItem() instanceof ItemGuardianWhitelist)) {
            return list;
         } else if (!stack.hasTagCompound()) {
            NBTTagCompound tag = new NBTTagCompound();
            tag.setTag("Names", new NBTTagList());
            stack.setTagCompound(tag);
            return list;
         } else {
            NBTTagList nbttaglist = stack.getTagCompound().getTagList("Names", 10);

            for(int i = 0; i < nbttaglist.tagCount(); ++i) {
               NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
               list.add(nbttagcompound1.getString("name"));
            }

            return list;
         }
      } else {
         return list;
      }
   }

   public static void setList(List<String> list, ItemStack stack) {
      if (stack.getItem() instanceof ItemGuardianWhitelist) {
         if (!stack.hasTagCompound()) {
            NBTTagCompound tag = new NBTTagCompound();
            tag.setTag("Names", new NBTTagList());
            stack.setTagCompound(tag);
         }

         NBTTagList nbttaglist = new NBTTagList();

         for(int i = 0; i < list.size(); ++i) {
            NBTTagCompound tag = new NBTTagCompound();
            tag.setString("name", (String)list.get(i));
            nbttaglist.appendTag(tag);
         }

         NBTTagCompound tag = new NBTTagCompound();
         tag.setTag("Names", nbttaglist);
         stack.setTagCompound(tag);
      }
   }

   public static List<String> addName(String name, List<String> list) {
      list = removeName(name, list);
      list.add(name);
      return list;
   }

   public static List<String> removeName(String name, List<String> list) {
      for(int i = 0; i < list.size(); ++i) {
         if (((String)list.get(i)).equalsIgnoreCase(name)) {
            list.remove(i);
         }
      }

      return list;
   }

   public static boolean check(ItemStack stack, String name) {
      List<String> list = getList(stack);
      return list.contains(name);
   }
}
