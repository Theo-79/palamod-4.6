package fr.paladium.palamod.items;

import net.minecraft.item.Item;

public class ItemObsidianUpgrade extends Item {
   public ItemObsidianUpgrade.Upgrade type;

   public ItemObsidianUpgrade(ItemObsidianUpgrade.Upgrade upgrade) {
      this.type = upgrade;
   }

   public static enum Upgrade {
      Explode,
      Fake,
      TwoLife,
      Camouflage;
   }
}
