package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class ItemPaladiumApple extends ItemFood {
   public ItemPaladiumApple() {
      super(100, true);
      this.setMaxStackSize(16);
      this.setUnlocalizedName("paladiumapple");
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:PaladiumApple");
      this.setAlwaysEdible();
   }

   protected void onFoodEaten(ItemStack stack, World world, EntityPlayer player) {
      super.onFoodEaten(stack, world, player);
      if (!world.isRemote) {
         player.addPotionEffect(new PotionEffect(Potion.regeneration.id, 10, 2));
         player.setAbsorptionAmount(10.0F);
         player.addPotionEffect(new PotionEffect(Potion.fireResistance.id, 500, 1));
      }

   }
}
