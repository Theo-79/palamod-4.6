package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class ItemPotion extends Item {
   PotionEffect effect;

   public ItemPotion(String name, String texturename, PotionEffect effect) {
      this.setMaxStackSize(1);
      this.setMaxDamage(16);
      this.setUnlocalizedName(name);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:" + texturename);
      this.effect = effect;
   }

   public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
      player.setItemInUse(stack, this.getMaxItemUseDuration(stack));
      return stack;
   }

   public ItemStack onEaten(ItemStack stack, World world, EntityPlayer player) {
      if (!player.capabilities.isCreativeMode) {
         --stack.stackSize;
      }

      if (!world.isRemote) {
         player.addPotionEffect(this.effect);
      }

      if (!player.capabilities.isCreativeMode) {
         if (stack.stackSize <= 0) {
            return new ItemStack(Items.glass_bottle);
         }

         player.inventory.addItemStackToInventory(new ItemStack(Items.glass_bottle));
      }

      return stack;
   }

   public int getMaxItemUseDuration(ItemStack stack) {
      return 32;
   }

   public EnumAction getItemUseAction(ItemStack stack) {
      return EnumAction.drink;
   }
}
