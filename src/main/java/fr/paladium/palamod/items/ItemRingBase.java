package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.items.armors.RepairableArmor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemRingBase extends Item {
   public ItemRingBase(int durability, String name, String texture) {
      this.setMaxStackSize(1);
      this.setMaxDamage(durability);
      this.setUnlocalizedName(name);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:" + texture);
   }

   public void onUpdate(ItemStack stack, World world, Entity entity, int slot, boolean selected) {
      if (entity instanceof EntityPlayer) {
         EntityPlayer player = (EntityPlayer)entity;

         for(int i = 0; i < 4; ++i) {
            ItemStack armor = player.getCurrentArmor(i);
            if (armor != null && armor.getItem() instanceof RepairableArmor) {
               ((RepairableArmor)armor.getItem()).repair(armor, stack);
            }
         }
      }

   }
}
