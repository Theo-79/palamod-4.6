package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import java.util.LinkedList;
import java.util.List;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemRune extends Item {
   public static final int PERCENT = 0;
   public int level;
   private List a = new LinkedList();

   public ItemRune(int level) {
      this.setMaxStackSize(1);
      this.level = level;
      this.setUnlocalizedName("rune_" + level);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:Rune_" + level);
   }

   public int getLevel() {
      return this.level;
   }

   public String getType(List list) {
      int r = (int)(Math.random() * (double)(this.a.size() - 1));
      return (String)this.a.get(r);
   }

   public static String getDisplayType(int type) {
      switch(type) {
      case 0:
         return "%";
      default:
         return "";
      }
   }

   public static int getLevel(ItemStack stack) {
      return stack.getItem() instanceof ItemRune && stack.hasTagCompound() && stack.getTagCompound().hasKey("level") ? stack.getTagCompound().getInteger("level") : 0;
   }

   public static double getBonus(ItemStack stack) {
      return stack.getItem() instanceof ItemRune && stack.hasTagCompound() && stack.getTagCompound().hasKey("Bonus") ? stack.getTagCompound().getDouble("Bonus") : 0.0D;
   }

   public static double getMalus(ItemStack stack) {
      return stack.getItem() instanceof ItemRune && stack.hasTagCompound() && stack.getTagCompound().hasKey("Malus") ? stack.getTagCompound().getDouble("Malus") : 0.0D;
   }

   public static int getType(ItemStack stack) {
      return stack.getItem() instanceof ItemRune && stack.hasTagCompound() && stack.getTagCompound().hasKey("Type") ? stack.getTagCompound().getInteger("Type") : 0;
   }
}
