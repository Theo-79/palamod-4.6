package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemShardRune extends Item {
   private int level;

   public ItemShardRune(int level) {
      this.setMaxStackSize(4);
      this.level = level;
      this.setUnlocalizedName("shard_" + level);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:shard_" + level);
   }
}
