package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.entities.mobs.EntityCustomWither;
import fr.paladium.palamod.util.WitherData;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Facing;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;
import net.minecraft.world.World;

public class ItemSpawnWither extends Item {
   public ItemSpawnWither() {
      this.setUnlocalizedName("spawnwither");
      this.setTextureName("palamod:spawntest");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10) {
      if (par3World.isRemote) {
         return true;
      } else {
         Block block = par3World.getBlock(par4, par5, par6);
         par4 += Facing.offsetsXForSide[par7];
         par5 += Facing.offsetsYForSide[par7];
         par6 += Facing.offsetsZForSide[par7];
         double d0 = 0.0D;
         if (par7 == 1 && block.getRenderType() == 11) {
            d0 = 0.5D;
         }

         Entity entity = this.spawnEntity(par3World, (double)par4 + 0.5D, (double)par5 + d0, (double)par6 + 0.5D);
         if (entity != null) {
            if (entity instanceof EntityLivingBase && par1ItemStack.hasDisplayName()) {
               ((EntityLiving)entity).setCustomNameTag(par1ItemStack.getDisplayName());
            }

            if (!par2EntityPlayer.capabilities.isCreativeMode) {
               --par1ItemStack.stackSize;
            }
         }

         return true;
      }
   }

   public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
      if (par2World.isRemote) {
         return par1ItemStack;
      } else {
         MovingObjectPosition movingobjectposition = this.getMovingObjectPositionFromPlayer(par2World, par3EntityPlayer, true);
         if (movingobjectposition == null) {
            return par1ItemStack;
         } else {
            if (movingobjectposition.typeOfHit == MovingObjectType.BLOCK) {
               int i = movingobjectposition.blockX;
               int j = movingobjectposition.blockY;
               int k = movingobjectposition.blockZ;
               if (!par2World.canMineBlock(par3EntityPlayer, i, j, k)) {
                  return par1ItemStack;
               }

               if (!par3EntityPlayer.canPlayerEdit(i, j, k, movingobjectposition.sideHit, par1ItemStack)) {
                  return par1ItemStack;
               }

               if (par2World.getBlock(i, j, k) instanceof BlockLiquid) {
                  Entity entity = this.spawnEntity(par2World, (double)i, (double)j, (double)k);
                  if (entity != null) {
                     if (entity instanceof EntityLivingBase && par1ItemStack.hasDisplayName()) {
                        ((EntityLiving)entity).setCustomNameTag(par1ItemStack.getDisplayName());
                     }

                     if (!par3EntityPlayer.capabilities.isCreativeMode) {
                        --par1ItemStack.stackSize;
                     }
                  }
               }
            }

            return par1ItemStack;
         }
      }
   }

   public Entity spawnEntity(World parWorld, double parX, double parY, double parZ) {
      EntityCustomWither entityToSpawn = null;
      if (!parWorld.isRemote) {
         entityToSpawn = new EntityCustomWither(parWorld, new WitherData());
         entityToSpawn.setLocationAndAngles(parX, parY, parZ, MathHelper.wrapAngleTo180_float(parWorld.rand.nextFloat() * 360.0F), 0.0F);
         parWorld.spawnEntityInWorld(entityToSpawn);
         entityToSpawn.onSpawnWithEgg((IEntityLivingData)null);
         entityToSpawn.playLivingSound();
      }

      return entityToSpawn;
   }
}
