package fr.paladium.palamod.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.entities.projectiles.EntitySplashPotion;
import java.util.List;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class ItemSplashPotion extends Item {
   PotionEffect effect;
   private String[] type = new String[]{"SicknessPotion", "WebPotion"};
   private IIcon[] iconArray;

   public ItemSplashPotion(String name, String texturename, PotionEffect effect, int damageValue) {
      this.setMaxStackSize(1);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setHasSubtypes(true);
      this.effect = effect;
   }

   public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
      if (!world.isRemote) {
         world.spawnEntityInWorld(new EntitySplashPotion(world, player, this.getDamage(stack)));
      }

      return stack;
   }

   public int getMetadata(int metadata) {
      return metadata;
   }

   public String getUnlocalizedName(ItemStack stack) {
      int metadata = stack.getItemDamage();
      if (metadata > this.type.length || metadata < 0) {
         metadata = 0;
      }

      return "splashpotion." + this.type[metadata].toLowerCase();
   }

   public void registerIcons(IIconRegister iconregister) {
      this.iconArray = new IIcon[this.type.length];

      for(int i = 0; i < this.type.length; ++i) {
         this.iconArray[i] = iconregister.registerIcon("palamod:" + this.type[i]);
      }

   }

   @SideOnly(Side.CLIENT)
   public void getSubItems(Item Item, CreativeTabs creativeTabs, List list) {
      for(int metadata = 0; metadata < this.type.length; ++metadata) {
         list.add(new ItemStack(Item, 1, metadata));
      }

   }

   @SideOnly(Side.CLIENT)
   public IIcon getIconFromDamage(int metadata) {
      return metadata < this.type.length && metadata >= 0 ? this.iconArray[metadata] : this.iconArray[0];
   }
}
