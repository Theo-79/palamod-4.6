package fr.paladium.palamod.items;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemStartBook extends Item {
   public ItemStartBook() {
      this.setMaxStackSize(1);
      this.setUnlocalizedName("startbook");
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:StartBook");
   }

   public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
      if (!world.isRemote) {
         player.openGui(PalaMod.instance, 11, world, 0, 0, 0);
      }

      return stack;
   }
}
