package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityPotion;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ItemStickBase extends Item {
   public static final int HEAL = 0;
   public static final int SPEED = 1;
   public static final int STRENGHT = 2;
   public static final int JUMP = 3;
   public static final int GOD = 4;
   public static final int DAMAGE = 5;
   public static final int HYPERJUMP = 6;
   int type;
   int cooldown;
   double[] color;

   public ItemStickBase(int durability, String name, String texture, int cooldown, int type, double[] color) {
      this.setMaxStackSize(1);
      this.setMaxDamage(durability);
      this.setUnlocalizedName(name);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:" + texture);
      this.type = type;
      this.cooldown = cooldown;
      this.color = color;
   }

   public boolean isFull3D() {
      return true;
   }

   public void onUpdate(ItemStack item, World world, Entity player, int slotIndex, boolean inHand) {
      if (item.hasTagCompound()) {
         if (item.stackTagCompound.getInteger("timer") > 0) {
            item.stackTagCompound.setInteger("timer", item.stackTagCompound.getInteger("timer") + 1);
         }

         if (item.stackTagCompound.getInteger("timer") >= this.cooldown * 20) {
            item.stackTagCompound.setInteger("timer", 0);
         }
      }

      super.onUpdate(item, world, player, slotIndex, inHand);
   }

   public boolean hasEffect(ItemStack item, int pass) {
      if (item.hasTagCompound()) {
         return item.stackTagCompound.getInteger("timer") == 0;
      } else {
         return true;
      }
   }

   public ItemStack onItemRightClick(ItemStack item, World world, EntityPlayer player) {
      if (!item.hasTagCompound()) {
         item.setTagCompound(new NBTTagCompound());
         item.stackTagCompound.setInteger("timer", 0);
      }

      if (item.stackTagCompound.getInteger("timer") == 0) {
         item.damageItem(1, player);
         this.getTypeEffect(player);

         for(int i = 0; i < 200; ++i) {
            world.spawnParticle("reddust", player.posX + world.rand.nextDouble() * 2.0D - 1.0D, player.posY + world.rand.nextDouble() * 2.0D - 2.0D, player.posZ + world.rand.nextDouble() * 2.0D - 1.0D, this.color[0], this.color[1], this.color[2]);
         }

         item.stackTagCompound.setInteger("timer", 1);
      } else if (world.isRemote) {
         player.addChatComponentMessage(new ChatComponentTranslation(EnumChatFormatting.RED + "Tu dois attendre que le baton se recharge !", new Object[0]));
      }

      return item;
   }

   public void getTypeEffect(EntityPlayer player) {
      switch(this.type) {
      case 0:
         player.heal(6.0F);
         return;
      case 1:
         player.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 25, 9));
         return;
      case 2:
         player.addPotionEffect(new PotionEffect(Potion.damageBoost.id, 60, 2));
         return;
      case 3:
         ++player.motionY;
         return;
      case 4:
         player.heal(10.0F);
         player.setAbsorptionAmount(6.0F);
         player.addPotionEffect(new PotionEffect(Potion.field_76443_y.id, 30, 1));
         player.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 50, 11));
         player.addPotionEffect(new PotionEffect(Potion.damageBoost.id, 40, 2));
         return;
      case 5:
         if (!player.worldObj.isRemote) {
            player.worldObj.spawnEntityInWorld(new EntityPotion(player.worldObj, player, 12));
         }

         return;
      case 6:
         player.motionY += 6.0D;
         return;
      default:
      }
   }
}
