package fr.paladium.palamod.items;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ItemStuffSwitcher extends Item {
   public ItemStuffSwitcher() {
      this.setMaxStackSize(1);
      this.setUnlocalizedName("stuffswitcher");
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:StuffSwitcher");
   }

   public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
      if (player.isSneaking()) {
         if (!world.isRemote) {
            player.openGui(PalaMod.instance, 14, world, 0, 0, 0);
         }
      } else {
         if (stack.hasTagCompound() && stack.getTagCompound().getInteger("Cooldown") > 0) {
            if (world.isRemote) {
               player.addChatComponentMessage(new ChatComponentTranslation(EnumChatFormatting.RED + "Tu ne dois pas spammer le stuff switcher !", new Object[0]));
            }

            return stack;
         }

         if (world.isRemote) {
            player.addChatComponentMessage(new ChatComponentTranslation(EnumChatFormatting.RED + "Cet item est temporairement bloquÃ© !", new Object[0]));
         }
      }

      return stack;
   }

   public void onUpdate(ItemStack stack, World world, Entity player, int p_77663_4_, boolean b) {
      if (stack.hasTagCompound()) {
         NBTTagCompound tag = stack.getTagCompound();
         int cd = tag.getInteger("Cooldown");
         if (cd > 0) {
            tag.setInteger("Cooldown", cd - 1);
         }

         stack.setTagCompound(tag);
      }

      if (stack.hasTagCompound() && stack.getTagCompound().hasKey("Open") && !b) {
         stack.getTagCompound().setBoolean("Open", false);
      }

   }
}
