package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemToolTrap extends Item {
   public ItemToolTrap() {
      this.setMaxStackSize(1);
      this.setUnlocalizedName("tooltrap");
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:ToolTrap");
   }

   public boolean isFull3D() {
      return true;
   }
}
