package fr.paladium.palamod.items;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemVoidStone extends Item {
   public ItemVoidStone() {
      this.setMaxStackSize(1);
      this.setUnlocalizedName("voidstone");
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:VoidStone");
   }

   public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
      if (!world.isRemote) {
         player.openGui(PalaMod.instance, 10, world, 0, 0, 0);
      }

      return stack;
   }
}
