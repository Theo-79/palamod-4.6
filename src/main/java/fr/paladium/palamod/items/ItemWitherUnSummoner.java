package fr.paladium.palamod.items;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemWitherUnSummoner extends Item {
   public ItemWitherUnSummoner() {
      this.setUnlocalizedName("witherunsumoner");
      this.setTextureName("palamod:WitherUnSummoner");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
