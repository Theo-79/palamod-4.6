package fr.paladium.palamod.items;

import cpw.mods.fml.common.registry.GameRegistry;
import fr.paladium.palamod.blocks.ModBlocks;
import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.items.armors.ItemArmorAmethyst;
import fr.paladium.palamod.items.armors.ItemArmorEndium;
import fr.paladium.palamod.items.armors.ItemArmorPaladium;
import fr.paladium.palamod.items.armors.ItemArmorTitane;
import fr.paladium.palamod.items.armors.ItemArmorTravel;
import fr.paladium.palamod.items.core.ItemAmethystStick;
import fr.paladium.palamod.items.core.ItemBowModifier;
import fr.paladium.palamod.items.core.ItemBowRangeModifier;
import fr.paladium.palamod.items.core.ItemBowSpeedModifier;
import fr.paladium.palamod.items.core.ItemGrinderModifier;
import fr.paladium.palamod.items.core.ItemHealOrb;
import fr.paladium.palamod.items.core.ItemJumpOrb;
import fr.paladium.palamod.items.core.ItemKnockbackOrb;
import fr.paladium.palamod.items.core.ItemMagicalAnvilBottom;
import fr.paladium.palamod.items.core.ItemMagicalAnvilMiddle;
import fr.paladium.palamod.items.core.ItemMagicalAnvilTop;
import fr.paladium.palamod.items.core.ItemPaladiumCore;
import fr.paladium.palamod.items.core.ItemPaladiumStick;
import fr.paladium.palamod.items.core.ItemPatern;
import fr.paladium.palamod.items.core.ItemSpeedOrb;
import fr.paladium.palamod.items.core.ItemStrenghtOrb;
import fr.paladium.palamod.items.core.ItemStringDiamond;
import fr.paladium.palamod.items.core.ItemTitaneStick;
import fr.paladium.palamod.items.core.ItemToolPart;
import fr.paladium.palamod.items.core.ItemWing;
import fr.paladium.palamod.items.core.ItemWitherSkullFragment;
import fr.paladium.palamod.items.ores.ItemAmethyst;
import fr.paladium.palamod.items.ores.ItemFindium;
import fr.paladium.palamod.items.ores.ItemPaladium;
import fr.paladium.palamod.items.ores.ItemTitane;
import fr.paladium.palamod.items.tools.ItemAmethystAxe;
import fr.paladium.palamod.items.tools.ItemAmethystHammer;
import fr.paladium.palamod.items.tools.ItemAmethystPickaxe;
import fr.paladium.palamod.items.tools.ItemAmethystShovel;
import fr.paladium.palamod.items.tools.ItemEndiumAxe;
import fr.paladium.palamod.items.tools.ItemEndiumPickaxe;
import fr.paladium.palamod.items.tools.ItemPaladiumAxe;
import fr.paladium.palamod.items.tools.ItemPaladiumHammer;
import fr.paladium.palamod.items.tools.ItemPaladiumPickaxe;
import fr.paladium.palamod.items.tools.ItemPaladiumShovel;
import fr.paladium.palamod.items.tools.ItemSmithHammer;
import fr.paladium.palamod.items.tools.ItemTitaneAxe;
import fr.paladium.palamod.items.tools.ItemTitaneHammer;
import fr.paladium.palamod.items.tools.ItemTitanePickaxe;
import fr.paladium.palamod.items.tools.ItemTitaneShovel;
import fr.paladium.palamod.items.weapons.ItemAmethystBroadsword;
import fr.paladium.palamod.items.weapons.ItemAmethystFastsword;
import fr.paladium.palamod.items.weapons.ItemAmethystSword;
import fr.paladium.palamod.items.weapons.ItemEndiumSword;
import fr.paladium.palamod.items.weapons.ItemInfernalKnocker;
import fr.paladium.palamod.items.weapons.ItemPaladiumBow;
import fr.paladium.palamod.items.weapons.ItemPaladiumBroadsword;
import fr.paladium.palamod.items.weapons.ItemPaladiumFastsword;
import fr.paladium.palamod.items.weapons.ItemPaladiumSword;
import fr.paladium.palamod.items.weapons.ItemPotionLauncher;
import fr.paladium.palamod.items.weapons.ItemTitaneBroadsword;
import fr.paladium.palamod.items.weapons.ItemTitaneFastsword;
import fr.paladium.palamod.items.weapons.ItemTitaneSword;
import fr.paladium.palamod.potion.ModPotions;
import net.minecraft.item.Item;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;

public class ModItems {
   public static ItemFindium findium;
   public static ItemPaladium paladium;
   public static ItemAmethyst amethyst;
   public static ItemTitane titane;
   public static ItemPaladiumPickaxe paladiumPickaxe;
   public static ItemTitanePickaxe titanePickaxe;
   public static ItemAmethystPickaxe amethystPickaxe;
   public static ItemEndiumPickaxe endiumPickaxe;
   public static ItemPaladiumShovel paladiumShovel;
   public static ItemTitaneShovel titaneShovel;
   public static ItemAmethystShovel amethystShovel;
   public static ItemPaladiumAxe paladiumAxe;
   public static ItemTitaneAxe titaneAxe;
   public static ItemAmethystAxe amethystAxe;
   public static ItemEndiumAxe endiumAxe;
   public static ItemPaladiumHammer paladiumHammer;
   public static ItemTitaneHammer titaneHammer;
   public static ItemAmethystHammer amethystHammer;
   public static ItemSmithHammer smithHammer;
   public static ItemPaladiumSword paladiumSword;
   public static ItemTitaneSword titaneSword;
   public static ItemAmethystSword amethystSword;
   public static ItemEndiumSword endiumSword;
   public static ItemInfernalKnocker infernalKnocker;
   public static ItemPaladiumBroadsword paladiumBroadsword;
   public static ItemAmethystBroadsword amethystBroadsword;
   public static ItemTitaneBroadsword titaneBroadsword;
   public static ItemPaladiumFastsword paladiumFastSword;
   public static ItemAmethystFastsword amethystFastSword;
   public static ItemTitaneFastsword titaneFastSword;
   public static ItemPaladiumBow paladiumBow;
   public static ItemArmorPaladium paladiumHelmet;
   public static ItemArmorPaladium paladiumChestplate;
   public static ItemArmorPaladium paladiumLeggings;
   public static ItemArmorPaladium paladiumBoots;
   public static ItemArmorTitane titaneHelmet;
   public static ItemArmorTitane titaneChestplate;
   public static ItemArmorTitane titaneLeggings;
   public static ItemArmorTitane titaneBoots;
   public static ItemArmorAmethyst amethystHelmet;
   public static ItemArmorAmethyst amethystChestplate;
   public static ItemArmorAmethyst amethystLeggings;
   public static ItemArmorAmethyst amethystBoots;
   public static ItemArmorEndium endiumHelmet;
   public static ItemArmorEndium endiumChestplate;
   public static ItemArmorEndium endiumLeggings;
   public static ItemArmorEndium endiumBoots;
   public static ItemArmorTravel travelLeggings;
   public static ItemArmorTravel travelBoots;
   public static ItemArmorTravel jumpChest;
   public static ItemArmorTravel slimyHelmet;
   public static ItemArmorTravel scubaHelmet;
   public static ItemArmorTravel hoodHelmet;
   public static ItemExtrapolatedBucket extrapolatedBucket;
   public static ItemUnclaimFinder unclaimFinder;
   public static ItemPaladiumApple paladiumApple;
   public static ItemHangGlider hangGlider;
   public static ItemFurnaceUpgrade furnaceUpgrade;
   public static ItemExplosiveStoneBase explosiveStone;
   public static ItemExperienceBerry experienceBerry;
   public static ItemGuardianStone guardianStone;
   public static ItemBackpack backpack;
   public static ItemVoidStone voidStone;
   public static ItemChestExplorer chestExplorer;
   public static ItemStuffSwitcher stuffSwitcher;
   public static ItemDynamite dynamite;
   public static ItemDynamiteNinja dynamiteNinja;
   public static ItemDynamiteBig dynamiteBig;
   public static ItemGuardianWhitelist guardianWhitelist;
   public static ItemGuardianUpgrade guardianXpUpgrade;
   public static ItemGuardianCosmeticUpgrade guardianNametagUpgrade;
   public static ItemRingBase smallRing;
   public static ItemRingBase mediumRing;
   public static ItemRingBase bigRing;
   public static ItemHealOrb healOrb;
   public static ItemSpeedOrb speedOrb;
   public static ItemStrenghtOrb strenghtOrb;
   public static ItemJumpOrb jumpOrb;
   public static ItemKnockbackOrb knockbackOrb;
   public static ItemPaladiumStick paladiumStick;
   public static Item compressedPaladium;
   public static Item compressedTitane;
   public static ItemWitherSkullFragment witherSkullFragment;
   public static ItemPaladiumCore paladiumCore;
   public static ItemAmethystStick amethystStick;
   public static ItemTitaneStick titaneStick;
   public static ItemStringDiamond diamondString;
   public static ItemWing wing;
   public static ItemBowModifier bowModifer;
   public static ItemGrinderModifier smeltModifier;
   public static ItemGrinderModifier fortuneModifier;
   public static ItemGrinderModifier speedModifier;
   public static ItemGrinderModifier damageModifier;
   public static ItemGrinderModifier flameModifier;
   public static ItemGrinderModifier knockbackModifier;
   public static ItemGrinderModifier autoRepairModifier;
   public static ItemToolTrap toolTrap;
   public static ItemStickBase healStick;
   public static ItemStickBase speedStick;
   public static ItemStickBase strenghtStick;
   public static ItemStickBase jumpStick;
   public static ItemStickBase godStick;
   public static ItemStickBase damageStick;
   public static ItemStickBase hyperJumpStick;
   public static ItemPotion potionWither;
   public static ItemPotion potionFire;
   public static ItemPotion potionPoison;
   public static ItemSplashPotion sicknessPotion;
   public static ItemPotionLauncher potionLauncher;
   public static ItemArrowBase arrowPoison;
   public static ItemArrowBase arrowSlowness;
   public static ItemArrowBase arrowWither;
   public static ItemArrowBase arrowSwitch;
   public static ItemBucketBase bucketSulfuric;
   public static ItemBucketBase bucketAngelic;
   public static ItemBowRangeModifier bowRangeModifier;
   public static ItemBowSpeedModifier bowSpeedModifier;
   public static ItemPatern paternPickaxe;
   public static ItemPatern paternBroadsword;
   public static ItemPatern paternSword;
   public static ItemPatern paternFastSword;
   public static ItemPatern paternHammer;
   public static ItemPatern paternAxe;
   public static ItemPatern paternShovel;
   public static ItemPatern paternPaladium;
   public static ItemPatern paternIngot;
   public static ItemPatern paternBlock;
   public static ItemPatern paternSocket;
   public static ItemToolPart pickaxeHead;
   public static ItemToolPart broadSwordHead;
   public static ItemToolPart swordHead;
   public static ItemToolPart fastSwordHead;
   public static ItemToolPart hammerHead;
   public static ItemToolPart shovelHead;
   public static ItemToolPart axeHead;
   public static ItemMagicalAnvilTop MagicalAnvilTop;
   public static ItemMagicalAnvilMiddle MagicalAnvilMiddle;
   public static ItemMagicalAnvilBottom MagicalAnvilBottom;
   public static ItemRune itemrune;
   public static ItemRune itemrune2;
   public static ItemRune itemrune3;
   public static ItemRune itemrune4;
   public static ItemShardRune itemshardrune;
   public static ItemShardRune itemshardrune2;
   public static ItemShardRune itemshardrune3;
   public static ItemShardRune itemshardrune4;
   public static ItemSpawnWither itemSpawnWither;
   public static ItemWitherUnSummoner unSummoner;
   public static Item config_saver;
   public static Item explode_obsidian_upgrade;
   public static Item fake_obsidian_upgrade;
   public static Item twoLife_obsidian_upgrade;
   public static Item camouflage_obsidian_upgrade;
   public static Item camera_tablet;
   public static ItemGuardianWand guardianWand;
   public static ItemGrinderModifier obsidianUpgrade;
   public static ItemGrinderModifier moreUpgrade;
   public static ItemGuardianUpgrade anchorUpgrade;
   public static ItemGuardianUpgrade regenOneUpgrade;
   public static ItemGuardianUpgrade regenTwoUpgrade;
   public static ItemGuardianUpgrade regenThreeUpgrade;
   public static ItemGuardianUpgrade chestUpgrade;
   public static ItemGuardianUpgrade guardianMoreUpgrade;
   public static ItemGuardianUpgrade guardianLifeUpgrade1;
   public static ItemGuardianUpgrade guardianLifeUpgrade2;
   public static ItemGuardianUpgrade guardianLifeUpgrade3;
   public static ItemGuardianUpgrade guardianDamageUpgrade1;
   public static ItemGuardianUpgrade guardianDamageUpgrade2;
   public static ItemGuardianUpgrade guardianDamageUpgrade3;
   public static ItemGuardianUpgrade guardianPeneUpgrade1;
   public static ItemGuardianUpgrade guardianPeneUpgrade2;
   public static ItemGuardianUpgrade guardianPeneUpgrade3;
   public static ItemGuardianUpgrade guardianExplosiveUpgrade;
   public static ItemGuardianUpgrade guardianThornsUpgrade1;
   public static ItemGuardianUpgrade guardianThornsUpgrade2;
   public static ItemGuardianUpgrade guardianThornsUpgrade3;
   public static ItemGuardianUpgrade guardianWeaponUpgrade;
   public static ItemGuardianUpgrade guardianFarmUpgrade;
   public static ItemGuardianUpgrade guardianAutoXPUpgrade;
   public static ItemGuardianUpgrade guardianHideWandUpgrade;

   public static void init() {
      itemrune = new ItemRune(1);
      itemrune2 = new ItemRune(2);
      itemrune3 = new ItemRune(3);
      itemrune4 = new ItemRune(4);
      itemshardrune = new ItemShardRune(1);
      itemshardrune2 = new ItemShardRune(2);
      itemshardrune3 = new ItemShardRune(3);
      itemshardrune4 = new ItemShardRune(4);
      findium = new ItemFindium();
      paladium = new ItemPaladium();
      titane = new ItemTitane();
      amethyst = new ItemAmethyst();
      paladiumPickaxe = new ItemPaladiumPickaxe();
      titanePickaxe = new ItemTitanePickaxe();
      amethystPickaxe = new ItemAmethystPickaxe();
      endiumPickaxe = new ItemEndiumPickaxe();
      paladiumShovel = new ItemPaladiumShovel();
      titaneShovel = new ItemTitaneShovel();
      amethystShovel = new ItemAmethystShovel();
      paladiumAxe = new ItemPaladiumAxe();
      titaneAxe = new ItemTitaneAxe();
      amethystAxe = new ItemAmethystAxe();
      endiumAxe = new ItemEndiumAxe();
      paladiumHammer = new ItemPaladiumHammer();
      titaneHammer = new ItemTitaneHammer();
      amethystHammer = new ItemAmethystHammer();
      smithHammer = new ItemSmithHammer();
      paladiumSword = new ItemPaladiumSword();
      titaneSword = new ItemTitaneSword();
      amethystSword = new ItemAmethystSword();
      endiumSword = new ItemEndiumSword();
      infernalKnocker = new ItemInfernalKnocker();
      paladiumBroadsword = new ItemPaladiumBroadsword();
      amethystBroadsword = new ItemAmethystBroadsword();
      titaneBroadsword = new ItemTitaneBroadsword();
      paladiumFastSword = new ItemPaladiumFastsword();
      amethystFastSword = new ItemAmethystFastsword();
      titaneFastSword = new ItemTitaneFastsword();
      toolTrap = new ItemToolTrap();
      paladiumBow = new ItemPaladiumBow();
      paladiumHelmet = new ItemArmorPaladium(0, "paladiumhelmet", "PaladiumHelmet");
      paladiumChestplate = new ItemArmorPaladium(1, "paladiumchestplate", "PaladiumChestplate");
      paladiumLeggings = new ItemArmorPaladium(2, "paladiumleggings", "PaladiumLeggings");
      paladiumBoots = new ItemArmorPaladium(3, "paladiumboots", "PaladiumBoots");
      titaneHelmet = new ItemArmorTitane(0, "titanehelmet", "TitaneHelmet");
      titaneChestplate = new ItemArmorTitane(1, "titanechestplate", "TitaneChestplate");
      titaneLeggings = new ItemArmorTitane(2, "titaneleggings", "TitaneLeggings");
      titaneBoots = new ItemArmorTitane(3, "titaneboots", "TitaneBoots");
      amethystHelmet = new ItemArmorAmethyst(0, "amethysthelmet", "AmethystHelmet");
      amethystChestplate = new ItemArmorAmethyst(1, "amethystchestplate", "AmethystChestplate");
      amethystLeggings = new ItemArmorAmethyst(2, "amethystleggings", "AmethystLeggings");
      amethystBoots = new ItemArmorAmethyst(3, "amethystboots", "AmethystBoots");
      endiumHelmet = new ItemArmorEndium(0, "endiumhelmet", "EndiumHelmet");
      endiumChestplate = new ItemArmorEndium(1, "endiumchestplate", "EndiumChestplate");
      endiumLeggings = new ItemArmorEndium(2, "endiumleggings", "EndiumLeggings");
      endiumBoots = new ItemArmorEndium(3, "endiumboots", "EndiumBoots");
      travelLeggings = new ItemArmorTravel(2, "travellegging", "TravelLeggings", 1);
      travelBoots = new ItemArmorTravel(3, "travelboots", "TravelBoots", 0);
      jumpChest = new ItemArmorTravel(1, "jumpchest", "JumpChest", 2);
      slimyHelmet = new ItemArmorTravel(0, "slimyhelmet", "SlimyHelmet", 3);
      scubaHelmet = new ItemArmorTravel(0, "scubahelmet", "ScubaHelmet", 4);
      hoodHelmet = new ItemArmorTravel(0, "hoodhelmet", "HoodHelmet", 5);
      chestExplorer = new ItemChestExplorer();
      dynamite = new ItemDynamite();
      dynamiteNinja = new ItemDynamiteNinja();
      dynamiteBig = new ItemDynamiteBig();
      extrapolatedBucket = new ItemExtrapolatedBucket();
      unclaimFinder = new ItemUnclaimFinder();
      paladiumApple = new ItemPaladiumApple();
      hangGlider = new ItemHangGlider();
      furnaceUpgrade = new ItemFurnaceUpgrade();
      explosiveStone = new ItemExplosiveStoneBase("explosivestone", "ExplosiveStone", 0);
      experienceBerry = new ItemExperienceBerry();
      guardianStone = new ItemGuardianStone();
      voidStone = new ItemVoidStone();
      stuffSwitcher = new ItemStuffSwitcher();
      backpack = new ItemBackpack();
      MagicalAnvilTop = new ItemMagicalAnvilTop();
      MagicalAnvilMiddle = new ItemMagicalAnvilMiddle();
      MagicalAnvilBottom = new ItemMagicalAnvilBottom();
      guardianWhitelist = new ItemGuardianWhitelist();
      guardianXpUpgrade = new ItemGuardianUpgrade("guardianxpupgrade", "GuardianExperienceUpgrade", 0, 5);
      guardianNametagUpgrade = new ItemGuardianCosmeticUpgrade("guardiannametagupgrade", "GuardianNametagUpgrade", 0, 0);
      smallRing = new ItemRingBase(500, "smallring", "SmallRing");
      mediumRing = new ItemRingBase(1000, "mediumring", "MediumRing");
      bigRing = new ItemRingBase(3000, "bigring", "BigRing");
      healOrb = new ItemHealOrb();
      speedOrb = new ItemSpeedOrb();
      strenghtOrb = new ItemStrenghtOrb();
      jumpOrb = new ItemJumpOrb();
      knockbackOrb = new ItemKnockbackOrb();
      paladiumStick = new ItemPaladiumStick();
      compressedPaladium = (new Item()).setUnlocalizedName("compressedpaladium").setTextureName("palamod:CompressedPaladium").setCreativeTab(TabPaladium.INSTANCE);
      compressedTitane = (new Item()).setUnlocalizedName("compressedtitane").setTextureName("palamod:CompressedTitane").setCreativeTab(TabPaladium.INSTANCE);
      witherSkullFragment = new ItemWitherSkullFragment();
      paladiumCore = new ItemPaladiumCore();
      titaneStick = new ItemTitaneStick();
      amethystStick = new ItemAmethystStick();
      diamondString = new ItemStringDiamond();
      wing = new ItemWing();
      bowModifer = new ItemBowModifier();
      smeltModifier = new ItemGrinderModifier("smeltmodifier", "SmeltModifier");
      fortuneModifier = new ItemGrinderModifier("fortunemodifier", "FortuneModifier");
      speedModifier = new ItemGrinderModifier("speedmodifier", "SpeedModifier");
      damageModifier = new ItemGrinderModifier("damagemodifier", "DamageModifier");
      flameModifier = new ItemGrinderModifier("flamemodifier", "FlameModifier");
      knockbackModifier = new ItemGrinderModifier("knockbackmodifier", "KnockbackModifier");
      autoRepairModifier = new ItemGrinderModifier("autorepairmodifier", "AutoRepairModifier");
      healStick = new ItemStickBase(15, "healstick", "HealStick", 10, 0, new double[]{255.0D, 0.0D, 0.0D});
      speedStick = new ItemStickBase(15, "speedstick", "SpeedStick", 15, 1, new double[]{73.0D, 173.0D, 244.0D});
      strenghtStick = new ItemStickBase(15, "strenghtstick", "StrenghtStick", 15, 2, new double[]{244.0D, 212.0D, 66.0D});
      jumpStick = new ItemStickBase(15, "jumpstick", "JumpStick", 6, 3, new double[]{244.0D, 143.0D, 66.0D});
      godStick = new ItemStickBase(8, "stickofgod", "StickOfGod", 30, 4, new double[]{244.0D, 238.0D, 66.0D});
      damageStick = new ItemStickBase(16, "damagestick", "DamageStick", 6, 5, new double[]{255.0D, 0.0D, 0.0D});
      hyperJumpStick = new ItemStickBase(5, "hyperjumpstick", "HyperJumpStick", 12, 6, new double[]{80.0D, 244.0D, 66.0D});
      potionWither = new ItemPotion("witherimbue", "PotionWither", new PotionEffect(ModPotions.potionWither.id, 2000, 1));
      potionFire = new ItemPotion("fireimbue", "PotionFire", new PotionEffect(ModPotions.potionFire.id, 2000, 1));
      potionPoison = new ItemPotion("poisonimbue", "PotionPoison", new PotionEffect(ModPotions.potionPoison.id, 2000, 1));
      sicknessPotion = new ItemSplashPotion("sicknesspotion", "SicknessPotion", new PotionEffect(Potion.confusion.id, 200, 1), 0);
      potionLauncher = new ItemPotionLauncher();
      arrowPoison = new ItemArrowBase("arrowpoison", "ArrowPoison", 0);
      arrowSlowness = new ItemArrowBase("arrowslowness", "ArrowSlowness", 2);
      arrowWither = new ItemArrowBase("arrowwither", "ArrowWither", 1);
      arrowSwitch = new ItemArrowBase("arrowswitch", "ArrowSwitch", 3);
      bowRangeModifier = new ItemBowRangeModifier();
      bowSpeedModifier = new ItemBowSpeedModifier();
      bucketSulfuric = new ItemBucketBase(ModBlocks.sulfuricWater, "sulfuricwater", "BucketSulfuric");
      bucketAngelic = new ItemBucketBase(ModBlocks.angelicWater, "angelicwater", "BucketAngelic");
      paternAxe = new ItemPatern("paternaxe", "PaternAxe", ItemPatern.AXE);
      paternHammer = new ItemPatern("paternhammer", "paternHammer", ItemPatern.HAMMER);
      paternShovel = new ItemPatern("paternshovel", "paternShovel", ItemPatern.SHOVEL);
      paternBroadsword = new ItemPatern("paternbroadsword", "PaternBroadSword", ItemPatern.BROADSWORD);
      paternFastSword = new ItemPatern("paternfastsword", "PaternFastSword", ItemPatern.FASTSWORD);
      paternPickaxe = new ItemPatern("paternpickaxe", "PaternPickaxe", ItemPatern.PICKAXE);
      paternSword = new ItemPatern("paternsword", "PaternSword", ItemPatern.SWORD);
      paternIngot = new ItemPatern("paterningot", "PaternIngot", ItemPatern.INGOT);
      paternBlock = new ItemPatern("paternblock", "PaternBlock", 8);
      paternSocket = new ItemPatern("paternsocket", "PaternSocket", ItemPatern.SOCKET);
      fastSwordHead = new ItemToolPart("FastSwordHead", "fastswordhead");
      broadSwordHead = new ItemToolPart("BroadSwordHead", "broadswordhead");
      pickaxeHead = new ItemToolPart("PickaxeHead", "pickaxehead");
      swordHead = new ItemToolPart("SwordHead", "swordhead");
      hammerHead = new ItemToolPart("HammerHead", "hammerhead");
      axeHead = new ItemToolPart("AxeHead", "axehead");
      shovelHead = new ItemToolPart("ShovelHead", "shovelhead");
      config_saver = (new ItemConfigSaver()).setUnlocalizedName("config_saver").setFull3D().setMaxStackSize(1);
      itemSpawnWither = new ItemSpawnWither();
      unSummoner = new ItemWitherUnSummoner();
      explode_obsidian_upgrade = (new ItemObsidianUpgrade(ItemObsidianUpgrade.Upgrade.Explode)).setUnlocalizedName("explode_obsidian_upgrade").setTextureName("palamod:explode_obsidian_upgrade").setCreativeTab(TabPaladium.INSTANCE);
      fake_obsidian_upgrade = (new ItemObsidianUpgrade(ItemObsidianUpgrade.Upgrade.Fake)).setUnlocalizedName("fake_obsidian_upgrade").setTextureName("palamod:fake_obsidian_upgrade").setCreativeTab(TabPaladium.INSTANCE);
      twoLife_obsidian_upgrade = (new ItemObsidianUpgrade(ItemObsidianUpgrade.Upgrade.TwoLife)).setUnlocalizedName("twoLife_obsidian_upgrade").setTextureName("palamod:twoLife_obsidian_upgrade").setCreativeTab(TabPaladium.INSTANCE);
      camouflage_obsidian_upgrade = (new ItemObsidianUpgrade(ItemObsidianUpgrade.Upgrade.Camouflage)).setUnlocalizedName("camouflage_obsidian_upgrade").setTextureName("palamod:camouflage_obsidian_upgrade").setCreativeTab(TabPaladium.INSTANCE);
      camera_tablet = (new ItemCameraTablet()).setUnlocalizedName("camera_tablet").setTextureName("palamod:camera_tablet").setCreativeTab(TabPaladium.INSTANCE);
      guardianWand = new ItemGuardianWand();
      obsidianUpgrade = new ItemGrinderModifier("obsidianupgrade", "ObsidianUpgrade");
      moreUpgrade = new ItemGrinderModifier("moreupgrade", "MoreUpgrade");
      anchorUpgrade = new ItemGuardianUpgrade("guardiananchorupgrade", "GuardianAnchorUpgrade", 1, 10);
      regenOneUpgrade = new ItemGuardianUpgrade("guardianregenoneupgrade", "GuardianRegenOneUpgrade", 2, 5);
      regenTwoUpgrade = new ItemGuardianUpgrade("guardianregentwoupgrade", "GuardianRegenTwoUpgrade", 3, 30);
      regenThreeUpgrade = new ItemGuardianUpgrade("guardianregenthreeupgrade", "GuardianRegenThreeUpgrade", 4, 40);
      chestUpgrade = new ItemGuardianUpgrade("guardianchestupgrade", "GuardianChestUpgrade", 5, 30);
      guardianMoreUpgrade = new ItemGuardianUpgrade("guardianmoreupgrade", "GuardianMoreUpgrade", 6, 20);
      guardianLifeUpgrade1 = new ItemGuardianUpgrade("guardianlifeupgradeone", "GuardianLifeUpgradeOne", 7, 5);
      guardianLifeUpgrade2 = new ItemGuardianUpgrade("guardianlifeupgradetwo", "GuardianLifeUpgradeTwo", 8, 20);
      guardianLifeUpgrade3 = new ItemGuardianUpgrade("guardianlifeupgradethree", "GuardianLifeUpgradeThree", 9, 30);
      guardianDamageUpgrade1 = new ItemGuardianUpgrade("guardiandamageupgradeone", "GuardianDamageUpgradeOne", 10, 20);
      guardianDamageUpgrade2 = new ItemGuardianUpgrade("guardiandamageupgradetwo", "GuardianDamageUpgradeTwo", 11, 40);
      guardianDamageUpgrade3 = new ItemGuardianUpgrade("guardiandamageupgradethree", "GuardianDamageUpgradeThree", 12, 60);
      guardianPeneUpgrade1 = new ItemGuardianUpgrade("guardianpeneupgradeone", "GuardianPeneUpgradeOne", 13, 20);
      guardianPeneUpgrade2 = new ItemGuardianUpgrade("guardianpeneupgradetwo", "GuardianPeneUpgradeTwo", 14, 40);
      guardianPeneUpgrade3 = new ItemGuardianUpgrade("guardianpeneupgradethree", "GuardianPeneUpgradeThree", 15, 60);
      guardianExplosiveUpgrade = new ItemGuardianUpgrade("guardianexplosionupgrade", "GuardianExplosionUpgrade", 16, 20);
      guardianThornsUpgrade1 = new ItemGuardianUpgrade("guardianthornsupgradeone", "GuardianThornsUpgradeOne", 17, 10);
      guardianThornsUpgrade2 = new ItemGuardianUpgrade("guardianthornsupgradetwo", "GuardianThornsUpgradeTwo", 18, 20);
      guardianThornsUpgrade3 = new ItemGuardianUpgrade("guardianthornsupgradethree", "GuardianThornsUpgradeThree", 19, 30);
      guardianWeaponUpgrade = new ItemGuardianUpgrade("guardianweaponupgrade", "GuardianWeaponUpgrade", 20, 30);
      guardianFarmUpgrade = new ItemGuardianUpgrade("guardianfarmupgrade", "GuardianFarmUpgrade", 21, 30);
      guardianAutoXPUpgrade = new ItemGuardianUpgrade("guardianautoxpupgrade", "GuardianAutoXPUpgrade", 22, 10);
      GameRegistry.registerItem(findium, "findium");
      GameRegistry.registerItem(paladium, "paladium");
      GameRegistry.registerItem(amethyst, "amethyst");
      GameRegistry.registerItem(titane, "titane");
      GameRegistry.registerItem(paladiumPickaxe, "paladiumPickaxe");
      GameRegistry.registerItem(titanePickaxe, "titanePickaxe");
      GameRegistry.registerItem(amethystPickaxe, "amethystPickaxe");
      GameRegistry.registerItem(endiumPickaxe, "endiumPickaxe");
      GameRegistry.registerItem(paladiumShovel, "paladiumShovel");
      GameRegistry.registerItem(titaneShovel, "titaneShovel");
      GameRegistry.registerItem(amethystShovel, "amethystShovel");
      GameRegistry.registerItem(paladiumAxe, "paladiumAxe");
      GameRegistry.registerItem(titaneAxe, "titaneAxe");
      GameRegistry.registerItem(amethystAxe, "amethystAxe");
      GameRegistry.registerItem(endiumAxe, "endiumAxe");
      GameRegistry.registerItem(paladiumHammer, "paladiumHammer");
      GameRegistry.registerItem(titaneHammer, "titaneHammer");
      GameRegistry.registerItem(amethystHammer, "amethystHammer");
      GameRegistry.registerItem(smithHammer, "smithHammer");
      GameRegistry.registerItem(paladiumSword, "paladiumSword");
      GameRegistry.registerItem(titaneSword, "titaneSword");
      GameRegistry.registerItem(amethystSword, "amethystSword");
      GameRegistry.registerItem(endiumSword, "endiumSword");
      GameRegistry.registerItem(infernalKnocker, "infernalKnocker");
      GameRegistry.registerItem(paladiumBroadsword, "paladiumBroadsword");
      GameRegistry.registerItem(titaneBroadsword, "titaneBroadsword");
      GameRegistry.registerItem(amethystBroadsword, "amethystBroadsword");
      GameRegistry.registerItem(paladiumFastSword, "paladiumFastSword");
      GameRegistry.registerItem(amethystFastSword, "amethystFastSword");
      GameRegistry.registerItem(titaneFastSword, "titaneFastSword");
      GameRegistry.registerItem(toolTrap, "toolTrap");
      GameRegistry.registerItem(paladiumBow, "paladiumBow");
      GameRegistry.registerItem(paladiumHelmet, "paladiumHelmet");
      GameRegistry.registerItem(paladiumChestplate, "paladiumChestplate");
      GameRegistry.registerItem(paladiumLeggings, "paladiumLeggings");
      GameRegistry.registerItem(paladiumBoots, "paladiumBoots");
      GameRegistry.registerItem(titaneHelmet, "titaneHelmet");
      GameRegistry.registerItem(titaneChestplate, "titaneChestplate");
      GameRegistry.registerItem(titaneLeggings, "titaneLeggings");
      GameRegistry.registerItem(titaneBoots, "titaneBoots");
      GameRegistry.registerItem(amethystHelmet, "amethystHelmet");
      GameRegistry.registerItem(amethystChestplate, "amethystChestplate");
      GameRegistry.registerItem(amethystLeggings, "amethystLeggings");
      GameRegistry.registerItem(amethystBoots, "amethystBoots");
      GameRegistry.registerItem(endiumHelmet, "endiumHelmet");
      GameRegistry.registerItem(endiumChestplate, "endiumChestplate");
      GameRegistry.registerItem(endiumLeggings, "endiumLeggings");
      GameRegistry.registerItem(endiumBoots, "endiumBoots");
      GameRegistry.registerItem(travelLeggings, "travelLeggings");
      GameRegistry.registerItem(travelBoots, "travelBoots");
      GameRegistry.registerItem(jumpChest, "jumpChest");
      GameRegistry.registerItem(slimyHelmet, "slimyHelmet");
      GameRegistry.registerItem(scubaHelmet, "scubaHelmet");
      GameRegistry.registerItem(hoodHelmet, "hoodHelmet");
      GameRegistry.registerItem(autoRepairModifier, "autoRepairModifier");
      GameRegistry.registerItem(extrapolatedBucket, "extrapolatedBucket");
      GameRegistry.registerItem(unclaimFinder, "unclaimFinder");
      GameRegistry.registerItem(paladiumApple, "paladiumApple");
      GameRegistry.registerItem(hangGlider, "hangGlider");
      GameRegistry.registerItem(furnaceUpgrade, "furnaceUpgrade");
      GameRegistry.registerItem(experienceBerry, "experienceBerry");
      GameRegistry.registerItem(guardianStone, "guardianStone");
      GameRegistry.registerItem(voidStone, "voidStone");
      GameRegistry.registerItem(chestExplorer, "chestExplorer");
      GameRegistry.registerItem(stuffSwitcher, "stuffSwitcher");
      GameRegistry.registerItem(dynamite, "dynamite");
      GameRegistry.registerItem(dynamiteNinja, "dynamiteNinja");
      GameRegistry.registerItem(dynamiteBig, "dynamiteBig");
      GameRegistry.registerItem(MagicalAnvilTop, "MagicalAnvilTop");
      GameRegistry.registerItem(MagicalAnvilMiddle, "MagicalAnvilMiddle");
      GameRegistry.registerItem(MagicalAnvilBottom, "MagicalAnvilBottom");
      GameRegistry.registerItem(backpack, "backpack");
      GameRegistry.registerItem(guardianWhitelist, "guardianWhitelist");
      GameRegistry.registerItem(guardianXpUpgrade, "guardianXpUpgrade");
      GameRegistry.registerItem(guardianNametagUpgrade, "guardianNametagUpgrade");
      GameRegistry.registerItem(smallRing, "smallRing");
      GameRegistry.registerItem(mediumRing, "mediumRing");
      GameRegistry.registerItem(bigRing, "bigRing");
      GameRegistry.registerItem(healOrb, "healOrb");
      GameRegistry.registerItem(speedOrb, "speedOrb");
      GameRegistry.registerItem(strenghtOrb, "strenghtOrb");
      GameRegistry.registerItem(jumpOrb, "jumpOrb");
      GameRegistry.registerItem(knockbackOrb, "knockbackOrb");
      GameRegistry.registerItem(paladiumStick, "paladiumStick");
      GameRegistry.registerItem(compressedPaladium, "compressedPaladium");
      GameRegistry.registerItem(compressedTitane, "compressedTitane");
      GameRegistry.registerItem(witherSkullFragment, "witherSkullFragment");
      GameRegistry.registerItem(paladiumCore, "paladiumCore");
      GameRegistry.registerItem(amethystStick, "amethystStick");
      GameRegistry.registerItem(titaneStick, "titaneStick");
      GameRegistry.registerItem(diamondString, "diamondString");
      GameRegistry.registerItem(wing, "wing");
      GameRegistry.registerItem(bowModifer, "bowModifer");
      GameRegistry.registerItem(healStick, "healStick");
      GameRegistry.registerItem(speedStick, "speedStick");
      GameRegistry.registerItem(strenghtStick, "strenghtStick");
      GameRegistry.registerItem(jumpStick, "jumpStick");
      GameRegistry.registerItem(godStick, "godStick");
      GameRegistry.registerItem(damageStick, "damageStick");
      GameRegistry.registerItem(hyperJumpStick, "hyperJumpStick");
      GameRegistry.registerItem(potionWither, "potionWither");
      GameRegistry.registerItem(potionFire, "potionFire");
      GameRegistry.registerItem(potionPoison, "potionPoison");
      GameRegistry.registerItem(sicknessPotion, "sicknessPotion");
      GameRegistry.registerItem(potionLauncher, "potionLauncher");
      GameRegistry.registerItem(arrowPoison, "arrowPoison");
      GameRegistry.registerItem(arrowWither, "arrowWither");
      GameRegistry.registerItem(arrowSlowness, "arrowSlowness");
      GameRegistry.registerItem(arrowSwitch, "arrowSpeed");
      GameRegistry.registerItem(bowRangeModifier, "bowRangeModifier");
      GameRegistry.registerItem(bowSpeedModifier, "bowSpeedModifier");
      GameRegistry.registerItem(knockbackModifier, "knockbackModifier");
      GameRegistry.registerItem(flameModifier, "flameModifier");
      GameRegistry.registerItem(fortuneModifier, "fortuneModifier");
      GameRegistry.registerItem(smeltModifier, "smeltModifier");
      GameRegistry.registerItem(damageModifier, "damageModifier");
      GameRegistry.registerItem(speedModifier, "speedModifier");
      GameRegistry.registerItem(bucketSulfuric, "bucketSulfuric");
      GameRegistry.registerItem(bucketAngelic, "bucketAngelic");
      GameRegistry.registerItem(paternBroadsword, "paternBroadsword");
      GameRegistry.registerItem(paternFastSword, "paternFastSword");
      GameRegistry.registerItem(paternSword, "paternSword");
      GameRegistry.registerItem(paternPickaxe, "paternPickaxe");
      GameRegistry.registerItem(paternAxe, "paternAxe");
      GameRegistry.registerItem(paternShovel, "paternShovel");
      GameRegistry.registerItem(paternHammer, "paternHammer");
      GameRegistry.registerItem(paternBlock, "paternBlock");
      GameRegistry.registerItem(paternIngot, "paternIngot");
      GameRegistry.registerItem(paternSocket, "paternsocket");
      GameRegistry.registerItem(broadSwordHead, "broadSwordHead");
      GameRegistry.registerItem(fastSwordHead, "fastSwordHead");
      GameRegistry.registerItem(swordHead, "swordHead");
      GameRegistry.registerItem(pickaxeHead, "pickaxeHead");
      GameRegistry.registerItem(hammerHead, "hammerHead");
      GameRegistry.registerItem(axeHead, "axeHead");
      GameRegistry.registerItem(shovelHead, "shovelHead");
      GameRegistry.registerItem(itemrune, "itemrune");
      GameRegistry.registerItem(itemrune2, "itemrune2");
      GameRegistry.registerItem(itemrune3, "itemrune3");
      GameRegistry.registerItem(itemrune4, "itemrune4");
      GameRegistry.registerItem(itemshardrune, "itemshardrune");
      GameRegistry.registerItem(itemshardrune2, "itemshardrune2");
      GameRegistry.registerItem(itemshardrune3, "itemshardrune3");
      GameRegistry.registerItem(itemshardrune4, "itemshardrune4");
      GameRegistry.registerItem(config_saver, "config_saver");
      GameRegistry.registerItem(explode_obsidian_upgrade, "explode_obsidian_upgrade");
      GameRegistry.registerItem(fake_obsidian_upgrade, "fake_obsidian_upgrade");
      GameRegistry.registerItem(twoLife_obsidian_upgrade, "twoLife_obsidian_upgrade");
      GameRegistry.registerItem(camouflage_obsidian_upgrade, "camouflage_obsidian_upgrade");
      GameRegistry.registerItem(itemSpawnWither, "spawnwither");
      GameRegistry.registerItem(unSummoner, "unsumoner");
      GameRegistry.registerItem(camera_tablet, "camera_tablet");
      GameRegistry.registerItem(guardianWand, "guardianWand");
      GameRegistry.registerItem(obsidianUpgrade, "obsidianUpgrade");
      GameRegistry.registerItem(moreUpgrade, "moreUpgrade");
      GameRegistry.registerItem(anchorUpgrade, "anchorUpgrade");
      GameRegistry.registerItem(regenOneUpgrade, "regenOneUpgrade");
      GameRegistry.registerItem(regenTwoUpgrade, "regenTwoUpgrade");
      GameRegistry.registerItem(regenThreeUpgrade, "regenThreeUpgrade");
      GameRegistry.registerItem(chestUpgrade, "chestUpgrade");
      GameRegistry.registerItem(guardianWeaponUpgrade, "guardianWeaponUpgrade");
      GameRegistry.registerItem(guardianAutoXPUpgrade, "guardianAutoXpUpgrade");
      GameRegistry.registerItem(guardianDamageUpgrade1, "guardianDamageUpgrade1");
      GameRegistry.registerItem(guardianDamageUpgrade2, "guardianDamageUpgrade2");
      GameRegistry.registerItem(guardianDamageUpgrade3, "guardianDamageUpgrade3");
      GameRegistry.registerItem(guardianExplosiveUpgrade, "guardianExplosiveUpgrade");
      GameRegistry.registerItem(guardianFarmUpgrade, "guardianFarmUpgrade");
      GameRegistry.registerItem(guardianLifeUpgrade1, "guardianLifeUpgrade1");
      GameRegistry.registerItem(guardianLifeUpgrade2, "guardianLifeUpgrade2");
      GameRegistry.registerItem(guardianLifeUpgrade3, "guardianLifeUpgrade3");
      GameRegistry.registerItem(guardianPeneUpgrade1, "guardianPeneUpgrade1");
      GameRegistry.registerItem(guardianPeneUpgrade2, "guardianPeneUpgrade2");
      GameRegistry.registerItem(guardianPeneUpgrade3, "guardianPeneUpgrade3");
      GameRegistry.registerItem(guardianMoreUpgrade, "guardianMoreUpgrade");
      GameRegistry.registerItem(guardianThornsUpgrade1, "guardianThornsUpgrade1");
      GameRegistry.registerItem(guardianThornsUpgrade2, "guardianThornsUpgrade2");
      GameRegistry.registerItem(guardianThornsUpgrade3, "guardianThornsUpgrade3");
   }
}
