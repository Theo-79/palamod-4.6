package fr.paladium.palamod.items.armors;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.items.ItemBackpack;
import fr.paladium.palamod.items.ItemHangGlider;
import fr.paladium.palamod.items.ItemStuffSwitcher;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.client.event.RenderLivingEvent.Specials.Pre;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;

public class EventHandlerArmor {
   public final int BP_TIME = 60;

   @SubscribeEvent
   public void onLiving(LivingUpdateEvent event) {
      if (event.entityLiving instanceof EntityPlayer) {
         EntityPlayer player = (EntityPlayer)event.entityLiving;
         if (player.getCurrentArmor(3) == null || !(player.getCurrentArmor(3).getItem() instanceof ItemArmorTravel)) {
            player.stepHeight = 0.5F;
         }

         if (player.worldObj.isRemote && ItemHangGlider.usingHangGliderClient.contains(player) && (player.getHeldItem() == null || !(player.getHeldItem().getItem() instanceof ItemHangGlider))) {
            ItemHangGlider.usingHangGliderClient.remove(player);
         }

         if (!player.worldObj.isRemote && ItemHangGlider.usingHangGliderServer.contains(player) && (player.getHeldItem() == null || !(player.getHeldItem().getItem() instanceof ItemHangGlider))) {
            ItemHangGlider.usingHangGliderServer.remove(player);
         }

         if (player.worldObj.getWorldTime() % 100L == 0L) {
            int backpacks = 0;
            int stuffswitcher = 0;
            int Hanglider = 0;

            for(int i = 0; i < player.inventory.getSizeInventory(); ++i) {
               ItemStack stack = player.inventory.getStackInSlot(i);
               EntityItem eItem;
               if (stack != null && stack.getItem() instanceof ItemBackpack) {
                  ++backpacks;
                  if (backpacks > 1) {
                     player.inventory.setInventorySlotContents(i, (ItemStack)null);
                     eItem = new EntityItem(player.worldObj, player.posX, player.posY, player.posZ, stack);
                     if (!player.worldObj.isRemote) {
                        player.worldObj.spawnEntityInWorld(eItem);
                     }

                     player.addChatComponentMessage(new ChatComponentTranslation(EnumChatFormatting.RED + "Vous ne pouvez pas avoir plus d'un sac sur vous !", new Object[0]));
                     if (player.worldObj.isRemote) {
                        player.addChatComponentMessage(new ChatComponentTranslation(EnumChatFormatting.RED + "Vous ne pouvez pas avoir plus d'un sac sur vous !", new Object[0]));
                     }
                  }
               }

               if (stack != null && stack.getItem() instanceof ItemHangGlider) {
                  ++Hanglider;
                  if (Hanglider > 1) {
                     player.inventory.setInventorySlotContents(i, (ItemStack)null);
                     eItem = new EntityItem(player.worldObj, player.posX, player.posY, player.posZ, stack);
                     if (!player.worldObj.isRemote) {
                        player.worldObj.spawnEntityInWorld(eItem);
                     }

                     player.addChatComponentMessage(new ChatComponentTranslation(EnumChatFormatting.RED + "Vous ne pouvez pas avoir plus d'un Hanglider sur vous !", new Object[0]));
                     if (player.worldObj.isRemote) {
                        player.addChatComponentMessage(new ChatComponentTranslation(EnumChatFormatting.RED + "Vous ne pouvez pas avoir plus d'un Hanglider sur vous !", new Object[0]));
                     }
                  }
               }

               if (stack != null && stack.getItem() instanceof ItemStuffSwitcher) {
                  ++stuffswitcher;
                  if (stuffswitcher > 1) {
                     player.inventory.setInventorySlotContents(i, (ItemStack)null);
                     eItem = new EntityItem(player.worldObj, player.posX, player.posY, player.posZ, stack);
                     if (!player.worldObj.isRemote) {
                        player.worldObj.spawnEntityInWorld(eItem);
                     }

                     player.addChatComponentMessage(new ChatComponentTranslation(EnumChatFormatting.RED + "Vous ne pouvez pas avoir plus d'un stuff switcher sur vous !", new Object[0]));
                     if (player.worldObj.isRemote) {
                        player.addChatComponentMessage(new ChatComponentTranslation(EnumChatFormatting.RED + "Vous ne pouvez pas avoir plus d'un stuff switcher sur vous !", new Object[0]));
                     }
                  }
               }
            }
         }
      }

   }

   @SubscribeEvent
   public void onPickUp(EntityItemPickupEvent event) {
      EntityPlayer player;
      int hanglider;
      int i;
      ItemStack stack;
      ItemStack bp;
      NBTTagCompound tag;
      int time;
      if (event.item.getEntityItem().getItem() instanceof ItemBackpack) {
         player = event.entityPlayer;
         hanglider = 0;

         for(i = 0; i < player.inventory.getSizeInventory(); ++i) {
            stack = player.inventory.getStackInSlot(i);
            if (stack != null && stack.getItem() instanceof ItemBackpack) {
               ++hanglider;
               if (hanglider >= 1) {
                  event.setCanceled(true);
               }
            }
         }

         bp = event.item.getEntityItem();
         if (!bp.hasTagCompound()) {
            tag = new NBTTagCompound();
            tag.setInteger("canPickup", 60);
            bp.setTagCompound(tag);
         }

         if (!bp.getTagCompound().hasKey("canPickup")) {
            bp.getTagCompound().setInteger("canPickup", 60);
         }

         if (!event.isCanceled() && event.entityPlayer.inventory.getFirstEmptyStack() != -1) {
            time = bp.getTagCompound().getInteger("canPickup");
            if (time > 0) {
               bp.getTagCompound().setInteger("canPickup", time - 1);
               event.setCanceled(true);
            } else {
               bp.getTagCompound().setInteger("canPickup", 60);
            }
         } else {
            bp.getTagCompound().setInteger("canPickup", 60);
         }
      }

      if (event.item.getEntityItem().getItem() instanceof ItemStuffSwitcher) {
         player = event.entityPlayer;
         hanglider = 0;

         for(i = 0; i < player.inventory.getSizeInventory(); ++i) {
            stack = player.inventory.getStackInSlot(i);
            if (stack != null && stack.getItem() instanceof ItemStuffSwitcher) {
               ++hanglider;
               if (hanglider >= 1) {
                  event.setCanceled(true);
               }
            }
         }

         bp = event.item.getEntityItem();
         if (!bp.hasTagCompound()) {
            tag = new NBTTagCompound();
            tag.setInteger("canPickup", 60);
            bp.setTagCompound(tag);
         }

         if (!bp.getTagCompound().hasKey("canPickup")) {
            bp.getTagCompound().setInteger("canPickup", 60);
         }

         if (!event.isCanceled()) {
            time = bp.getTagCompound().getInteger("canPickup");
            if (time > 0) {
               bp.getTagCompound().setInteger("canPickup", time - 1);
               event.setCanceled(true);
            } else {
               bp.getTagCompound().setInteger("canPickup", 60);
            }
         }
      }

      if (event.item.getEntityItem().getItem() instanceof ItemHangGlider) {
         player = event.entityPlayer;
         hanglider = 0;

         for(i = 0; i < player.inventory.getSizeInventory(); ++i) {
            stack = player.inventory.getStackInSlot(i);
            if (stack != null && stack.getItem() instanceof ItemHangGlider) {
               ++hanglider;
               if (hanglider >= 1) {
                  event.setCanceled(true);
               }
            }
         }

         bp = event.item.getEntityItem();
         if (!bp.hasTagCompound()) {
            tag = new NBTTagCompound();
            tag.setInteger("canPickup", 60);
            bp.setTagCompound(tag);
         }

         if (!bp.getTagCompound().hasKey("canPickup")) {
            bp.getTagCompound().setInteger("canPickup", 60);
         }

         if (!event.isCanceled()) {
            time = bp.getTagCompound().getInteger("canPickup");
            if (time > 0) {
               bp.getTagCompound().setInteger("canPickup", time - 1);
               event.setCanceled(true);
            } else {
               bp.getTagCompound().setInteger("canPickup", 60);
            }
         }
      }

   }

   @SideOnly(Side.CLIENT)
   @SubscribeEvent(
      priority = EventPriority.NORMAL
   )
   public void nameTag(Pre event) {
      EntityPlayer player = Minecraft.getMinecraft().thePlayer;
      if (event.entity instanceof EntityPlayer) {
         ItemStack helmet = ((EntityPlayer)event.entity).inventory.armorItemInSlot(3);
         if (helmet != null && helmet.getItem() instanceof ItemArmorTravel && ((ItemArmorTravel)helmet.getItem()).getType() == 5) {
            event.setCanceled(true);
         }
      }

   }
}
