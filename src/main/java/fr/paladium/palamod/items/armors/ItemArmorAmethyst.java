package fr.paladium.palamod.items.armors;

import fr.paladium.palamod.common.ArmorMaterials;
import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class ItemArmorAmethyst extends ItemArmor {
   public ItemArmorAmethyst(int type, String name, String textureName) {
      super(ArmorMaterials.armorAmethyst, 0, type);
      this.setUnlocalizedName(name);
      this.setTextureName("palamod:" + textureName);
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
      return slot == 2 ? "palamod:textures/models/AmethystArmor_2.png" : "palamod:textures/models/AmethystArmor_1.png";
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == ModItems.amethyst;
   }
}
