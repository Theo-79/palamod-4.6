package fr.paladium.palamod.items.armors;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.items.ModItems;
import fr.paladium.palamod.items.weapons.ItemBroadsword;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class ItemArmorPaladium extends RepairableArmor {
   static int[] maxRepair = new int[]{8000, 12000, 10000, 7000};

   public ItemArmorPaladium(int type, String name, String textureName) {
      super(type, maxRepair);
      this.setUnlocalizedName(name);
      this.setTextureName("palamod:" + textureName);
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
      return slot == 2 ? "palamod:textures/models/PaladiumArmor_2.png" : "palamod:textures/models/PaladiumArmor_1.png";
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == ModItems.paladium;
   }

   public void onArmorTick(World world, EntityPlayer player, ItemStack stack) {
      if (world.getTotalWorldTime() % 20L == 0L && stack.hasTagCompound() && stack.getTagCompound().hasKey("AttributeModifiers")) {
         NBTTagList list = stack.getTagCompound().getTagList("AttributeModifiers", 10);
         NBTTagList newList = new NBTTagList();

         for(int i = 0; i < list.tagCount(); ++i) {
            NBTTagCompound comp = list.getCompoundTagAt(i);
            if (comp.getDouble("Amount") > 0.3D) {
               comp.setDouble("Amount", 0.3D);
            }

            newList.appendTag(comp);
         }

         stack.getTagCompound().setTag("AttributeModifiers", newList);
      }

      switch(this.armorType) {
      case 0:
         player.addPotionEffect(new PotionEffect(Potion.nightVision.id, 220, 1));
         break;
      case 1:
         player.addPotionEffect(new PotionEffect(Potion.damageBoost.id, 50, 0));
         break;
      case 2:
         if (player.getHeldItem() != null && !(player.getHeldItem().getItem() instanceof ItemBroadsword)) {
            player.addPotionEffect(new PotionEffect(Potion.digSpeed.id, 10, 0));
         }
         break;
      case 3:
         player.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 50, 0));
      }

   }

   public int getCost() {
      switch(this.armorType) {
      case 0:
         return 5;
      case 1:
         return 8;
      case 2:
         return 7;
      case 3:
         return 4;
      default:
         return 0;
      }
   }
}
