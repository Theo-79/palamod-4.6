package fr.paladium.palamod.items.armors;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import org.lwjgl.input.Keyboard;

public class ItemArmorTravel extends ItemArmor {
   public static final int TRAVEL_BOOTS = 0;
   public static final int TRAVEL_LEGGINGS = 1;
   public static final int JUMP_CHEST = 2;
   public static final int SLIMY_HELMET = 3;
   public static final int SCUBA_HELMET = 4;
   public static final int HOOD_HELMET = 5;
   public int id;

   public ItemArmorTravel(int type, String name, String textureName, int id) {
      super(ArmorMaterial.IRON, 0, type);
      this.setUnlocalizedName(name);
      this.setTextureName("palamod:" + textureName);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setMaxStackSize(1);
      this.id = id;
   }

   public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
      switch(this.id) {
      case 2:
         return "palamod:textures/models/JumpArmor.png";
      case 3:
         return "palamod:textures/models/Slimy_Helmet.png";
      case 4:
         return "palamod:textures/models/ScubaArmor.png";
      case 5:
         return "palamod:textures/models/HoodHelmet.png";
      default:
         return "palamod:textures/models/PaladiumArmor_1.png";
      }
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == Items.iron_ingot;
   }

   public int getType() {
      return this.id;
   }

   public void onArmorTick(World world, EntityPlayer player, ItemStack stack) {
      switch(this.id) {
      case 0:
         player.stepHeight = 1.0F;
         break;
      case 1:
         player.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 10, 2));
         break;
      case 2:
         player.addPotionEffect(new PotionEffect(Potion.jump.id, 50, 2));
         break;
      case 3:
         player.fallDistance = 0.0F;
         if (world.isRemote) {
            int l = MathHelper.floor_double(player.posX);
            int i1 = MathHelper.floor_double(player.posY + 1.0D);
            int j1 = MathHelper.floor_double(player.posZ);
            if (world.getBlock(l, i1, j1).getMaterial().isSolid()) {
               if (Keyboard.isKeyDown(Minecraft.getMinecraft().gameSettings.keyBindJump.getKeyCode())) {
                  player.motionY = 0.1D;
               } else {
                  player.motionY *= 0.6D;
               }
            }
         }
         break;
      case 4:
         player.setAir(300);
      }

   }
}
