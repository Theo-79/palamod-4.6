package fr.paladium.palamod.items.armors;

import fr.paladium.palamod.common.ArmorMaterials;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class RepairableArmor extends ItemArmor {
   int[] maxRepair;

   public RepairableArmor(int type, int[] maxRepair) {
      super(ArmorMaterials.armorPaladium, 0, type);
      this.maxRepair = maxRepair;
   }

   public int getRepair(ItemStack armor) {
      if (!armor.hasTagCompound() || !armor.getTagCompound().hasKey("repaired")) {
         NBTTagCompound tag;
         if (!armor.hasTagCompound()) {
            tag = new NBTTagCompound();
         } else {
            tag = armor.getTagCompound();
         }

         tag.setInteger("repaired", ((RepairableArmor)armor.getItem()).getMaxRepair(armor));
         armor.setTagCompound(tag);
      }

      return armor.getTagCompound().getInteger("repaired");
   }

   public int getMaxRepair(ItemStack stack) {
      return this.maxRepair[this.armorType];
   }

   public void repair(ItemStack armor, ItemStack ring) {
      if (!armor.hasTagCompound() || !armor.getTagCompound().hasKey("repaired")) {
         NBTTagCompound tag;
         if (!armor.hasTagCompound()) {
            tag = new NBTTagCompound();
         } else {
            tag = armor.getTagCompound();
         }

         tag.setInteger("repaired", ((RepairableArmor)armor.getItem()).getMaxRepair(armor));
         armor.setTagCompound(tag);
      }

      if (armor.getItemDamage() > 0 && ring.getItemDamage() < ring.getMaxDamage() && this.getRepair(armor) > 0) {
         ring.setItemDamage(ring.getItemDamage() + 1);
         armor.setItemDamage(armor.getItemDamage() - 1);
         armor.getTagCompound().setInteger("repaired", armor.getTagCompound().getInteger("repaired") - 1);
      }

   }

   public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) {
      super.addInformation(stack, player, list, b);
      list.add("RÃ©paration rÃ©stantes : " + this.getRepair(stack) + "/" + this.getMaxRepair(stack));
   }

   public static float getFraction(ItemStack stack) {
      return !(stack.getItem() instanceof RepairableArmor) ? 0.0F : (float)((RepairableArmor)stack.getItem()).getRepair(stack) / (float)((RepairableArmor)stack.getItem()).getMaxRepair(stack);
   }
}
