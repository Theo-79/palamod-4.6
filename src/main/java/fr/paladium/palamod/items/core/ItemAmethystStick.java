package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemAmethystStick extends Item {
   public ItemAmethystStick() {
      this.setUnlocalizedName("amethyststick");
      this.setTextureName("palamod:AmethystStick");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
