package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemBowModifier extends Item {
   public ItemBowModifier() {
      this.setUnlocalizedName("emptybowmodifier");
      this.setTextureName("palamod:EmptyBowModifier");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
