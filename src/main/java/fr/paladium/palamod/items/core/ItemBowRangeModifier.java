package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemBowRangeModifier extends Item {
   public ItemBowRangeModifier() {
      this.setUnlocalizedName("bowrangemodifier");
      this.setTextureName("palamod:BowRangeModifier");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
