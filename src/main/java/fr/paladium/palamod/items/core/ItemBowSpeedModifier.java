package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemBowSpeedModifier extends Item {
   public ItemBowSpeedModifier() {
      this.setUnlocalizedName("bowspeedmodifier");
      this.setTextureName("palamod:BowSpeedModifier");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
