package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemCompressedPaladium extends Item {
   public ItemCompressedPaladium() {
      this.setUnlocalizedName("compressedpaladium");
      this.setTextureName("palamod:CompressedPaladium");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
