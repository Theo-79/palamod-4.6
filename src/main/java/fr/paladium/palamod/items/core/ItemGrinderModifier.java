package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemGrinderModifier extends Item {
   public ItemGrinderModifier(String name, String texture) {
      this.setUnlocalizedName(name);
      this.setTextureName("palamod:" + texture);
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
