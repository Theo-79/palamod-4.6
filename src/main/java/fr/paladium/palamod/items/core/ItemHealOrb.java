package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemHealOrb extends Item {
   public ItemHealOrb() {
      this.setUnlocalizedName("healorb");
      this.setTextureName("palamod:HealOrb");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
