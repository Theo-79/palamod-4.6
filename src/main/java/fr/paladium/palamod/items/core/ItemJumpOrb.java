package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemJumpOrb extends Item {
   public ItemJumpOrb() {
      this.setUnlocalizedName("jumporb");
      this.setTextureName("palamod:JumpOrb");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
