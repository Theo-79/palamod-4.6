package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemKnockbackOrb extends Item {
   public ItemKnockbackOrb() {
      this.setUnlocalizedName("knockbackorb");
      this.setTextureName("palamod:KnockbackOrb");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
