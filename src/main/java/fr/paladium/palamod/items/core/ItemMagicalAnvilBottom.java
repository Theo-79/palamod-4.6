package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemMagicalAnvilBottom extends Item {
   public ItemMagicalAnvilBottom() {
      this.setUnlocalizedName("magicalanvilmiddle");
      this.setTextureName("palamod:MagicalAnvilBottom");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
