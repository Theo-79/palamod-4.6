package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemMagicalAnvilMiddle extends Item {
   public ItemMagicalAnvilMiddle() {
      this.setUnlocalizedName("magicalanvilmiddle");
      this.setTextureName("palamod:MagicalAnvilMiddle");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
