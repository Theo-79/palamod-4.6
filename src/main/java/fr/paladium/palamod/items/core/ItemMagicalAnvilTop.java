package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemMagicalAnvilTop extends Item {
   public ItemMagicalAnvilTop() {
      this.setUnlocalizedName("magicalanvilmiddle");
      this.setTextureName("palamod:MagicalAnvilTop");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
