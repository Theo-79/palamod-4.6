package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemPaladiumCore extends Item {
   public ItemPaladiumCore() {
      this.setUnlocalizedName("paladiumcore");
      this.setTextureName("palamod:PaladiumCore");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
