package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemPaladiumStick extends Item {
   public ItemPaladiumStick() {
      this.setUnlocalizedName("paladiumstick");
      this.setTextureName("palamod:PaladiumStick");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
