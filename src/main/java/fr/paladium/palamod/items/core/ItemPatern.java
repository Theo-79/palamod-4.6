package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemPatern extends Item {
   public static int SOCKET = -1;
   public static int BROADSWORD = 0;
   public static int FASTSWORD = 1;
   public static int SWORD = 2;
   public static int PICKAXE = 3;
   public static int HAMMER = 4;
   public static int SHOVEL = 5;
   public static int AXE = 6;
   public static int INGOT = 7;
   public static final int BLOCK = 8;
   int type;

   public ItemPatern(String name, String texture, int type) {
      this.setUnlocalizedName(name);
      this.setTextureName("palamod:" + texture);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.type = type;
   }
}
