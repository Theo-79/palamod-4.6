package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemSpeedOrb extends Item {
   public ItemSpeedOrb() {
      this.setUnlocalizedName("speedorb");
      this.setTextureName("palamod:SpeedOrb");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
