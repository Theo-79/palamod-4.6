package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemStrenghtOrb extends Item {
   public ItemStrenghtOrb() {
      this.setUnlocalizedName("strenghtorb");
      this.setTextureName("palamod:StrenghtOrb");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
