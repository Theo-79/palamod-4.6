package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemStringDiamond extends Item {
   public ItemStringDiamond() {
      this.setUnlocalizedName("stringdiamond");
      this.setTextureName("palamod:StringDiamond");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
