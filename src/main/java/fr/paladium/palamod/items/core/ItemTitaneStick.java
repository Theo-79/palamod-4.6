package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemTitaneStick extends Item {
   public ItemTitaneStick() {
      this.setUnlocalizedName("titanestick");
      this.setTextureName("palamod:TitaneStick");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
