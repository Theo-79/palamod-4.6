package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemToolPart extends Item {
   public ItemToolPart(String texture, String name) {
      this.setUnlocalizedName(name);
      this.setTextureName("palamod:" + texture);
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean isFull3D() {
      return false;
   }
}
