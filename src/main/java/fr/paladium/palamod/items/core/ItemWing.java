package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemWing extends Item {
   public ItemWing() {
      this.setUnlocalizedName("wing");
      this.setTextureName("palamod:Wing");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
