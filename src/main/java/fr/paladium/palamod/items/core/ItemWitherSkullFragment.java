package fr.paladium.palamod.items.core;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.item.Item;

public class ItemWitherSkullFragment extends Item {
   public ItemWitherSkullFragment() {
      this.setUnlocalizedName("witherskullfragment");
      this.setTextureName("palamod:WitherSkullFragment");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
