package fr.paladium.palamod.items.ores;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.util.GuardianHelper;
import java.util.List;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemAmethyst extends Item {
   public ItemAmethyst() {
      this.setUnlocalizedName("amethyst");
      this.setTextureName("palamod:Amethyst");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) {
      super.addInformation(stack, player, list, b);
      if (GuiScreen.isShiftKeyDown()) {
         list.add("Guardian food: §c" + GuardianHelper.getXpFromItem(this) + "XP");
      }

   }
}
