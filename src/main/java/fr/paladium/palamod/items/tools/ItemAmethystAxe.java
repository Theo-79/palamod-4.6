package fr.paladium.palamod.items.tools;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;

public class ItemAmethystAxe extends ItemAxe {
   public ItemAmethystAxe() {
      super(ToolMaterialPaladium.toolTypeAmethyst);
      this.setUnlocalizedName("amethystaxe");
      this.setTextureName("palamod:AmethystAxe");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == ModItems.amethyst;
   }
}
