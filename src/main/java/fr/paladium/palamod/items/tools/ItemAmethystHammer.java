package fr.paladium.palamod.items.tools;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;

public class ItemAmethystHammer extends ItemHammer {
   public ItemAmethystHammer() {
      super(ToolMaterialPaladium.toolTypeAmethyst);
      this.setUnlocalizedName("amethysthammer");
      this.setTextureName("palamod:AmethystHammer");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
