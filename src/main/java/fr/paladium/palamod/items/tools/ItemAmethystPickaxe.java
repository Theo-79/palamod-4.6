package fr.paladium.palamod.items.tools;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;

public class ItemAmethystPickaxe extends ItemPickaxe {
   public ItemAmethystPickaxe() {
      super(ToolMaterialPaladium.toolTypeAmethyst);
      this.setUnlocalizedName("amethystpickaxe");
      this.setTextureName("palamod:AmethystPickaxe");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == ModItems.amethyst;
   }
}
