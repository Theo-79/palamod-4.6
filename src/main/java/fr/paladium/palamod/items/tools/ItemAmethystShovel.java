package fr.paladium.palamod.items.tools;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;

public class ItemAmethystShovel extends ItemSpade {
   public ItemAmethystShovel() {
      super(ToolMaterialPaladium.toolTypeAmethyst);
      this.setUnlocalizedName("amethystshovel");
      this.setTextureName("palamod:AmethystShovel");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == ModItems.amethyst;
   }
}
