package fr.paladium.palamod.items.tools;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;

public class ItemEndiumAxe extends ItemAxe {
   public ItemEndiumAxe() {
      super(ToolMaterialPaladium.toolTypePaladium);
      this.setUnlocalizedName("endiumaxe");
      this.setTextureName("palamod:EndiumAxe");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return false;
   }
}
