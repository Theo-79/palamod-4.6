package fr.paladium.palamod.items.tools;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;

public class ItemEndiumPickaxe extends ItemPickaxe {
   public ItemEndiumPickaxe() {
      super(ToolMaterialPaladium.toolTypePaladium);
      this.setUnlocalizedName("endiumpickaxe");
      this.setTextureName("palamod:EndiumPickaxe");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return false;
   }
}
