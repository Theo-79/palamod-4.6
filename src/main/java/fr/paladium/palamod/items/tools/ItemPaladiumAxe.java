package fr.paladium.palamod.items.tools;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;

public class ItemPaladiumAxe extends ItemAxe {
   public ItemPaladiumAxe() {
      super(ToolMaterialPaladium.toolTypePaladium);
      this.setUnlocalizedName("paladiumaxe");
      this.setTextureName("palamod:PaladiumAxe");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == ModItems.paladium;
   }
}
