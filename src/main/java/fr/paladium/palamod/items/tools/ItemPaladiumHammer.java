package fr.paladium.palamod.items.tools;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import fr.paladium.palamod.util.ToolHandler;
import fr.paladium.palamod.util.UpgradeHelper;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class ItemPaladiumHammer extends ItemHammer {
   public ItemPaladiumHammer() {
      super(ToolMaterialPaladium.toolTypePaladium);
      this.setUnlocalizedName("paladiumhammer");
      this.setTextureName("palamod:PaladiumHammer");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) {
      int[] modifiers = UpgradeHelper.getUpgradeAmmount(stack);
      if (modifiers != null) {
         if (modifiers.length != 0) {
            list.add("§l§nUpgrades:");
            list.add("");
         }

         for(int i = 0; i < modifiers.length; i += 2) {
            if (modifiers[i] != 7) {
               int level = UpgradeHelper.getModifier(stack, modifiers[i]);
               String display = "" + level;
               if (level <= 1) {
                  display = "";
               }

               list.add(UpgradeHelper.getUpgradeName(modifiers[i]) + " " + display);
            }
         }

      }
   }

   public void breakOtherBlock(EntityPlayer player, ItemStack stack, int x, int y, int z, int originX, int originY, int originZ, int side) {
      World world = player.worldObj;
      Material mat = world.getBlock(x, y, z).getMaterial();
      if (!world.isAirBlock(x, y, z)) {
         ForgeDirection direction = ForgeDirection.getOrientation(side);
         int fortune = UpgradeHelper.getModifier(stack, 1);
         boolean smelt = UpgradeHelper.getModifier(stack, 0) == 1;
         boolean obsidian = UpgradeHelper.getModifier(stack, 6) == 1;
         int range = Math.max(0, 1);
         int rangeY = Math.max(1, range);
         boolean doX = direction.offsetX == 0;
         boolean doY = direction.offsetY == 0;
         boolean doZ = direction.offsetZ == 0;
         ToolHandler.removeBlocksInIteration(player, stack, world, x, y, z, doX ? -range : 0, doY ? -1 : 0, doZ ? -range : 0, doX ? range + 1 : 1, doY ? rangeY * 2 : 1, doZ ? range + 1 : 1, (Block)null, MATERIALS, smelt, fortune, true, obsidian);
      }
   }

   public float getDigSpeed(ItemStack stack, Block block, int meta) {
      return super.getDigSpeed(stack, block, meta) * (float)(UpgradeHelper.getModifier(stack, 2) + 1);
   }

   public boolean isBookEnchantable(ItemStack stack, ItemStack book) {
      return false;
   }
}
