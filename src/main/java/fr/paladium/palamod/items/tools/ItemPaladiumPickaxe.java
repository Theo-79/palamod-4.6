package fr.paladium.palamod.items.tools;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;

public class ItemPaladiumPickaxe extends ItemPickaxe {
   public ItemPaladiumPickaxe() {
      super(ToolMaterialPaladium.toolTypePaladium);
      this.setUnlocalizedName("paladiumpickaxe");
      this.setTextureName("palamod:PaladiumPickaxe");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == ModItems.paladium;
   }
}
