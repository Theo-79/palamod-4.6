package fr.paladium.palamod.items.tools;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;

public class ItemPaladiumShovel extends ItemSpade {
   public ItemPaladiumShovel() {
      super(ToolMaterialPaladium.toolTypePaladium);
      this.setUnlocalizedName("paladiumshovel");
      this.setTextureName("palamod:PaladiumShovel");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == ModItems.paladium;
   }
}
