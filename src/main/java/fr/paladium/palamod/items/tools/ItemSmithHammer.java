package fr.paladium.palamod.items.tools;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.block.BlockAnvil;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.world.World;

public class ItemSmithHammer extends ItemHammer {
   public ItemSmithHammer() {
      super(ToolMaterial.WOOD);
      this.setMaxDamage(10);
      this.setUnlocalizedName("smithhammer");
      this.setTextureName("palamod:IronSmithHammer");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
      if (!(world.getBlock(x, y, z) instanceof BlockAnvil)) {
         return false;
      } else {
         int meta = world.getBlockMetadata(x, y, z);
         if (meta == 0) {
            return false;
         } else {
            world.setBlockToAir(x, y, z);
            world.setBlock(x, y, z, Blocks.anvil, meta - 4, 0);
            if (!player.capabilities.isCreativeMode) {
               stack.damageItem(1, player);
            }

            return true;
         }
      }
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == Items.iron_ingot;
   }
}
