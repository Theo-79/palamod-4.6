package fr.paladium.palamod.items.tools;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;

public class ItemTitaneAxe extends ItemAxe {
   public ItemTitaneAxe() {
      super(ToolMaterialPaladium.toolTypeTitane);
      this.setUnlocalizedName("titaneaxe");
      this.setTextureName("palamod:TitaneAxe");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == ModItems.titane;
   }
}
