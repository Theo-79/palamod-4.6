package fr.paladium.palamod.items.tools;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;

public class ItemTitaneHammer extends ItemHammer {
   public ItemTitaneHammer() {
      super(ToolMaterialPaladium.toolTypeTitane);
      this.setUnlocalizedName("titanehammer");
      this.setTextureName("palamod:TitaneHammer");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }
}
