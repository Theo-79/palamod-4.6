package fr.paladium.palamod.items.tools;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;

public class ItemTitaneShovel extends ItemSpade {
   public ItemTitaneShovel() {
      super(ToolMaterialPaladium.toolTypeTitane);
      this.setUnlocalizedName("titaneshovel");
      this.setTextureName("palamod:TitaneShovel");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == ModItems.titane;
   }
}
