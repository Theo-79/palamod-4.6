package fr.paladium.palamod.items.weapons;

import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class ItemAmethystBroadsword extends ItemBroadsword {
   public ItemAmethystBroadsword() {
      super(8.0F, 800);
      this.setUnlocalizedName("amethystbroadsword");
      this.setTextureName("palamod:AmethystBroadsword");
   }

   public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) {
      list.add("§cDamages: " + super.getDamages(stack));
   }
}
