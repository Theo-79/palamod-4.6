package fr.paladium.palamod.items.weapons;

import fr.paladium.palamod.common.TabPaladium;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class ItemAmethystFastsword extends ItemFastsword {
   public ItemAmethystFastsword() {
      super(6.0F, 400);
      this.setUnlocalizedName("amethystfastsword");
      this.setTextureName("palamod:AmethystFastsword");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) {
      list.add("§cDamages: " + super.getDamages(stack));
   }
}
