package fr.paladium.palamod.items.weapons;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;

public class ItemAmethystSword extends ItemSword {
   public ItemAmethystSword() {
      super(ToolMaterialPaladium.toolTypeAmethyst);
      this.setUnlocalizedName("amethystsword");
      this.setTextureName("palamod:AmethystSword");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == ModItems.amethyst;
   }
}
