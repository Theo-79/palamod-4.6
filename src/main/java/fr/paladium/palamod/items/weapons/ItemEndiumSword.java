package fr.paladium.palamod.items.weapons;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;

public class ItemEndiumSword extends ItemSword {
   public ItemEndiumSword() {
      super(ToolMaterialPaladium.toolTypePaladium);
      this.setUnlocalizedName("endiumsword");
      this.setTextureName("palamod:EndiumSword");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return false;
   }
}
