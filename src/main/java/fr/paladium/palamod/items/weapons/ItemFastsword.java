package fr.paladium.palamod.items.weapons;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.util.EntityHelper;
import fr.paladium.palamod.util.UpgradeHelper;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

public class ItemFastsword extends Item {
   float damageBase;

   public ItemFastsword(float damages, int max) {
      this.setMaxStackSize(1);
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setMaxDamage(max);
      this.damageBase = damages;
   }

   public boolean hitEntity(ItemStack stack, EntityLivingBase base, EntityLivingBase base2) {
      base.attackEntityFrom(DamageSource.causePlayerDamage((EntityPlayer)base2), this.damageBase + (float)(UpgradeHelper.getModifier(stack, 3) * 3));
      int flame = UpgradeHelper.getModifier(stack, 4);
      int knockback = UpgradeHelper.getModifier(stack, 5);
      if (flame == 1) {
         base.setFire(200);
      }

      if (knockback > 0) {
         EntityHelper.knockbackEntity(base, (double)knockback);
      }

      stack.damageItem(1, base2);
      return true;
   }

   public float getDamages(ItemStack stack) {
      return this.damageBase + (float)(UpgradeHelper.getModifier(stack, 3) * 3);
   }

   public boolean onBlockDestroyed(ItemStack p_150894_1_, World p_150894_2_, Block p_150894_3_, int p_150894_4_, int p_150894_5_, int p_150894_6_, EntityLivingBase p_150894_7_) {
      if ((double)p_150894_3_.getBlockHardness(p_150894_2_, p_150894_4_, p_150894_5_, p_150894_6_) != 0.0D) {
         p_150894_1_.damageItem(2, p_150894_7_);
      }

      return true;
   }

   public boolean isBookEnchantable(ItemStack stack, ItemStack book) {
      return false;
   }

   @SideOnly(Side.CLIENT)
   public boolean isFull3D() {
      return true;
   }

   public EnumAction getItemUseAction(ItemStack p_77661_1_) {
      return EnumAction.block;
   }

   public int getMaxItemUseDuration(ItemStack p_77626_1_) {
      return 72000;
   }

   public ItemStack onItemRightClick(ItemStack p_77659_1_, World p_77659_2_, EntityPlayer p_77659_3_) {
      p_77659_3_.setItemInUse(p_77659_1_, this.getMaxItemUseDuration(p_77659_1_));
      return p_77659_1_;
   }

   public boolean func_150897_b(Block p_150897_1_) {
      return p_150897_1_ == Blocks.web;
   }

   public void onUpdate(ItemStack stack, World world, Entity entity, int p_77663_4_, boolean p_77663_5_) {
      if (entity instanceof EntityPlayer) {
         EntityPlayer player = (EntityPlayer)entity;
         if (player.getHeldItem() != null && player.getHeldItem().getItem() instanceof ItemFastsword) {
            ((EntityPlayer)entity).addPotionEffect(new PotionEffect(Potion.digSpeed.id, 1, 3));
         }
      }

      super.onUpdate(stack, world, entity, p_77663_4_, p_77663_5_);
   }
}
