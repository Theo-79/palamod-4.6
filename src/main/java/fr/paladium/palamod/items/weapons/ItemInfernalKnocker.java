package fr.paladium.palamod.items.weapons;

import fr.paladium.palamod.common.TabPaladium;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.world.World;

public class ItemInfernalKnocker extends ItemSword {
   public ItemInfernalKnocker() {
      super(ToolMaterial.WOOD);
      this.setUnlocalizedName("infernalknocker");
      this.setTextureName("palamod:InfernalKnocker");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public void onCreated(ItemStack stack, World world, EntityPlayer player) {
      stack.addEnchantment(Enchantment.knockback, 5);
      super.onCreated(stack, world, player);
   }

   public void onUpdate(ItemStack stack, World world, Entity player, int p_77663_4_, boolean p_77663_5_) {
      if (!stack.isItemEnchanted()) {
         stack.addEnchantment(Enchantment.knockback, 5);
      }

   }

   public boolean hasEffect(ItemStack stack) {
      return false;
   }

   public boolean isBookEnchantable(ItemStack stack, ItemStack book) {
      return false;
   }
}
