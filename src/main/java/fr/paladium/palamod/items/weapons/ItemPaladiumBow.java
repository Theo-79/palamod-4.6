package fr.paladium.palamod.items.weapons;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.entities.projectiles.EntityCustomArrow;
import fr.paladium.palamod.items.ItemArrowBase;
import fr.paladium.palamod.util.BowHelper;
import java.util.List;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Items;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.ArrowLooseEvent;
import net.minecraftforge.event.entity.player.ArrowNockEvent;

public class ItemPaladiumBow extends ItemBow {
   public static final String[] bowPullIconNames = new String[]{"PaladiumBow_0", "PaladiumBow_1", "PaladiumBow_2"};
   private IIcon[] iconArray;
   public IIcon iconSpeed;
   public IIcon iconRange;

   public ItemPaladiumBow() {
      this.setMaxDamage(900);
      this.setMaxStackSize(1);
      this.setUnlocalizedName("paladiumbow");
      this.setTextureName("palamod:PaladiumBow");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public void onPlayerStoppedUsing(ItemStack stack, World world, EntityPlayer player, int time) {
      int maxItemUse = this.getMaxItemUseDuration(stack) - time;
      ArrowLooseEvent event = new ArrowLooseEvent(player, stack, maxItemUse);
      if (!event.isCanceled()) {
         maxItemUse = event.charge;
         MinecraftForge.EVENT_BUS.post(event);
         ItemStack ammo = this.checkForAmmo(player);
         boolean infiniteAmmo;
         int punchLevel;
         if (ammo.getItem() instanceof ItemArrowBase) {
            infiniteAmmo = !player.capabilities.isCreativeMode && EnchantmentHelper.getEnchantmentLevel(Enchantment.infinity.effectId, stack) <= 0;
            if (ammo == null && !infiniteAmmo) {
               return;
            }

            int type;
            if (ammo != null) {
               type = ((ItemArrowBase)ammo.getItem()).getEffect();
            } else {
               type = 0;
            }

            float scaledItemUse = (float)maxItemUse / 20.0F;
            if (!BowHelper.canApply(stack, 1)) {
               scaledItemUse *= 2.0F;
            }

            scaledItemUse = (scaledItemUse * scaledItemUse + scaledItemUse * 2.0F) / 3.0F;
            if ((double)scaledItemUse < 0.1D) {
               return;
            }

            if (scaledItemUse > 1.0F) {
               scaledItemUse = 1.0F;
            }

            int[] modifiers = BowHelper.getModifiers(stack);
            float range = 1.5F;
            if (BowHelper.canApply(stack, 0)) {
               range = 2.5F;
            }

            EntityCustomArrow entityarrow = new EntityCustomArrow(world, player, scaledItemUse * range / 2.0F, type, infiniteAmmo);
            if (scaledItemUse == 1.0F) {
               entityarrow.setIsCritical(true);
            }

            punchLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.power.effectId, stack);
            if (punchLevel > 0) {
               entityarrow.setDamage(entityarrow.getDamage() + (double)punchLevel * 0.5D + 0.5D);
            }

            int punchLevel1 = EnchantmentHelper.getEnchantmentLevel(Enchantment.punch.effectId, stack);
            if (punchLevel1 > 0) {
               entityarrow.setKnockbackStrength(punchLevel1);
            }

            if (EnchantmentHelper.getEnchantmentLevel(Enchantment.flame.effectId, stack) > 0) {
               entityarrow.setFire(100);
            }

            stack.damageItem(1, player);
            world.playSoundAtEntity(player, "random.bow", 1.0F, 1.0F / (itemRand.nextFloat() * 0.4F + 1.2F) + scaledItemUse * 0.5F);
            if (infiniteAmmo) {
               player.inventory.consumeInventoryItem(ammo.getItem());
            }

            if (!world.isRemote) {
               world.spawnEntityInWorld(entityarrow);
            }
         }

         if (ammo.getItem() == Items.arrow) {
            infiniteAmmo = !player.capabilities.isCreativeMode && EnchantmentHelper.getEnchantmentLevel(Enchantment.infinity.effectId, stack) <= 0;
            if (ammo == null && !infiniteAmmo) {
               return;
            }

            float scaledItemUse = (float)maxItemUse / 20.0F;
            if (!BowHelper.canApply(stack, 1)) {
               scaledItemUse *= 2.0F;
            }

            scaledItemUse = (scaledItemUse * scaledItemUse + scaledItemUse * 2.0F) / 3.0F;
            if ((double)scaledItemUse < 0.1D) {
               return;
            }

            if (scaledItemUse > 1.0F) {
               scaledItemUse = 1.0F;
            }

            int[] modifiers = BowHelper.getModifiers(stack);
            float range = 3.0F;
            if (BowHelper.canApply(stack, 0)) {
               range = 4.0F;
            }

            EntityArrow entityarrow = new EntityArrow(world, player, scaledItemUse * range / 2.0F);
            if (scaledItemUse == 1.0F) {
               entityarrow.setIsCritical(true);
            }

            int powerLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.power.effectId, stack);
            if (powerLevel > 0) {
               entityarrow.setDamage(entityarrow.getDamage() + (double)powerLevel * 0.5D + 0.5D);
            }

            punchLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.punch.effectId, stack);
            if (punchLevel > 0) {
               entityarrow.setKnockbackStrength(punchLevel);
            }

            if (EnchantmentHelper.getEnchantmentLevel(Enchantment.flame.effectId, stack) > 0) {
               entityarrow.setFire(100);
            }

            stack.damageItem(1, player);
            world.playSoundAtEntity(player, "random.bow", 1.0F, 1.0F / (itemRand.nextFloat() * 0.4F + 1.2F) + scaledItemUse * 0.5F);
            if (infiniteAmmo) {
               player.inventory.consumeInventoryItem(ammo.getItem());
            }

            if (!world.isRemote) {
               world.spawnEntityInWorld(entityarrow);
            }
         }

      }
   }

   private ItemStack checkForAmmo(EntityPlayer player) {
      InventoryPlayer inventory = player.inventory;

      for(int i = 0; i < inventory.getSizeInventory(); ++i) {
         ItemStack result = inventory.getStackInSlot(i);
         if (result != null && (result.getItem() instanceof ItemArrowBase || result.getItem() == Items.arrow)) {
            return result;
         }
      }

      return null;
   }

   public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean par4) {
      int[] modifiers = BowHelper.getModifiers(stack);
      if (modifiers != null) {
         if (modifiers.length != 0) {
            list.add("\\u00an Modifiers:");
            list.add("");
         }

         for(int i = 0; i < modifiers.length; ++i) {
            list.add(BowHelper.getModifierName(modifiers[i]));
         }

      }
   }

   public ItemStack onEaten(ItemStack stack, World world, EntityPlayer player) {
      return stack;
   }

   public int getMaxItemUseDuration(ItemStack stack) {
      int time = 72000;
      if (!BowHelper.canApply(stack, 1)) {
         time = 31000;
      }

      return time;
   }

   public EnumAction getItemUseAction(ItemStack p_77661_1_) {
      return EnumAction.bow;
   }

   public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
      ArrowNockEvent event = new ArrowNockEvent(player, stack);
      MinecraftForge.EVENT_BUS.post(event);
      if (event.isCanceled()) {
         return event.result;
      } else {
         if (player.capabilities.isCreativeMode || this.checkForAmmo(player) != null) {
            player.setItemInUse(stack, this.getMaxItemUseDuration(stack));
         }

         return stack;
      }
   }

   public int getRenderPasses(int metadata) {
      return 3;
   }

   public boolean requiresMultipleRenderPasses() {
      return true;
   }

   public int getItemEnchantability() {
      return 1;
   }

   public boolean isFull3D() {
      return true;
   }

   @SideOnly(Side.CLIENT)
   public void registerIcons(IIconRegister icons) {
      this.itemIcon = icons.registerIcon(this.getIconString());
      this.iconArray = new IIcon[bowPullIconNames.length];
      this.iconSpeed = icons.registerIcon("palamod:upgrade_speed");
      this.iconRange = icons.registerIcon("palamod:upgrade_range");

      for(int i = 0; i < this.iconArray.length; ++i) {
         this.iconArray[i] = icons.registerIcon(this.getIconString() + "_" + i);
      }

   }

   @SideOnly(Side.CLIENT)
   public IIcon getItemIconForUseDuration(int par1) {
      return this.iconArray[par1];
   }

   public IIcon getIcon(ItemStack stack, int renderPass, EntityPlayer player, ItemStack usingItem, int useRemaining) {
      if (renderPass == 0) {
         if (player.getItemInUse() == null) {
            return this.itemIcon;
         } else {
            int pulling1 = 18;
            int pulling2 = 13;
            if (!BowHelper.canApply(stack, 1)) {
               pulling1 = 8;
               pulling2 = 7;
            }

            int pulling = stack.getMaxItemUseDuration() - useRemaining;
            if (pulling >= pulling1) {
               return this.iconArray[2];
            } else {
               return pulling > pulling2 ? this.iconArray[1] : this.iconArray[0];
            }
         }
      } else if (renderPass == 1) {
         return !BowHelper.canApply(stack, 1) ? this.iconSpeed : null;
      } else if (renderPass == 2) {
         return !BowHelper.canApply(stack, 0) ? this.iconRange : null;
      } else {
         return this.iconSpeed;
      }
   }
}
