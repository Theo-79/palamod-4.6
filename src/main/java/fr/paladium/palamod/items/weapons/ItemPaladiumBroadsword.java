package fr.paladium.palamod.items.weapons;

import com.google.common.collect.Multimap;
import fr.paladium.palamod.util.UpgradeHelper;
import java.util.List;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class ItemPaladiumBroadsword extends ItemBroadsword {
   public ItemPaladiumBroadsword() {
      super(12.0F, 3000);
      this.setUnlocalizedName("paladiumbroadsword");
      this.setTextureName("palamod:PaladiumBroadsword");
   }

   public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) {
      int[] modifiers = UpgradeHelper.getUpgradeAmmount(stack);
      if (modifiers != null) {
         if (modifiers.length != 0) {
            list.add("§l§nUpgrades:");
            list.add("");
         }

         for(int i = 0; i < modifiers.length; i += 2) {
            int level = UpgradeHelper.getModifier(stack, modifiers[i]);
            String display = "" + level;
            if (level <= 1) {
               display = "";
            }

            list.add(UpgradeHelper.getUpgradeName(modifiers[i]) + " " + display);
         }

         list.add("");
         list.add("§cDamages: " + super.getDamages(stack));
      }
   }

   public Multimap getItemAttributeModifiers() {
      Multimap multimap = super.getItemAttributeModifiers();
      multimap.put(SharedMonsterAttributes.attackDamage.getAttributeUnlocalizedName(), new AttributeModifier(field_111210_e, "Weapon modifier", (double)this.damageBase, 0));
      return multimap;
   }
}
