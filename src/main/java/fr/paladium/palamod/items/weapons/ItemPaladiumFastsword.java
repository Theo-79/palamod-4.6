package fr.paladium.palamod.items.weapons;

import fr.paladium.palamod.util.UpgradeHelper;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class ItemPaladiumFastsword extends ItemFastsword {
   public ItemPaladiumFastsword() {
      super(7.0F, 1500);
      this.setUnlocalizedName("paladiumfastsword");
      this.setTextureName("palamod:PaladiumFastsword");
   }

   public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) {
      int[] modifiers = UpgradeHelper.getUpgradeAmmount(stack);
      if (modifiers != null) {
         if (modifiers.length != 0) {
            list.add("§l§nUpgrades:");
            list.add("");
         }

         for(int i = 0; i < modifiers.length; i += 2) {
            int level = UpgradeHelper.getModifier(stack, modifiers[i]);
            String display = "" + level;
            if (level <= 1) {
               display = "";
            }

            list.add(UpgradeHelper.getUpgradeName(modifiers[i]) + " " + display);
         }

         list.add("§cDamages: " + super.getDamages(stack));
      }
   }
}
