package fr.paladium.palamod.items.weapons;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;

public class ItemPaladiumSword extends ItemSword {
   public ItemPaladiumSword() {
      super(ToolMaterialPaladium.toolTypePaladium);
      this.setUnlocalizedName("paladiumsword");
      this.setTextureName("palamod:PaladiumSword");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == ModItems.paladium;
   }

   public boolean isItemTool() {
      return true;
   }
}
