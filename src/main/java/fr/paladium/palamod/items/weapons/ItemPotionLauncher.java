package fr.paladium.palamod.items.weapons;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.entities.projectiles.EntityPotionGun;
import fr.paladium.palamod.entities.projectiles.EntitySplashPotion;
import fr.paladium.palamod.items.ItemSplashPotion;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemPotionLauncher extends Item {
   public ItemPotionLauncher() {
      this.setMaxStackSize(1);
      this.setUnlocalizedName("potionlauncher");
      this.setCreativeTab(TabPaladium.INSTANCE);
      this.setTextureName("palamod:PotionLauncher");
      this.setMaxDamage(256);
   }

   public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
      if (this.checkForPotion(player.inventory) == -1) {
         return stack;
      } else {
         int potionSlot = this.checkForPotion(player.inventory);
         ItemStack potionStack = player.inventory.getStackInSlot(potionSlot);
         if (potionStack != null) {
            if (!(potionStack.getItem() instanceof ItemSplashPotion)) {
               ItemPotion var10000 = (ItemPotion)potionStack.getItem();
               if (!ItemPotion.isSplash(potionStack.getItemDamage())) {
                  return stack;
               }
            }

            world.playSoundAtEntity(player, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));
            --potionStack.stackSize;
            if (!world.isRemote) {
               if (potionStack.getItem() instanceof ItemPotion) {
                  world.spawnEntityInWorld(new EntityPotionGun(world, player, potionStack));
               } else {
                  world.spawnEntityInWorld(new EntitySplashPotion(world, player, potionStack.getItemDamage(), true));
               }
            }

            if (potionStack.stackSize <= 0) {
               potionStack = null;
            }

            player.inventory.setInventorySlotContents(potionSlot, potionStack);
         }

         return stack;
      }
   }

   public boolean isFull3D() {
      return true;
   }

   private int checkForPotion(InventoryPlayer inventory) {
      boolean i = false;

      for(int i1 = 0; i1 < inventory.getSizeInventory(); ++i1) {
         if (inventory.getStackInSlot(i1) != null && (inventory.getStackInSlot(i1).getItem() instanceof ItemPotion || inventory.getStackInSlot(i1).getItem() instanceof ItemSplashPotion)) {
            return i1;
         }
      }

      return -1;
   }
}
