package fr.paladium.palamod.items.weapons;

import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class ItemTitaneBroadsword extends ItemBroadsword {
   public ItemTitaneBroadsword() {
      super(10.0F, 2000);
      this.setUnlocalizedName("titanebroadsword");
      this.setTextureName("palamod:TitaneBroadsword");
   }

   public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) {
      list.add("§cDamages: " + super.getDamages(stack));
   }
}
