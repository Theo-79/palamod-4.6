package fr.paladium.palamod.items.weapons;

import fr.paladium.palamod.common.TabPaladium;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class ItemTitaneFastsword extends ItemFastsword {
   public ItemTitaneFastsword() {
      super(5.0F, 1000);
      this.setUnlocalizedName("titanefastsword");
      this.setTextureName("palamod:TitaneFastsword");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) {
      list.add("§cDamages: " + super.getDamages(stack));
   }
}
