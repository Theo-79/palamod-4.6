package fr.paladium.palamod.items.weapons;

import fr.paladium.palamod.common.TabPaladium;
import fr.paladium.palamod.common.ToolMaterialPaladium;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;

public class ItemTitaneSword extends ItemSword {
   public ItemTitaneSword() {
      super(ToolMaterialPaladium.toolTypeTitane);
      this.setUnlocalizedName("titanesword");
      this.setTextureName("palamod:TitaneSword");
      this.setCreativeTab(TabPaladium.INSTANCE);
   }

   public boolean getIsRepairable(ItemStack input, ItemStack repair) {
      return repair.getItem() == ModItems.titane;
   }
}
