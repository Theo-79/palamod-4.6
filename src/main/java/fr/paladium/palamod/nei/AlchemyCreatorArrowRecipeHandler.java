package fr.paladium.palamod.nei;

import codechicken.lib.gui.GuiDraw;
import codechicken.nei.NEIServerUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.TemplateRecipeHandler;
import codechicken.nei.recipe.TemplateRecipeHandler.CachedRecipe;
import codechicken.nei.recipe.TemplateRecipeHandler.RecipeTransferRect;
import fr.paladium.palamod.client.gui.GuiAlchemyCreatorArrow;
import fr.paladium.palamod.recipies.AlchemyCreatorArrowRecipies;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class AlchemyCreatorArrowRecipeHandler extends TemplateRecipeHandler {
   public static final String IDENTIFIER = "palamod.alchemy_creator_arrow";
   private static final String TEXTURE;
   private static final String TEXTURE2;

   public String getRecipeName() {
      return "AlchemyCreator Arrow";
   }

   public String getGuiTexture() {
      return TEXTURE;
   }

   public Class<? extends GuiContainer> getGuiClass() {
      return GuiAlchemyCreatorArrow.class;
   }

   public String getOverlayIdentifier() {
      return "palamod.alchemy_creator_arrow";
   }

   public void loadTransferRects() {
      this.transferRects.add(new RecipeTransferRect(new Rectangle(143, 15, 18, 18), "palamod.alchemy_creator_potion", new Object[0]));
      this.transferRects.add(new RecipeTransferRect(new Rectangle(143, 35, 18, 18), "palamod.alchemy_creator_arrow", new Object[0]));
   }

   public void drawExtras(int recipe) {
      GuiDraw.changeTexture(TEXTURE2);
      this.drawProgressBar(92, 36, 0, 89, 10, 30, 48, 1);
      this.drawProgressBar(58, 35, 10, 89, 14, 29, 100, 1);
   }

   public int recipiesPerPage() {
      return 2;
   }

   public void drawBackground(int recipe) {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GuiDraw.changeTexture(this.getGuiTexture());
      GuiDraw.drawTexturedModalRect(0, 0, 5, 14, 166, 64);
      GuiDraw.changeTexture(TEXTURE2);
      GuiDraw.drawTexturedModalRect(143, 15, 0, 56, 18, 18);
      GuiDraw.drawTexturedModalRect(143, 35, 0, 74, 18, 16);
   }

   public void loadCraftingRecipes(String outputId, Object... results) {
      if (outputId.equals("palamod.alchemy_creator_arrow") && this.getClass() == AlchemyCreatorArrowRecipeHandler.class) {
         Map<ItemStack[], ItemStack> recipes = AlchemyCreatorArrowRecipies.getManager().getSmeltingList();
         Iterator var4 = recipes.entrySet().iterator();

         while(var4.hasNext()) {
            Entry<ItemStack[], ItemStack> recipe = (Entry)var4.next();
            this.arecipes.add(new AlchemyCreatorArrowRecipeHandler.SmeltingPair((ItemStack[])recipe.getKey(), (ItemStack)recipe.getValue()));
         }
      } else {
         super.loadCraftingRecipes(outputId, results);
      }

   }

   public void loadCraftingRecipes(ItemStack result) {
      Map<ItemStack[], ItemStack> recipes = AlchemyCreatorArrowRecipies.getManager().getSmeltingList();
      Iterator var3 = recipes.entrySet().iterator();

      while(var3.hasNext()) {
         Entry<ItemStack[], ItemStack> recipe = (Entry)var3.next();
         if (NEIServerUtils.areStacksSameType((ItemStack)recipe.getValue(), result)) {
            this.arecipes.add(new AlchemyCreatorArrowRecipeHandler.SmeltingPair((ItemStack[])recipe.getKey(), (ItemStack)recipe.getValue()));
         }
      }

   }

   public void loadUsageRecipes(String inputId, Object... ingredients) {
      super.loadUsageRecipes(inputId, ingredients);
   }

   public void loadUsageRecipes(ItemStack ingredient) {
      Map<ItemStack[], ItemStack> recipes = AlchemyCreatorArrowRecipies.getManager().getSmeltingList();
      Iterator var3 = recipes.entrySet().iterator();

      while(var3.hasNext()) {
         Entry<ItemStack[], ItemStack> recipe = (Entry)var3.next();
         ItemStack[] var5 = (ItemStack[])recipe.getKey();
         int var6 = var5.length;

         for(int var7 = 0; var7 < var6; ++var7) {
            ItemStack stack = var5[var7];
            if (NEIServerUtils.areStacksSameTypeCrafting(stack, ingredient)) {
               AlchemyCreatorArrowRecipeHandler.SmeltingPair arecipe = new AlchemyCreatorArrowRecipeHandler.SmeltingPair((ItemStack[])recipe.getKey(), (ItemStack)recipe.getValue());
               arecipe.setIngredientPermutation(arecipe.ingreds, ingredient);
               this.arecipes.add(arecipe);
            }
         }
      }

   }

   static {
      TEXTURE = GuiAlchemyCreatorArrow.background.toString();
      TEXTURE2 = GuiAlchemyCreatorArrow.widgets.toString();
   }

   public class SmeltingPair extends CachedRecipe {
      List<PositionedStack> ingreds = new ArrayList();
      PositionedStack result;

      public SmeltingPair(ItemStack[] ingreds, ItemStack result) {
         super();
         this.ingreds.add(new PositionedStack(ingreds[0], 50, 2));
         this.ingreds.add(new PositionedStack(ingreds[1], 74, 2));
         this.ingreds.add(new PositionedStack(ingreds[2], 98, 2));
         this.ingreds.add(new PositionedStack(ingreds[3], 74, 24));
         this.result = new PositionedStack(result, 74, 46);
      }

      public List<PositionedStack> getIngredients() {
         return this.getCycledIngredients(AlchemyCreatorArrowRecipeHandler.this.cycleticks / 48, this.ingreds);
      }

      public PositionedStack getResult() {
         return this.result;
      }
   }
}
