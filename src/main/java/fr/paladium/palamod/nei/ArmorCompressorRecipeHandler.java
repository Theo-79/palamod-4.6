package fr.paladium.palamod.nei;

import codechicken.lib.gui.GuiDraw;
import codechicken.nei.NEIServerUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.TemplateRecipeHandler;
import codechicken.nei.recipe.TemplateRecipeHandler.CachedRecipe;
import codechicken.nei.recipe.TemplateRecipeHandler.RecipeTransferRect;
import fr.paladium.palamod.client.gui.GuiArmorCompressor;
import fr.paladium.palamod.items.ModItems;
import fr.paladium.palamod.recipies.ArmorCompressorRecipe;
import java.awt.Rectangle;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class ArmorCompressorRecipeHandler extends TemplateRecipeHandler {
   public static final String IDENTIFIER = "palamod.armor_compressor";
   private static final String TEXTURE;

   public String getRecipeName() {
      return "Armor Compressor";
   }

   public String getGuiTexture() {
      return TEXTURE;
   }

   public Class<? extends GuiContainer> getGuiClass() {
      return GuiArmorCompressor.class;
   }

   public String getOverlayIdentifier() {
      return "palamod.armor_compressor";
   }

   public void loadTransferRects() {
      this.transferRects.add(new RecipeTransferRect(new Rectangle(47, 29, 23, 17), "palamod.armor_compressor", new Object[0]));
      this.transferRects.add(new RecipeTransferRect(new Rectangle(92, 29, 23, 17), "palamod.armor_compressor", new Object[0]));
   }

   public void drawExtras(int recipe) {
      this.drawProgressBar(46, 28, 180, 144, 28, 19, 48, 0);
      this.drawProgressBar(91, 28, 211, 144, 28, 19, 48, 0);
   }

   public int recipiesPerPage() {
      return 1;
   }

   public void drawBackground(int recipe) {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GuiDraw.changeTexture(this.getGuiTexture());
      GuiDraw.drawTexturedModalRect(3, 0, 8, 2, 160, 140);
   }

   public void loadCraftingRecipes(String outputId, Object... results) {
      if (outputId.equals("palamod.armor_compressor") && this.getClass() == ArmorCompressorRecipeHandler.class) {
         Map<ItemStack, ItemStack> recipes = ArmorCompressorRecipe.getManager().getSmeltingList();
         Iterator var4 = recipes.entrySet().iterator();

         while(var4.hasNext()) {
            Entry<ItemStack, ItemStack> recipe = (Entry)var4.next();
            this.arecipes.add(new ArmorCompressorRecipeHandler.SmeltingPair((ItemStack)recipe.getKey(), (ItemStack)recipe.getValue()));
         }
      } else {
         super.loadCraftingRecipes(outputId, results);
      }

   }

   public void loadCraftingRecipes(ItemStack result) {
      Map<ItemStack, ItemStack> recipes = ArmorCompressorRecipe.getManager().getSmeltingList();
      Iterator var3 = recipes.entrySet().iterator();

      while(var3.hasNext()) {
         Entry<ItemStack, ItemStack> recipe = (Entry)var3.next();
         if (NEIServerUtils.areStacksSameType((ItemStack)recipe.getValue(), result)) {
            this.arecipes.add(new ArmorCompressorRecipeHandler.SmeltingPair((ItemStack)recipe.getKey(), (ItemStack)recipe.getValue()));
         }
      }

   }

   public void loadUsageRecipes(String inputId, Object... ingredients) {
      super.loadUsageRecipes(inputId, ingredients);
   }

   public void loadUsageRecipes(ItemStack ingredient) {
      Map<ItemStack, ItemStack> recipes = ArmorCompressorRecipe.getManager().getSmeltingList();
      Iterator var3 = recipes.entrySet().iterator();

      while(var3.hasNext()) {
         Entry<ItemStack, ItemStack> recipe = (Entry)var3.next();
         if (NEIServerUtils.areStacksSameTypeCrafting((ItemStack)recipe.getKey(), ingredient)) {
            ArmorCompressorRecipeHandler.SmeltingPair arecipe = new ArmorCompressorRecipeHandler.SmeltingPair((ItemStack)recipe.getKey(), (ItemStack)recipe.getValue());
            arecipe.setIngredientPermutation(Arrays.asList(arecipe.ingred), ingredient);
            this.arecipes.add(arecipe);
         }
      }

   }

   static {
      TEXTURE = GuiArmorCompressor.background.toString();
   }

   public class SmeltingPair extends CachedRecipe {
      PositionedStack ingred;
      PositionedStack result;

      public SmeltingPair(ItemStack ingred, ItemStack result) {
         super();
         if (ingred.getItem() == ModItems.itemshardrune) {
            ingred.stackSize = 4;
         }

         this.ingred = new PositionedStack(ingred, 74, 20);
         this.result = new PositionedStack(result, 74, 91);
      }

      public List<PositionedStack> getIngredients() {
         return this.getCycledIngredients(ArmorCompressorRecipeHandler.this.cycleticks / 48, Arrays.asList(this.ingred));
      }

      public PositionedStack getResult() {
         return this.result;
      }
   }
}
