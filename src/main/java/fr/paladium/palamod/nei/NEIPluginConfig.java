package fr.paladium.palamod.nei;

import codechicken.nei.api.API;
import codechicken.nei.api.IConfigureNEI;

public class NEIPluginConfig implements IConfigureNEI {
   public void loadConfig() {
      API.registerRecipeHandler(new PaladiumMachineRecipeHandler());
      API.registerUsageHandler(new PaladiumMachineRecipeHandler());
      API.registerRecipeHandler(new PaladiumGrinderRecipeHandler());
      API.registerUsageHandler(new PaladiumGrinderRecipeHandler());
      API.registerRecipeHandler(new ArmorCompressorRecipeHandler());
      API.registerUsageHandler(new ArmorCompressorRecipeHandler());
      API.registerRecipeHandler(new AlchemyCreatorArrowRecipeHandler());
      API.registerUsageHandler(new AlchemyCreatorArrowRecipeHandler());
      API.registerRecipeHandler(new AlchemyCreatorPotionRecipeHandler());
      API.registerUsageHandler(new AlchemyCreatorPotionRecipeHandler());
   }

   public String getName() {
      return "PalaModNEIPlugin";
   }

   public String getVersion() {
      return "1.0";
   }
}
