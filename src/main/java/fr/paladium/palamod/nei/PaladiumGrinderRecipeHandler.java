package fr.paladium.palamod.nei;

import codechicken.lib.gui.GuiDraw;
import codechicken.nei.NEIServerUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.TemplateRecipeHandler;
import codechicken.nei.recipe.TemplateRecipeHandler.CachedRecipe;
import codechicken.nei.recipe.TemplateRecipeHandler.RecipeTransferRect;
import fr.paladium.palamod.client.gui.GuiPaladiumGrinder;
import fr.paladium.palamod.recipies.PaladiumGrinderRecipies;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class PaladiumGrinderRecipeHandler extends TemplateRecipeHandler {
   public static final String IDENTIFIER = "palamod.paladium_grinder";
   private static final String TEXTURE = (new ResourceLocation("palamod:textures/gui/PaladiumGrinderNEI.png")).toString();

   public String getRecipeName() {
      return "Paladium Grinder";
   }

   public String getGuiTexture() {
      return TEXTURE;
   }

   public Class<? extends GuiContainer> getGuiClass() {
      return GuiPaladiumGrinder.class;
   }

   public String getOverlayIdentifier() {
      return "palamod.paladium_grinder";
   }

   public void loadTransferRects() {
      this.transferRects.add(new RecipeTransferRect(new Rectangle(35, 19, 29, 15), "palamod.paladium_grinder", new Object[0]));
   }

   public void drawExtras(int recipe) {
      this.drawProgressBar(35, 20, 152, 0, 30, 15, 50, 0);
      ItemStack key = PaladiumGrinderRecipies.getManager().getSmeltingResult(new ItemStack[]{((PositionedStack)((CachedRecipe)this.arecipes.get(recipe)).getIngredients().get(0)).item, ((PositionedStack)((CachedRecipe)this.arecipes.get(recipe)).getIngredients().get(1)).item});
      int ammount = PaladiumGrinderRecipies.getManager().getSmeltingAmmount(key);
      GuiDraw.drawTexturedModalRect(106, 4, 0, 60, 53, 54 - ammount * 54 / 100);
      int k = (GuiDraw.displaySize().width - 175) / 2;
      int l = (GuiDraw.displaySize().height - 165) / 2;
      GuiDraw.drawString("" + ammount, k + 3, l - 11, 16733440, true);
   }

   public int recipiesPerPage() {
      return 2;
   }

   public void drawBackground(int recipe) {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GuiDraw.changeTexture(this.getGuiTexture());
      GuiDraw.drawTexturedModalRect(10, 2, 0, 0, 152, 60);
   }

   public void loadCraftingRecipes(String outputId, Object... results) {
      if (outputId.equals("palamod.paladium_grinder") && this.getClass() == PaladiumGrinderRecipeHandler.class) {
         Map<ItemStack[], ItemStack> recipes = PaladiumGrinderRecipies.getManager().getSmeltingList();
         Iterator var4 = recipes.entrySet().iterator();

         while(var4.hasNext()) {
            Entry<ItemStack[], ItemStack> recipe = (Entry)var4.next();
            this.arecipes.add(new PaladiumGrinderRecipeHandler.SmeltingPair((ItemStack[])recipe.getKey(), (ItemStack)recipe.getValue()));
         }
      } else {
         super.loadCraftingRecipes(outputId, results);
      }

   }

   public void loadCraftingRecipes(ItemStack result) {
      Map<ItemStack[], ItemStack> recipes = PaladiumGrinderRecipies.getManager().getSmeltingList();
      Iterator var3 = recipes.entrySet().iterator();

      while(var3.hasNext()) {
         Entry<ItemStack[], ItemStack> recipe = (Entry)var3.next();
         if (NEIServerUtils.areStacksSameType((ItemStack)recipe.getValue(), result)) {
            this.arecipes.add(new PaladiumGrinderRecipeHandler.SmeltingPair((ItemStack[])recipe.getKey(), (ItemStack)recipe.getValue()));
         }
      }

   }

   public void loadUsageRecipes(String inputId, Object... ingredients) {
      super.loadUsageRecipes(inputId, ingredients);
   }

   public void loadUsageRecipes(ItemStack ingredient) {
      Map<ItemStack[], ItemStack> recipes = PaladiumGrinderRecipies.getManager().getSmeltingList();
      Iterator var3 = recipes.entrySet().iterator();

      while(var3.hasNext()) {
         Entry<ItemStack[], ItemStack> recipe = (Entry)var3.next();
         ItemStack[] var5 = (ItemStack[])recipe.getKey();
         int var6 = var5.length;

         for(int var7 = 0; var7 < var6; ++var7) {
            ItemStack stack = var5[var7];
            if (NEIServerUtils.areStacksSameTypeCrafting(stack, ingredient)) {
               PaladiumGrinderRecipeHandler.SmeltingPair arecipe = new PaladiumGrinderRecipeHandler.SmeltingPair((ItemStack[])recipe.getKey(), (ItemStack)recipe.getValue());
               arecipe.setIngredientPermutation(arecipe.ingreds, ingredient);
               this.arecipes.add(arecipe);
            }
         }
      }

   }

   public class SmeltingPair extends CachedRecipe {
      List<PositionedStack> ingreds = new ArrayList();
      PositionedStack result;

      public SmeltingPair(ItemStack[] ingreds, ItemStack result) {
         super();
         this.ingreds.add(new PositionedStack(ingreds[0], 15, 13));
         this.ingreds.add(new PositionedStack(ingreds[1], 15, 31));
         this.result = new PositionedStack(result, 69, 22);
      }

      public List<PositionedStack> getIngredients() {
         return this.getCycledIngredients(PaladiumGrinderRecipeHandler.this.cycleticks / 48, this.ingreds);
      }

      public PositionedStack getResult() {
         return this.result;
      }
   }
}
