package fr.paladium.palamod.nei;

import codechicken.lib.gui.GuiDraw;
import codechicken.nei.NEIServerUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.TemplateRecipeHandler;
import codechicken.nei.recipe.TemplateRecipeHandler.CachedRecipe;
import codechicken.nei.recipe.TemplateRecipeHandler.RecipeTransferRect;
import fr.paladium.palamod.client.gui.GuiPaladiumMachine;
import fr.paladium.palamod.recipies.PaladiumMachineRecipies;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class PaladiumMachineRecipeHandler extends TemplateRecipeHandler {
   public static final String IDENTIFIER = "palamod.paladium_machine";
   private static final String TEXTURE;

   public String getRecipeName() {
      return "Paladium Machine";
   }

   public String getGuiTexture() {
      return TEXTURE;
   }

   public Class<? extends GuiContainer> getGuiClass() {
      return GuiPaladiumMachine.class;
   }

   public String getOverlayIdentifier() {
      return "palamod.paladium_machine";
   }

   public void loadTransferRects() {
      this.transferRects.add(new RecipeTransferRect(new Rectangle(79, 56, 8, 11), "palamod.paladium_machine", new Object[0]));
   }

   public void drawExtras(int recipe) {
      this.drawProgressBar(76, 55, 201, 105, 20, 16, 48, 1);
   }

   public int recipiesPerPage() {
      return 1;
   }

   public void drawBackground(int recipe) {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GuiDraw.changeTexture(this.getGuiTexture());
      GuiDraw.drawTexturedModalRect(10, 26, 24, 94, 146, 69);
   }

   public void loadCraftingRecipes(String outputId, Object... results) {
      if (outputId.equals("palamod.paladium_machine") && this.getClass() == PaladiumMachineRecipeHandler.class) {
         Map<ItemStack[], ItemStack> recipes = PaladiumMachineRecipies.getManager().getSmeltingList();
         Iterator var4 = recipes.entrySet().iterator();

         while(var4.hasNext()) {
            Entry<ItemStack[], ItemStack> recipe = (Entry)var4.next();
            this.arecipes.add(new PaladiumMachineRecipeHandler.SmeltingPair((ItemStack[])recipe.getKey(), (ItemStack)recipe.getValue()));
         }
      } else {
         super.loadCraftingRecipes(outputId, results);
      }

   }

   public void loadCraftingRecipes(ItemStack result) {
      Map<ItemStack[], ItemStack> recipes = PaladiumMachineRecipies.getManager().getSmeltingList();
      Iterator var3 = recipes.entrySet().iterator();

      while(var3.hasNext()) {
         Entry<ItemStack[], ItemStack> recipe = (Entry)var3.next();
         if (NEIServerUtils.areStacksSameType((ItemStack)recipe.getValue(), result)) {
            this.arecipes.add(new PaladiumMachineRecipeHandler.SmeltingPair((ItemStack[])recipe.getKey(), (ItemStack)recipe.getValue()));
         }
      }

   }

   public void loadUsageRecipes(String inputId, Object... ingredients) {
      super.loadUsageRecipes(inputId, ingredients);
   }

   public void loadUsageRecipes(ItemStack ingredient) {
      Map<ItemStack[], ItemStack> recipes = PaladiumMachineRecipies.getManager().getSmeltingList();
      Iterator var3 = recipes.entrySet().iterator();

      while(var3.hasNext()) {
         Entry<ItemStack[], ItemStack> recipe = (Entry)var3.next();
         ItemStack[] var5 = (ItemStack[])recipe.getKey();
         int var6 = var5.length;

         for(int var7 = 0; var7 < var6; ++var7) {
            ItemStack stack = var5[var7];
            if (NEIServerUtils.areStacksSameTypeCrafting(stack, ingredient)) {
               PaladiumMachineRecipeHandler.SmeltingPair arecipe = new PaladiumMachineRecipeHandler.SmeltingPair((ItemStack[])recipe.getKey(), (ItemStack)recipe.getValue());
               arecipe.setIngredientPermutation(arecipe.ingreds, ingredient);
               this.arecipes.add(arecipe);
            }
         }
      }

   }

   static {
      TEXTURE = GuiPaladiumMachine.background.toString();
   }

   public class SmeltingPair extends CachedRecipe {
      List<PositionedStack> ingreds;
      PositionedStack result;

      public SmeltingPair(ItemStack[] ingreds, ItemStack result) {
         super();
         if (ingreds[0].getItem() == result.getItem()) {
            ingreds[0].setItemDamage(ingreds[0].getMaxDamage() / 2);
         }

         this.ingreds = new ArrayList();
         this.ingreds.add(new PositionedStack(ingreds[0], 75, 27));
         this.ingreds.add(new PositionedStack(ingreds[1], 11, 35));
         this.ingreds.add(new PositionedStack(ingreds[2], 39, 55));
         this.ingreds.add(new PositionedStack(ingreds[3], 111, 55));
         this.ingreds.add(new PositionedStack(ingreds[4], 139, 35));
         this.result = new PositionedStack(result, 75, 78);
      }

      public List<PositionedStack> getIngredients() {
         return this.getCycledIngredients(PaladiumMachineRecipeHandler.this.cycleticks / 48, this.ingreds);
      }

      public PositionedStack getResult() {
         return this.result;
      }
   }
}
