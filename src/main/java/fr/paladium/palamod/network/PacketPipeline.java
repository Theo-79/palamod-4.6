package fr.paladium.palamod.network;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.FMLEmbeddedChannel;
import cpw.mods.fml.common.network.FMLOutboundHandler;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.FMLOutboundHandler.OutboundTarget;
import cpw.mods.fml.common.network.internal.FMLProxyPacket;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.network.packets.PacketCamera;
import fr.paladium.palamod.network.packets.PacketCrash;
import fr.paladium.palamod.network.packets.PacketGolem;
import fr.paladium.palamod.network.packets.PacketGuardianRadius;
import fr.paladium.palamod.network.packets.PacketGuardianWhitelist;
import fr.paladium.palamod.network.packets.PacketHash;
import fr.paladium.palamod.network.packets.PacketMessageBoss;
import fr.paladium.palamod.network.packets.PacketMulticolorBlock;
import fr.paladium.palamod.network.packets.PacketOnlineDetector;
import fr.paladium.palamod.network.packets.PacketOpenGui;
import fr.paladium.palamod.network.packets.PacketSendMessage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.handler.codec.MessageToMessageCodec;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.INetHandler;
import net.minecraft.network.NetHandlerPlayServer;

@Sharable
public class PacketPipeline extends MessageToMessageCodec<FMLProxyPacket, AbstractPacket> {
   private EnumMap<Side, FMLEmbeddedChannel> channels;
   private LinkedList<Class<? extends AbstractPacket>> packets = new LinkedList();
   private boolean isPostInitialised = false;

   public boolean registerPacket(Class<? extends AbstractPacket> clazz) {
      if (this.packets.size() > 256) {
         return false;
      } else if (this.packets.contains(clazz)) {
         return false;
      } else if (this.isPostInitialised) {
         return false;
      } else {
         this.packets.add(clazz);
         return true;
      }
   }

   protected void encode(ChannelHandlerContext ctx, AbstractPacket msg, List<Object> out) throws Exception {
      ByteBuf buffer = Unpooled.buffer();
      Class<? extends AbstractPacket> clazz = msg.getClass();
      if (!this.packets.contains(msg.getClass())) {
         throw new NullPointerException("No Packet Registered for: " + msg.getClass().getCanonicalName());
      } else {
         byte discriminator = (byte)this.packets.indexOf(clazz);
         buffer.writeByte(discriminator);
         msg.encodeInto(ctx, buffer);
         FMLProxyPacket proxyPacket = new FMLProxyPacket(buffer.copy(), (String)ctx.channel().attr(NetworkRegistry.FML_CHANNEL).get());
         out.add(proxyPacket);
      }
   }

   protected void decode(ChannelHandlerContext ctx, FMLProxyPacket msg, List<Object> out) throws Exception {
      ByteBuf payload = msg.payload();
      byte discriminator = payload.readByte();
      Class<? extends AbstractPacket> clazz = (Class)this.packets.get(discriminator);
      if (clazz == null) {
         throw new NullPointerException("No packet registered for discriminator: " + discriminator);
      } else {
         AbstractPacket pkt = (AbstractPacket)clazz.newInstance();
         pkt.decodeInto(ctx, payload.slice());
         switch(FMLCommonHandler.instance().getEffectiveSide()) {
         case CLIENT:
            EntityPlayer player = this.getClientPlayer();
            pkt.handleClientSide(player);
            break;
         case SERVER:
            INetHandler netHandler = (INetHandler)ctx.channel().attr(NetworkRegistry.NET_HANDLER).get();
            EntityPlayer player1 = ((NetHandlerPlayServer)netHandler).playerEntity;
            pkt.handleServerSide(player1);
         }

      }
   }

   public void initalise() {
      this.channels = NetworkRegistry.INSTANCE.newChannel("PalaMod", new ChannelHandler[]{this});
      this.registerPackets();
   }

   public void registerPackets() {
      this.registerPacket(PacketOpenGui.class);
      this.registerPacket(PacketOnlineDetector.class);
      this.registerPacket(PacketGuardianWhitelist.class);
      this.registerPacket(PacketGolem.class);
      this.registerPacket(PacketSendMessage.class);
      this.registerPacket(PacketMessageBoss.class);
      this.registerPacket(PacketCrash.class);
      this.registerPacket(PacketHash.class);
      this.registerPacket(PacketGuardianRadius.class);
      this.registerPacket(PacketMulticolorBlock.class);
      this.registerPacket(PacketCamera.class);
   }

   public void postInitialise() {
      if (!this.isPostInitialised) {
         this.isPostInitialised = true;
         Collections.sort(this.packets, new Comparator<Class<? extends AbstractPacket>>() {
            public int compare(Class<? extends AbstractPacket> clazz1, Class<? extends AbstractPacket> clazz2) {
               int com = String.CASE_INSENSITIVE_ORDER.compare(clazz1.getCanonicalName(), clazz2.getCanonicalName());
               if (com == 0) {
                  com = clazz1.getCanonicalName().compareTo(clazz2.getCanonicalName());
               }

               return com;
            }
         });
      }
   }

   @SideOnly(Side.CLIENT)
   private EntityPlayer getClientPlayer() {
      return Minecraft.getMinecraft().thePlayer;
   }

   public void sendToAll(AbstractPacket message) {
      ((FMLEmbeddedChannel)this.channels.get(Side.SERVER)).attr(FMLOutboundHandler.FML_MESSAGETARGET).set(OutboundTarget.ALL);
      ((FMLEmbeddedChannel)this.channels.get(Side.SERVER)).writeAndFlush(message);
   }

   public void sendTo(AbstractPacket message, EntityPlayer player) {
      ((FMLEmbeddedChannel)this.channels.get(Side.SERVER)).attr(FMLOutboundHandler.FML_MESSAGETARGET).set(OutboundTarget.PLAYER);
      ((FMLEmbeddedChannel)this.channels.get(Side.SERVER)).attr(FMLOutboundHandler.FML_MESSAGETARGETARGS).set(player);
      ((FMLEmbeddedChannel)this.channels.get(Side.SERVER)).writeAndFlush(message);
   }

   public void sendToServer(AbstractPacket message) {
      ((FMLEmbeddedChannel)this.channels.get(Side.CLIENT)).attr(FMLOutboundHandler.FML_MESSAGETARGET).set(OutboundTarget.TOSERVER);
      ((FMLEmbeddedChannel)this.channels.get(Side.CLIENT)).writeAndFlush(message);
   }
}
