package fr.paladium.palamod.network.packets;

import fr.paladium.palamod.network.AbstractPacket;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;

public class PacketCamera extends AbstractPacket {
   NBTTagCompound compound;

   public void addInformations(NBTTagCompound compound) {
      this.compound = compound;
   }

   public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      buffer.writeBoolean(this.compound.getBoolean("cam1"));
      buffer.writeInt(this.compound.getInteger("cam1X"));
      buffer.writeInt(this.compound.getInteger("cam1Y"));
      buffer.writeInt(this.compound.getInteger("cam1Z"));
      buffer.writeInt(this.compound.getInteger("cam1Yaw"));
      buffer.writeBoolean(this.compound.getBoolean("cam2"));
      buffer.writeInt(this.compound.getInteger("cam2X"));
      buffer.writeInt(this.compound.getInteger("cam2Y"));
      buffer.writeInt(this.compound.getInteger("cam2Z"));
      buffer.writeInt(this.compound.getInteger("cam2Yaw"));
      buffer.writeBoolean(this.compound.getBoolean("cam3"));
      buffer.writeInt(this.compound.getInteger("cam3X"));
      buffer.writeInt(this.compound.getInteger("cam3Y"));
      buffer.writeInt(this.compound.getInteger("cam3Z"));
      buffer.writeInt(this.compound.getInteger("cam3Yaw"));
      buffer.writeInt(this.compound.getInteger("Index"));
   }

   public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      this.compound = new NBTTagCompound();
      this.compound.setBoolean("cam1", buffer.readBoolean());
      this.compound.setInteger("cam1X", buffer.readInt());
      this.compound.setInteger("cam1Y", buffer.readInt());
      this.compound.setInteger("cam1Z", buffer.readInt());
      this.compound.setInteger("cam1Yaw", buffer.readInt());
      this.compound.setBoolean("cam2", buffer.readBoolean());
      this.compound.setInteger("cam2X", buffer.readInt());
      this.compound.setInteger("cam2Y", buffer.readInt());
      this.compound.setInteger("cam2Z", buffer.readInt());
      this.compound.setInteger("cam2Yaw", buffer.readInt());
      this.compound.setBoolean("cam3", buffer.readBoolean());
      this.compound.setInteger("cam3X", buffer.readInt());
      this.compound.setInteger("cam3Y", buffer.readInt());
      this.compound.setInteger("cam3Z", buffer.readInt());
      this.compound.setInteger("cam3Yaw", buffer.readInt());
      this.compound.setInteger("Index", buffer.readInt());
   }

   public void handleClientSide(EntityPlayer player) {
   }

   public void handleServerSide(EntityPlayer player) {
      if (player.getHeldItem() != null) {
         player.getHeldItem().setTagCompound(this.compound);
      }

   }
}
