package fr.paladium.palamod.network.packets;

import cpw.mods.fml.common.network.ByteBufUtils;
import fr.paladium.palamod.common.KiwiHandler;
import fr.paladium.palamod.network.AbstractPacket;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;

public class PacketCrash extends AbstractPacket {
   String reason;

   public void addInformations(String reason) {
      this.reason = reason;
   }

   public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      ByteBufUtils.writeUTF8String(buffer, this.reason);
   }

   public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      this.reason = ByteBufUtils.readUTF8String(buffer);
   }

   public void handleClientSide(EntityPlayer player) {
      KiwiHandler.crash(this.reason);
   }

   public void handleServerSide(EntityPlayer player) {
   }
}
