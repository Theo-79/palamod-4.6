package fr.paladium.palamod.network.packets;

import cpw.mods.fml.common.network.ByteBufUtils;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.network.AbstractPacket;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class PacketGolem extends AbstractPacket {
   int level;
   int subLevel;
   int id;
   ItemStack weapon;

   public void addInformations(int level, int subLevel, int id, ItemStack weapon) {
      this.level = level;
      this.subLevel = subLevel;
      this.id = id;
      this.weapon = weapon;
   }

   public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      buffer.writeInt(this.level);
      buffer.writeInt(this.subLevel);
      buffer.writeInt(this.id);
      ByteBufUtils.writeItemStack(buffer, this.weapon);
   }

   public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      this.level = buffer.readInt();
      this.subLevel = buffer.readInt();
      this.id = buffer.readInt();
      this.weapon = ByteBufUtils.readItemStack(buffer);
   }

   public void handleClientSide(EntityPlayer player) {
      if (player != null) {
         EntityGuardianGolem golem = (EntityGuardianGolem)player.worldObj.getEntityByID(this.id);
         if (golem != null) {
            golem.setClientInfos(this.level, this.subLevel, this.weapon);
         }

      }
   }

   public void handleServerSide(EntityPlayer player) {
   }
}
