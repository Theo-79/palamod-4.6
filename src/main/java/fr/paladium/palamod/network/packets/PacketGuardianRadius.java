package fr.paladium.palamod.network.packets;

import fr.paladium.palamod.network.AbstractPacket;
import fr.paladium.palamod.tiles.TileEntityGuardianAnchor;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;

public class PacketGuardianRadius extends AbstractPacket {
   int radius;
   int x;
   int y;
   int z;
   TileEntityGuardianAnchor te;

   public void addInformations(int radius, TileEntityGuardianAnchor te) {
      this.radius = radius;
      this.te = te;
   }

   public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      this.radius = buffer.readInt();
      this.x = buffer.readInt();
      this.y = buffer.readInt();
      this.z = buffer.readInt();
   }

   public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      buffer.writeInt(this.radius);
      buffer.writeInt(this.x);
      buffer.writeInt(this.y);
      buffer.writeInt(this.z);
   }

   public void handleServerSide(EntityPlayer player) {
      TileEntityGuardianAnchor guardianAnchor = (TileEntityGuardianAnchor)player.worldObj.getTileEntity(this.x, this.y, this.z);
      if (guardianAnchor != null) {
         guardianAnchor.setRadius(this.radius);
      }

   }

   public void handleClientSide(EntityPlayer player) {
   }
}
