package fr.paladium.palamod.network.packets;

import cpw.mods.fml.common.network.ByteBufUtils;
import fr.paladium.palamod.items.ItemGuardianWhitelist;
import fr.paladium.palamod.network.AbstractPacket;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class PacketGuardianWhitelist extends AbstractPacket {
   String player;
   boolean b;

   public void addInformations(String player, boolean b) {
      this.player = player;
      this.b = b;
   }

   public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      ByteBufUtils.writeUTF8String(buffer, this.player);
      buffer.writeBoolean(this.b);
   }

   public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      this.player = ByteBufUtils.readUTF8String(buffer);
      this.b = buffer.readBoolean();
   }

   public void handleClientSide(EntityPlayer player) {
   }

   public void handleServerSide(EntityPlayer player) {
      ItemStack stack = player.getHeldItem();
      if (stack != null) {
         if (!this.player.equals("")) {
            if (this.b) {
               ItemGuardianWhitelist.setList(ItemGuardianWhitelist.addName(this.player, ItemGuardianWhitelist.getList(stack)), stack);
            } else {
               ItemGuardianWhitelist.setList(ItemGuardianWhitelist.removeName(this.player, ItemGuardianWhitelist.getList(stack)), stack);
            }

         }
      }
   }
}
