package fr.paladium.palamod.network.packets;

import cpw.mods.fml.common.network.ByteBufUtils;
import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.network.AbstractPacket;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;

public class PacketHash extends AbstractPacket {
   String sentHash;

   public void addInformations(String hash) {
   }

   public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      ByteBufUtils.writeUTF8String(buffer, "FGYGYGE2YUZG635764YUgfgf_-&et(55T4T");
   }

   public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      this.sentHash = ByteBufUtils.readUTF8String(buffer);
   }

   public void handleClientSide(EntityPlayer player) {
   }

   public void handleServerSide(EntityPlayer player) {
      if (!this.sentHash.equals("FGYGYGE2YUZG635764YUgfgf_-&et(55T4T")) {
         PacketCrash packet = new PacketCrash();
         packet.addInformations("Unchecked PalaClient");
         PalaMod.proxy.packetPipeline.sendTo(packet, player);
      }

   }
}
