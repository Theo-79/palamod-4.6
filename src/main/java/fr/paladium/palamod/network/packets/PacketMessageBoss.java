package fr.paladium.palamod.network.packets;

import fr.paladium.palamod.client.overlay.OverlayMessageBoss;
import fr.paladium.palamod.network.AbstractPacket;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;

public class PacketMessageBoss extends AbstractPacket {
   public static final byte START = 0;
   public static final byte STOP = 1;
   byte type;
   int x;
   int z;
   int time;

   public void addInformations(int x, int z, byte type, int time) {
      this.type = type;
      this.x = x;
      this.z = z;
      this.time = time;
   }

   public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      buffer.writeInt(this.x);
      buffer.writeInt(this.z);
      buffer.writeInt(this.time);
      buffer.writeByte(this.type);
   }

   public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      this.x = buffer.readInt();
      this.z = buffer.readInt();
      this.time = buffer.readInt();
      this.type = buffer.readByte();
   }

   public void handleClientSide(EntityPlayer player) {
      if (this.type == 1) {
         OverlayMessageBoss.reset();
      } else {
         OverlayMessageBoss.startCd(this.x, this.z, this.time);
      }

   }

   public void handleServerSide(EntityPlayer player) {
   }
}
