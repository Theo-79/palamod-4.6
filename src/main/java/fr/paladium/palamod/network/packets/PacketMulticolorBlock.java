package fr.paladium.palamod.network.packets;

import fr.paladium.palamod.network.AbstractPacket;
import fr.paladium.palamod.tiles.TileEntityMulticolorBlock;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;

public class PacketMulticolorBlock extends AbstractPacket {
   private int color1;
   private int color2;
   private int color3;
   private int color4;
   private int time;
   private int x;
   private int y;
   private int z;
   private boolean activate2;
   private boolean activate3;
   private boolean activate4;

   public void setInformations(int color1, int color2, int color3, int color4, boolean activate2, boolean activate3, boolean activate4, int time, int x, int y, int z) {
      this.color1 = color1;
      this.color2 = color2;
      this.color3 = color3;
      this.color4 = color4;
      this.activate2 = activate2;
      this.activate3 = activate3;
      this.activate4 = activate4;
      this.time = time;
      this.x = x;
      this.y = y;
      this.z = z;
   }

   public void encodeInto(ChannelHandlerContext ctx, ByteBuf buf) {
      buf.writeInt(this.x);
      buf.writeInt(this.y);
      buf.writeInt(this.z);
      buf.writeInt(this.color1);
      buf.writeInt(this.color2);
      buf.writeInt(this.color3);
      buf.writeInt(this.color4);
      buf.writeBoolean(this.activate2);
      buf.writeBoolean(this.activate3);
      buf.writeBoolean(this.activate4);
      buf.writeInt(this.time);
   }

   public void decodeInto(ChannelHandlerContext ctx, ByteBuf buf) {
      this.x = buf.readInt();
      this.y = buf.readInt();
      this.z = buf.readInt();
      this.color1 = buf.readInt();
      this.color2 = buf.readInt();
      this.color3 = buf.readInt();
      this.color4 = buf.readInt();
      this.activate2 = buf.readBoolean();
      this.activate3 = buf.readBoolean();
      this.activate4 = buf.readBoolean();
      this.time = buf.readInt();
   }

   public void handleClientSide(EntityPlayer player) {
   }

   public void handleServerSide(EntityPlayer player) {
      TileEntity tile = player.worldObj.getTileEntity(this.x, this.y, this.z);
      if (tile instanceof TileEntityMulticolorBlock) {
         TileEntityMulticolorBlock te = (TileEntityMulticolorBlock)tile;
         te.setColor1(this.color1);
         te.setColor2(this.color2);
         te.setColor3(this.color3);
         te.setColor4(this.color4);
         te.setActivate2(this.activate2);
         te.setActivate3(this.activate3);
         te.setActivate4(this.activate4);
         te.setTime(this.time);
         te.setCounter(0);
         te.getWorldObj().markBlockForUpdate(te.xCoord, te.yCoord, te.zCoord);
      }

   }
}
