package fr.paladium.palamod.network.packets;

import cpw.mods.fml.common.network.ByteBufUtils;
import fr.paladium.palamod.network.AbstractPacket;
import fr.paladium.palamod.tiles.TileEntityOnlineDetector;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;

public class PacketOnlineDetector extends AbstractPacket {
   String player;
   TileEntityOnlineDetector te;
   int x;
   int y;
   int z;

   public void addInformations(String player, TileEntityOnlineDetector te) {
      this.player = player;
      this.te = te;
   }

   public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      ByteBufUtils.writeUTF8String(buffer, this.player);
      buffer.writeInt(this.te.xCoord);
      buffer.writeInt(this.te.yCoord);
      buffer.writeInt(this.te.zCoord);
   }

   public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      this.player = ByteBufUtils.readUTF8String(buffer);
      this.x = buffer.readInt();
      this.y = buffer.readInt();
      this.z = buffer.readInt();
   }

   public void handleClientSide(EntityPlayer player) {
   }

   public void handleServerSide(EntityPlayer player) {
      TileEntityOnlineDetector detector = (TileEntityOnlineDetector)player.worldObj.getTileEntity(this.x, this.y, this.z);
      if (detector != null) {
         detector.setName(this.player);
      }

   }
}
