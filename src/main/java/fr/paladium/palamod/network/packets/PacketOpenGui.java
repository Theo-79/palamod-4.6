package fr.paladium.palamod.network.packets;

import fr.paladium.palamod.PalaMod;
import fr.paladium.palamod.network.AbstractPacket;
import fr.paladium.palamod.tiles.TileEntityAlchemyCreator;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;

public class PacketOpenGui extends AbstractPacket {
   TileEntityAlchemyCreator tile;
   byte id;
   int x;
   int y;
   int z;

   public void setInformations(byte id) {
      this.id = id;
   }

   public void setInformations(byte id, int x, int y, int z) {
      this.id = id;
      this.x = x;
      this.y = y;
      this.z = z;
   }

   public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      buffer.writeByte(this.id);
      buffer.writeInt(this.x);
      buffer.writeInt(this.y);
      buffer.writeInt(this.z);
   }

   public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      this.id = buffer.readByte();
      this.x = buffer.readInt();
      this.y = buffer.readInt();
      this.z = buffer.readInt();
   }

   public void handleClientSide(EntityPlayer player) {
   }

   public void handleServerSide(EntityPlayer player) {
      if (this.id != 2 && this.id != 1) {
         player.openGui(PalaMod.instance, this.id, player.worldObj, this.x, this.y, this.z);
      } else {
         TileEntityAlchemyCreator tile = (TileEntityAlchemyCreator)TileEntityAlchemyCreator.oppenedGui.get(player);
         if (tile != null) {
            player.openGui(PalaMod.instance, this.id, player.worldObj, tile.xCoord, tile.yCoord, tile.zCoord);
         }

      }
   }
}
