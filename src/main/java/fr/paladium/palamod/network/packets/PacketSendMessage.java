package fr.paladium.palamod.network.packets;

import cpw.mods.fml.common.network.ByteBufUtils;
import fr.paladium.palamod.client.DisplayMessage;
import fr.paladium.palamod.client.overlay.OverlayMessage;
import fr.paladium.palamod.network.AbstractPacket;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;

public class PacketSendMessage extends AbstractPacket {
   String message;
   String subMessage;
   int time;

   public void addInformations(String title, String subTitle, int time) {
      this.message = title;
      this.subMessage = subTitle;
      this.time = time;
   }

   public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      ByteBufUtils.writeUTF8String(buffer, this.message);
      ByteBufUtils.writeUTF8String(buffer, this.subMessage);
      buffer.writeInt(this.time);
   }

   public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
      this.message = ByteBufUtils.readUTF8String(buffer);
      this.subMessage = ByteBufUtils.readUTF8String(buffer);
      this.time = buffer.readInt();
   }

   public void handleClientSide(EntityPlayer player) {
      DisplayMessage message = new DisplayMessage(this.message, this.subMessage, this.time);
      OverlayMessage.addMessageToQueue(message);
   }

   public void handleServerSide(EntityPlayer player) {
   }
}
