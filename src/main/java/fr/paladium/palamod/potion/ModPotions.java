package fr.paladium.palamod.potion;

public class ModPotions {
   public static PotionWitherImbue potionWither;
   public static PotionFireImbue potionFire;
   public static PotionPoisonImbue potionPoison;

   public static void init() {
      potionWither = (PotionWitherImbue)(new PotionWitherImbue(32, false, 0)).setIconIndex(0, 0).setPotionName("potion.imbuewither");
      potionFire = (PotionFireImbue)(new PotionFireImbue(33, false, 0)).setIconIndex(0, 0).setPotionName("potion.imbuefire");
      potionPoison = (PotionPoisonImbue)(new PotionPoisonImbue(34, false, 0)).setIconIndex(0, 0).setPotionName("potion.imbuepoison");
   }
}
