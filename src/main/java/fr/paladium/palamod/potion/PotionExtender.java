package fr.paladium.palamod.potion;

import java.lang.reflect.Field;
import net.minecraft.potion.Potion;

public class PotionExtender {
   public static void preInit() {
      extendPotionArray();
   }

   private static void extendPotionArray() {
      Potion[] potionTypes = null;
      Field[] var1 = Potion.class.getDeclaredFields();
      int var2 = var1.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         Field f = var1[var3];
         f.setAccessible(true);

         try {
            if (f.getName().equals("potionTypes") || f.getName().equals("field_76425_a")) {
               Field modfield = Field.class.getDeclaredField("modifiers");
               modfield.setAccessible(true);
               modfield.setInt(f, f.getModifiers() & -17);
               potionTypes = (Potion[])((Potion[])f.get((Object)null));
               Potion[] newPotionTypes = new Potion[256];
               System.arraycopy(potionTypes, 0, newPotionTypes, 0, potionTypes.length);
               f.set((Object)null, newPotionTypes);
            }
         } catch (Exception var7) {
            System.err.println("Severe error, please report this to the mod author:");
            System.err.println(var7);
         }
      }

   }
}
