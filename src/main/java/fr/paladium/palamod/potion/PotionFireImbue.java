package fr.paladium.palamod.potion;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.util.GuiDrawer;
import net.minecraft.client.Minecraft;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ResourceLocation;

public class PotionFireImbue extends Potion {
   ResourceLocation icon = new ResourceLocation("palamod:textures/gui/effects/FireEffect.png");

   public PotionFireImbue(int par1, boolean par2, int par3) {
      super(par1, par2, par3);
   }

   public Potion setIconIndex(int par1, int par2) {
      super.setIconIndex(par1, par2);
      return this;
   }

   @SideOnly(Side.CLIENT)
   public void renderInventoryEffect(int x, int y, PotionEffect effect, Minecraft mc) {
      mc.renderEngine.bindTexture(this.icon);
      GuiDrawer.drawTexturedQuadFit((double)(x + 5), (double)(y + 5), 20.0D, 20.0D, 1.0D);
   }

   @SideOnly(Side.CLIENT)
   public boolean hasStatusIcon() {
      return false;
   }
}
