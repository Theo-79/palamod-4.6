package fr.paladium.palamod.potion;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.EntityDamageSourceIndirect;
import net.minecraftforge.event.entity.living.LivingAttackEvent;

public class PotionHandler {
   @SubscribeEvent(
      priority = EventPriority.NORMAL
   )
   public void imbueHandler(LivingAttackEvent event) {
      if (!event.entityLiving.worldObj.isRemote && !event.isCanceled() && event.source instanceof EntityDamageSource && !(event.source instanceof EntityDamageSourceIndirect)) {
         EntityDamageSource damageSource = (EntityDamageSource)event.source;
         if (damageSource.getEntity() != null && damageSource.getEntity() instanceof EntityLivingBase) {
            EntityLivingBase livingEntity = (EntityLivingBase)damageSource.getEntity();
            if (livingEntity.isPotionActive(ModPotions.potionWither) && !livingEntity.isPotionActive(ModPotions.potionPoison)) {
               event.entityLiving.addPotionEffect(new PotionEffect(Potion.wither.id, 100, 1));
            }

            if (livingEntity.isPotionActive(ModPotions.potionPoison) && !livingEntity.isPotionActive(ModPotions.potionWither)) {
               event.entityLiving.addPotionEffect(new PotionEffect(Potion.poison.id, 100, 1));
            }

            if (livingEntity.isPotionActive(ModPotions.potionFire)) {
               event.entityLiving.setFire(20);
            }
         }
      }

   }
}
