package fr.paladium.palamod.recipies;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.item.ItemStack;

public class AlchemyCreatorArrowRecipies {
   private static final AlchemyCreatorArrowRecipies instance = new AlchemyCreatorArrowRecipies();
   private Map smeltingList = new HashMap();

   public void add(ItemStack stack1, ItemStack stack2, ItemStack stack3, ItemStack stack4, ItemStack stack) {
      ItemStack[] stackList = new ItemStack[]{stack1, stack2, stack3, stack4};
      this.smeltingList.put(stackList, stack);
   }

   public ItemStack getSmeltingResult(ItemStack[] stack) {
      Iterator iterator = this.smeltingList.entrySet().iterator();

      while(iterator.hasNext()) {
         Entry entry = (Entry)iterator.next();
         if (this.isSameKey(stack, (ItemStack[])((ItemStack[])entry.getKey()))) {
            return (ItemStack)entry.getValue();
         }
      }

      return null;
   }

   private boolean isSameKey(ItemStack[] stackList, ItemStack[] stackList2) {
      boolean isSame = false;

      for(int i = 0; i <= 3; ++i) {
         if (stackList[i].getItem() != stackList2[i].getItem()) {
            return false;
         }

         isSame = true;
      }

      return isSame;
   }

   public static AlchemyCreatorArrowRecipies getManager() {
      return instance;
   }

   public Map<ItemStack[], ItemStack> getSmeltingList() {
      return this.smeltingList;
   }
}
