package fr.paladium.palamod.recipies;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.item.ItemStack;

public class ArmorCompressorRecipe {
   private static final ArmorCompressorRecipe instance = new ArmorCompressorRecipe();
   private Map smeltingList = new HashMap();

   public void add(ItemStack stack1, ItemStack stack) {
      this.smeltingList.put(stack1, stack);
   }

   public ItemStack getSmeltingResult(ItemStack stack) {
      Iterator iterator = this.smeltingList.entrySet().iterator();

      while(iterator.hasNext()) {
         Entry entry = (Entry)iterator.next();
         if (this.isSameKey(stack, (ItemStack)entry.getKey())) {
            return (ItemStack)entry.getValue();
         }
      }

      return null;
   }

   private boolean isSameKey(ItemStack stackList, ItemStack stackList2) {
      boolean isSame = false;
      if (stackList.getItem() == stackList2.getItem()) {
         isSame = true;
         return isSame;
      } else {
         return false;
      }
   }

   public Map<ItemStack, ItemStack> getSmeltingList() {
      return this.smeltingList;
   }

   public static ArmorCompressorRecipe getManager() {
      return instance;
   }
}
