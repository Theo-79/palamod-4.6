package fr.paladium.palamod.recipies;

import java.util.HashMap;
import java.util.Map;
import net.minecraft.item.Item;

public class BowMachineRecipies {
   public static final int RANGE = 0;
   public static BowMachineRecipies instance = new BowMachineRecipies();
   private Map<Item, Integer> modifiers = new HashMap();

   public void addRecipie(Item stack, int type) {
      this.modifiers.put(stack, type);
   }

   public boolean hasRecipie(Item stack) {
      return this.modifiers.containsKey(stack);
   }

   public int getModifier(Item stack) {
      return this.modifiers.containsKey(stack) ? (Integer)this.modifiers.get(stack) : -1;
   }
}
