package fr.paladium.palamod.recipies;

import fr.paladium.palamod.items.ItemObsidianUpgrade;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class IncompatibleObsidianUpgrades {
   private static final IncompatibleObsidianUpgrades instance = new IncompatibleObsidianUpgrades();
   private Map smeltingList = new HashMap();

   public void add(ItemObsidianUpgrade.Upgrade upgrade1, ItemObsidianUpgrade.Upgrade upgrade2) {
      this.smeltingList.put(upgrade1, upgrade2);
   }

   public Boolean isCompatible(ItemObsidianUpgrade.Upgrade upgrade1, ItemObsidianUpgrade.Upgrade upgrade2) {
      Iterator iterator = this.smeltingList.entrySet().iterator();

      while(iterator.hasNext()) {
         Entry entry = (Entry)iterator.next();
         if ((ItemObsidianUpgrade.Upgrade)entry.getKey() == upgrade1) {
            int value = ((ItemObsidianUpgrade.Upgrade)entry.getValue()).ordinal();
            int value2 = upgrade2.ordinal();
            if (value == value2) {
               return false;
            }
         }
      }

      return true;
   }

   public static IncompatibleObsidianUpgrades getManager() {
      return instance;
   }
}
