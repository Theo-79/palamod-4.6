package fr.paladium.palamod.recipies;

import cpw.mods.fml.common.registry.GameRegistry;
import fr.paladium.palamod.blocks.ModBlocks;
import fr.paladium.palamod.items.ModItems;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class ModRecipies {
   public static void init() {
      craftingRecipies();
      furnaceRecipies();
      paladiumMachineRecipies();
      AlchemyCreatorRecipies();
      BowRecipies();
      paladiumGrinderRecipies();
      ArmorCompressorRecipies();
   }

   private static void ArmorCompressorRecipies() {
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.amethystBoots, 1), new ItemStack(ModItems.itemrune, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.amethystChestplate, 1), new ItemStack(ModItems.itemrune, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.amethystHelmet, 1), new ItemStack(ModItems.itemrune, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.amethystLeggings, 1), new ItemStack(ModItems.itemrune, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.itemshardrune, 4), new ItemStack(ModItems.itemrune, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.titaneBoots, 1), new ItemStack(ModItems.itemrune2, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.titaneChestplate, 1), new ItemStack(ModItems.itemrune2, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.titaneHelmet, 1), new ItemStack(ModItems.itemrune2, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.titaneLeggings, 1), new ItemStack(ModItems.itemrune2, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.itemshardrune2, 4), new ItemStack(ModItems.itemrune2, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.paladiumBoots, 1), new ItemStack(ModItems.itemrune3, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.paladiumChestplate, 1), new ItemStack(ModItems.itemrune3, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.paladiumHelmet, 1), new ItemStack(ModItems.itemrune3, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.paladiumLeggings, 1), new ItemStack(ModItems.itemrune3, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.itemshardrune3, 4), new ItemStack(ModItems.itemrune3, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.endiumBoots, 1), new ItemStack(ModItems.itemrune4, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.endiumChestplate, 1), new ItemStack(ModItems.itemrune4, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.endiumHelmet, 1), new ItemStack(ModItems.itemrune4, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.endiumLeggings, 1), new ItemStack(ModItems.itemrune4, 1));
      ArmorCompressorRecipe.getManager().add(new ItemStack(ModItems.itemshardrune4, 4), new ItemStack(ModItems.itemrune4, 1));
   }

   private static void paladiumGrinderRecipies() {
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.paternBroadsword), new ItemStack(ModItems.paternSocket), new ItemStack(ModItems.broadSwordHead), 4);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.paternPickaxe), new ItemStack(ModItems.paternSocket), new ItemStack(ModItems.pickaxeHead), 3);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.paternFastSword), new ItemStack(ModItems.paternSocket), new ItemStack(ModItems.fastSwordHead), 1);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.paternSword), new ItemStack(ModItems.paternSocket), new ItemStack(ModItems.swordHead), 2);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.paternHammer), new ItemStack(ModItems.paternSocket), new ItemStack(ModItems.hammerHead), 6);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.paternShovel), new ItemStack(ModItems.paternSocket), new ItemStack(ModItems.shovelHead), 1);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.paternAxe), new ItemStack(ModItems.paternSocket), new ItemStack(ModItems.axeHead), 3);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.paternIngot), new ItemStack(ModItems.paternSocket), new ItemStack(ModItems.paladium), 1);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.broadSwordHead), new ItemStack(ModItems.paladiumStick), new ItemStack(ModItems.paladiumBroadsword), 0);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.broadSwordHead), new ItemStack(Items.stick), new ItemStack(ModItems.paladiumBroadsword), 1);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.pickaxeHead), new ItemStack(Items.stick), new ItemStack(ModItems.paladiumPickaxe), 0);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.swordHead), new ItemStack(Items.stick), new ItemStack(ModItems.paladiumSword), 0);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.fastSwordHead), new ItemStack(ModItems.paladiumStick), new ItemStack(ModItems.paladiumFastSword), 0);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.fastSwordHead), new ItemStack(Items.stick), new ItemStack(ModItems.paladiumFastSword), 1);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.hammerHead), new ItemStack(Items.stick), new ItemStack(ModItems.paladiumHammer), 1);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.hammerHead), new ItemStack(ModItems.paladiumStick), new ItemStack(ModItems.paladiumHammer), 0);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.axeHead), new ItemStack(Items.stick), new ItemStack(ModItems.paladiumAxe), 0);
      PaladiumGrinderRecipies.getManager().add(new ItemStack(ModItems.shovelHead), new ItemStack(Items.stick), new ItemStack(ModItems.paladiumShovel), 0);
      PaladiumGrinderRecipies.getManager().addUpgrade(ModItems.paladiumHammer, ModItems.smeltModifier, 0);
      PaladiumGrinderRecipies.getManager().addUpgrade(ModItems.paladiumHammer, ModItems.speedModifier, 2);
      PaladiumGrinderRecipies.getManager().addUpgrade(ModItems.paladiumHammer, ModItems.fortuneModifier, 1);
      PaladiumGrinderRecipies.getManager().addUpgrade(ModItems.paladiumHammer, ModItems.obsidianUpgrade, 6);
      PaladiumGrinderRecipies.getManager().addUpgrade(ModItems.paladiumHammer, ModItems.moreUpgrade, 7);
      PaladiumGrinderRecipies.getManager().addUpgrade(ModItems.paladiumBroadsword, ModItems.damageModifier, 3);
      PaladiumGrinderRecipies.getManager().addUpgrade(ModItems.paladiumBroadsword, ModItems.flameModifier, 4);
      PaladiumGrinderRecipies.getManager().addUpgrade(ModItems.paladiumBroadsword, ModItems.knockbackModifier, 5);
      PaladiumGrinderRecipies.getManager().addUpgrade(ModItems.paladiumBroadsword, ModItems.moreUpgrade, 7);
   }

   private static void BowRecipies() {
      BowMachineRecipies.instance.addRecipie(ModItems.bowRangeModifier, 0);
      BowMachineRecipies.instance.addRecipie(ModItems.bowSpeedModifier, 1);
   }

   private static void AlchemyCreatorRecipies() {
      AlchemyCreatorPotionRecipies.getManager().add(new ItemStack(Items.fermented_spider_eye), new ItemStack(Items.nether_wart), new ItemStack(Items.poisonous_potato), new ItemStack(Items.glass_bottle, 1, 0), new ItemStack(ModItems.potionPoison));
      AlchemyCreatorPotionRecipies.getManager().add(new ItemStack(Items.blaze_powder), new ItemStack(Items.nether_wart), new ItemStack(Items.fire_charge), new ItemStack(Items.glass_bottle, 1, 0), new ItemStack(ModItems.potionFire));
      AlchemyCreatorPotionRecipies.getManager().add(new ItemStack(ModItems.witherSkullFragment), new ItemStack(Items.nether_wart), new ItemStack(Items.bone), new ItemStack(Items.glass_bottle, 1, 0), new ItemStack(ModItems.potionWither));
      AlchemyCreatorPotionRecipies.getManager().add(new ItemStack(Items.blaze_powder, 1, 0), new ItemStack(Items.nether_wart), new ItemStack(Items.fermented_spider_eye), new ItemStack(Items.glass_bottle, 1, 0), new ItemStack(ModItems.sicknessPotion, 1, 0));
      AlchemyCreatorPotionRecipies.getManager().add(new ItemStack(Items.blaze_powder), new ItemStack(Items.nether_wart), new ItemStack(Items.fermented_spider_eye), new ItemStack(Items.glass_bottle, 1, 0), new ItemStack(ModItems.sicknessPotion, 1, 1));
      AlchemyCreatorArrowRecipies.getManager().add(new ItemStack(Items.fermented_spider_eye), new ItemStack(Items.nether_wart), new ItemStack(Items.poisonous_potato), new ItemStack(Items.arrow, 16), new ItemStack(ModItems.arrowPoison, 16));
      AlchemyCreatorArrowRecipies.getManager().add(new ItemStack(ModItems.witherSkullFragment), new ItemStack(Items.nether_wart), new ItemStack(Items.bone), new ItemStack(Items.arrow, 16), new ItemStack(ModItems.arrowWither, 16));
      AlchemyCreatorArrowRecipies.getManager().add(new ItemStack(Items.spider_eye), new ItemStack(Items.nether_wart), new ItemStack(Items.carrot), new ItemStack(Items.arrow, 16), new ItemStack(ModItems.arrowSlowness, 16));
      AlchemyCreatorArrowRecipies.getManager().add(new ItemStack(Items.ender_pearl), new ItemStack(Items.nether_wart), new ItemStack(Items.ender_pearl), new ItemStack(Items.arrow, 16), new ItemStack(ModItems.arrowSwitch, 16));
   }

   private static void paladiumMachineRecipies() {
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.paladium), new ItemStack(ModItems.paladium), new ItemStack(ModItems.paladium), new ItemStack(ModItems.paladium), new ItemStack(ModItems.smallRing, 1));
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModItems.smallRing), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.paladium), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.paladium), new ItemStack(ModItems.mediumRing, 1));
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModItems.mediumRing), new ItemStack(ModItems.compressedPaladium), new ItemStack(ModItems.paladium), new ItemStack(ModItems.compressedPaladium), new ItemStack(ModItems.paladium), new ItemStack(ModItems.bigRing, 1));
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModItems.smallRing), new ItemStack(ModItems.paladium), new ItemStack(ModItems.paladium), new ItemStack(ModItems.paladium), new ItemStack(ModItems.paladium), new ItemStack(ModItems.smallRing, 1));
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModItems.mediumRing), new ItemStack(ModItems.paladium), new ItemStack(ModItems.paladium), new ItemStack(ModItems.paladium), new ItemStack(ModItems.paladium), new ItemStack(ModItems.mediumRing, 1));
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModItems.bigRing), new ItemStack(ModItems.paladium), new ItemStack(ModItems.paladium), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.bigRing, 1));
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModItems.speedOrb), new ItemStack(ModItems.paladiumStick), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.paladiumStick), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.speedStick, 1));
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModItems.strenghtOrb), new ItemStack(ModItems.paladiumStick), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.paladiumStick), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.strenghtStick, 1));
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModItems.jumpOrb), new ItemStack(ModItems.paladiumStick), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.paladiumStick), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.jumpStick, 1));
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModItems.healOrb), new ItemStack(ModItems.paladiumStick), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.paladiumStick), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.healStick, 1));
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModItems.healStick), new ItemStack(ModItems.strenghtStick), new ItemStack(ModItems.paladiumCore), new ItemStack(ModItems.speedStick), new ItemStack(ModItems.paladiumCore), new ItemStack(ModItems.godStick, 1));
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModItems.jumpOrb), new ItemStack(ModItems.jumpStick), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.jumpStick), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.hyperJumpStick, 1));
      PaladiumMachineRecipies.getManager().add(new ItemStack(Items.potionitem, 1, 8204), new ItemStack(ModItems.paladiumStick), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.paladiumStick), new ItemStack(ModBlocks.paladiumBlock), new ItemStack(ModItems.damageStick, 1));
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModItems.unclaimFinder, 1, 0), new ItemStack(ModItems.findium), new ItemStack(ModItems.findium), new ItemStack(ModItems.findium), new ItemStack(ModItems.findium), new ItemStack(ModItems.unclaimFinder, 1, 1));
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModItems.unclaimFinder, 1, 1), new ItemStack(ModItems.findium), new ItemStack(ModItems.findium), new ItemStack(ModItems.findium), new ItemStack(ModItems.findium), new ItemStack(ModItems.unclaimFinder, 1, 2));
      PaladiumMachineRecipies.getManager().add(new ItemStack(ModItems.unclaimFinder, 1, 2), new ItemStack(ModItems.findium), new ItemStack(ModItems.findium), new ItemStack(ModItems.findium), new ItemStack(ModItems.findium), new ItemStack(ModItems.unclaimFinder, 1, 3));
   }

   private static void furnaceRecipies() {
      GameRegistry.addSmelting(new ItemStack(ModBlocks.paladiumOre), new ItemStack(ModItems.paladium), 0.1F);
      GameRegistry.addSmelting(new ItemStack(ModBlocks.amethystOre), new ItemStack(ModItems.amethyst), 0.1F);
      GameRegistry.addSmelting(new ItemStack(ModBlocks.titaneOre), new ItemStack(ModItems.titane), 0.1F);
   }

   private static void craftingRecipies() {
      GameRegistry.addRecipe(new ItemStack(ModItems.itemshardrune, 1), new Object[]{"Y", 'Y', new ItemStack(ModItems.itemrune)});
      GameRegistry.addRecipe(new ItemStack(ModItems.itemshardrune2, 1), new Object[]{"Y", 'Y', new ItemStack(ModItems.itemrune2)});
      GameRegistry.addRecipe(new ItemStack(ModItems.itemshardrune3, 1), new Object[]{"Y", 'Y', new ItemStack(ModItems.itemrune3)});
      GameRegistry.addRecipe(new ItemStack(ModItems.itemshardrune4, 1), new Object[]{"Y", 'Y', new ItemStack(ModItems.itemrune4)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paternPickaxe, 1), new Object[]{"YXY", "XZX", "YXY", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(ModItems.titanePickaxe), 'Y', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.compressor, 1), new Object[]{"YXY", "AZA", "BYB", 'X', new ItemStack(ModBlocks.paladiumBlock), 'Z', new ItemStack(ModItems.compressedPaladium), 'Y', new ItemStack(Blocks.cobblestone), 'A', new ItemStack(ModBlocks.titaneBlock), 'B', new ItemStack(ModBlocks.amethystBlock)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paternSword, 1), new Object[]{"YXY", "XZX", "YXY", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(ModItems.titaneSword), 'Y', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paternFastSword, 1), new Object[]{"YXY", "XZX", "YXY", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(ModItems.titaneFastSword), 'Y', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paternBroadsword, 1), new Object[]{"YXY", "XZX", "YXY", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(ModItems.titaneBroadsword), 'Y', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paternHammer, 1), new Object[]{"YXY", "XZX", "YXY", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(ModItems.titaneHammer), 'Y', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paternIngot, 1), new Object[]{"YXY", "XZX", "YXY", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(ModItems.titane), 'Y', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paternShovel, 1), new Object[]{"YXY", "XZX", "YXY", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(ModItems.titaneShovel), 'Y', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paternAxe, 1), new Object[]{"YXY", "XZX", "YXY", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(ModItems.titaneAxe), 'Y', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paternSocket, 1), new Object[]{"YXY", "X X", "YXY", 'X', new ItemStack(ModItems.paladium), 'Y', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paladiumHelmet), new Object[]{"XXX", "X X", 'X', new ItemStack(ModItems.paladium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paladiumChestplate), new Object[]{"X X", "XXX", "XXX", 'X', new ItemStack(ModItems.paladium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paladiumLeggings), new Object[]{"XXX", "X X", "X X", 'X', new ItemStack(ModItems.paladium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paladiumBoots), new Object[]{"X X", "X X", 'X', new ItemStack(ModItems.paladium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.compressedPaladium), new Object[]{"XXX", "XYX", "XXX", 'Y', ModItems.paladium, 'X', new ItemStack(ModBlocks.paladiumBlock)});
      GameRegistry.addRecipe(new ItemStack(ModItems.extrapolatedBucket), new Object[]{"X X", " X ", 'X', new ItemStack(ModItems.paladium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paladiumStick, 4), new Object[]{"X", "X", 'X', new ItemStack(ModItems.paladium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.travelLeggings, 1), new Object[]{"XXX", "Y Y", "Z Z", 'X', new ItemStack(Items.leather), 'Y', new ItemStack(Items.feather), 'Z', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.travelBoots, 1), new Object[]{"X X", "Y Y", 'X', new ItemStack(Items.feather), 'Y', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paladiumApple, 1), new Object[]{"XXX", "XYX", "XXX", 'X', new ItemStack(ModItems.paladium), 'Y', new ItemStack(Items.apple)});
      GameRegistry.addRecipe(new ItemStack(ModItems.infernalKnocker, 1), new Object[]{"X", "Z", "Y", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(ModItems.knockbackOrb), 'Y', new ItemStack(ModItems.paladiumStick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.amethystPickaxe, 1), new Object[]{"XXX", " Z ", " Z ", 'X', new ItemStack(ModItems.amethyst), 'Z', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.titanePickaxe, 1), new Object[]{"XXX", " Y ", " Y ", 'X', new ItemStack(ModItems.titane), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.titaneAxe, 1), new Object[]{" XX", " ZX", " Z ", 'X', new ItemStack(ModItems.titane), 'Z', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.titaneAxe, 1), new Object[]{"XX ", "XZ ", " Z ", 'X', new ItemStack(ModItems.titane), 'Z', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.amethystAxe, 1), new Object[]{" XX", " ZX", " Z ", 'X', new ItemStack(ModItems.amethyst), 'Z', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.amethystAxe, 1), new Object[]{"XX ", "XZ ", " Z ", 'X', new ItemStack(ModItems.amethyst), 'Z', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.amethystSword, 1), new Object[]{"X", "X", "Y", 'X', new ItemStack(ModItems.amethyst), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.titaneSword, 1), new Object[]{"X", "X", "Y", 'X', new ItemStack(ModItems.titane), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.titaneShovel, 1), new Object[]{"X", "Z", "Z", 'X', new ItemStack(ModItems.titane), 'Z', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.amethystShovel, 1), new Object[]{"X", "Z", "Z", 'X', new ItemStack(ModItems.amethyst), 'Z', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.titaneBroadsword, 1), new Object[]{"XX", "XX", "Y ", 'X', new ItemStack(ModItems.titane), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.titaneStick, 1), new Object[]{"X", "X", 'X', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.amethystStick, 1), new Object[]{"X", "X", 'X', new ItemStack(ModItems.amethyst)});
      GameRegistry.addRecipe(new ItemStack(ModItems.amethystBroadsword, 1), new Object[]{"XX", "XX", "Y ", 'X', new ItemStack(ModItems.amethyst), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.amethystFastSword, 1), new Object[]{"X", "X", "Y", 'X', new ItemStack(ModItems.amethystStick), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.titaneFastSword, 1), new Object[]{"X", "X", "Y", 'X', new ItemStack(ModItems.titaneStick), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.diamondString, 9), new Object[]{"XYX", "XYX", "XYX", 'X', new ItemStack(Items.string), 'Y', new ItemStack(Items.diamond)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paladiumBow, 1), new Object[]{" XY", "X Y", " XY", 'X', new ItemStack(ModItems.paladiumStick), 'Y', new ItemStack(ModItems.diamondString)});
      GameRegistry.addRecipe(new ItemStack(ModItems.titaneHelmet), new Object[]{"XXX", "X X", 'X', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.titaneChestplate), new Object[]{"X X", "XXX", "XXX", 'X', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.titaneLeggings), new Object[]{"XXX", "X X", "X X", 'X', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.titaneBoots), new Object[]{"X X", "X X", 'X', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.amethystHelmet), new Object[]{"XXX", "X X", 'X', new ItemStack(ModItems.amethyst)});
      GameRegistry.addRecipe(new ItemStack(ModItems.amethystChestplate), new Object[]{"X X", "XXX", "XXX", 'X', new ItemStack(ModItems.amethyst)});
      GameRegistry.addRecipe(new ItemStack(ModItems.amethystLeggings), new Object[]{"XXX", "X X", "X X", 'X', new ItemStack(ModItems.amethyst)});
      GameRegistry.addRecipe(new ItemStack(ModItems.amethystBoots), new Object[]{"X X", "X X", 'X', new ItemStack(ModItems.amethyst)});
      GameRegistry.addRecipe(new ItemStack(ModItems.jumpChest, 1), new Object[]{"X X", "YZY", "YZY", 'X', new ItemStack(Items.feather), 'Z', new ItemStack(Items.iron_ingot), 'Y', new ItemStack(Items.leather)});
      GameRegistry.addRecipe(new ItemStack(ModItems.amethystHammer, 1), new Object[]{"XXX", "XXX", " Y ", 'X', new ItemStack(ModItems.amethyst), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.titaneHammer, 1), new Object[]{"XXX", "XXX", " Y ", 'X', new ItemStack(ModItems.titane), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.slimyHelmet, 1), new Object[]{"XXX", "Y Y", 'X', new ItemStack(Items.slime_ball), 'Y', new ItemStack(Items.diamond)});
      GameRegistry.addRecipe(new ItemStack(ModItems.scubaHelmet, 1), new Object[]{"XXX", "XZX", "XXX", 'X', new ItemStack(ModItems.titane), 'Z', new ItemStack(Blocks.glass_pane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.hoodHelmet, 1), new Object[]{"XXX", "XZX", "XXX", 'X', new ItemStack(Blocks.wool), 'Z', new ItemStack(ModItems.paladiumCore)});
      GameRegistry.addRecipe(new ItemStack(ModItems.wing, 2), new Object[]{"  X", " XZ", "XZZ", 'X', new ItemStack(ModItems.paladiumStick), 'Z', new ItemStack(Items.leather)});
      GameRegistry.addRecipe(new ItemStack(ModItems.hangGlider, 1), new Object[]{" Y ", "XYX", 'X', new ItemStack(ModItems.wing), 'Y', new ItemStack(Items.leather)});
      GameRegistry.addRecipe(new ItemStack(ModItems.furnaceUpgrade, 4), new Object[]{" X ", "XUX", " X ", 'X', new ItemStack(ModItems.paladium), 'U', new ItemStack(Items.diamond)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianStone, 1), new Object[]{" X ", "XYX", " X ", 'X', new ItemStack(Blocks.stone), 'Y', new ItemStack(ModItems.compressedPaladium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.voidStone, 1), new Object[]{" X ", "XZX", " X ", 'X', new ItemStack(ModItems.amethyst), 'Z', new ItemStack(Blocks.chest)});
      GameRegistry.addRecipe(new ItemStack(ModItems.chestExplorer, 1), new Object[]{"XYX", "XZX", "XYX", 'X', new ItemStack(ModItems.findium), 'Z', new ItemStack(ModItems.compressedPaladium), 'Y', new ItemStack(ModItems.paladium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.stuffSwitcher, 1), new Object[]{" X ", "YZY", " X ", 'X', new ItemStack(ModItems.titane), 'Z', new ItemStack(ModItems.amethystChestplate), 'Y', new ItemStack(ModItems.paladiumCore)});
      GameRegistry.addRecipe(new ItemStack(ModItems.dynamite, 4), new Object[]{"X", "Z", "Z", 'X', new ItemStack(ModItems.diamondString), 'Z', new ItemStack(Blocks.tnt)});
      GameRegistry.addRecipe(new ItemStack(ModItems.dynamiteNinja, 1), new Object[]{"XYX", "XYX", "XYX", 'X', new ItemStack(ModItems.titane), 'Y', new ItemStack(ModItems.dynamite)});
      GameRegistry.addRecipe(new ItemStack(ModItems.dynamiteBig, 1), new Object[]{"XXX", 'X', new ItemStack(ModItems.dynamite)});
      GameRegistry.addRecipe(new ItemStack(ModItems.backpack, 1), new Object[]{"XXX", "YZY", "YYY", 'X', new ItemStack(ModItems.paladiumStick), 'Z', new ItemStack(Blocks.chest), 'Y', new ItemStack(Items.leather)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianWhitelist, 1), new Object[]{"XXX", "XZX", "XXX", 'X', new ItemStack(Items.paper), 'Z', new ItemStack(ModItems.guardianStone)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianXpUpgrade, 1), new Object[]{" X ", "XZX", " X ", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(ModItems.guardianStone)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianLifeUpgrade1, 1), new Object[]{"XYZ", "MVM", "ZYX", 'X', new ItemStack(ModItems.paladiumApple), 'Y', new ItemStack(ModBlocks.amethystBlock), 'Z', new ItemStack(ModBlocks.titaneBlock), 'M', new ItemStack(ModItems.paladiumCore), 'V', new ItemStack(ModItems.guardianXpUpgrade)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianLifeUpgrade2, 1), new Object[]{"XZX", "MVM", "XZX", 'X', new ItemStack(ModItems.paladiumApple), 'Z', new ItemStack(ModBlocks.titaneBlock), 'M', new ItemStack(ModItems.paladiumCore), 'V', new ItemStack(ModItems.guardianLifeUpgrade1)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianLifeUpgrade3, 1), new Object[]{"XXX", "MVM", "XXX", 'X', new ItemStack(ModItems.paladiumApple), 'Z', new ItemStack(ModBlocks.titaneBlock), 'M', new ItemStack(ModItems.paladiumCore), 'V', new ItemStack(ModItems.guardianLifeUpgrade2)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianDamageUpgrade1, 1), new Object[]{"XYX", "MVM", "XYX", 'X', new ItemStack(ModItems.amethystSword), 'Y', new ItemStack(ModBlocks.amethystBlock), 'M', new ItemStack(ModItems.paladiumCore), 'V', new ItemStack(ModItems.guardianXpUpgrade)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianDamageUpgrade2, 1), new Object[]{"XYX", "MVM", "XYX", 'X', new ItemStack(ModItems.titaneSword), 'Y', new ItemStack(ModBlocks.titaneBlock), 'M', new ItemStack(ModItems.paladiumCore), 'V', new ItemStack(ModItems.guardianDamageUpgrade1)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianDamageUpgrade3, 1), new Object[]{"XYX", "MVM", "XYX", 'X', new ItemStack(ModItems.paladiumSword), 'Y', new ItemStack(ModBlocks.paladiumBlock), 'M', new ItemStack(ModItems.paladiumCore), 'V', new ItemStack(ModItems.guardianDamageUpgrade2)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianPeneUpgrade1, 1), new Object[]{"XYX", "MVM", "XYX", 'X', new ItemStack(ModItems.amethystChestplate), 'Y', new ItemStack(ModItems.amethyst), 'M', new ItemStack(ModItems.paladiumCore), 'V', new ItemStack(ModItems.guardianXpUpgrade)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianPeneUpgrade2, 1), new Object[]{"XYX", "MVM", "XYX", 'X', new ItemStack(ModItems.titaneChestplate), 'Y', new ItemStack(ModItems.titane), 'M', new ItemStack(ModItems.paladiumCore), 'V', new ItemStack(ModItems.guardianPeneUpgrade1)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianPeneUpgrade3, 1), new Object[]{"XYX", "MVM", "XYX", 'X', new ItemStack(ModItems.paladiumChestplate), 'Y', new ItemStack(ModItems.paladium), 'M', new ItemStack(ModItems.paladiumCore), 'V', new ItemStack(ModItems.guardianPeneUpgrade2)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianExplosiveUpgrade, 1), new Object[]{"XXX", "MVM", "XXX", 'X', new ItemStack(Blocks.obsidian), 'M', new ItemStack(ModItems.paladiumCore), 'V', new ItemStack(ModItems.guardianXpUpgrade)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianThornsUpgrade1, 1), new Object[]{"XXX", "MVM", "XXX", 'X', new ItemStack(ModBlocks.spikeAmethyst), 'M', new ItemStack(ModItems.paladiumCore), 'V', new ItemStack(ModItems.guardianXpUpgrade)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianThornsUpgrade2, 1), new Object[]{"XXX", "MVM", "XXX", 'X', new ItemStack(ModBlocks.spikeTitane), 'M', new ItemStack(ModItems.paladiumCore), 'V', new ItemStack(ModItems.guardianThornsUpgrade1)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianThornsUpgrade3, 1), new Object[]{"XXX", "MVM", "XXX", 'X', new ItemStack(ModBlocks.spikePaladium), 'M', new ItemStack(ModItems.paladiumCore), 'V', new ItemStack(ModItems.guardianThornsUpgrade2)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianWeaponUpgrade, 1), new Object[]{"XXX", "MVM", "XXX", 'X', new ItemStack(ModBlocks.paladiumBlock), 'V', new ItemStack(ModItems.paladiumSword), 'M', new ItemStack(ModItems.guardianXpUpgrade)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianFarmUpgrade, 1), new Object[]{"XYZ", "MVM", "ZYX", 'X', new ItemStack(ModItems.paladiumHammer), 'Y', new ItemStack(ModBlocks.titaneBlock), 'Z', new ItemStack(ModItems.paladiumPickaxe), 'M', new ItemStack(ModBlocks.amethystBlock), 'V', new ItemStack(ModItems.guardianXpUpgrade)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianAutoXPUpgrade, 1), new Object[]{"XXX", "MVM", "XXX", 'X', new ItemStack(ModItems.experienceBerry), 'V', new ItemStack(ModItems.paladiumSword), 'M', new ItemStack(ModItems.guardianXpUpgrade)});
      GameRegistry.addRecipe(new ItemStack(ModItems.healOrb, 1), new Object[]{" X ", "XZX", " X ", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(Items.speckled_melon)});
      GameRegistry.addRecipe(new ItemStack(ModItems.strenghtOrb, 1), new Object[]{" X ", "XZX", " X ", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(Items.blaze_powder)});
      GameRegistry.addRecipe(new ItemStack(ModItems.jumpOrb, 1), new Object[]{" X ", "XZX", " X ", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(Items.nether_wart)});
      GameRegistry.addRecipe(new ItemStack(ModItems.healOrb, 1), new Object[]{" X ", "XZX", " X ", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(Items.book)});
      GameRegistry.addRecipe(new ItemStack(ModItems.potionLauncher, 1), new Object[]{"XXX", "ZYY", "XXX", 'X', new ItemStack(ModItems.amethyst), 'Z', new ItemStack(ModItems.paladiumCore), 'Y', new ItemStack(Items.glass_bottle)});
      GameRegistry.addRecipe(new ItemStack(ModItems.bowModifer, 1), new Object[]{" X ", "XYX", " X ", 'X', new ItemStack(ModItems.paladiumStick), 'Y', new ItemStack(Items.paper)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paladiumCore, 1), new Object[]{"YXY", "XZX", "YXY", 'X', new ItemStack(ModItems.paladium), 'Y', new ItemStack(ModItems.titane), 'Z', new ItemStack(ModItems.paladium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.speedModifier, 1), new Object[]{" X ", "XZX", " X ", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(ModItems.speedOrb)});
      GameRegistry.addRecipe(new ItemStack(ModItems.smeltModifier, 1), new Object[]{" X ", "XZX", " X ", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(Items.blaze_rod)});
      GameRegistry.addRecipe(new ItemStack(ModItems.knockbackModifier, 1), new Object[]{" X ", "XZX", " X ", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(ModItems.knockbackOrb)});
      GameRegistry.addRecipe(new ItemStack(ModItems.knockbackOrb, 1), new Object[]{" X ", "XZX", " X ", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(Items.ghast_tear)});
      GameRegistry.addRecipe(new ItemStack(ModItems.fortuneModifier, 1), new Object[]{" X ", "XZX", " X ", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(Blocks.gold_block)});
      GameRegistry.addRecipe(new ItemStack(ModItems.flameModifier, 1), new Object[]{" X ", "XZX", " X ", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(Items.flint_and_steel)});
      GameRegistry.addRecipe(new ItemStack(ModItems.damageModifier, 1), new Object[]{" X ", "XZX", " X ", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(Items.mushroom_stew)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paladiumCore, 1), new Object[]{"YXY", "XZX", "YXY", 'X', new ItemStack(ModItems.paladium), 'Y', new ItemStack(ModItems.titane), 'Z', new ItemStack(ModItems.paladium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paladiumAxe, 1), new Object[]{" XX", " YX", " Y ", 'X', new ItemStack(ModItems.paladium), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.MagicalAnvilTop, 1), new Object[]{"XYX", "YZY", "XYX", 'X', new ItemStack(ModBlocks.paladiumBlock), 'Y', new ItemStack(ModItems.findium), 'Z', new ItemStack(ModItems.compressedPaladium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.MagicalAnvilMiddle, 1), new Object[]{"XXX", "XYX", "XXX", 'X', new ItemStack(Blocks.cobblestone), 'Y', new ItemStack(ModItems.findium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.MagicalAnvilBottom, 1), new Object[]{"   ", " X ", "XYX", 'X', new ItemStack(Blocks.cobblestone), 'Y', new ItemStack(ModItems.findium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paladiumAxe, 1), new Object[]{"XX ", "XY ", " Y ", 'X', new ItemStack(ModItems.paladium), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paladiumPickaxe, 1), new Object[]{"XXX", " Y ", " Y ", 'X', new ItemStack(ModItems.paladium), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paladiumShovel, 1), new Object[]{"X", "Y", "Y", 'X', new ItemStack(ModItems.paladium), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paladiumSword, 1), new Object[]{"X", "X", "Y", 'X', new ItemStack(ModItems.paladium), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.bucketSulfuric, 1), new Object[]{"Y", "X", 'X', new ItemStack(Items.water_bucket), 'Y', new ItemStack(Items.potionitem, 1, 8268), 'Y', new ItemStack(ModItems.paladiumStick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.bucketAngelic, 1), new Object[]{"Y", "X", 'X', new ItemStack(Items.water_bucket), 'Y', new ItemStack(Items.potionitem, 1, 8193)});
      GameRegistry.addRecipe(new ItemStack(ModItems.speedOrb, 1), new Object[]{" X ", "XZX", " X ", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(Items.sugar)});
      GameRegistry.addRecipe(new ItemStack(ModItems.unclaimFinder, 1), new Object[]{" X ", "YZY", " X ", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(ModItems.findium), 'Y', new ItemStack(ModItems.paladiumCore)});
      GameRegistry.addShapelessRecipe(new ItemStack(ModItems.witherSkullFragment, 9), new Object[]{new ItemStack(Items.skull, 1, 1)});
      GameRegistry.addShapelessRecipe(new ItemStack(ModItems.titane, 9), new Object[]{new ItemStack(ModBlocks.titaneBlock, 1)});
      GameRegistry.addShapelessRecipe(new ItemStack(ModItems.paladium, 9), new Object[]{new ItemStack(ModBlocks.paladiumBlock, 1)});
      GameRegistry.addShapelessRecipe(new ItemStack(ModItems.amethyst, 9), new Object[]{new ItemStack(ModBlocks.amethystBlock, 1)});
      GameRegistry.addShapelessRecipe(new ItemStack(ModItems.bowSpeedModifier, 1), new Object[]{new ItemStack(ModItems.bowModifer, 1), new ItemStack(Items.potionitem, 1, 8258)});
      GameRegistry.addShapelessRecipe(new ItemStack(ModItems.bowRangeModifier, 1), new Object[]{new ItemStack(ModItems.bowModifer, 1), new ItemStack(Items.potionitem, 1, 8201)});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.paladiumBlock), new Object[]{"XXX", "XXX", "XXX", 'X', new ItemStack(ModItems.paladium)});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.titaneBlock), new Object[]{"XXX", "XXX", "XXX", 'X', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.amethystBlock), new Object[]{"XXX", "XXX", "XXX", 'X', new ItemStack(ModItems.amethyst)});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.paladiumMachine), new Object[]{"XXX", "XYX", "XXX", 'Y', ModItems.compressedPaladium, 'X', new ItemStack(Blocks.stone)});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.paladiumFurnace), new Object[]{"XXX", "X X", "XXX", 'X', new ItemStack(ModItems.paladium)});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.paladiumChest), new Object[]{"XXX", "Y Y", "XXX", 'Y', ModItems.compressedPaladium, 'X', new ItemStack(ModItems.paladium)});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.bowMachine), new Object[]{"XXX", "XYX", "XXX", 'Y', ModItems.compressedPaladium, 'X', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.alchemyCreator), new Object[]{"YUY", "XZX", "XXX", 'Y', Items.blaze_powder, 'U', new ItemStack(ModItems.compressedPaladium), 'X', ModItems.titane, 'Z', ModItems.paladiumCore});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.onlineDetector), new Object[]{"YYY", "YXY", "ZZZ", 'Y', Items.dye, 'U', new ItemStack(ModItems.witherSkullFragment), 'X', ModItems.paladium, 'Z', ModItems.titane});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.paladiumGrinder), new Object[]{"YYY", "YXY", "YYY", 'Y', ModItems.paladium, 'X', new ItemStack(ModBlocks.paladiumFurnace)});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.grinderCasing), new Object[]{"Y Y", "Y Y", "Y Y", 'Y', ModItems.paladium});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.grinderFrame), new Object[]{"YZY", "   ", "YZY", 'Y', ModItems.paladium, 'Z', ModItems.titane});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.guardianBlock), new Object[]{"YYY", "YXY", "YYY", 'X', ModItems.guardianStone, 'Y', ModItems.titane});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.magicalAnvil), new Object[]{" Y ", " X ", " Z ", 'Y', ModItems.MagicalAnvilTop, 'X', ModItems.MagicalAnvilMiddle, 'Z', ModItems.MagicalAnvilBottom});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.guardianKeeper), new Object[]{"YYY", "ZXZ", "YYY", 'Y', ModItems.paladium, 'Z', new ItemStack(ModItems.compressedPaladium), 'X', ModItems.guardianStone});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.spikeIron), new Object[]{" Y ", "YXY", "XXX", 'Y', Items.wooden_sword, 'X', Items.iron_ingot});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.spikeAmethyst), new Object[]{" Y ", "YXY", "XXX", 'Y', Items.diamond_sword, 'X', ModItems.amethyst});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.spikeDiamond), new Object[]{" Y ", "YXY", "XXX", 'Y', Items.golden_sword, 'X', Items.diamond});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.spikeGold), new Object[]{" Y ", "YXY", "XXX", 'Y', Items.golden_sword, 'X', Items.gold_ingot});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.spikePaladium), new Object[]{" Y ", "YXY", "XXX", 'Y', ModItems.titaneSword, 'X', ModItems.paladium});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.spikeTitane), new Object[]{" Y ", "YXY", "XXX", 'Y', ModItems.amethystSword, 'X', ModItems.titane});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.spikeWood), new Object[]{" Y ", "YXY", "XXX", 'Y', Items.wooden_sword, 'X', Blocks.planks});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.slimePad), new Object[]{"XXX", "XXX", 'X', Items.slime_ball});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.slowBlock), new Object[]{"Y", "X", 'Y', Blocks.web, 'X', Blocks.soul_sand});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.cave), new Object[]{"XXX", "XYX", "XXX", 'X', new ItemStack(Blocks.glass), 'Y', new ItemStack(ModItems.findium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.camera_tablet), new Object[]{"X X", "XYX", 'X', new ItemStack(ModBlocks.titaneBlock), 'Y', new ItemStack(ModItems.findium)});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.camera), new Object[]{"XXX", "YZ ", "XXX", 'X', new ItemStack(ModBlocks.titaneBlock), 'Y', new ItemStack(ModBlocks.paladiumBlock), 'Z', new ItemStack(ModItems.compressedPaladium)});
      GameRegistry.addRecipe(new ItemStack(ModItems.moreUpgrade), new Object[]{" X ", "XYX", " X ", 'X', new ItemStack(ModItems.paladium), 'Y', new ItemStack(ModItems.paladiumHammer)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianWand), new Object[]{"X  ", " Y ", "  X", 'X', new ItemStack(Items.stick), 'Y', new ItemStack(ModItems.guardianStone)});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.obsidian_upgrader), new Object[]{"XYX", "YZY", "XYX", 'X', new ItemStack(Blocks.obsidian), 'Y', new ItemStack(ModItems.paladiumCore), 'Z', new ItemStack(Items.lava_bucket)});
      GameRegistry.addRecipe(new ItemStack(ModItems.twoLife_obsidian_upgrade), new Object[]{"XYX", "WZW", "XIX", 'X', new ItemStack(Blocks.obsidian), 'Y', new ItemStack(ModBlocks.paladiumBlock), 'Z', new ItemStack(ModItems.compressedTitane), 'W', new ItemStack(Items.ender_eye), 'I', new ItemStack(ModItems.paladiumCore)});
      GameRegistry.addRecipe(new ItemStack(ModItems.explode_obsidian_upgrade), new Object[]{"XYX", "WZW", "XIX", 'X', new ItemStack(Blocks.obsidian), 'Y', new ItemStack(ModBlocks.paladiumBlock), 'Z', new ItemStack(ModItems.compressedTitane), 'W', new ItemStack(ModItems.dynamiteBig), 'I', new ItemStack(ModItems.paladiumCore)});
      GameRegistry.addRecipe(new ItemStack(ModItems.camouflage_obsidian_upgrade), new Object[]{"XYX", "WZW", "XIX", 'X', new ItemStack(Blocks.obsidian), 'Y', new ItemStack(ModBlocks.paladiumBlock), 'Z', new ItemStack(ModItems.compressedTitane), 'W', new ItemStack(ModBlocks.amethystBlock), 'I', new ItemStack(ModItems.paladiumCore)});
      GameRegistry.addRecipe(new ItemStack(ModItems.fake_obsidian_upgrade), new Object[]{"XYX", "WZW", "XIX", 'X', new ItemStack(ModItems.compressedTitane), 'Y', new ItemStack(ModBlocks.paladiumBlock), 'Z', new ItemStack(ModItems.compressedTitane), 'W', new ItemStack(ModBlocks.amethystBlock), 'I', new ItemStack(ModItems.paladiumCore)});
      GameRegistry.addRecipe(new ItemStack(ModItems.compressedTitane), new Object[]{"XXX", "XYX", "XXX", 'X', new ItemStack(ModBlocks.titaneBlock), 'Y', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModBlocks.turret), new Object[]{" X ", "XYX", "Z Z", 'X', new ItemStack(ModItems.paladium), 'Y', new ItemStack(ModItems.compressedPaladium), 'Z', new ItemStack(ModItems.compressedTitane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.autoRepairModifier), new Object[]{" X ", "XYX", " X ", 'X', new ItemStack(ModItems.paladium), 'Y', new ItemStack(ModItems.titaneHammer)});
      GameRegistry.addRecipe(new ItemStack(ModItems.obsidianUpgrade), new Object[]{" X ", "XYX", " X ", 'X', new ItemStack(ModItems.paladium), 'Y', new ItemStack(Blocks.obsidian)});
      GameRegistry.addRecipe(new ItemStack(ModItems.paternBlock, 1), new Object[]{"YXY", "XZX", "YXY", 'X', new ItemStack(ModItems.paladium), 'Z', new ItemStack(Blocks.obsidian), 'Y', new ItemStack(ModItems.titane)});
      GameRegistry.addRecipe(new ItemStack(ModItems.unSummoner, 1), new Object[]{" X", "Y ", 'X', new ItemStack(Items.skull), 'Y', new ItemStack(Items.stick)});
      GameRegistry.addRecipe(new ItemStack(ModItems.guardianLifeUpgrade2, 1), new Object[]{"XZX", "MMM", "XZX", 'X', new ItemStack(ModItems.paladiumApple), 'Z', new ItemStack(ModBlocks.titaneBlock), 'M', new ItemStack(ModItems.guardianStone), 'V', new ItemStack(ModItems.guardianLifeUpgrade1)});
   }
}
