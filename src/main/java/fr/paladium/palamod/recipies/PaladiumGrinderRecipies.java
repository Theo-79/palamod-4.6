package fr.paladium.palamod.recipies;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class PaladiumGrinderRecipies {
   private static final PaladiumGrinderRecipies instance = new PaladiumGrinderRecipies();
   private Map<ItemStack, Integer> smeltingAmmount = new HashMap();
   private Map smeltingList = new HashMap();
   private Map upgradeList = new HashMap();
   private Map upgradeResult = new HashMap();

   public void add(ItemStack stack1, ItemStack stack2, ItemStack stack, int ammount) {
      ItemStack[] stackList = new ItemStack[]{stack1, stack2};
      this.smeltingAmmount.put(stack, ammount);
      this.smeltingList.put(stackList, stack);
   }

   public void addUpgrade(Item tool, Item upgrade, int type) {
      this.upgradeList.put(upgrade, tool);
      this.upgradeResult.put(upgrade, type);
   }

   public ItemStack getSmeltingResult(ItemStack[] stack) {
      Iterator iterator = this.smeltingList.entrySet().iterator();

      while(iterator.hasNext()) {
         Entry entry = (Entry)iterator.next();
         if (this.isSameKey(stack, (ItemStack[])((ItemStack[])entry.getKey()))) {
            return (ItemStack)entry.getValue();
         }
      }

      return null;
   }

   private boolean isSameKey(ItemStack[] stackList, ItemStack[] stackList2) {
      boolean isSame = false;

      for(int i = 0; i <= 1; ++i) {
         if (stackList[i].getItem() != stackList2[i].getItem()) {
            return false;
         }

         isSame = true;
      }

      return isSame;
   }

   public boolean isUpgradable(Item tool, Item upgrade) {
      return this.upgradeList.containsKey(upgrade);
   }

   public int getUpgrade(Item upgrade) {
      return (Integer)this.upgradeResult.get(upgrade);
   }

   public Map getSmeltingList() {
      return this.smeltingList;
   }

   public static PaladiumGrinderRecipies getManager() {
      return instance;
   }

   public int getSmeltingAmmount(ItemStack stack) {
      return (Integer)this.smeltingAmmount.get(stack);
   }

   public Map<ItemStack, Integer> getSmeltingAmmount() {
      return this.smeltingAmmount;
   }
}
