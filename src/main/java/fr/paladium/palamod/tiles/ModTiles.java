package fr.paladium.palamod.tiles;

import cpw.mods.fml.common.registry.GameRegistry;

public class ModTiles {
   public static void init() {
      GameRegistry.registerTileEntityWithAlternatives(TileEntityPaladiumMachine.class, "palamod:paladiummachine", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityAlchemyCreator.class, "palamod:alchemycreator", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityPaladiumFurnace.class, "palamod:paladiumfurnace", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityPaladiumChest.class, "palamod:paladiumchest", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityBowMachine.class, "palamod:bowmachine", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityAlchemyStacker.class, "palamod:alchemystacker", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityOnlineDetector.class, "palamod:onlinedetector", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityPaladiumGrinder.class, "palamod:paladiumgrinder", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityPaladiumOven.class, "palamod:paladiumoven", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityMagicalAnvil.class, "palamod:palamagicalanvil", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityArmorCompressor.class, "palamodpalacompressor", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityMulticolorBlock.class, "palamod:mutlicolorblock", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityObsidianUpgrader.class, "palamod:obsidianUpgrader", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityUpgradedObsidian.class, "palamod:upgradedObsidian", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityCamera.class, "palamod:camera", new String[0]);
      GameRegistry.registerTileEntityWithAlternatives(TileEntityTurret.class, "palamod:turret", new String[0]);
   }
}
