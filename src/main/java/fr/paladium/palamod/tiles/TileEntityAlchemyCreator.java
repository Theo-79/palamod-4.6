package fr.paladium.palamod.tiles;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.recipies.AlchemyCreatorArrowRecipies;
import fr.paladium.palamod.recipies.AlchemyCreatorPotionRecipies;
import java.util.HashMap;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;

public class TileEntityAlchemyCreator extends TileEntity implements IInventory, ISidedInventory {
   public ItemStack[] content = new ItemStack[9];
   private int[] workingTime = new int[2];
   private int[] timeNeeded = new int[2];
   public static int POTION;
   public static int ARROW;
   public static HashMap<EntityPlayer, TileEntityAlchemyCreator> oppenedGui = new HashMap();

   public TileEntityAlchemyCreator() {
      this.timeNeeded[0] = 200;
      this.timeNeeded[1] = 20;
   }

   public int getSizeInventory() {
      return this.content.length;
   }

   public ItemStack getStackInSlot(int slot) {
      return this.content[slot];
   }

   public ItemStack decrStackSize(int slotIndex, int amount) {
      if (this.content[slotIndex] != null) {
         ItemStack itemstack;
         if (this.content[slotIndex].stackSize <= amount) {
            itemstack = this.content[slotIndex];
            this.content[slotIndex] = null;
            this.markDirty();
            return itemstack;
         } else {
            itemstack = this.content[slotIndex].splitStack(amount);
            if (this.content[slotIndex].stackSize == 0) {
               this.content[slotIndex] = null;
            }

            this.markDirty();
            return itemstack;
         }
      } else {
         return null;
      }
   }

   public ItemStack getStackInSlotOnClosing(int slotIndex) {
      if (this.content[slotIndex] != null) {
         ItemStack itemstack = this.content[slotIndex];
         this.content[slotIndex] = null;
         return itemstack;
      } else {
         return null;
      }
   }

   public void setInventorySlotContents(int slotIndex, ItemStack stack) {
      this.content[slotIndex] = stack;
      if (stack != null && stack.stackSize > this.getInventoryStackLimit()) {
         stack.stackSize = this.getInventoryStackLimit();
      }

      this.markDirty();
   }

   public String getInventoryName() {
      return "TileEntity.AlchemyCreator";
   }

   public boolean hasCustomInventoryName() {
      return false;
   }

   public int getInventoryStackLimit() {
      return 64;
   }

   public boolean isUseableByPlayer(EntityPlayer player) {
      return !(this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) instanceof TileEntityAlchemyCreator) ? false : player.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
   }

   public void openInventory() {
   }

   public void closeInventory() {
   }

   public boolean isItemValidForSlot(int slot, ItemStack stack) {
      return true;
   }

   public void writeToNBT(NBTTagCompound compound) {
      super.writeToNBT(compound);
      NBTTagList nbttaglist = new NBTTagList();

      for(int i = 0; i < this.content.length; ++i) {
         if (this.content[i] != null) {
            NBTTagCompound nbttagcompound1 = new NBTTagCompound();
            nbttagcompound1.setByte("Slot", (byte)i);
            this.content[i].writeToNBT(nbttagcompound1);
            nbttaglist.appendTag(nbttagcompound1);
         }
      }

      compound.setTag("Items", nbttaglist);
      compound.setInteger("workingTimePotion", this.workingTime[0]);
      compound.setInteger("workingTimeArrow", this.timeNeeded[1]);
   }

   public void readFromNBT(NBTTagCompound compound) {
      super.readFromNBT(compound);
      NBTTagList nbttaglist = compound.getTagList("Items", 10);
      this.content = new ItemStack[this.getSizeInventory()];

      for(int i = 0; i < nbttaglist.tagCount(); ++i) {
         NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
         int j = nbttagcompound1.getByte("Slot") & 255;
         if (j >= 0 && j < this.content.length) {
            this.content[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
         }
      }

      this.workingTime[0] = compound.getInteger("workingTimePotion");
      this.timeNeeded[1] = compound.getInteger("workingTimeArrow");
   }

   public void updateEntity() {
      if (!this.canSmeltPotion()) {
         this.workingTime[0] = 0;
      }

      int var10002;
      if (this.isBurningPotion() && this.canSmeltPotion()) {
         var10002 = this.workingTime[0]++;
      }

      if (this.canSmeltPotion() && !this.isBurningPotion()) {
         this.workingTime[0] = 1;
      }

      if (this.canSmeltPotion() && this.workingTime[0] == this.timeNeeded[0]) {
         this.smeltItemPotion();
         this.workingTime[0] = 0;
      }

      if (!this.canSmeltArrow()) {
         this.workingTime[1] = 0;
      }

      if (this.isBurningArrow() && this.canSmeltArrow()) {
         var10002 = this.workingTime[1]++;
      }

      if (this.canSmeltArrow() && !this.isBurningArrow()) {
         this.workingTime[1] = 1;
      }

      if (this.canSmeltArrow() && this.workingTime[1] == this.timeNeeded[1]) {
         this.smeltItemArrow();
         this.workingTime[1] = 0;
      }

   }

   private boolean canSmeltPotion() {
      if (this.content[0] != null && this.content[1] != null && this.content[2] != null && this.content[3] != null) {
         ItemStack itemstack = AlchemyCreatorPotionRecipies.getManager().getSmeltingResult(new ItemStack[]{this.content[0], this.content[1], this.content[2], this.content[3]});
         return itemstack != null;
      } else {
         return false;
      }
   }

   private boolean canSmeltArrow() {
      if (this.content[4] != null && this.content[5] != null && this.content[6] != null && this.content[7] != null) {
         ItemStack itemstack = AlchemyCreatorArrowRecipies.getManager().getSmeltingResult(new ItemStack[]{this.content[4], this.content[5], this.content[6], this.content[7]});
         if (itemstack == null) {
            return false;
         } else if (this.content[8] != null && this.content[8].stackSize > this.getInventoryStackLimit() - itemstack.stackSize) {
            return false;
         } else if (this.content[8] != null && itemstack.getItem() != this.content[8].getItem()) {
            return false;
         } else {
            return this.content[7].stackSize >= itemstack.stackSize;
         }
      } else {
         return false;
      }
   }

   public boolean isBurningPotion() {
      return this.workingTime[0] > 0;
   }

   public boolean isBurningArrow() {
      return this.workingTime[1] > 0;
   }

   public void smeltItemPotion() {
      if (this.canSmeltPotion()) {
         ItemStack itemstack = AlchemyCreatorPotionRecipies.getManager().getSmeltingResult(new ItemStack[]{this.content[0], this.content[1], this.content[2], this.content[3]});
         this.content[3] = itemstack.copy();
         --this.content[0].stackSize;
         --this.content[1].stackSize;
         --this.content[2].stackSize;
         if (this.content[0].stackSize <= 0) {
            this.content[0] = null;
         }

         if (this.content[1].stackSize <= 0) {
            this.content[1] = null;
         }

         if (this.content[2].stackSize <= 0) {
            this.content[2] = null;
         }

         if (this.content[3].stackSize <= 0) {
            this.content[3] = null;
         }
      }

   }

   public void smeltItemArrow() {
      if (this.canSmeltArrow()) {
         ItemStack itemstack = AlchemyCreatorArrowRecipies.getManager().getSmeltingResult(new ItemStack[]{this.content[4], this.content[5], this.content[6], this.content[7]});
         if (this.content[8] == null) {
            this.content[8] = itemstack.copy();
         } else if (this.content[8].stackSize > 0) {
            ItemStack var10000 = this.content[8];
            var10000.stackSize += itemstack.stackSize;
         }

         --this.content[4].stackSize;
         --this.content[5].stackSize;
         --this.content[6].stackSize;
         this.content[7].stackSize -= itemstack.stackSize;
         if (this.content[4].stackSize <= 0) {
            this.content[4] = null;
         }

         if (this.content[5].stackSize <= 0) {
            this.content[5] = null;
         }

         if (this.content[6].stackSize <= 0) {
            this.content[6] = null;
         }

         if (this.content[7].stackSize <= 0) {
            this.content[7] = null;
         }
      }

   }

   @SideOnly(Side.CLIENT)
   public int getCookProgressPotion() {
      return this.workingTime[0];
   }

   @SideOnly(Side.CLIENT)
   public int getCookProgressArrow() {
      return this.workingTime[1];
   }

   public int[] getAccessibleSlotsFromSide(int side) {
      return new int[0];
   }

   public boolean canInsertItem(int side, ItemStack stack, int slot) {
      return false;
   }

   public boolean canExtractItem(int side, ItemStack stack, int slot) {
      return false;
   }
}
