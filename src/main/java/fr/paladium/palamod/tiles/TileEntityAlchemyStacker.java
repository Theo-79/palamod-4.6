package fr.paladium.palamod.tiles;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;

public class TileEntityAlchemyStacker extends TileEntity implements IInventory, ISidedInventory {
   private ItemStack[] content = new ItemStack[2];
   private int workedTime = 0;
   private int timeNeeded = 5;

   public int getSizeInventory() {
      return this.content.length;
   }

   public ItemStack getStackInSlot(int slot) {
      return this.content[slot];
   }

   public ItemStack decrStackSize(int slotIndex, int amount) {
      if (this.content[slotIndex] != null) {
         ItemStack itemstack;
         if (this.content[slotIndex].stackSize <= amount) {
            itemstack = this.content[slotIndex];
            this.content[slotIndex] = null;
            this.markDirty();
            return itemstack;
         } else {
            itemstack = this.content[slotIndex].splitStack(amount);
            if (this.content[slotIndex].stackSize == 0) {
               this.content[slotIndex] = null;
            }

            this.markDirty();
            return itemstack;
         }
      } else {
         return null;
      }
   }

   public ItemStack getStackInSlotOnClosing(int slotIndex) {
      if (this.content[slotIndex] != null) {
         ItemStack itemstack = this.content[slotIndex];
         this.content[slotIndex] = null;
         return itemstack;
      } else {
         return null;
      }
   }

   public void setInventorySlotContents(int slotIndex, ItemStack stack) {
      this.content[slotIndex] = stack;
      if (stack != null && stack.stackSize > this.getInventoryStackLimit()) {
         stack.stackSize = this.getInventoryStackLimit();
      }

      this.markDirty();
   }

   public void writeToNBT(NBTTagCompound compound) {
      super.writeToNBT(compound);
      NBTTagList nbttaglist = new NBTTagList();

      for(int i = 0; i < this.content.length; ++i) {
         if (this.content[i] != null) {
            NBTTagCompound nbttagcompound1 = new NBTTagCompound();
            nbttagcompound1.setByte("Slot", (byte)i);
            this.content[i].writeToNBT(nbttagcompound1);
            nbttaglist.appendTag(nbttagcompound1);
         }
      }

      compound.setInteger("workedTime", this.workedTime);
      compound.setTag("Items", nbttaglist);
   }

   public void readFromNBT(NBTTagCompound compound) {
      super.readFromNBT(compound);
      NBTTagList nbttaglist = compound.getTagList("Items", 10);
      this.content = new ItemStack[this.getSizeInventory()];

      for(int i = 0; i < nbttaglist.tagCount(); ++i) {
         NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
         int j = nbttagcompound1.getByte("Slot") & 255;
         if (j >= 0 && j < this.content.length) {
            this.content[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
         }
      }

      this.workedTime = compound.getInteger("workedTime");
   }

   public String getInventoryName() {
      return "TileEntity.AlchemyStacker";
   }

   public boolean hasCustomInventoryName() {
      return false;
   }

   public int getInventoryStackLimit() {
      return 64;
   }

   public void markDirty() {
   }

   public boolean isUseableByPlayer(EntityPlayer player) {
      return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : player.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
   }

   public void openInventory() {
   }

   public void closeInventory() {
   }

   public boolean isItemValidForSlot(int slot, ItemStack stack) {
      return true;
   }

   public void updateEntity() {
      super.updateEntity();
   }

   private void smeltItem() {
      if (this.canSmelt()) {
         --this.content[0].stackSize;
         if (this.content[1] != null) {
            ++this.content[1].stackSize;
         } else {
            this.content[1] = new ItemStack(this.content[0].getItem(), 1, this.content[0].getItemDamage());
         }

         if (this.content[0].stackSize <= 0) {
            this.content[0] = null;
         }

         this.workedTime = 0;
      }
   }

   public boolean canSmelt() {
      if (this.content[0] == null) {
         return false;
      } else if (this.content[1] == null) {
         return true;
      } else if (this.content[1].stackSize >= this.getInventoryStackLimit()) {
         return false;
      } else if (this.content[1].getItem() != this.content[0].getItem()) {
         return false;
      } else if (this.content[1].getItem() instanceof ItemPotion && this.content[0].getItemDamage() != this.content[1].getItemDamage()) {
         return false;
      } else {
         return this.content[1].stackSize < 16;
      }
   }

   @SideOnly(Side.CLIENT)
   public int getCookProgressScaled(int value) {
      return this.workedTime == 0 ? 0 : value * this.workedTime / this.timeNeeded;
   }

   public int[] getAccessibleSlotsFromSide(int side) {
      return new int[0];
   }

   public boolean canInsertItem(int side, ItemStack stack, int slot) {
      return false;
   }

   public boolean canExtractItem(int side, ItemStack stack, int slot) {
      return false;
   }
}
