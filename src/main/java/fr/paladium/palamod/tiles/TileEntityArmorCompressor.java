package fr.paladium.palamod.tiles;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.items.ItemRune;
import fr.paladium.palamod.items.ItemShardRune;
import fr.paladium.palamod.items.weapons.ItemBroadsword;
import fr.paladium.palamod.items.weapons.ItemFastsword;
import fr.paladium.palamod.recipies.ArmorCompressorRecipe;
import java.util.LinkedList;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumChatFormatting;

public class TileEntityArmorCompressor extends TileEntity implements IInventory {
   public ItemStack[] inventory = new ItemStack[this.getSizeInventory()];
   public String customName = "Armor Compressor";
   private int workingTime = 0;
   private int timeNeeded = 600;

   public String getCustomName() {
      return this.customName;
   }

   public boolean hasCustomName() {
      return true;
   }

   public void setCustomName(String customName) {
      this.customName = customName;
   }

   public int getSizeInventory() {
      return 9;
   }

   public ItemStack getStackInSlot(int index) {
      return index >= 0 && index < this.getSizeInventory() ? this.inventory[index] : null;
   }

   public ItemStack decrStackSize(int index, int count) {
      if (this.getStackInSlot(index) != null) {
         ItemStack itemstack;
         if (this.getStackInSlot(index).stackSize <= count) {
            itemstack = this.getStackInSlot(index);
            this.setInventorySlotContents(index, (ItemStack)null);
            this.markDirty();
            return itemstack;
         } else {
            itemstack = this.getStackInSlot(index).splitStack(count);
            if (this.getStackInSlot(index).stackSize <= 0) {
               this.setInventorySlotContents(index, (ItemStack)null);
            } else {
               this.setInventorySlotContents(index, this.getStackInSlot(index));
            }

            this.markDirty();
            return itemstack;
         }
      } else {
         return null;
      }
   }

   public ItemStack getStackInSlotOnClosing(int index) {
      ItemStack stack = this.getStackInSlot(index);
      this.setInventorySlotContents(index, (ItemStack)null);
      return stack;
   }

   public void setInventorySlotContents(int index, ItemStack stack) {
      if (index >= 0 && index < this.getSizeInventory()) {
         if (stack != null && stack.stackSize > this.getInventoryStackLimit()) {
            stack.stackSize = this.getInventoryStackLimit();
         }

         if (stack != null && stack.stackSize == 0) {
            stack = null;
         }

         this.inventory[index] = stack;
         this.markDirty();
      }
   }

   public String getInventoryName() {
      return this.customName;
   }

   public boolean hasCustomInventoryName() {
      return true;
   }

   public int getInventoryStackLimit() {
      return 4;
   }

   public boolean isUseableByPlayer(EntityPlayer player) {
      return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) == this && player.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
   }

   public void openInventory() {
   }

   public void closeInventory() {
   }

   public boolean isItemValidForSlot(int p_94041_1_, ItemStack p_94041_2_) {
      return p_94041_2_.getItem() instanceof ItemSword || p_94041_2_.getItem() instanceof ItemArmor || p_94041_2_.getItem() instanceof ItemBroadsword || p_94041_2_.getItem() instanceof ItemFastsword;
   }

   public void writeToNBT(NBTTagCompound nbt) {
      super.writeToNBT(nbt);
      NBTTagList list = new NBTTagList();

      for(int i = 0; i < this.getSizeInventory(); ++i) {
         if (this.getStackInSlot(i) != null) {
            NBTTagCompound stackTag = new NBTTagCompound();
            stackTag.setByte("Slot", (byte)i);
            this.getStackInSlot(i).writeToNBT(stackTag);
            list.appendTag(stackTag);
         }
      }

      nbt.setTag("Items", list);
      if (this.hasCustomName()) {
         nbt.setString("CustomName", this.getCustomName());
      }

   }

   public void readFromNBT(NBTTagCompound nbt) {
      super.readFromNBT(nbt);
      NBTTagList list = nbt.getTagList("Items", 10);

      for(int i = 0; i < list.tagCount(); ++i) {
         NBTTagCompound stackTag = list.getCompoundTagAt(i);
         int slot = stackTag.getByte("Slot") & 255;
         this.setInventorySlotContents(slot, ItemStack.loadItemStackFromNBT(stackTag));
      }

      if (nbt.hasKey("CustomName", 8)) {
         this.setCustomName(nbt.getString("CustomName"));
      }

   }

   public Object getDisplayName() {
      return this.customName;
   }

   public void markDirty() {
      super.markDirty();
   }

   public void updateEntity() {
      if (!this.canCompress()) {
         this.workingTime = 0;
      }

      if (this.isCompressing() && this.canCompress()) {
         ++this.workingTime;
      }

      if (this.canCompress() && !this.isCompressing()) {
         this.workingTime = 1;
      }

      if (this.canCompress() && this.workingTime == this.timeNeeded) {
         this.compress();
         this.workingTime = 0;
      }

   }

   public boolean isCompressing() {
      return this.workingTime > 0;
   }

   private boolean canCompress() {
      if (this.inventory[0] == null) {
         return false;
      } else if (this.inventory[0].getItem() instanceof ItemShardRune && this.inventory[0].stackSize < 4) {
         return false;
      } else {
         ItemStack itemstack = ArmorCompressorRecipe.getManager().getSmeltingResult(this.inventory[0]);
         return itemstack != null;
      }
   }

   public void compress() {
      int enchantLevel = 0;
      if (this.canCompress() && this.inventory[1] == null) {
         ItemStack itemRune = ArmorCompressorRecipe.getManager().getSmeltingResult(this.inventory[0]);
         ItemStack itemRuneCopy = itemRune.copy();
         ItemStack itemArmor = this.inventory[0];
         if (itemArmor.stackTagCompound != null && itemArmor.getEnchantmentTagList() != null) {
            NBTTagList enchants = itemArmor.getEnchantmentTagList();

            for(int i = 0; i < enchants.tagCount(); ++i) {
               NBTTagCompound enchantCompound = enchants.getCompoundTagAt(i);
               enchantLevel += enchantCompound.getInteger("lvl");
            }
         }

         itemRuneCopy = this.applyRunnig(itemRuneCopy, enchantLevel);
         this.inventory[1] = itemRuneCopy;
         this.inventory[0].stackSize -= this.inventory[0].stackSize;
         if (this.inventory[0].stackSize <= 0) {
            this.inventory[0] = null;
         }

         if (this.inventory[1].stackSize <= 0) {
            this.inventory[1] = null;
         }
      }

   }

   private ItemStack applyRunnig(ItemStack item, int enchantLevel) {
      double malusMaths = 0.0D;
      List modifiersList = new LinkedList();
      int levelRune = 0;
      if (item.getItem() instanceof ItemRune) {
         levelRune = ((ItemRune)item.getItem()).getLevel();
      }

      if (levelRune == 1) {
         if (enchantLevel == 0) {
            enchantLevel = 0;
            malusMaths = 8.0D;
         } else if (enchantLevel > 0 && enchantLevel < 4) {
            enchantLevel = 2;
            malusMaths = 6.0D;
         } else if (enchantLevel > 4 && enchantLevel < 8) {
            enchantLevel = 4;
            malusMaths = 4.0D;
         } else if (enchantLevel > 8 && enchantLevel < 12) {
            enchantLevel = 6;
            malusMaths = 2.0D;
         } else if (enchantLevel > 12 && enchantLevel < 20) {
            enchantLevel = 8;
            malusMaths = 0.0D;
         }
      } else if (levelRune == 2) {
         if (enchantLevel == 0) {
            enchantLevel = 0;
            malusMaths = 8.0D;
         } else if (enchantLevel > 0 && enchantLevel < 4) {
            enchantLevel = 4;
            malusMaths = 6.0D;
         } else if (enchantLevel > 4 && enchantLevel < 8) {
            enchantLevel = 6;
            malusMaths = 4.0D;
         } else if (enchantLevel > 8 && enchantLevel < 12) {
            enchantLevel = 8;
            malusMaths = 2.0D;
         } else if (enchantLevel > 12 && enchantLevel < 20) {
            enchantLevel = 12;
            malusMaths = 0.0D;
         }
      } else if (levelRune == 3) {
         if (enchantLevel == 0) {
            enchantLevel = 0;
            malusMaths = 10.0D;
         } else if (enchantLevel > 0 && enchantLevel < 4) {
            enchantLevel = 6;
            malusMaths = 8.0D;
         } else if (enchantLevel > 4 && enchantLevel < 8) {
            enchantLevel = 8;
            malusMaths = 4.0D;
         } else if (enchantLevel > 8 && enchantLevel < 12) {
            enchantLevel = 12;
            malusMaths = 2.0D;
         } else if (enchantLevel > 12 && enchantLevel < 20) {
            enchantLevel = 17;
            malusMaths = 0.0D;
         }
      } else if (levelRune == 4) {
         if (enchantLevel == 0) {
            enchantLevel = 0;
            malusMaths = 9.0D;
         } else if (enchantLevel > 0 && enchantLevel < 4) {
            enchantLevel = 8;
            malusMaths = 8.0D;
         } else if (enchantLevel > 4 && enchantLevel < 8) {
            enchantLevel = 12;
            malusMaths = 4.0D;
         } else if (enchantLevel > 8 && enchantLevel < 12) {
            enchantLevel = 17;
            malusMaths = 2.0D;
         } else if (enchantLevel > 12 && enchantLevel < 20) {
            enchantLevel = 22;
            malusMaths = 0.0D;
         }
      }

      double bonus = round((double)((1 + enchantLevel) / 12) + Math.random() * (double)(this.getBonusMax(levelRune) / 4 - (1 + enchantLevel) / 4), 2);
      double malus = round(Math.random() * ((1.0D - malusMaths) / 4.0D - (double)(this.getMalusMax(levelRune) / 4)), 2);
      if (levelRune == 4) {
         double aa = Math.random();
         if (aa > 0.9D) {
            malus = 0.0D;
         }
      }

      if (bonus > 1.0D) {
         bonus = 1.0D;
      }

      if (malus > 1.0D) {
         malus = 1.0D;
      }

      if (bonus < 0.0D) {
         bonus = 0.0D;
      }

      if (malus < 0.0D) {
         malus = 0.0D;
      }

      modifiersList.clear();
      modifiersList.add("");
      modifiersList.add("Vie");
      modifiersList.add("Knockback Resistance");
      modifiersList.add("Vitesse");
      String btype = this.getType(modifiersList);
      String mtype = this.getType(modifiersList);
      int type = 0;
      item.stackTagCompound = new NBTTagCompound();
      item.stackTagCompound.setInteger("Level", levelRune);
      item.stackTagCompound.setInteger("Type", type);
      item.stackTagCompound.setDouble("Bonus", bonus);
      item.stackTagCompound.setDouble("Malus", malus);
      item.stackTagCompound.setString("BonusType", btype);
      item.stackTagCompound.setString("MalusType", mtype);
      NBTTagList lore = new NBTTagList();
      String displayLevel = "Niveau " + levelRune;
      String displayType = ItemRune.getDisplayType(type);
      NBTTagCompound stackTagCompound = item.stackTagCompound;
      lore.appendTag(new NBTTagString(EnumChatFormatting.BLUE + displayLevel));
      lore.appendTag(new NBTTagString(EnumChatFormatting.GREEN + "+" + bonus + displayType + "  " + btype));
      lore.appendTag(new NBTTagString(EnumChatFormatting.RED + "" + malus + displayType + "  " + mtype));
      NBTTagCompound display = new NBTTagCompound();
      display.setTag("Lore", lore);
      stackTagCompound.setTag("display", display);
      item.setTagCompound(stackTagCompound);
      return item;
   }

   public int getBonusMax(int level) {
      if (level == 1) {
         return 1;
      } else if (level == 2) {
         return 2;
      } else if (level == 3) {
         return 3;
      } else {
         return level == 4 ? 4 : 0;
      }
   }

   public int getMalusMax(int level) {
      if (level == 1) {
         return 1;
      } else if (level == 2) {
         return 2;
      } else if (level == 3) {
         return 3;
      } else {
         return level == 4 ? 4 : 0;
      }
   }

   public static double round(double value, int places) {
      if (places < 0) {
         throw new IllegalArgumentException();
      } else {
         long factor = (long)Math.pow(10.0D, (double)places);
         value *= (double)factor;
         long tmp = Math.round(value);
         return (double)tmp / (double)factor;
      }
   }

   public String getType(List list) {
      int r = (int)(1.0D + Math.random() * (double)(list.size() - 1));
      return (String)list.get(r);
   }

   @SideOnly(Side.CLIENT)
   public int getCompressProgress() {
      return this.workingTime;
   }
}
