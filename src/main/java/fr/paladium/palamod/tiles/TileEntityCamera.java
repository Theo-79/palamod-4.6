package fr.paladium.palamod.tiles;

import net.minecraft.tileentity.TileEntity;

public class TileEntityCamera extends TileEntity {
   float rotation;

   public TileEntityCamera() {
   }

   public TileEntityCamera(float rotation) {
      this.rotation = rotation;
   }

   public void setRotation(float rotation) {
      this.rotation = rotation;
   }

   public float getRotation() {
      return this.rotation;
   }
}
