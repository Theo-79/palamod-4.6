package fr.paladium.palamod.tiles;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

public class TileEntityGuardianAnchor extends TileEntity {
   static final int MAX_RADIUS = 16;
   int radius = 16;

   public void setRadius(int radius) {
      this.radius = radius;
      this.markDirty();
      this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
   }

   public int getRadius() {
      return this.radius;
   }

   public boolean inRadius(double x, double y, double z) {
      return !(Math.sqrt(Math.pow(x - (double)this.xCoord, 2.0D) + Math.pow(y - (double)this.yCoord, 2.0D) + Math.pow(z - (double)this.zCoord, 2.0D)) > (double)this.radius);
   }

   public void writeToNBT(NBTTagCompound compound) {
      super.writeToNBT(compound);
      compound.setInteger("radius", this.radius);
   }

   public void readFromNBT(NBTTagCompound compound) {
      super.readFromNBT(compound);
      this.radius = compound.getInteger("radius");
   }

   public Packet getDescriptionPacket() {
      NBTTagCompound nbtTag = new NBTTagCompound();
      this.writeToNBT(nbtTag);
      return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 1, nbtTag);
   }

   public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity packet) {
      this.readFromNBT(packet.func_148857_g());
   }
}
