package fr.paladium.palamod.tiles;

import fr.paladium.palamod.items.weapons.ItemBroadsword;
import fr.paladium.palamod.items.weapons.ItemFastsword;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;

public class TileEntityMagicalAnvil extends TileEntity implements IInventory {
   public ItemStack[] inventory = new ItemStack[this.getSizeInventory()];
   public String customName = "Magical Anvil";

   public String getCustomName() {
      return this.customName;
   }

   public boolean hasCustomName() {
      return true;
   }

   public void setCustomName(String customName) {
      this.customName = customName;
   }

   public int getSizeInventory() {
      return 9;
   }

   public ItemStack getStackInSlot(int index) {
      return index >= 0 && index < this.getSizeInventory() ? this.inventory[index] : null;
   }

   public ItemStack decrStackSize(int index, int count) {
      if (this.getStackInSlot(index) != null) {
         ItemStack itemstack;
         if (this.getStackInSlot(index).stackSize <= count) {
            itemstack = this.getStackInSlot(index);
            this.setInventorySlotContents(index, (ItemStack)null);
            this.markDirty();
            return itemstack;
         } else {
            itemstack = this.getStackInSlot(index).splitStack(count);
            if (this.getStackInSlot(index).stackSize <= 0) {
               this.setInventorySlotContents(index, (ItemStack)null);
            } else {
               this.setInventorySlotContents(index, this.getStackInSlot(index));
            }

            this.markDirty();
            return itemstack;
         }
      } else {
         return null;
      }
   }

   public ItemStack getStackInSlotOnClosing(int index) {
      ItemStack stack = this.getStackInSlot(index);
      this.setInventorySlotContents(index, (ItemStack)null);
      return stack;
   }

   public void setInventorySlotContents(int index, ItemStack stack) {
      if (index >= 0 && index < this.getSizeInventory()) {
         if (stack != null && stack.stackSize > this.getInventoryStackLimit()) {
            stack.stackSize = this.getInventoryStackLimit();
         }

         if (stack != null && stack.stackSize == 0) {
            stack = null;
         }

         this.inventory[index] = stack;
         this.markDirty();
      }
   }

   public String getInventoryName() {
      return this.customName;
   }

   public boolean hasCustomInventoryName() {
      return true;
   }

   public int getInventoryStackLimit() {
      return 1;
   }

   public boolean isUseableByPlayer(EntityPlayer player) {
      return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) == this && player.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
   }

   public void openInventory() {
   }

   public void closeInventory() {
   }

   public boolean isItemValidForSlot(int p_94041_1_, ItemStack p_94041_2_) {
      return p_94041_2_.getItem() instanceof ItemSword || p_94041_2_.getItem() instanceof ItemArmor || p_94041_2_.getItem() instanceof ItemBroadsword || p_94041_2_.getItem() instanceof ItemFastsword;
   }

   public void writeToNBT(NBTTagCompound nbt) {
      super.writeToNBT(nbt);
      NBTTagList list = new NBTTagList();

      for(int i = 0; i < this.getSizeInventory(); ++i) {
         if (this.getStackInSlot(i) != null) {
            NBTTagCompound stackTag = new NBTTagCompound();
            stackTag.setByte("Slot", (byte)i);
            this.getStackInSlot(i).writeToNBT(stackTag);
            list.appendTag(stackTag);
         }
      }

      nbt.setTag("Items", list);
      if (this.hasCustomName()) {
         nbt.setString("CustomName", this.getCustomName());
      }

   }

   public void readFromNBT(NBTTagCompound nbt) {
      super.readFromNBT(nbt);
      NBTTagList list = nbt.getTagList("Items", 10);

      for(int i = 0; i < list.tagCount(); ++i) {
         NBTTagCompound stackTag = list.getCompoundTagAt(i);
         int slot = stackTag.getByte("Slot") & 255;
         this.setInventorySlotContents(slot, ItemStack.loadItemStackFromNBT(stackTag));
      }

      if (nbt.hasKey("CustomName", 8)) {
         this.setCustomName(nbt.getString("CustomName"));
      }

   }

   public Object getDisplayName() {
      return this.customName;
   }

   public void markDirty() {
      super.markDirty();
   }
}
