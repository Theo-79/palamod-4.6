package fr.paladium.palamod.tiles;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

public abstract class TileEntityMultiblock extends TileEntity {
   private boolean hasMaster = false;
   protected boolean isMaster = false;
   private int masterX = 0;
   private int masterY = 0;
   private int masterZ = 0;

   public void readFromNBT(NBTTagCompound compound) {
      super.readFromNBT(compound);
      this.hasMaster = compound.getBoolean("HasMaster");
      this.isMaster = compound.getBoolean("IsMaster");
      this.masterX = compound.getInteger("MasterX");
      this.masterY = compound.getInteger("MasterY");
      this.masterZ = compound.getInteger("MasterZ");
   }

   public void writeToNBT(NBTTagCompound compound) {
      super.writeToNBT(compound);
      compound.setBoolean("HasMaster", this.hasMaster);
      compound.setBoolean("IsMaster", this.isMaster);
      compound.setInteger("MasterX", this.masterX);
      compound.setInteger("MasterY", this.masterY);
      compound.setInteger("MasterZ", this.masterZ);
   }

   public boolean hasMaster() {
      return this.hasMaster;
   }

   public boolean isMaster() {
      return this.isMaster;
   }

   public int getMasterX() {
      return this.masterX;
   }

   public int getMasterY() {
      return this.masterY;
   }

   public int getMasterZ() {
      return this.masterZ;
   }

   public void setMaster(int x, int y, int z) {
      this.hasMaster = true;
      this.masterX = x;
      this.masterY = y;
      this.masterZ = z;
   }

   public void reset() {
      this.isMaster = false;
      this.hasMaster = false;
      this.masterX = 0;
      this.masterY = 0;
      this.masterZ = 0;
   }

   public void setStructure(int sizeX, int sizeY, int sizeZ) {
      if (this.isMaster) {
         if (!this.checkStructure(sizeX, sizeY, sizeZ)) {
            this.resetStructure(sizeX, sizeY, sizeZ);
         }
      } else if (this.hasMaster) {
         if (!this.checkForMaster()) {
            this.reset();
         }
      } else if (!this.hasMaster && this.checkStructure(sizeX, sizeY, sizeZ)) {
         this.setupStructure(sizeX, sizeY, sizeZ);
      }

      this.markDirty();
   }

   protected boolean checkStructure(int sizeX, int sizeY, int sizeZ) {
      int number = sizeX * sizeY * sizeZ;
      int n = 0;

      for(int x = this.xCoord - 1; x < this.xCoord + sizeX - 1; ++x) {
         for(int y = this.yCoord; y < this.yCoord + sizeY; ++y) {
            for(int z = this.zCoord - 1; z < this.zCoord + sizeZ - 1; ++z) {
               TileEntity tile = this.worldObj.getTileEntity(x, y, z);
               if (tile != null && tile instanceof TileEntityMultiblock) {
                  TileEntityMultiblock t = (TileEntityMultiblock)tile;
                  if (t.hasMaster) {
                     if (this.xCoord == t.getMasterX() && this.yCoord == t.getMasterY() && this.zCoord == t.getMasterZ()) {
                        ++n;
                     }
                  } else {
                     ++n;
                  }
               }
            }
         }
      }

      if (n == number) {
         return true;
      } else {
         return false;
      }
   }

   private void setupStructure(int sizeX, int sizeY, int sizeZ) {
      for(int x = this.xCoord - 1; x < this.xCoord + sizeX - 1; ++x) {
         for(int y = this.yCoord; y < this.yCoord + sizeY; ++y) {
            for(int z = this.zCoord - 1; z < this.zCoord + sizeZ - 1; ++z) {
               TileEntity tile = this.worldObj.getTileEntity(x, y, z);
               if (tile != null && tile instanceof TileEntityMultiblock) {
                  TileEntityMultiblock t = (TileEntityMultiblock)tile;
                  t.setMaster(this.xCoord, this.yCoord, this.zCoord);
                  this.isMaster = true;
                  this.worldObj.markTileEntityChunkModified(x, y, z, this);
                  this.worldObj.markBlockForUpdate(x, y, z);
               }
            }
         }
      }

   }

   private void resetStructure(int sizeX, int sizeY, int sizeZ) {
      for(int x = this.xCoord - 1; x < this.xCoord + sizeX - 1; ++x) {
         for(int y = this.yCoord; y < this.yCoord + sizeY; ++y) {
            for(int z = this.zCoord - 1; z < this.zCoord + sizeZ - 1; ++z) {
               TileEntity tile = this.worldObj.getTileEntity(x, y, z);
               if (tile != null && tile instanceof TileEntityMultiblock) {
                  TileEntityMultiblock t = (TileEntityMultiblock)tile;
                  t.reset();
               }
            }
         }
      }

   }

   private boolean checkForMaster() {
      TileEntity tile = this.worldObj.getTileEntity(this.masterX, this.masterY, this.masterZ);
      if (tile != null && tile instanceof TileEntityMultiblock) {
         TileEntityMultiblock t = (TileEntityMultiblock)tile;
         if (t.isMaster()) {
            return true;
         }
      }

      return false;
   }

   public Packet getDescriptionPacket() {
      NBTTagCompound nbttagcompound = new NBTTagCompound();
      this.writeToNBT(nbttagcompound);
      return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, this.getBlockMetadata(), nbttagcompound);
   }

   public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {
      this.readFromNBT(pkt.func_148857_g());
      this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
   }
}
