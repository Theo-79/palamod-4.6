package fr.paladium.palamod.tiles;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

public class TileEntityMulticolorBlock extends TileEntity {
   private int color1 = -1;
   private int color2 = -1;
   private int color3 = -1;
   private int color4 = -1;
   private int currentColor = -1;
   private int index = 0;
   private boolean activate2 = true;
   private boolean activate3 = true;
   private boolean activate4 = true;
   private int time = 10;
   private int counter = 0;

   public void writeToNBT(NBTTagCompound compound) {
      super.writeToNBT(compound);
      compound.setInteger("Color1", this.color1);
      compound.setInteger("Color2", this.color2);
      compound.setInteger("Color3", this.color3);
      compound.setInteger("Color4", this.color4);
      compound.setInteger("CurrentColor", this.currentColor);
      compound.setBoolean("Activate2", this.activate2);
      compound.setBoolean("Activate3", this.activate3);
      compound.setBoolean("Activate4", this.activate4);
      compound.setInteger("Time", this.time);
      compound.setInteger("Counter", this.counter);
      compound.setInteger("Index", this.index);
   }

   public void readFromNBT(NBTTagCompound compound) {
      super.readFromNBT(compound);
      this.color1 = compound.getInteger("Color1");
      this.color2 = compound.getInteger("Color2");
      this.color3 = compound.getInteger("Color3");
      this.color4 = compound.getInteger("Color4");
      this.currentColor = compound.getInteger("CurrentColor");
      this.activate2 = compound.getBoolean("Activate2");
      this.activate3 = compound.getBoolean("Activate3");
      this.activate4 = compound.getBoolean("Activate4");
      this.time = compound.getInteger("Time");
      this.counter = compound.getInteger("Counter");
      this.index = compound.getInteger("Index");
   }

   public void updateEntity() {
      if (this.counter > 0) {
         --this.counter;
      }

      if (this.counter == 0) {
         if (this.index == 1) {
            if (this.activate2) {
               this.currentColor = this.color2;
            } else {
               ++this.index;
            }
         }

         if (this.index == 2) {
            if (this.activate3) {
               this.currentColor = this.color3;
            } else {
               ++this.index;
            }
         }

         if (this.index == 3) {
            if (this.activate4) {
               this.currentColor = this.color4;
            } else {
               ++this.index;
            }
         }

         if (this.index > 3) {
            this.index = 0;
         }

         if (this.index == 0) {
            this.currentColor = this.color1;
         }

         ++this.index;
         this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
         this.counter = this.time * 20;
      }

   }

   public int getTime() {
      return this.time;
   }

   public void setTime(int time) {
      this.time = time;
   }

   public int getColor1() {
      return this.color1;
   }

   public void setColor1(int color1) {
      this.color1 = color1;
   }

   public int getColor2() {
      return this.color2;
   }

   public void setColor2(int color2) {
      this.color2 = color2;
   }

   public int getColor3() {
      return this.color3;
   }

   public void setColor3(int color3) {
      this.color3 = color3;
   }

   public int getColor4() {
      return this.color4;
   }

   public void setColor4(int color4) {
      this.color4 = color4;
   }

   public boolean isActivate2() {
      return this.activate2;
   }

   public void setActivate2(boolean activate2) {
      this.activate2 = activate2;
   }

   public boolean isActivate3() {
      return this.activate3;
   }

   public void setActivate3(boolean activate3) {
      this.activate3 = activate3;
   }

   public boolean isActivate4() {
      return this.activate4;
   }

   public void setActivate4(boolean activate4) {
      this.activate4 = activate4;
   }

   public int getCurrentColor() {
      return this.currentColor;
   }

   public void setCurrentColor(int currentColor) {
      this.currentColor = currentColor;
   }

   public int getCounter() {
      return this.counter;
   }

   public void setCounter(int counter) {
      this.counter = counter;
   }

   public int getIndex() {
      return this.index;
   }

   public void setIndex(int index) {
      this.index = index;
   }

   public Packet getDescriptionPacket() {
      NBTTagCompound nbttagcompound = new NBTTagCompound();
      this.writeToNBT(nbttagcompound);
      return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, this.getBlockMetadata(), nbttagcompound);
   }

   public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {
      this.readFromNBT(pkt.func_148857_g());
      this.worldObj.markBlockRangeForRenderUpdate(this.xCoord, this.yCoord, this.zCoord, this.xCoord, this.yCoord, this.zCoord);
   }
}
