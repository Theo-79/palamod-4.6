package fr.paladium.palamod.tiles;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.blocks.ModBlocks;
import fr.paladium.palamod.items.ItemObsidianUpgrade;
import fr.paladium.palamod.items.ModItems;
import fr.paladium.palamod.recipies.IncompatibleObsidianUpgrades;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntityFurnace;

public class TileEntityObsidianUpgrader extends TileEntityMultiblock implements IInventory {
   private ItemStack[] contents = new ItemStack[6];
   private int obsidian = 0;
   private int timeMax = 150;
   private int timeMaxObsi = 175;
   private int itemBurnTime = 0;
   private int timeInput = 0;
   private int timeOutput = 0;
   private int timeObsi = 0;
   private int timeFuel = 0;
   private Boolean explode = false;
   private Boolean fake = false;
   private Boolean twoLife = false;
   private Boolean camouflage = false;
   private int upgradeNumber = 0;

   public void writeToNBT(NBTTagCompound compound) {
      super.writeToNBT(compound);
      NBTTagList nbttaglist = new NBTTagList();

      for(int i = 0; i < this.contents.length; ++i) {
         if (this.contents[i] != null) {
            NBTTagCompound nbttagcompound1 = new NBTTagCompound();
            nbttagcompound1.setByte("Slot", (byte)i);
            this.contents[i].writeToNBT(nbttagcompound1);
            nbttaglist.appendTag(nbttagcompound1);
         }
      }

      compound.setTag("Items", nbttaglist);
      compound.setInteger("Obsidian", this.obsidian);
      compound.setInteger("TimeInput", this.timeInput);
      compound.setInteger("TimeOutput", this.timeOutput);
      compound.setInteger("TimeObsi", this.timeObsi);
      compound.setInteger("TimeFuel", this.timeFuel);
      compound.setInteger("ItemBurnTime", this.itemBurnTime);
      compound.setBoolean("Explode", this.explode);
      compound.setBoolean("Fake", this.fake);
      compound.setBoolean("TwoLife", this.twoLife);
      compound.setBoolean("Camouflage", this.camouflage);
      compound.setInteger("UpgradeNumber", this.upgradeNumber);
   }

   public void readFromNBT(NBTTagCompound compound) {
      super.readFromNBT(compound);
      NBTTagList nbttaglist = compound.getTagList("Items", 10);
      this.contents = new ItemStack[this.getSizeInventory()];

      for(int i = 0; i < nbttaglist.tagCount(); ++i) {
         NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
         int j = nbttagcompound1.getByte("Slot") & 255;
         if (j >= 0 && j < this.contents.length) {
            this.contents[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
         }
      }

      this.obsidian = compound.getInteger("Obsidian");
      this.timeInput = compound.getInteger("TimeInput");
      this.timeOutput = compound.getInteger("TimeOutput");
      this.timeObsi = compound.getInteger("TimeObsi");
      this.timeFuel = compound.getInteger("TimeFuel");
      this.itemBurnTime = compound.getInteger("ItemBurnTime");
      this.explode = compound.getBoolean("Explode");
      this.fake = compound.getBoolean("Fake");
      this.twoLife = compound.getBoolean("TwoLife");
      this.camouflage = compound.getBoolean("Camouflage");
      this.upgradeNumber = compound.getInteger("UpgradeNumber");
   }

   public Boolean hasObsidian() {
      return this.obsidian > 0;
   }

   private Boolean isMaxObsidian() {
      return this.obsidian >= 64;
   }

   public Boolean isBurning(TileEntityObsidianUpgrader.Id id) {
      if (id == TileEntityObsidianUpgrader.Id.input) {
         return this.timeInput > 0;
      } else if (id == TileEntityObsidianUpgrader.Id.output) {
         return this.timeOutput > 0;
      } else if (id == TileEntityObsidianUpgrader.Id.obsi) {
         return this.timeObsi > 0;
      } else {
         return id == TileEntityObsidianUpgrader.Id.fuel ? this.timeFuel > 0 : false;
      }
   }

   private Boolean canSmelt(TileEntityObsidianUpgrader.Id id) {
      if (this.contents[0] != null && this.contents[0].getItem() == Item.getItemFromBlock(Blocks.obsidian)) {
         if (id == TileEntityObsidianUpgrader.Id.input && this.contents[1] != null && this.obsidian != 0 && this.upgradeNumber < 3) {
            if (this.contents[1].getItem() instanceof ItemObsidianUpgrade) {
               ItemObsidianUpgrade.Upgrade upgrade = ((ItemObsidianUpgrade)this.contents[1].getItem()).type;
               return !this.isUpgraded(upgrade) && this.isCompatible(upgrade);
            }
         } else {
            if (id == TileEntityObsidianUpgrader.Id.output && !this.isBurning(TileEntityObsidianUpgrader.Id.input)) {
               if (this.hasObsidian() && this.contents[2] != null && this.contents[3] != null && this.contents[2].getItem() == ModItems.paternBlock && this.contents[3].getItem() == ModItems.paternSocket) {
                  return true;
               }

               return false;
            }

            if (id == TileEntityObsidianUpgrader.Id.obsi && this.contents[0] != null) {
               if (this.isBurning(TileEntityObsidianUpgrader.Id.fuel)) {
                  return this.obsidian != 0 ? this.canAdd(this.contents[0]) && !this.isMaxObsidian() : !this.isMaxObsidian();
               }
            } else if (id == TileEntityObsidianUpgrader.Id.fuel && this.contents[5] != null) {
               return !this.isBurning(TileEntityObsidianUpgrader.Id.fuel);
            }
         }

         return false;
      } else {
         return false;
      }
   }

   public Boolean isUpgraded(ItemObsidianUpgrade.Upgrade upgrade) {
      switch(upgrade) {
      case Explode:
         return this.explode;
      case Fake:
         return this.fake;
      case TwoLife:
         return this.twoLife;
      case Camouflage:
         return this.camouflage;
      default:
         return false;
      }
   }

   public boolean isCompatible(ItemObsidianUpgrade.Upgrade upgrade) {
      if (this.explode && !IncompatibleObsidianUpgrades.getManager().isCompatible(ItemObsidianUpgrade.Upgrade.Explode, upgrade)) {
         return false;
      } else if (this.fake && !IncompatibleObsidianUpgrades.getManager().isCompatible(ItemObsidianUpgrade.Upgrade.Fake, upgrade)) {
         return false;
      } else if (this.twoLife && !IncompatibleObsidianUpgrades.getManager().isCompatible(ItemObsidianUpgrade.Upgrade.TwoLife, upgrade)) {
         return false;
      } else {
         return !this.camouflage || IncompatibleObsidianUpgrades.getManager().isCompatible(ItemObsidianUpgrade.Upgrade.Camouflage, upgrade);
      }
   }

   public void updateEntity() {
      this.setStructure(2, 1, 2);
      if (this.isMaster) {
         if (this.isBurning(TileEntityObsidianUpgrader.Id.input) && this.canSmelt(TileEntityObsidianUpgrader.Id.input)) {
            ++this.timeInput;
         }

         if (this.isBurning(TileEntityObsidianUpgrader.Id.output) && this.canSmelt(TileEntityObsidianUpgrader.Id.output)) {
            ++this.timeOutput;
         }

         if (this.isBurning(TileEntityObsidianUpgrader.Id.obsi) && this.canSmelt(TileEntityObsidianUpgrader.Id.obsi)) {
            ++this.timeObsi;
         }

         if (this.isBurning(TileEntityObsidianUpgrader.Id.fuel)) {
            --this.timeFuel;
         }

         if (this.canSmelt(TileEntityObsidianUpgrader.Id.input) && !this.isBurning(TileEntityObsidianUpgrader.Id.input)) {
            this.timeInput = 1;
         }

         if (this.canSmelt(TileEntityObsidianUpgrader.Id.output) && !this.isBurning(TileEntityObsidianUpgrader.Id.output)) {
            this.timeOutput = 1;
         }

         if (this.canSmelt(TileEntityObsidianUpgrader.Id.obsi) && !this.isBurning(TileEntityObsidianUpgrader.Id.obsi)) {
            this.timeObsi = 1;
         }

         if (this.canSmelt(TileEntityObsidianUpgrader.Id.fuel)) {
            this.itemBurnTime = this.timeFuel = TileEntityFurnace.getItemBurnTime(this.contents[5]);
            if (this.timeFuel > 0) {
               --this.contents[5].stackSize;
               if (this.contents[5].stackSize == 0) {
                  this.contents[5] = null;
               }
            }
         }

         if (!this.canSmelt(TileEntityObsidianUpgrader.Id.input)) {
            this.timeInput = 0;
         }

         if (!this.canSmelt(TileEntityObsidianUpgrader.Id.output)) {
            this.timeOutput = 0;
         }

         if (!this.canSmelt(TileEntityObsidianUpgrader.Id.obsi)) {
            this.timeObsi = 0;
         }

         if (this.canSmelt(TileEntityObsidianUpgrader.Id.input) && this.timeInput == this.timeMax) {
            this.upgrade();
            this.timeInput = 0;
         }

         if (this.canSmelt(TileEntityObsidianUpgrader.Id.output) && this.timeOutput == this.timeMax) {
            this.smelt();
            this.timeOutput = 0;
         }

         if (this.canSmelt(TileEntityObsidianUpgrader.Id.obsi) && this.timeObsi == this.timeMaxObsi) {
            this.addObsidian();
            this.timeObsi = 0;
         }
      }

      this.markDirty();
   }

   public void reset() {
      super.reset();
      this.contents = new ItemStack[6];
      this.obsidian = 0;
      this.timeMax = 150;
      this.timeMaxObsi = 100;
      this.itemBurnTime = 0;
      this.timeInput = 0;
      this.timeOutput = 0;
      this.timeObsi = 0;
      this.timeFuel = 0;
      this.explode = false;
      this.fake = false;
      this.twoLife = false;
      this.camouflage = false;
      this.upgradeNumber = 0;
   }

   public void addObsidian() {
      if (this.obsidian != 0) {
         if (this.canAdd(this.contents[0])) {
            --this.contents[0].stackSize;
            ++this.obsidian;
            if (this.contents[0].stackSize == 0) {
               this.contents[0] = null;
            }
         }
      } else {
         ItemStack stack = this.contents[0];
         NBTTagCompound compound;
         if (!stack.hasTagCompound()) {
            compound = new NBTTagCompound();
            compound.setBoolean("Explode", false);
            compound.setBoolean("Fake", false);
            compound.setBoolean("TwoLife", false);
            stack.setTagCompound(compound);
         }

         compound = stack.getTagCompound();
         this.explode = compound.getBoolean("Explode");
         this.fake = compound.getBoolean("Fake");
         this.twoLife = compound.getBoolean("TwoLife");
         this.camouflage = compound.getBoolean("Camouflage");
         --this.contents[0].stackSize;
         ++this.obsidian;
         if (this.contents[0].stackSize == 0) {
            this.contents[0] = null;
         }
      }

      this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
   }

   public Boolean canAdd(ItemStack stack) {
      if (stack != null) {
         NBTTagCompound compound;
         if (!stack.hasTagCompound()) {
            compound = new NBTTagCompound();
            compound.setBoolean("Explode", false);
            compound.setBoolean("Fake", false);
            compound.setBoolean("TwoLife", false);
            compound.setBoolean("Camouflage", false);
            stack.setTagCompound(compound);
         }

         compound = stack.getTagCompound();
         if (compound.getBoolean("Explode") == this.explode && compound.getBoolean("Fake") == this.fake && compound.getBoolean("TwoLife") == this.twoLife && compound.getBoolean("Camouflage") == this.camouflage) {
            return true;
         }
      }

      return false;
   }

   public void upgrade() {
      if (this.contents[1] != null && this.contents[1].getItem() instanceof ItemObsidianUpgrade) {
         ItemObsidianUpgrade upgrade = (ItemObsidianUpgrade)this.contents[1].getItem();
         if (upgrade.type == ItemObsidianUpgrade.Upgrade.Explode) {
            this.explode = true;
         } else if (upgrade.type == ItemObsidianUpgrade.Upgrade.Fake) {
            this.fake = true;
         } else if (upgrade.type == ItemObsidianUpgrade.Upgrade.TwoLife) {
            this.twoLife = true;
         } else if (upgrade.type == ItemObsidianUpgrade.Upgrade.Camouflage) {
            this.camouflage = true;
         }

         ++this.upgradeNumber;
         --this.contents[1].stackSize;
         if (this.contents[1].stackSize == 0) {
            this.contents[1] = null;
         }
      }

   }

   public void smelt() {
      ItemStack stack = this.getUpgradedObsidian();
      if (this.contents[4] == null) {
         this.contents[4] = stack.copy();
         this.obsidianDown();
      } else if ((this.contents[4].getItem() == Item.getItemFromBlock(Blocks.obsidian) || this.contents[4].getItem() == Item.getItemFromBlock(ModBlocks.upgraded_obsidian)) && this.canAdd(this.contents[4])) {
         ++this.contents[4].stackSize;
         this.obsidianDown();
      }

      this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
   }

   public void obsidianDown() {
      --this.obsidian;
      if (this.obsidian == 0) {
         this.explode = false;
         this.fake = false;
         this.twoLife = false;
         this.camouflage = false;
      }

   }

   public ItemStack getUpgradedObsidian() {
      if (!this.explode && !this.fake && !this.twoLife && !this.camouflage) {
         return new ItemStack(Blocks.obsidian);
      } else {
         ItemStack stack = new ItemStack(ModBlocks.upgraded_obsidian);
         if (!stack.hasTagCompound()) {
            stack.setTagCompound(new NBTTagCompound());
         }

         NBTTagCompound compound = stack.getTagCompound();
         compound.setBoolean("Explode", this.explode);
         compound.setBoolean("Fake", this.fake);
         compound.setBoolean("TwoLife", this.twoLife);
         compound.setBoolean("Camouflage", this.camouflage);
         stack.setTagCompound(compound);
         return stack;
      }
   }

   public int getSizeInventory() {
      return 6;
   }

   public ItemStack getStackInSlot(int slotIndex) {
      return this.contents[slotIndex];
   }

   public ItemStack decrStackSize(int slotIndex, int amount) {
      if (this.contents[slotIndex] != null) {
         ItemStack itemstack;
         if (this.contents[slotIndex].stackSize <= amount) {
            itemstack = this.contents[slotIndex];
            this.contents[slotIndex] = null;
            this.markDirty();
            return itemstack;
         } else {
            itemstack = this.contents[slotIndex].splitStack(amount);
            if (this.contents[slotIndex].stackSize == 0) {
               this.contents[slotIndex] = null;
            }

            this.markDirty();
            return itemstack;
         }
      } else {
         return null;
      }
   }

   public ItemStack getStackInSlotOnClosing(int slotIndex) {
      if (this.contents[slotIndex] != null) {
         ItemStack itemstack = this.contents[slotIndex];
         this.contents[slotIndex] = null;
         return itemstack;
      } else {
         return null;
      }
   }

   public void setInventorySlotContents(int slotIndex, ItemStack stack) {
      this.contents[slotIndex] = stack;
      if (stack != null && stack.stackSize > this.getInventoryStackLimit()) {
         stack.stackSize = this.getInventoryStackLimit();
      }

      this.markDirty();
   }

   public String getInventoryName() {
      return "gui.obsidian_upgrader";
   }

   public boolean hasCustomInventoryName() {
      return false;
   }

   public void setCustomName(String customName) {
   }

   public int getInventoryStackLimit() {
      return 64;
   }

   public boolean isUseableByPlayer(EntityPlayer player) {
      return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : player.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
   }

   public void openInventory() {
   }

   public void closeInventory() {
   }

   public boolean isItemValidForSlot(int slot, ItemStack stack) {
      return slot != 3;
   }

   @SideOnly(Side.CLIENT)
   public int getProgress(TileEntityObsidianUpgrader.Id id, int size) {
      if (id == TileEntityObsidianUpgrader.Id.input) {
         return this.timeInput * size / this.timeMax;
      } else if (id == TileEntityObsidianUpgrader.Id.output) {
         return this.timeOutput * size / this.timeMax;
      } else if (id == TileEntityObsidianUpgrader.Id.obsi) {
         return this.timeObsi * size / this.timeMaxObsi;
      } else {
         return id == TileEntityObsidianUpgrader.Id.fuel ? this.timeFuel * size / this.itemBurnTime : 0;
      }
   }

   public int getObsidian() {
      return this.obsidian;
   }

   public List<Object> getInformations() {
      List<Object> list = new ArrayList();
      list.add(this.obsidian);
      list.add(this.explode);
      list.add(this.fake);
      list.add(this.twoLife);
      list.add(this.camouflage);
      return list;
   }

   public void setInformations(List<Object> list) {
      this.obsidian = (Integer)list.get(0);
      this.explode = (Boolean)list.get(1);
      this.fake = (Boolean)list.get(2);
      this.twoLife = (Boolean)list.get(3);
      this.camouflage = (Boolean)list.get(4);
   }

   public static enum Id {
      input,
      output,
      obsi,
      fuel;
   }
}
