package fr.paladium.palamod.tiles;

import fr.paladium.palamod.util.PlayerHelper;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

public class TileEntityOnlineDetector extends TileEntity {
   String player = "";
   boolean playerOnline = false;

   public String getPlayerName() {
      return this.player;
   }

   public void setName(String name) {
      this.player = name;
      this.markDirty();
      this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
   }

   public Packet getDescriptionPacket() {
      NBTTagCompound nbtTag = new NBTTagCompound();
      this.writeToNBT(nbtTag);
      return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 1, nbtTag);
   }

   public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity packet) {
      this.readFromNBT(packet.func_148857_g());
   }

   public void updateEntity() {
      if (this.worldObj.getTotalWorldTime() % 20L == 0L) {
         boolean playerCheck = PlayerHelper.isPlayerOnline(this.player);
         if (playerCheck != this.playerOnline) {
            this.markDirty();
            byte meta;
            if (playerCheck) {
               meta = 1;
            } else {
               meta = 0;
            }

            this.worldObj.setBlockMetadataWithNotify(this.xCoord, this.yCoord, this.zCoord, meta, 3);
            this.playerOnline = playerCheck;
         }
      }

      super.updateEntity();
   }

   public void writeToNBT(NBTTagCompound nbt) {
      super.writeToNBT(nbt);
      nbt.setString("username", this.player);
   }

   public void readFromNBT(NBTTagCompound nbt) {
      super.readFromNBT(nbt);
      this.player = nbt.getString("username");
   }
}
