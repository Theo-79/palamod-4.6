package fr.paladium.palamod.tiles;

import fr.paladium.palamod.items.ItemRingBase;
import fr.paladium.palamod.items.armors.RepairableArmor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;

public class TileEntityPaladiumChest extends TileEntity implements IInventory, ISidedInventory {
   public ItemStack[] content = new ItemStack[112];
   public boolean openning = false;
   public boolean closing = false;
   public float prevLid;
   public float lid;

   public int getSizeInventory() {
      return this.content.length;
   }

   public ItemStack getStackInSlot(int slot) {
      return this.content[slot];
   }

   public ItemStack decrStackSize(int slotIndex, int amount) {
      if (this.content[slotIndex] != null) {
         ItemStack itemstack;
         if (this.content[slotIndex].stackSize <= amount) {
            itemstack = this.content[slotIndex];
            this.content[slotIndex] = null;
            this.markDirty();
            return itemstack;
         } else {
            itemstack = this.content[slotIndex].splitStack(amount);
            if (this.content[slotIndex].stackSize == 0) {
               this.content[slotIndex] = null;
            }

            this.markDirty();
            return itemstack;
         }
      } else {
         return null;
      }
   }

   public void updateEntity() {
      for(int i = 108; i < 112; ++i) {
         ItemStack ring = this.content[i];
         if (ring != null && ring.getItemDamage() < ring.getMaxDamage()) {
            for(int j = 0; j < 12; ++j) {
               ItemStack armor = this.content[j];
               if (armor != null && armor.getItem() instanceof RepairableArmor && armor.getItemDamage() > 0) {
                  ((RepairableArmor)armor.getItem()).repair(armor, ring);
               }
            }
         }
      }

   }

   private void chestDisplay() {
      if (this.openning) {
         if (!(this.lid >= 1.0F) && !this.closing) {
            this.lid += 0.1F;
         } else {
            this.openning = false;
         }
      }

      if (this.closing) {
         if (!(this.lid <= 0.0F) && !this.openning) {
            this.lid -= 0.1F;
         } else {
            this.closing = false;
         }
      }

   }

   public ItemStack getStackInSlotOnClosing(int slotIndex) {
      if (this.content[slotIndex] != null) {
         ItemStack itemstack = this.content[slotIndex];
         this.content[slotIndex] = null;
         return itemstack;
      } else {
         return null;
      }
   }

   public void setInventorySlotContents(int slotIndex, ItemStack stack) {
      this.content[slotIndex] = stack;
      if (stack != null && stack.stackSize > this.getInventoryStackLimit()) {
         stack.stackSize = this.getInventoryStackLimit();
      }

      this.markDirty();
   }

   public String getInventoryName() {
      return "TileEntity.PaladiumChest";
   }

   public boolean hasCustomInventoryName() {
      return false;
   }

   public int getInventoryStackLimit() {
      return 64;
   }

   public boolean isUseableByPlayer(EntityPlayer player) {
      return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : player.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
   }

   public void openInventory() {
      this.openning = true;
   }

   public void closeInventory() {
      this.closing = true;
   }

   public boolean isItemValidForSlot(int slot, ItemStack item) {
      if (slot < 108) {
         return true;
      } else {
         return item.getItem() instanceof ItemRingBase;
      }
   }

   public void writeToNBT(NBTTagCompound compound) {
      super.writeToNBT(compound);
      NBTTagList nbttaglist = new NBTTagList();

      for(int i = 0; i < this.content.length; ++i) {
         if (this.content[i] != null) {
            NBTTagCompound nbttagcompound1 = new NBTTagCompound();
            nbttagcompound1.setByte("Slot", (byte)i);
            this.content[i].writeToNBT(nbttagcompound1);
            nbttaglist.appendTag(nbttagcompound1);
         }
      }

      compound.setTag("Items", nbttaglist);
   }

   public void readFromNBT(NBTTagCompound compound) {
      super.readFromNBT(compound);
      NBTTagList nbttaglist = compound.getTagList("Items", 10);
      this.content = new ItemStack[this.getSizeInventory()];

      for(int i = 0; i < nbttaglist.tagCount(); ++i) {
         NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
         int j = nbttagcompound1.getByte("Slot") & 255;
         if (j >= 0 && j < this.content.length) {
            this.content[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
         }
      }

   }

   public int[] getAccessibleSlotsFromSide(int side) {
      int[] slots = new int[108];

      for(int i = 0; i < 108; slots[i] = i++) {
      }

      return slots;
   }

   public boolean canInsertItem(int side, ItemStack stack, int slot) {
      return slot < 108;
   }

   public boolean canExtractItem(int side, ItemStack stack, int slot) {
      return slot < 108;
   }
}
