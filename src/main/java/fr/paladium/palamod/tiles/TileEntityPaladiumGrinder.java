package fr.paladium.palamod.tiles;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.blocks.ModBlocks;
import fr.paladium.palamod.items.armors.ItemArmorPaladium;
import fr.paladium.palamod.items.core.ItemPatern;
import fr.paladium.palamod.items.ores.ItemPaladium;
import fr.paladium.palamod.items.tools.ItemPaladiumPickaxe;
import fr.paladium.palamod.items.weapons.ItemPaladiumSword;
import fr.paladium.palamod.recipies.PaladiumGrinderRecipies;
import fr.paladium.palamod.util.UpgradeHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

public class TileEntityPaladiumGrinder extends TileEntity implements IInventory, ISidedInventory {
   private ItemStack[] content = new ItemStack[7];
   private int paladiumAmount;
   private int maxPaladium = 100;
   private int workedTimeCreation;
   private int workedTimeUpgrade;
   private int timeNeededCreation = 100;
   private int timeNeededUpgrade = 100;
   private int timeNeededPaladium = 20;
   private int workedTimePaladium;

   public int getSizeInventory() {
      return this.content.length;
   }

   public ItemStack getStackInSlot(int slot) {
      return this.content[slot];
   }

   public ItemStack decrStackSize(int slotIndex, int amount) {
      if (this.content[slotIndex] != null) {
         ItemStack itemstack;
         if (this.content[slotIndex].stackSize <= amount) {
            itemstack = this.content[slotIndex];
            this.content[slotIndex] = null;
            this.markDirty();
            return itemstack;
         } else {
            itemstack = this.content[slotIndex].splitStack(amount);
            if (this.content[slotIndex].stackSize == 0) {
               this.content[slotIndex] = null;
            }

            this.markDirty();
            return itemstack;
         }
      } else {
         return null;
      }
   }

   public ItemStack getStackInSlotOnClosing(int slotIndex) {
      if (this.content[slotIndex] != null) {
         ItemStack itemstack = this.content[slotIndex];
         this.content[slotIndex] = null;
         return itemstack;
      } else {
         return null;
      }
   }

   public void setInventorySlotContents(int slotIndex, ItemStack stack) {
      this.content[slotIndex] = stack;
      if (stack != null && stack.stackSize > this.getInventoryStackLimit()) {
         stack.stackSize = this.getInventoryStackLimit();
      }

      this.markDirty();
   }

   public String getInventoryName() {
      return "Tile.paladiumgrinder";
   }

   public boolean hasCustomInventoryName() {
      return false;
   }

   public int getInventoryStackLimit() {
      return 64;
   }

   public boolean isUseableByPlayer(EntityPlayer player) {
      return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : player.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
   }

   public void openInventory() {
   }

   public void closeInventory() {
   }

   public boolean isItemValidForSlot(int slot, ItemStack stack) {
      return true;
   }

   public Packet getDescriptionPacket() {
      NBTTagCompound nbtTag = new NBTTagCompound();
      this.writeToNBT(nbtTag);
      return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 1, nbtTag);
   }

   public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity packet) {
      this.readFromNBT(packet.func_148857_g());
   }

   public void updateEntity() {
      if (this.content[6] != null && this.paladiumAmount < 100) {
         if (this.timeNeededPaladium > this.workedTimePaladium) {
            ++this.workedTimePaladium;
         } else {
            this.consumePaladium();
            this.workedTimePaladium = 0;
         }
      } else {
         this.workedTimePaladium = 0;
      }

      if (this.canSmeltTool()) {
         ++this.workedTimeCreation;
         if (this.workedTimeCreation >= this.timeNeededCreation) {
            this.smeltTool();
            this.workedTimeCreation = 0;
         }
      } else {
         this.workedTimeCreation = 0;
      }

      if (this.canSmeltUpgrade()) {
         ++this.workedTimeUpgrade;
         if (this.workedTimeUpgrade >= this.timeNeededUpgrade) {
            this.smeltUpgrade();
            this.workedTimeUpgrade = 0;
         }
      } else {
         this.workedTimeUpgrade = 0;
      }

      super.updateEntity();
   }

   public boolean canSmeltTool() {
      if (this.content[1] != null && this.content[2] != null && this.paladiumAmount != 0) {
         ItemStack itemstack = PaladiumGrinderRecipies.getManager().getSmeltingResult(new ItemStack[]{this.content[1], this.content[2]});
         if (itemstack == null) {
            return false;
         } else if (this.content[0] != null && (!(this.content[0].getItem() instanceof ItemPaladium) || this.content[0].stackSize >= 64)) {
            return false;
         } else {
            return PaladiumGrinderRecipies.getManager().getSmeltingAmmount(itemstack) <= this.paladiumAmount;
         }
      } else {
         return false;
      }
   }

   public boolean canSmeltUpgrade() {
      if (this.content[4] != null && this.content[5] != null) {
         if (!PaladiumGrinderRecipies.getManager().isUpgradable(this.content[5].getItem(), this.content[4].getItem())) {
            return false;
         } else {
            int upgrade = PaladiumGrinderRecipies.getManager().getUpgrade(this.content[4].getItem());
            return UpgradeHelper.canApplyUpgrade(this.content[4], PaladiumGrinderRecipies.getManager().getUpgrade(this.content[4].getItem()), this.content[5]);
         }
      } else {
         return false;
      }
   }

   public void smeltUpgrade() {
      if (this.content[4] != null && this.content[5] != null) {
         if (PaladiumGrinderRecipies.getManager().isUpgradable(this.content[5].getItem(), this.content[4].getItem())) {
            int upgrade = PaladiumGrinderRecipies.getManager().getUpgrade(this.content[4].getItem());
            --this.content[4].stackSize;
            if (this.content[4].stackSize <= 0) {
               this.content[4] = null;
            }

            UpgradeHelper.applyUpgrade(this.content[5], upgrade);
            this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
            this.markDirty();
         }
      }
   }

   public void smeltTool() {
      if (this.content[1] != null && this.content[2] != null) {
         ItemStack itemstack = PaladiumGrinderRecipies.getManager().getSmeltingResult(new ItemStack[]{this.content[1], this.content[2]});
         if (itemstack != null) {
            this.paladiumAmount -= PaladiumGrinderRecipies.getManager().getSmeltingAmmount(itemstack);
            if (!(this.content[1].getItem() instanceof ItemPatern) || !(this.content[2].getItem() instanceof ItemPatern)) {
               --this.content[1].stackSize;
               --this.content[2].stackSize;
               if (this.content[1].stackSize <= 0) {
                  this.content[1] = null;
               }

               if (this.content[2].stackSize <= 0) {
                  this.content[2] = null;
               }
            }

            if (this.content[0] != null && this.content[0].getItem() instanceof ItemPaladium) {
               ++this.content[0].stackSize;
            } else {
               this.content[0] = itemstack.copy();
            }

            this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
            this.markDirty();
         }
      }
   }

   @SideOnly(Side.CLIENT)
   public float getScaledPaladium(int i) {
      return (float)(this.paladiumAmount * i / this.maxPaladium);
   }

   @SideOnly(Side.CLIENT)
   public int getScaledTool(int i) {
      return this.workedTimeCreation * i / this.timeNeededCreation;
   }

   @SideOnly(Side.CLIENT)
   public int getScaledUpgrade(int i) {
      return this.workedTimeUpgrade * i / this.timeNeededUpgrade;
   }

   @SideOnly(Side.CLIENT)
   public int getScaledProgress(int i) {
      return this.workedTimePaladium * i / this.timeNeededPaladium;
   }

   public int getPaladium() {
      return this.paladiumAmount;
   }

   @SideOnly(Side.CLIENT)
   public int getMaxPaladium() {
      return this.maxPaladium;
   }

   public void consumePaladium() {
      ItemStack stack = this.content[6].copy();
      int size = stack.stackSize;
      this.content[6] = null;
      int ammount = 0;
      if (stack.getItem() instanceof ItemPaladium) {
         ammount = size;
      }

      if (stack.getItem() == Item.getItemFromBlock(ModBlocks.paladiumBlock)) {
         ammount = size * 9;
      }

      if (stack.getItem() instanceof ItemArmorPaladium) {
         ammount = (stack.getMaxDamage() - stack.getItemDamage()) * ((ItemArmorPaladium)stack.getItem()).getCost() / stack.getMaxDamage();
      }

      if (stack.getItem() instanceof ItemPaladiumSword) {
         ammount = (stack.getMaxDamage() - stack.getItemDamage()) * 2 / stack.getMaxDamage();
      }

      if (stack.getItem() instanceof ItemPaladiumPickaxe) {
         ammount = (stack.getMaxDamage() - stack.getItemDamage()) * 3 / stack.getMaxDamage();
      }

      this.paladiumAmount += ammount;
      if (this.paladiumAmount > this.maxPaladium) {
         this.paladiumAmount = this.maxPaladium;
      }

   }

   public void writeToNBT(NBTTagCompound compound) {
      super.writeToNBT(compound);
      NBTTagList nbttaglist = new NBTTagList();

      for(int i = 0; i < this.content.length; ++i) {
         if (this.content[i] != null) {
            NBTTagCompound nbttagcompound1 = new NBTTagCompound();
            nbttagcompound1.setByte("Slot", (byte)i);
            this.content[i].writeToNBT(nbttagcompound1);
            nbttaglist.appendTag(nbttagcompound1);
         }
      }

      compound.setTag("Items", nbttaglist);
      compound.setShort("workedTimeCreation", (short)this.workedTimeCreation);
      compound.setShort("workedTimeUpgrade", (short)this.workedTimeUpgrade);
      compound.setShort("paladiumAmount", (short)this.paladiumAmount);
   }

   public void readFromNBT(NBTTagCompound compound) {
      super.readFromNBT(compound);
      NBTTagList nbttaglist = compound.getTagList("Items", 10);
      this.content = new ItemStack[this.getSizeInventory()];

      for(int i = 0; i < nbttaglist.tagCount(); ++i) {
         NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
         int j = nbttagcompound1.getByte("Slot") & 255;
         if (j >= 0 && j < this.content.length) {
            this.content[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
         }
      }

      this.workedTimeCreation = compound.getShort("workedTimeCreation");
      this.workedTimeUpgrade = compound.getShort("workedTimeUpgrade");
      this.paladiumAmount = compound.getShort("paladiumAmount");
   }

   public int[] getAccessibleSlotsFromSide(int side) {
      return new int[0];
   }

   public boolean canInsertItem(int side, ItemStack stack, int slot) {
      return false;
   }

   public boolean canExtractItem(int side, ItemStack stack, int slot) {
      return false;
   }
}
