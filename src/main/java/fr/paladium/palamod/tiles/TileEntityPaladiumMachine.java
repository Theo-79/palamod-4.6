package fr.paladium.palamod.tiles;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.paladium.palamod.recipies.PaladiumMachineRecipies;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;

public class TileEntityPaladiumMachine extends TileEntity implements IInventory, ISidedInventory {
   private ItemStack[] content = new ItemStack[6];
   private int workingTime = 0;
   private int timeNeeded = 200;

   public int getSizeInventory() {
      return this.content.length;
   }

   public ItemStack decrStackSize(int slotIndex, int amount) {
      if (this.content[slotIndex] != null) {
         ItemStack itemstack;
         if (this.content[slotIndex].stackSize <= amount) {
            itemstack = this.content[slotIndex];
            this.content[slotIndex] = null;
            this.markDirty();
            return itemstack;
         } else {
            itemstack = this.content[slotIndex].splitStack(amount);
            if (this.content[slotIndex].stackSize == 0) {
               this.content[slotIndex] = null;
            }

            this.markDirty();
            return itemstack;
         }
      } else {
         return null;
      }
   }

   public ItemStack getStackInSlotOnClosing(int slotIndex) {
      if (this.content[slotIndex] != null) {
         ItemStack itemstack = this.content[slotIndex];
         this.content[slotIndex] = null;
         return itemstack;
      } else {
         return null;
      }
   }

   public void setInventorySlotContents(int slotIndex, ItemStack stack) {
      this.content[slotIndex] = stack;
      if (stack != null && stack.stackSize > this.getInventoryStackLimit()) {
         stack.stackSize = this.getInventoryStackLimit();
      }

      this.markDirty();
   }

   public String getInventoryName() {
      return "TileEntity.PaladiumMachine";
   }

   public boolean hasCustomInventoryName() {
      return false;
   }

   public int getInventoryStackLimit() {
      return 64;
   }

   public boolean isUseableByPlayer(EntityPlayer player) {
      return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : player.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
   }

   public void openInventory() {
   }

   public void closeInventory() {
   }

   public boolean isItemValidForSlot(int slot, ItemStack stack) {
      return slot != 5;
   }

   public boolean isBurning() {
      return this.workingTime > 0;
   }

   private boolean canSmelt() {
      if (this.content[0] != null && this.content[1] != null && this.content[2] != null && this.content[3] != null && this.content[4] != null) {
         ItemStack itemstack = PaladiumMachineRecipies.getManager().getSmeltingResult(new ItemStack[]{this.content[0], this.content[1], this.content[2], this.content[3], this.content[4]});
         if (itemstack == null) {
            return false;
         } else if (this.content[5] == null) {
            return true;
         } else if (!this.content[5].isItemEqual(itemstack)) {
            return false;
         } else {
            int result = this.content[5].stackSize + itemstack.stackSize;
            return result <= this.getInventoryStackLimit() && result <= this.content[5].getMaxStackSize();
         }
      } else {
         return false;
      }
   }

   public void writeToNBT(NBTTagCompound compound) {
      super.writeToNBT(compound);
      NBTTagList nbttaglist = new NBTTagList();

      for(int i = 0; i < this.content.length; ++i) {
         if (this.content[i] != null) {
            NBTTagCompound nbttagcompound1 = new NBTTagCompound();
            nbttagcompound1.setByte("Slot", (byte)i);
            this.content[i].writeToNBT(nbttagcompound1);
            nbttaglist.appendTag(nbttagcompound1);
         }
      }

      compound.setTag("Items", nbttaglist);
      compound.setShort("workingTime", (short)this.workingTime);
      compound.setShort("workingTimeNeeded", (short)this.timeNeeded);
   }

   @SideOnly(Side.CLIENT)
   public int getCookProgress() {
      return this.workingTime * 16 / this.timeNeeded;
   }

   public void readFromNBT(NBTTagCompound compound) {
      super.readFromNBT(compound);
      NBTTagList nbttaglist = compound.getTagList("Items", 10);
      this.content = new ItemStack[this.getSizeInventory()];

      for(int i = 0; i < nbttaglist.tagCount(); ++i) {
         NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
         int j = nbttagcompound1.getByte("Slot") & 255;
         if (j >= 0 && j < this.content.length) {
            this.content[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
         }
      }

      this.workingTime = compound.getShort("workingTime");
      this.timeNeeded = compound.getShort("workingTimeNeeded");
   }

   public ItemStack getStackInSlot(int slot) {
      return this.content[slot];
   }

   public void updateEntity() {
      if (this.isBurning() && this.canSmelt()) {
         ++this.workingTime;
      }

      if (this.canSmelt() && !this.isBurning()) {
         this.workingTime = 1;
      }

      if (this.canSmelt() && this.workingTime == this.timeNeeded) {
         this.smeltItem();
         this.workingTime = 0;
      }

      if (!this.canSmelt()) {
         this.workingTime = 0;
      }

   }

   public void smeltItem() {
      if (this.canSmelt()) {
         ItemStack itemstack = PaladiumMachineRecipies.getManager().getSmeltingResult(new ItemStack[]{this.content[0], this.content[1], this.content[2], this.content[3], this.content[4]});
         if (this.content[5] == null) {
            this.content[5] = itemstack.copy();
         } else if (this.content[5].getItem() == itemstack.getItem()) {
            ItemStack var10000 = this.content[5];
            var10000.stackSize += itemstack.stackSize;
         }

         --this.content[0].stackSize;
         --this.content[1].stackSize;
         --this.content[2].stackSize;
         --this.content[3].stackSize;
         --this.content[4].stackSize;
         if (this.content[0].stackSize <= 0) {
            this.content[0] = null;
         }

         if (this.content[1].stackSize <= 0) {
            this.content[1] = null;
         }

         if (this.content[2].stackSize <= 0) {
            this.content[2] = null;
         }

         if (this.content[3].stackSize <= 0) {
            this.content[3] = null;
         }

         if (this.content[4].stackSize <= 0) {
            this.content[4] = null;
         }
      }

   }

   public int[] getAccessibleSlotsFromSide(int side) {
      return new int[0];
   }

   public boolean canInsertItem(int side, ItemStack stack, int slot) {
      return false;
   }

   public boolean canExtractItem(int side, ItemStack stack, int slot) {
      return false;
   }
}
