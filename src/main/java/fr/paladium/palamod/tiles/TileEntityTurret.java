package fr.paladium.palamod.tiles;

import com.sun.corba.se.impl.io.TypeMismatchException;
import fr.paladium.palamod.entities.mobs.EntityCustomWither;
import fr.paladium.palamod.entities.projectiles.EntityTurretBullet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.entity.Entity;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;

public class TileEntityTurret extends TileEntity {
   static final int cooldownBase = 5;
   static final int distanceTarget = 15;
   int timer = 0;
   int cooldown = 5;
   public float targetOffX;
   public float targetOffY;
   public float targetOffZ;
   public boolean activate = true;
   boolean checkOtherTurret = true;

   public void updateEntity() {
      if (!this.worldObj.isRemote && this.activate) {
         ++this.timer;
         if (this.timer >= this.cooldown) {
            this.timer = 0;
            EntityTurretBullet grenade = new EntityTurretBullet(this.worldObj);
            grenade.setPosition((double)((float)this.xCoord + 0.5F), (double)((float)this.yCoord + 0.5F), (double)((float)this.zCoord + 0.5F));
            List<Entity> withers = new ArrayList();
            withers.addAll(this.worldObj.getEntitiesWithinAABB(EntityWither.class, AxisAlignedBB.getBoundingBox((double)(this.xCoord - 15), (double)(this.yCoord - 15), (double)(this.zCoord - 15), (double)(this.xCoord + 15), (double)(this.yCoord + 15), (double)(this.zCoord + 15))));
            withers.addAll(this.worldObj.getEntitiesWithinAABB(EntityCustomWither.class, AxisAlignedBB.getBoundingBox((double)(this.xCoord - 15), (double)(this.yCoord - 15), (double)(this.zCoord - 15), (double)(this.xCoord + 15), (double)(this.yCoord + 15), (double)(this.zCoord + 15))));
            if (withers.isEmpty()) {
               this.checkOtherTurret = true;
               return;
            }

            Entity wither = (Entity)withers.get(this.worldObj.rand.nextInt(withers.size()));
            double offX = wither.posX - grenade.posX;
            double offY = wither.posY + (double)(wither.height / 2.0F) - grenade.posY;
            double offZ = wither.posZ - grenade.posZ;
            Vec3 off = Vec3.createVectorHelper(offX, offY, offZ);
            off = off.normalize();
            grenade.motionX = off.xCoord;
            grenade.motionY = off.yCoord;
            grenade.motionZ = off.zCoord;
            this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
            this.worldObj.spawnEntityInWorld(grenade);
            if (this.checkOtherTurret) {
               int count = this.countTurretInChunk();
               this.cooldown = 5 * count;
               this.checkOtherTurret = false;
            }
         }
      }

   }

   public int countTurretInChunk() {
      int count = 0;
      Map tileMap = this.worldObj.getChunkFromChunkCoords(this.worldObj.getChunkFromBlockCoords(this.xCoord, this.zCoord).xPosition, this.worldObj.getChunkFromBlockCoords(this.xCoord, this.zCoord).zPosition).chunkTileEntityMap;
      Iterator entries = tileMap.entrySet().iterator();

      while(entries.hasNext()) {
         try {
            Entry e = (Entry)entries.next();
            TileEntity te = (TileEntity)e.getValue();
            if (te != null && te instanceof TileEntityTurret) {
               ++count;
            }
         } catch (TypeMismatchException var6) {
            var6.printStackTrace();
         }
      }

      return count;
   }

   public void writeToNBT(NBTTagCompound compound) {
      super.writeToNBT(compound);
      compound.setFloat("targetOffX", this.targetOffX);
      compound.setFloat("targetOffY", this.targetOffY);
      compound.setFloat("targetOffZ", this.targetOffZ);
      compound.setBoolean("activate", this.activate);
   }

   public void readFromNBT(NBTTagCompound compound) {
      super.readFromNBT(compound);
      this.targetOffX = compound.getFloat("targetOffX");
      this.targetOffY = compound.getFloat("targetOffY");
      this.targetOffZ = compound.getFloat("targetOffZ");
      this.activate = compound.getBoolean("activate");
   }

   public Packet getDescriptionPacket() {
      NBTTagCompound nbttagcompound = new NBTTagCompound();
      this.writeToNBT(nbttagcompound);
      return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, this.getBlockMetadata(), nbttagcompound);
   }

   public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {
      this.readFromNBT(pkt.func_148857_g());
      this.worldObj.markBlockRangeForRenderUpdate(this.xCoord, this.yCoord, this.zCoord, this.xCoord, this.yCoord, this.zCoord);
   }
}
