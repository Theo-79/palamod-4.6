package fr.paladium.palamod.tiles;

import fr.paladium.palamod.items.ItemObsidianUpgrade;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class TileEntityUpgradedObsidian extends TileEntity {
   private Boolean explode = false;
   private Boolean fake = false;
   private Boolean twoLife = false;
   private Boolean camouflage = false;
   private int life = 1;

   public void writeToNBT(NBTTagCompound compound) {
      super.writeToNBT(compound);
      compound.setBoolean("Explode", this.explode);
      compound.setBoolean("Fake", this.fake);
      compound.setBoolean("TwoLife", this.twoLife);
      compound.setBoolean("Camouflage", this.camouflage);
      compound.setInteger("Life", this.life);
   }

   public void readFromNBT(NBTTagCompound compound) {
      super.readFromNBT(compound);
      this.explode = compound.getBoolean("Explode");
      this.fake = compound.getBoolean("Fake");
      this.twoLife = compound.getBoolean("TwoLife");
      this.camouflage = compound.getBoolean("Camouflage");
      this.life = compound.getInteger("Life");
   }

   public void setInformations(List<ItemObsidianUpgrade.Upgrade> upgrades) {
      for(int i = 0; i < upgrades.size(); ++i) {
         ItemObsidianUpgrade.Upgrade upgrade = (ItemObsidianUpgrade.Upgrade)upgrades.get(i);
         switch(upgrade) {
         case Explode:
            this.explode = true;
            break;
         case Fake:
            this.fake = true;
            break;
         case TwoLife:
            this.twoLife = true;
            break;
         case Camouflage:
            this.camouflage = true;
         }
      }

   }

   public List<ItemObsidianUpgrade.Upgrade> getInformations() {
      List<ItemObsidianUpgrade.Upgrade> upgrades = new ArrayList();
      if (this.explode) {
         upgrades.add(ItemObsidianUpgrade.Upgrade.Explode);
      }

      if (this.fake) {
         upgrades.add(ItemObsidianUpgrade.Upgrade.Fake);
      }

      if (this.twoLife) {
         upgrades.add(ItemObsidianUpgrade.Upgrade.TwoLife);
      }

      if (this.camouflage) {
         upgrades.add(ItemObsidianUpgrade.Upgrade.Camouflage);
      }

      return upgrades;
   }

   public Boolean hasUpgrade(ItemObsidianUpgrade.Upgrade upgrade) {
      switch(upgrade) {
      case Explode:
         return this.explode;
      case Fake:
         return this.fake;
      case TwoLife:
         return this.twoLife;
      case Camouflage:
         return this.camouflage;
      default:
         return false;
      }
   }

   public boolean isAlife() {
      if (this.life == 1) {
         --this.life;
         return true;
      } else {
         return false;
      }
   }

   public void setBaseLife() {
      this.life = 1;
   }

   public boolean shouldRefresh(Block oldBlock, Block newBlock, int oldMeta, int newMeta, World world, int x, int y, int z) {
      return false;
   }
}
