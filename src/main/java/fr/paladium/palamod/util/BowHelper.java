package fr.paladium.palamod.util;

import fr.paladium.palamod.items.ItemArrowBase;
import fr.paladium.palamod.items.ModItems;
import fr.paladium.palamod.items.weapons.ItemPaladiumBow;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class BowHelper {
   public static final int RANGE = 0;
   public static final int SPEED = 1;

   public static int[] getModifiers(ItemStack stack) {
      if (stack != null && stack.getItem() instanceof ItemPaladiumBow) {
         if (!stack.hasTagCompound()) {
            stack.setTagCompound(new NBTTagCompound());
            stack.getTagCompound().setInteger("modifiersammount", 0);
         }

         NBTTagCompound tag = stack.getTagCompound();
         if (!tag.hasKey("modifiersarray")) {
            tag.setIntArray("modifiersarray", new int[0]);
            return null;
         } else {
            return tag.getIntArray("modifiersarray");
         }
      } else {
         return null;
      }
   }

   public static void applyModifiers(ItemStack stack, int modifier) {
      if (stack != null && stack.getItem() instanceof ItemPaladiumBow) {
         if (!stack.hasTagCompound()) {
            stack.setTagCompound(new NBTTagCompound());
            stack.getTagCompound().setInteger("modifiersammount", 1);
         }

         NBTTagCompound tag = stack.getTagCompound();
         if (!tag.hasKey("modifiersarray")) {
            tag.setIntArray("modifiersarray", new int[0]);
         }

         int[] modifiersArray = tag.getIntArray("modifiersarray");
         int modifiersSzize = modifiersArray.length;
         int[] finalModifiers = new int[modifiersSzize + 1];

         for(int i = 0; i < modifiersSzize; ++i) {
            finalModifiers[i] = modifiersArray[i];
         }

         finalModifiers[modifiersSzize] = modifier;
         tag.setIntArray("modifiersarray", finalModifiers);
      }
   }

   public static boolean canApply(ItemStack stack, int type) {
      int[] result = getModifiers(stack);
      if (result == null) {
         return true;
      } else {
         for(int i = 0; i < result.length; ++i) {
            if (result[i] == type) {
               return false;
            }
         }

         return true;
      }
   }

   public static String getModifierName(int type) {
      switch(type) {
      case 0:
         return "§a Range+";
      case 1:
         return "§6 Speed+";
      default:
         return "";
      }
   }

   public static Item getItem(int type) {
      ItemArrowBase arrow;
      switch(type) {
      case 0:
         arrow = ModItems.arrowPoison;
         break;
      case 1:
         arrow = ModItems.arrowWither;
         break;
      case 2:
         arrow = ModItems.arrowSlowness;
         break;
      case 3:
         arrow = ModItems.arrowSwitch;
         break;
      default:
         arrow = null;
      }

      return arrow;
   }
}
