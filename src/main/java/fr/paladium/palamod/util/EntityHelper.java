package fr.paladium.palamod.util;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;

public class EntityHelper {
   public static void customKnockBack(EntityPlayer player, EntityPlayer attacker, double par3, double par4) {
      double motionY = player.motionY;
      double motionX = player.motionY;
      double motionZ = player.motionY;
      player.isAirBorne = true;
      float f1 = MathHelper.sqrt_double(par3 * par3 + par4 * par4);
      float f2 = 0.4F;
      motionX /= 2.0D;
      motionY /= 2.0D;
      motionZ /= 2.0D;
      motionX -= par3 / (double)f1 * (double)f2;
      motionY += (double)f2;
      player.motionZ -= par4 / (double)f1 * (double)f2;
      if (motionY > 0.4000000059604645D) {
         motionY = 0.4000000059604645D;
      }

      player.moveEntity(motionX, motionY, motionZ);
   }

   public static void knockbackEntity(EntityLivingBase living, double boost) {
      living.motionX *= boost;
      living.motionZ *= boost;
   }
}
