package fr.paladium.palamod.util;

import fr.paladium.palamod.common.inventory.InventoryGuardianMore;
import fr.paladium.palamod.entities.mobs.EntityGuardianGolem;
import fr.paladium.palamod.items.ItemGuardianUpgrade;
import fr.paladium.palamod.items.ores.ItemAmethyst;
import fr.paladium.palamod.items.ores.ItemFindium;
import fr.paladium.palamod.items.ores.ItemPaladium;
import fr.paladium.palamod.items.ores.ItemTitane;
import java.util.HashMap;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class GuardianHelper {
   public static HashMap xpTools = new HashMap();

   public static boolean checkXPStuff(Item item) {
      if (item instanceof ItemPaladium) {
         return true;
      } else if (item instanceof ItemFindium) {
         return true;
      } else if (item instanceof ItemTitane) {
         return true;
      } else {
         return item instanceof ItemAmethyst;
      }
   }

   public static int getXpFromItem(Item item) {
      if (item instanceof ItemPaladium) {
         return 5;
      } else if (item instanceof ItemFindium) {
         return 10;
      } else if (item instanceof ItemTitane) {
         return 2;
      } else {
         return item instanceof ItemAmethyst ? 1 : 0;
      }
   }

   public static boolean hasUpgrade(EntityGuardianGolem golem, int type) {
      int i;
      for(int i1 = 0; i1 < 3; ++i1) {
         if (golem.content[i1] != null) {
            i1 = ((ItemGuardianUpgrade)golem.content[i1].getItem()).getType();
            if (i1 == type) {
               return true;
            }
         }
      }

      InventoryGuardianMore guardianMore = new InventoryGuardianMore(golem);

      for(i = 0; i < guardianMore.getSizeInventory(); ++i) {
         ItemStack stack = guardianMore.getStackInSlot(i);
         if (stack != null && ((ItemGuardianUpgrade)stack.getItem()).getType() == type) {
            return true;
         }
      }

      return false;
   }

   public static ItemStack getWeaponFromUpgrade(ItemStack stack) {
      if (stack == null) {
         return null;
      } else if (!(stack.getItem() instanceof ItemGuardianUpgrade)) {
         return null;
      } else {
         return ((ItemGuardianUpgrade)stack.getItem()).getType() != 20 ? null : null;
      }
   }
}
