package fr.paladium.palamod.util;

public class LibObfuscation {
   public static final String[] PARTICLE_TEXTURES = new String[]{"particleTextures", "field_110737_b", "b"};
   public static final String[] TARGET_CLASS = new String[]{"targetClass", "field_75307_b", "a"};
   public static final String[] TARGET_ENTITY = new String[]{"targetEntity", "field_75309_a", "g"};
   public static final String[] CLASS_TARGET = new String[]{"classTarget", "field_75444_h", "g"};
   public static final String[] TARGET_ENTITY_CLASS = new String[]{"targetEntityClass", "field_75381_h", "i"};
   public static final String[] TIME_SINCE_IGNITED = new String[]{"timeSinceIgnited", "field_70833_d", "bq"};
   public static final String[] TEXTURE_UPLOADED = new String[]{"textureUploaded", "field_110559_g", "i"};
   public static final String[] BUFFERED_IMAGE = new String[]{"bufferedImage", "field_110560_d", "g"};
   public static final String[] IS_IMMUNE_TO_FIRE = new String[]{"isImmuneToFire", "field_70178_ae", "ag"};
   public static final String[] REED_ITEM = new String[]{"field_150935_a", "a"};
   public static final String[] IN_LOVE = new String[]{"inLove", "field_70881_d", "bp"};
   public static final String[] ITEM_IN_USE = new String[]{"itemInUse", "field_71074_e", "f"};
   public static final String[] ITEM_IN_USE_COUNT = new String[]{"itemInUseCount", "field_71072_f", "g"};
   public static final String[] IS_BAD_EFFECT = new String[]{"isBadEffect", "field_76418_K", "J"};
   public static final String[] HORSE_JUMP_STRENGTH = new String[]{"horseJumpStrength", "field_110271_bv", "bv"};
   public static final String[] HORSE_CHEST = new String[]{"horseChest", "field_110296_bG", "bG"};
   public static final String[] NET_CLIENT_HANDLER = new String[]{"netClientHandler", "field_78774_b", "b"};
   public static final String[] CURRENT_GAME_TYPE = new String[]{"currentGameType", "field_78779_k", "k"};
   public static final String[] SPAWN_RANGE = new String[]{"spawnRange", "field_98290_m", "m"};
   public static final String[] SPAWN_COUNT = new String[]{"spawnCount", "field_98294_i", "i"};
   public static final String[] MAX_NEARBY_ENTITIES = new String[]{"maxNearbyEntities", "field_98292_k", "k"};
   public static final String[] MAX_SPAWN_DELAY = new String[]{"maxSpawnDelay", "field_98293_h", "h"};
   public static final String[] MIN_SPAWN_DELAY = new String[]{"minSpawnDelay", "field_98283_g", "g"};
   public static final String[] POTENTIAL_ENTITY_SPAWNS = new String[]{"potentialEntitySpawns", "field_98285_e", "e"};
   public static final String[] REMAINING_HIGHLIGHT_TICKS = new String[]{"remainingHighlightTicks", "field_92017_k", "r"};
   public static final String[] GUI_OPTION_PARENT = new String[]{"field_146441_g", "f"};
   public static final String[] THROWER = new String[]{"thrower", "field_70192_c", "g"};
   public static final String[] THE_SLOT = new String[]{"theSlot", "field_147006_u", "u"};
   public static final String[] INPUT_FIELD = new String[]{"inputField", "field_146415_a", "a"};
   public static final String[] COMPLETE_FLAG = new String[]{"field_146414_r", "r"};
   public static final String[] GET_LIVING_SOUND = new String[]{"getLivingSound", "func_70639_aQ", "t"};
   public static final String[] ANIMATION_METADATA = new String[]{"animationMetadata", "field_110982_k", "j"};
   public static final String[] STAR_GL_CALL_LIST = new String[]{"starGLCallList", "field_72772_v", "F"};
   public static final String[] GL_SKY_LIST = new String[]{"glSkyList", "field_72771_w", "G"};
   public static final String[] GL_SKY_LIST2 = new String[]{"glSkyList2", "field_72781_x", "H"};
}
