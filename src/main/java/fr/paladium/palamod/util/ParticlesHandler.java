package fr.paladium.palamod.util;

import fr.paladium.palamod.client.particles.ExplosionParticles;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.entity.Entity;

public class ParticlesHandler {
   public static final int EXPLOSION = 0;

   public static void generateCustomExplosion(Entity theEntity, int color) {
      EntityFX explosion = new ExplosionParticles(Minecraft.getMinecraft().getTextureManager(), theEntity.worldObj, theEntity.posX, theEntity.posY, theEntity.posZ, (double)color, (double)color, (double)color);
      Minecraft.getMinecraft().effectRenderer.addEffect(explosion);
   }
}
