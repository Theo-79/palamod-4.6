package fr.paladium.palamod.util;

import cpw.mods.fml.common.FMLCommonHandler;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiPlayerInfo;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraftforge.common.util.FakePlayer;

public class PlayerHelper {
   public static boolean isPlayerOnline(String username) {
      if (FMLCommonHandler.instance().getEffectiveSide().isServer()) {
         return MinecraftServer.getServer().getConfigurationManager().func_152612_a(username) != null;
      } else {
         NetHandlerPlayClient netclienthandler = Minecraft.getMinecraft().thePlayer.sendQueue;
         List list = netclienthandler.playerInfoList;

         for(int i = 0; i < list.size(); ++i) {
            GuiPlayerInfo guiplayerinfo = (GuiPlayerInfo)list.get(i);
            if (guiplayerinfo.name.toLowerCase().equals(username.toLowerCase())) {
               return true;
            }
         }

         return false;
      }
   }

   public static void spawnItemAtPlayer(EntityPlayer player, ItemStack stack) {
      if (!player.worldObj.isRemote) {
         EntityItem entityitem = new EntityItem(player.worldObj, player.posX + 0.5D, player.posY + 0.5D, player.posZ + 0.5D, stack);
         player.worldObj.spawnEntityInWorld(entityitem);
         if (!(player instanceof FakePlayer)) {
            entityitem.onCollideWithPlayer(player);
         }
      }

   }

   public static void spawnItemArray(int x, int y, int z, ArrayList<ItemStack> stack, World world) {
      for(int i = 0; i < stack.size(); ++i) {
         EntityItem entityitem = new EntityItem(world, (double)x + 0.5D, (double)y + 0.5D, (double)z + 0.5D, (ItemStack)stack.get(i));
         world.spawnEntityInWorld(entityitem);
      }

   }

   public static EntityPlayer getPlayerByName(String name) {
      List<EntityPlayer> players = MinecraftServer.getServer().getConfigurationManager().playerEntityList;
      Iterator var2 = players.iterator();

      EntityPlayer p;
      do {
         if (!var2.hasNext()) {
            return null;
         }

         p = (EntityPlayer)var2.next();
      } while(!p.getDisplayName().equalsIgnoreCase(name));

      return p;
   }

   public static boolean hasItem(Item item, EntityPlayer player) {
      for(int i = 0; i < player.inventory.getSizeInventory(); ++i) {
         if (player.inventory.getStackInSlot(i) != null && player.inventory.getStackInSlot(i).getItem() == item) {
            return true;
         }
      }

      return false;
   }
}
