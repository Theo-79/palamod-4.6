package fr.paladium.palamod.util;

import fr.paladium.palamod.items.ItemRune;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class RuneItemSlot extends Slot {
   public RuneItemSlot(IInventory inventory, int index, int xPosition, int yPosition) {
      super(inventory, index, xPosition, yPosition);
   }

   public int getSlotStackLimit() {
      return 1;
   }

   public boolean isItemValid(ItemStack itemstack) {
      return itemstack.getItem() instanceof ItemRune;
   }
}
