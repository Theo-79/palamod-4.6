package fr.paladium.palamod.util;

import fr.paladium.palamod.items.weapons.ItemBroadsword;
import fr.paladium.palamod.items.weapons.ItemFastsword;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;

public class SingleItemSlot extends Slot {
   public SingleItemSlot(IInventory inventory, int index, int xPosition, int yPosition) {
      super(inventory, index, xPosition, yPosition);
   }

   public int getSlotStackLimit() {
      return 1;
   }

   public boolean isItemValid(ItemStack itemstack) {
      return itemstack.getItem() instanceof ItemSword || itemstack.getItem() instanceof ItemArmor || itemstack.getItem() instanceof ItemBroadsword || itemstack.getItem() instanceof ItemFastsword;
   }
}
