package fr.paladium.palamod.util;

import fr.paladium.palamod.items.tools.ItemPaladiumHammer;
import fr.paladium.palamod.items.weapons.ItemPaladiumBroadsword;
import fr.paladium.palamod.items.weapons.ItemPaladiumFastsword;
import java.util.HashMap;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class UpgradeHelper {
   public static HashMap<Integer, Integer> upgrades;
   public static HashMap<Integer, Integer> upgradeType;
   public static final int PHAMMER = 0;
   public static final int PSWORD = 1;
   public static final int SMELT = 0;
   public static final int FORTUNE = 1;
   public static final int SPEED = 2;
   public static final int OBSIDIAN = 6;
   public static final int DAMAGE = 3;
   public static final int FLAME = 4;
   public static final int KNOCKBACK = 5;
   public static final int MORE = 7;

   public static void init() {
      upgrades = new HashMap();
      upgradeType = new HashMap();
      upgrades.put(1, 3);
      upgradeType.put(1, 0);
      upgrades.put(0, 1);
      upgradeType.put(0, 0);
      upgrades.put(2, 3);
      upgradeType.put(2, 0);
      upgrades.put(6, 1);
      upgradeType.put(6, 0);
      upgrades.put(3, 3);
      upgradeType.put(3, 1);
      upgrades.put(4, 1);
      upgradeType.put(4, 1);
      upgrades.put(5, 2);
      upgradeType.put(5, 1);
      upgrades.put(7, 2);
   }

   public static boolean canApplyUpgrade(ItemStack stack, int type, ItemStack tool) {
      if (getMaxEnchants(tool) <= getEnchants(tool) && type != 7) {
         return false;
      } else {
         int toolType = -1;
         if (tool.getItem() instanceof ItemPaladiumHammer) {
            toolType = 0;
         }

         if (tool.getItem() instanceof ItemPaladiumBroadsword || tool.getItem() instanceof ItemPaladiumFastsword) {
            toolType = 1;
         }

         if (getModifier(tool, type) >= (Integer)upgrades.get(type)) {
            return false;
         } else {
            return !upgradeType.containsKey(type) || toolType == (Integer)upgradeType.get(type);
         }
      }
   }

   private static int getEnchants(ItemStack stack) {
      createDefaultNBT(stack);
      NBTTagCompound tag = stack.getTagCompound();
      return tag.getInteger("modifiersammount");
   }

   public static int getMaxEnchants(ItemStack stack) {
      createDefaultNBT(stack);
      NBTTagCompound tag = stack.getTagCompound();
      return tag.getInteger("modifiersmax") + getModifier(stack, 7) * 2;
   }

   public static int getModifier(ItemStack stack, int type) {
      createDefaultNBT(stack);
      NBTTagCompound tag = stack.getTagCompound();
      if (!tag.hasKey("upgradearray")) {
         tag.setIntArray("upgradearray", new int[0]);
         return 0;
      } else {
         int[] upgrades = tag.getIntArray("upgradearray");

         for(int i = 0; i < upgrades.length; i += 2) {
            if (upgrades[i] == type) {
               return upgrades[i + 1];
            }
         }

         return 0;
      }
   }

   public static void createDefaultNBT(ItemStack stack) {
      if (!stack.hasTagCompound()) {
         int modifiers = 3;
         if (stack.getItem() instanceof ItemPaladiumBroadsword) {
            modifiers = 6;
         }

         stack.setTagCompound(new NBTTagCompound());
         stack.getTagCompound().setInteger("modifiersammount", 0);
         stack.getTagCompound().setInteger("modifiersmax", modifiers);
      }

   }

   public static void applyUpgrade(ItemStack stack, int type) {
      createDefaultNBT(stack);
      NBTTagCompound tag = stack.getTagCompound();
      if (!tag.hasKey("upgradearray")) {
         int[] var10000 = new int[]{type, 1};
         tag.setIntArray("upgradearray", new int[2]);
      } else {
         int[] upgrades = tag.getIntArray("upgradearray");

         for(int i = 0; i < upgrades.length; i += 2) {
            if (upgrades[i] == type) {
               ++upgrades[i + 1];
               tag.setIntArray("upgradearray", upgrades);
               tag.setInteger("modifiersammount", tag.getInteger("modifiersammount") + 1);
               return;
            }
         }

         int[] array = new int[upgrades.length + 2];

         for(int i = 0; i < upgrades.length; ++i) {
            array[i] = upgrades[i];
         }

         array[array.length - 2] = type;
         array[array.length - 1] = 1;
         tag.setIntArray("upgradearray", array);
         tag.setInteger("modifiersammount", tag.getInteger("modifiersammount") + 1);
      }
   }

   public static String getUpgradeName(int type) {
      if (type == 0) {
         return "§3 Smelt";
      } else if (type == 1) {
         return "§6 Fortune";
      } else if (type == 2) {
         return "§c Speed";
      } else if (type == 3) {
         return "§9 Damage";
      } else if (type == 4) {
         return "§7 Flame";
      } else if (type == 5) {
         return "§2 Knockback";
      } else {
         return type == 6 ? "§0 Obsidian" : "";
      }
   }

   public static int[] getUpgradeAmmount(ItemStack stack) {
      createDefaultNBT(stack);
      NBTTagCompound tag = stack.getTagCompound();
      if (!tag.hasKey("upgradearray")) {
         tag.setIntArray("upgradearray", new int[0]);
         return null;
      } else {
         return tag.getIntArray("upgradearray");
      }
   }
}
