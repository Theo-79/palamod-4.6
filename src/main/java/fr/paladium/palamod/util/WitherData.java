package fr.paladium.palamod.util;

import net.minecraft.nbt.NBTTagCompound;

public class WitherData {
   public boolean nofakewater = false;
   public boolean nosound = false;
   public boolean nofly = false;
   public boolean remote = false;
   public int maxlife = 300;
   public float impactDamage = 1.0F;
   public short angrylevel = 0;
   public short explosion = 0;
   public short anvil = 0;

   public void readNBT(NBTTagCompound tagCompound) {
      this.maxlife = tagCompound.getInteger("maxlife");
      this.nofly = tagCompound.getBoolean("nofly");
      this.nofakewater = tagCompound.getBoolean("nofakewater");
      this.nosound = tagCompound.getBoolean("nosound");
      this.impactDamage = tagCompound.getFloat("impactDamage");
      this.angrylevel = tagCompound.getShort("angrylevel");
      this.explosion = tagCompound.getShort("explosion");
      this.anvil = tagCompound.getShort("anvil");
      this.remote = tagCompound.getBoolean("remote");
   }

   public void writeNBT(NBTTagCompound tagCompound) {
      tagCompound.setInteger("maxlife", 100);
      tagCompound.setBoolean("nofly", this.nofly);
      tagCompound.setBoolean("nofakewater", this.nofakewater);
      tagCompound.setBoolean("nosound", this.nosound);
      tagCompound.setFloat("impactDamage", this.impactDamage);
      tagCompound.setShort("angrylevel", this.angrylevel);
      tagCompound.setShort("explosion", this.explosion);
      tagCompound.setShort("anvil", this.anvil);
      tagCompound.setBoolean("remote", this.remote);
   }

   public static enum WitherUpgrade {
      LIFE {
         public boolean proccesUpgrade(WitherData data) {
            if (data.maxlife != 450) {
               data.maxlife += 75;
            } else {
               data.maxlife += 150;
            }

            return true;
         }
      },
      DAMAGE {
         public boolean proccesUpgrade(WitherData data) {
            data.impactDamage += 0.1F;
            return true;
         }
      },
      NOFLY {
         public boolean proccesUpgrade(WitherData data) {
            if (data.nofly) {
               return false;
            } else {
               data.nofly = true;
               return true;
            }
         }
      },
      NOSOUND {
         public boolean proccesUpgrade(WitherData data) {
            if (data.nosound) {
               return false;
            } else {
               data.nosound = true;
               return true;
            }
         }
      },
      NOFAKE {
         public boolean proccesUpgrade(WitherData data) {
            if (data.nofakewater) {
               return false;
            } else {
               data.nofakewater = true;
               return true;
            }
         }
      },
      ANGRY {
         public boolean proccesUpgrade(WitherData data) {
            if (data.angrylevel == 2) {
               return false;
            } else {
               ++data.angrylevel;
               return true;
            }
         }
      },
      EXPLOSION {
         public boolean proccesUpgrade(WitherData data) {
            ++data.explosion;
            return true;
         }
      },
      ANVIL {
         public boolean proccesUpgrade(WitherData data) {
            ++data.anvil;
            return true;
         }
      },
      REMOTE {
         public boolean proccesUpgrade(WitherData data) {
            if (data.remote) {
               return false;
            } else {
               data.remote = true;
               return true;
            }
         }
      };

      private WitherUpgrade() {
      }

      public abstract boolean proccesUpgrade(WitherData var1);

      // $FF: synthetic method
      WitherUpgrade(Object x2) {
         this();
      }
   }
}
