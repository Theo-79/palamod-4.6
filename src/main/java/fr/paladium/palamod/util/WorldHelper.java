package fr.paladium.palamod.util;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class WorldHelper {
   public static void dropItemStack(World world, ItemStack stack, int x, int y, int z) {
      if (stack != null) {
         float f = world.rand.nextFloat() * 0.8F + 0.1F;
         float f1 = world.rand.nextFloat() * 0.8F + 0.1F;

         EntityItem entityitem;
         for(float f2 = world.rand.nextFloat() * 0.8F + 0.1F; stack.stackSize > 0; world.spawnEntityInWorld(entityitem)) {
            int k1 = world.rand.nextInt(21) + 10;
            if (k1 > stack.stackSize) {
               k1 = stack.stackSize;
            }

            stack.stackSize -= k1;
            entityitem = new EntityItem(world, (double)((float)x + f), (double)((float)y + f1), (double)((float)z + f2), new ItemStack(stack.getItem(), k1, stack.getItemDamage()));
            float f3 = 0.05F;
            entityitem.motionX = (double)((float)world.rand.nextGaussian() * f3);
            entityitem.motionY = (double)((float)world.rand.nextGaussian() * f3 + 0.2F);
            entityitem.motionZ = (double)((float)world.rand.nextGaussian() * f3);
            if (stack.hasTagCompound()) {
               entityitem.getEntityItem().setTagCompound((NBTTagCompound)stack.getTagCompound().copy());
            }
         }
      }

   }
}
