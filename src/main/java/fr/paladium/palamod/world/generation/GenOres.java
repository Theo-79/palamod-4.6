package fr.paladium.palamod.world.generation;

import cpw.mods.fml.common.IWorldGenerator;
import fr.paladium.palamod.blocks.ModBlocks;
import java.util.Random;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;

public class GenOres implements IWorldGenerator {
   public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
      if (world.provider.dimensionId == 0) {
      }

   }

   private void genOverworld(World world, int x, int z, Random rand) {
      int i;
      for(i = 0; i <= 12; ++i) {
         int var10000 = x + rand.nextInt(16);
         int yPos = 10 + rand.nextInt(30);
         var10000 = z + rand.nextInt(16);
      }

      for(i = 0; (double)i < 2.5D; ++i) {
         (new WorldGenMinable(ModBlocks.amethystOre, 0, 8, Blocks.stone)).generate(world, rand, x + rand.nextInt(16), rand.nextInt(20), z + rand.nextInt(16));
      }

      for(i = 0; i < 2; ++i) {
         (new WorldGenMinable(ModBlocks.titaneOre, 0, 8, Blocks.stone)).generate(world, rand, x + rand.nextInt(16), rand.nextInt(20), z + rand.nextInt(16));
      }

      for(i = 0; i < 2; ++i) {
         (new WorldGenMinable(ModBlocks.paladiumOre, 0, 4, Blocks.stone)).generate(world, rand, x + rand.nextInt(16), rand.nextInt(20), z + rand.nextInt(16));
      }

      for(i = 0; i < 2; ++i) {
         (new WorldGenMinable(ModBlocks.findiumOre, 0, 3, Blocks.stone)).generate(world, rand, x + rand.nextInt(16), rand.nextInt(20), z + rand.nextInt(16));
      }

      for(i = 0; i < 12; ++i) {
         (new WorldGenMinable(ModBlocks.experienceBush, 0, 3, Blocks.stone)).generate(world, rand, x + rand.nextInt(16), rand.nextInt(50), z + rand.nextInt(16));
      }

   }
}
